﻿using System;
using System.Data;
//using System.Diagnostics.Contracts;
using System.Text;
using daobase;
using tir.dao.Entidades;

namespace tir.dao
{
    public class AplicacaoVerbaDao : DaoBase<AplicacaoVerba>
    {
        #region Métodos Públicos

        /// <summary>
        /// Método que retorna um item.
        /// </summary>
        /// <param name="codUnidNegcc">Código da Unidade de Negociação.</param>
        /// <param name="codTipoNegcc">Código do Tipo de Negociação.</param>
        /// <param name="codTipoEcxDcx">Código do Tipo de Encaixe/Desencaixe.</param>
        /// <returns>Objeto AplicacaoVerba.</returns>
        public AplicacaoVerba Obter(int codUnidNegcc, int codTipoNegcc, int codTipoEcxDcx)
        {
            #region Contracts
            //Contract.Requires(codUnidNegcc > 0, Mensagens.AplicacaoVerba.VAL_CODIGO_UNIDADE);
            //Contract.Requires(codTipoNegcc > 0, Mensagens.AplicacaoVerba.VAL_TIPO_NEGOCIACAO);
            //Contract.Requires(codTipoEcxDcx > 0, Mensagens.AplicacaoVerba.VAL_TIPO_ECX_DCX);
            #endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select a.CD_UNID_NEGCC, ");
                sb.Append("a.CD_TP_NEGCC, ");
                sb.Append("a.ID_RUBRICA_CORP, ");
                sb.Append("a.CD_TP_ENCX_DENCX ");
                sb.Append("from NEGCLI_APLICACAO_VERBA a ");
                sb.AppendFormat("where a.CD_UNID_NEGCC = {0} ", DaoUtil.FormatarParametro("codUnidNegcc"));
                sb.AppendFormat("and a.CD_UNID_NEGCC = {0} ", DaoUtil.FormatarParametro("codTipoNegcc"));
                sb.AppendFormat("and a.CD_TP_ENCX_DENCX = {0} ", DaoUtil.FormatarParametro("codTipoEcxDcx"));

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codUnidNegcc"), codUnidNegcc, typeof(Int16)));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codTipoNegcc"), codTipoNegcc, typeof(Int16)));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codTipoEcxDcx"), codTipoEcxDcx, typeof(Int16)));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //se há resultado
                if (dr.Read())
                {
                    //valor de retorno - alíquota de IOF
                    return MontarObjeto(dr);
                }
                else
                {
                    //valor de retorno nulo
                    return null;
                }
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("AplicacaoVerbaDao.Obter",
                //"Erro ao buscar uma Aplicação de Verba.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        #endregion

        #region Overrides
        protected override AplicacaoVerba MontarObjeto(IDataReader dr)
        {
            AplicacaoVerba ent = new AplicacaoVerba();

            try
            {
                ent.CodigoUnidadeNegociacao = (Int16)dr["CD_UNID_NEGCC"];
                ent.CodigoTipoNegociacao = (Int16)dr["CD_TP_NEGCC"];
                ent.IdRubricaCorp = (Int16)dr["ID_RUBRICA_CORP"];
                ent.CodigoTipoEncaixeDesencaixe = (Int16)dr["CD_TP_ENCX_DENCX"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
