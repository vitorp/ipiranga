﻿using System;
using TIR.Entidades.Encaixes;

namespace TIR.Entidades
{
    public class Produto
    {
        #region Propriedades

        public long CodProduto { get; set; }
        public long CodGrupo { get; set; }
        public bool IndCompressProp { get; set; }
        public float DescMargem { get; set; }
        public float PrazoDias { get; set; }
        public double MediaMesAno1 { get; set; }
        public double MediaMesAno2 { get; set; }
        public double MediaMesAno3 { get; set; }
        public double[] FluxoVol { get; set; }
        public double[] Fluxo { get; set; }
        //public double[] FluxoAluguelVol { get; set; }
        //public double[] FluxoCapGiro { get; set; }
        //public double[] FluxoMargens { get; set; }
        public AluguelVol AluguelVol { get; set; }
        public CapitalGiro CapitalGiro { get; set; }
        public Margem Margem { get; set; }
        public EstoqTecnico EstoqTecnico { get; set; }
        //Criado por Luiz Felipe em fev/2015 para conter os volumes mensais
        //de compensação dos meses de maturação
        public double[] volMensalComp { get; set; }

        #endregion

        #region Métodos Públicos

        /// <summary>
        /// Método público que calcula o volume anual
        /// para um determinado Produto.
        /// </summary>
        /// <param name="tir">Objeto TIR.</param>
        /// <returns>Verdadeiro se cálculo ok.</returns>
        public bool CalcularVolume(TIRNegociacao tir)
        {
            //Se for lubrificante, transforma volume de litros para m3
            //Por Luiz Felipe - abril/2012
            if (CodProduto == 12)
            {
                MediaMesAno1 /= 1000.0;
                MediaMesAno2 /= 1000.0;
                MediaMesAno3 /= 1000.0;
            }

            if (MediaMesAno1 + MediaMesAno2 + MediaMesAno3 == 0.0)
            {
                return true;
            }
            
            //----- Propagação de Volume

            if (MediaMesAno2 == 0.0 && MediaMesAno1 != 0.0 && tir.IsIncremental == false)
            {
                MediaMesAno2 = MediaMesAno1;
            }

            if (MediaMesAno3 == 0.0 && MediaMesAno2 != 0.0)
            {
                MediaMesAno3 = MediaMesAno2;
            }

            //int prazoTotal = tir.ContrAdicional + tir.ContrExistente + 2;
            //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
            int prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 2;

            //Alterado em jun/2014 para incluir os meses de compensação de volume no dimensionamento do fluxo
            //FluxoVol = new double[prazoTotal];
            FluxoVol = new double[prazoTotal + tir.MesesCompVolume];

            if (tir.IsMaturaVenda == true && tir.IsIncremental == true)
            {
                throw new Exception("Produto::CalcVolume: Erro ao calcular produto. A análise não pode ter incremental de volume e maturação das vendas ao mesmo tempo. Novo negócio não pode ter incremental de volume.");
            }

            if (tir.IsMaturaVenda == true && tir.ContrExistente > 0)
            {
                throw new Exception("Produto::CalcVolume: Erro ao calcular produto. Novo negócio não pode ter contrato existente.");
            }

            if (tir.IsIncremental == true && MediaMesAno3 < 0)
            {
                throw new Exception("Produto::CalcVolume: Erro ao calcular produto. Para volume incremental é preciso informar as quantidades do terceiro ano.");
            }

            if (tir.IsIncremental == true && tir.ContrExistente <= 0)
            {
                throw new Exception("Produto::CalcVolume: Para volume incremental é preciso informar o período dos contratos existentes.");
            }

            if (tir.IsIncremental == false && tir.IsMaturaVenda == false && MediaMesAno1 < 0.0)
            {
                throw new Exception("Produto::CalcVolume: Erro ao calcular produto. É preciso informar o volume do primeiro ano para esta categoria de projeto.");
            }

            for (int i = 0; i < prazoTotal; i++)
            {
                double Volume = 0.0;

                if (tir.IsMaturaVenda == true)
                {
                    if (tir.PeriodoMatura < i)
                    {
                        Volume = MediaMesAno1;
                        //Alterado em jul/2014, para considerar os meses sem venda
                        if (MediaMesAno2 > 0.0 && (i - tir.PeriodoMatura) > 12) Volume = MediaMesAno2;
                        if (MediaMesAno3 > 0.0 && (i - tir.PeriodoMatura) > 24) Volume = MediaMesAno3;

                        //for (int j = 0; j < 100; j++)
                        for (int j = 0; j < tir.MesMaturaVenda.Length; j++)
                        {
                            if (tir.MesMaturaVenda[j] == 0)
                            {
                                break;
                            }

                            //Alterado em jul/2014, para considerar os meses sem venda
                            if ((i - tir.PeriodoMatura) <= tir.MesMaturaVenda[j])
                            {
                                Volume = Volume * tir.PercMaturaVenda[j] / 100;
                                break;
                            }
                        }
                    }
                }
                else if (tir.IsIncremental == true)
                {
                    Volume = MediaMesAno3;

                    if (i > tir.PeriodoMatura)
                    {
                        //Alterado em jun/2014 para incluir os meses sem venda
                        //if (i <= tir.ContrExistente)
                        if (i <= tir.ContrExistente + tir.PeriodoMatura)
                        {
                            Volume = MediaMesAno3 - MediaMesAno1;
                        }
                    }
                    else
                    {
                        //não tem  volume no periodo sem venda
                        Volume = 0.0;
                    }
                }
                else
                {
                    if (tir.PeriodoMatura < i)
                    {
                        //Alterado em jun/2014 para incluir os
                        //meses de contrato existentes
                        //if (i > tir.ContrExistente)
                        if (i > tir.ContrExistente + tir.PeriodoMatura)
                        {
                            if (i > 24)
                            {
                                Volume = MediaMesAno3;
                            }
                            else if (i > 12)
                            {
                                Volume = MediaMesAno2;
                            }
                            else
                            {
                                Volume = MediaMesAno1;
                            }
                        }
                    }
                }

                FluxoVol[i] = Volume;
            }

            //Se existe necessidade de compensação de Volume
            if (tir.MesesCompVolume > 0)
            {
                //Como cada Produto pode ter uma quantidade de meses
                //de compensação, é preciso pegar apenas os valores
                //deste Produto
                for (int i = 0; i <= volMensalComp.GetUpperBound(0); i++)
                {
                    FluxoVol[prazoTotal + i - 1] = volMensalComp[i];
                }
                //Coloca um valor no último mês, senão o cálculo do
                //Capital de Giro não calcula para o penúltimo mês
                FluxoVol[FluxoVol.GetUpperBound(0)] = 1;
            }

            //Se for Lubrificante, retorna os volumes anuais para litros
            //Por Luiz Felipe - abril/2012
            if (CodProduto == 12)
            {
                MediaMesAno1 *= 1000.0;
                MediaMesAno2 *= 1000.0;
                MediaMesAno3 *= 1000.0;
            }

            //retorno da função com sucesso e sai da função
            return true;
        }

        #endregion
    }
}
