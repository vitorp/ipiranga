﻿using System.Collections.Generic;
using System;

namespace TIR.Entidades
{
    [Serializable()]
    public class ResultadoComponente : Resultado
    {
        #region Propriedades

        public double TIR { get; set; }         //valor da TIR
        public double NPV { get; set; }         //valor NPV
        public int Payback { get; set; }        //valor Payback
        public double[] Inflacao { get; set; }  //inflação

        public List<Resultado> ResultadosEncaixes { get; set; }

        #endregion
    }
}
