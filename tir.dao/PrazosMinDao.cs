﻿using System;
using System.Collections.Generic;
using System.Data;
//using System.Diagnostics.Contracts;
using System.Text;
using daobase;
using tir.dao.Entidades;

namespace tir.dao
{
    public class PrazosMinDao : DaoBase<PrazosMin>
    {
        #region Métodos Públicos
        /// <summary>
        /// Método que lista Prazos Mínimos por faixa de volume.
        /// </summary>
        /// <param name="UnidNeg">Código da Unidade de Negócio (GV para Revenda e Coordenadoria para Consumo).</param>
        /// <param name="SiglaUF">Sigla da UF do Cliente.</param>
        /// <param name="TpNegcc">Tipo de Negociação.</param>
        /// <param name="codLocaliza">Perfil.</param>
        /// <param name="codProdu">Código do Produto.</param>
        /// <returns>Lista de objetos PrazosMin.</returns>
        public List<PrazosMin> ListarPorFaixaVolume(string UnidNeg, string SiglaUF, string TpNegcc, int codLocaliza, string codProdu)
        {
            #region Contracts
            //Contract.Requires(TpNegcc != null, Mensagens.PrazosMin.VAL_TIPO_NEGOCIACAO);
            //Contract.Requires(codLocaliza > 0, Mensagens.PrazosMin.VAL_ID_PERFIL);
            //Contract.Requires(codProdu != null, Mensagens.PrazosMin.VAL_CODIGO_PRODUTO);
            #endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select p.Cd_GV, ");
                sb.Append("p.Sg_UF, ");
                sb.Append("p.Sg_Tpneg, ");
                sb.Append("p.Cd_Localiz_Posto, ");
                sb.Append("p.Cd_Prod, ");
                sb.Append("p.Vo_Faixa, ");
                sb.Append("p.Qt_Dias_Min ");
                sb.Append("from PrazosMinGV_UF p ");
                sb.AppendFormat("where p.Cd_GV = {0} ", DaoUtil.FormatarParametro("UnidNeg"));
                sb.AppendFormat("and p.Sg_UF = {0} ", DaoUtil.FormatarParametro("SiglaUF"));
                sb.AppendFormat("and p.Sg_Tpneg = {0} ", DaoUtil.FormatarParametro("TpNegcc"));
                sb.AppendFormat("and p.Cd_Localiz_Posto = {0} ", DaoUtil.FormatarParametro("codLocaliza"));
                sb.AppendFormat("and p.Cd_Prod = {0} ", DaoUtil.FormatarParametro("codProdu"));
                sb.Append("order by p.Vo_Faixa");

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("UnidNeg"), UnidNeg, typeof(String), 6));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("SiglaUF"), SiglaUF, typeof(String), 2));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("TpNegcc"), TpNegcc, typeof(String), 3));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codLocaliza"), codLocaliza, typeof(Int16)));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codProdu"), codProdu, typeof(String), 3));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //valor de retorno - alíquota de IOF
                return MontarLista(dr);

            }
            catch //(Exception ex)
            {
                throw; //new DaoException("PrazosMinDao.ListarPorFaixaVolume",
                //"Erro ao listar Prazos Mínimos por faixa de volume.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }
        #endregion

        #region Overrides

        protected override PrazosMin MontarObjeto(IDataReader dr)
        {
            PrazosMin ent = new PrazosMin();
            try
            {
                ent.UnidadeNegocio = (string)dr["Cd_GV"];
                ent.SiglaUF = (string)dr["Sg_UF"];
                ent.SiglaTipoNegocio = (string)dr["Sg_Tpneg"];
                ent.CodigoLocalizacaoPosto = (Int16)dr["Cd_Localiz_Posto"];
                ent.CodigoProduto = (string)dr["Cd_Prod"];
                ent.VolumeFaixa = (double)dr["Vo_Faixa"];
                ent.QuantidadeDiasMinimo = (Single)dr["Qt_Dias_Min"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
