﻿using TIR.Bo.Calculos;
using TIR.Entidades;
using TIR.Entidades.Componentes;
using TIR.Entidades.Encaixes;
using TIR.Entidades.ItensFranquias;

namespace TIR.Fabricas
{
    public class CalculoFactory
    {
        #region Métodos Públicos

        public static double[] Calcular(Encaixe enc, Componente comp, TIRNegociacao tir)
        {
            double[] result = null;

            if (enc is AluguelVol)
            {
                AluguelVol alugVol = (AluguelVol)enc;
                CalculoAluguelVolume calculo = new CalculoAluguelVolume();
                result = calculo.Calcular(enc, tir);
            }

            if (enc is AntecAluguel)
            {
                AntecAluguel anteAlug = (AntecAluguel)enc;
                CalculoAntecAluguel calculo = new CalculoAntecAluguel();
                result = calculo.Calcular(enc, tir);
            }

            if (enc is Benfeitoria)
            {
                Benfeitoria benfeitoria = (Benfeitoria)enc;
                CalculoBenfeitoria calculo = new CalculoBenfeitoria();
                calculo.Franquia = verificarSeFranquia(comp);
                result = calculo.Calcular(enc, tir);
            }

            if (enc is CapitalGiro)
            {
                CapitalGiro cap = (CapitalGiro)enc;
                CalculoCapitalGiro calculo = new CalculoCapitalGiro();
                result = calculo.Calcular(enc, tir);
            }

            if (enc is CapitalGiroFranquia)
            {
                CapitalGiroFranquia capFranquia = (CapitalGiroFranquia)enc;
                CalculoCapGiroFranquia calculo = new CalculoCapGiroFranquia();
                result = calculo.Calcular(enc, tir);
            }

            if (enc is DespReceita)
            {
                DespReceita desp = (DespReceita)enc;
                CalculoDespReceita calculo = new CalculoDespReceita();
                calculo.Franquia = verificarSeFranquia(comp);
                result = calculo.Calcular(enc, tir);
            }

            if (enc is Equipamento)
            {
                Equipamento eqp = (Equipamento)enc;
                CalculoEquipamento calculo = new CalculoEquipamento();
                calculo.Franquia = verificarSeFranquia(comp);
                result = calculo.Calcular(enc, tir);
            }

            if (enc is EstoqTecnico)
            {
                EstoqTecnico est = (EstoqTecnico)enc;
                CalculoEstoqTecnico calculo = new CalculoEstoqTecnico();
                result = calculo.Calcular(enc, tir);
            }

            if (enc is Financiamento)
            {
                Financiamento fin = (Financiamento)enc;
                //Novo cálculo do Financiamento
                //CalculoFinanciamento calculo = new CalculoFinanciamento();
                CalculoNovoFinanciamento calculo = new CalculoNovoFinanciamento();
                result = calculo.Calcular(enc, tir);
            }

            if (enc is FundoComercio)
            {
                FundoComercio fun = (FundoComercio)enc;
                CalculoFundoComercio calculo = new CalculoFundoComercio();
                result = calculo.Calcular(enc, tir);
            }

            if (enc is Margem)
            {
                Margem margem = (Margem)enc;
                CalculoMargem calculo = new CalculoMargem();
                result = calculo.Calcular(enc, tir);
            }

            if (enc is Overhead)
            {
                Overhead overhead = (Overhead)enc;
                CalculoOverhead calculo = new CalculoOverhead();
                calculo.Franquia = verificarSeFranquia(comp);
                result = calculo.Calcular(enc, tir);
            }

            if (enc is Propaganda)
            {
                Propaganda prop = (Propaganda)enc;
                CalculoPropaganda calculo = new CalculoPropaganda();
                calculo.Franquia = verificarSeFranquia(comp);
                result = calculo.Calcular(enc, tir);
            }

            if (enc is RecebLuvas)
            {
                RecebLuvas receb = (RecebLuvas)enc;
                CalculoRecebLuvas calculo = new CalculoRecebLuvas();
                calculo.Franquia = verificarSeFranquia(comp);
                result = calculo.Calcular(enc, tir);
            }

            if (enc is RoyaltiesArco)
            {
                RoyaltiesArco royal = (RoyaltiesArco)enc;
                CalculoRoyaltiesARCO calculo = new CalculoRoyaltiesARCO();
                calculo.Franquia = verificarSeFranquia(comp);
                result = calculo.Calcular(enc, tir);
            }

            if (enc is RoyaltyFixo)
            {
                RoyaltyFixo royal = (RoyaltyFixo)enc;
                CalculoRoyaltyFixo calculo = new CalculoRoyaltyFixo();
                calculo.Franquia = verificarSeFranquia(comp);
                result = calculo.Calcular(enc, tir);
            }

            if (enc is RoyaltyVar)
            {
                RoyaltyVar royal = (RoyaltyVar)enc;
                CalculoRoyaltyVar calculo = new CalculoRoyaltyVar();
                calculo.Franquia = verificarSeFranquia(comp);
                result = calculo.Calcular(enc, tir);
            }

            if (enc is TaxaFranquia)
            {
                TaxaFranquia taxa = (TaxaFranquia)enc;
                CalculoTaxaFranquia calculo = new CalculoTaxaFranquia();
                calculo.Franquia = verificarSeFranquia(comp);
                result = calculo.Calcular(enc, tir);
            }

            if (enc is Terceirizacao)
            {
                Terceirizacao terc = (Terceirizacao)enc;
                CalculoTerceirizacao calculo = new CalculoTerceirizacao();
                calculo.Franquia = verificarSeFranquia(comp);
                result = calculo.Calcular(enc, tir);
            }

            if (enc is Terreno)
            {
                Terreno terreno = (Terreno)enc;
                CalculoTerreno calculo = new CalculoTerreno();
                result = calculo.Calcular(enc, tir);
            }

            return result;
        }

        #endregion

        #region Métodos Privados

        /// <summary>
        /// Verifica se o componente é do tipo Franquia. Se for é feita uma
        /// conversão. Se não for é retornado um objeto nulo.
        /// </summary>
        /// <param name="comp">Componente a ser verificado.</param>
        /// <returns>Objeto do tipo Franquia.</returns>
        private static Franquia verificarSeFranquia(Componente comp)
        {
            //verifica se o componente é franquia
            if (comp is Franquia)
            {
                //se for franquia converte para o tipo correto
                return (Franquia)comp;
            }
            else
            {
                //se não for franquia retorna objeto nulo
                return null;
            }
        }

        #endregion
    }    
}
