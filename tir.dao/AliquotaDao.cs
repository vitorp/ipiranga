﻿using System;
using System.Data;
using System.Text;
using daobase;
//using System.Diagnostics.Contracts;
using tir.dao.Entidades;

namespace tir.dao
{
    public class AliquotaDao : DaoBase<Aliquota>
    {
        #region Métodos Públicos

        /// <summary>
        /// Busca uma alíquota de ICMS pela UF
        /// e pelo código do Grupo do Produto.
        /// </summary>
        /// <param name="siglaUF">Sigla da UF.</param>
        /// <param name="codGrupo">Código do Grupo do Produto.</param>
        /// <returns>Objeto Aliquota.</returns>
        public Aliquota ObterAliquota(string SiglaUF, string CodGrupo)
        {
            #region Contracts
            //Contract.Requires(SiglaUF != null, Mensagens.Aliquota.VAL_SIGLA_UF);
            //Contract.Requires(CodGrupo != null, Mensagens.Aliquota.VAL_CODIGO_GRUPO);
            #endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select a.Sg_UF, ");
                sb.Append("a.cd_grupo_prod, ");
                sb.Append("a.Pc_icms_interna ");
                sb.Append("from Aliquotas a ");
                sb.AppendFormat("where a.Sg_UF = {0} ", DaoUtil.FormatarParametro("SiglaUF"));
                sb.AppendFormat("and a.cd_grupo_prod = {0} ", DaoUtil.FormatarParametro("CodGrupo"));

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("SiglaUF"), SiglaUF, typeof(String), 2));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("CodGrupo"), CodGrupo, typeof(String), 3));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //se há resultado
                if (dr.Read())
                {
                    //valor de retorno - alíquota de IOF
                    return MontarObjeto(dr);
                }
                else
                {
                    //valor de retorno nulo
                    return null;
                }
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("AliqIOFDao.ObterAliquota",
                //"Erro ao obter alíquota de IOF pelo número máximo do mês.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        #endregion

        #region Overrides

        protected override Aliquota MontarObjeto(IDataReader dr)
        {
            Aliquota ent = new Aliquota();
            try
            {
                ent.SiglaUF = (string)dr["Sg_UF"];
                ent.GrupoProduto = (string)dr["cd_grupo_prod"];
                ent.PercentualICMS = (Single)dr["Pc_icms_interna"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
