﻿using System.Runtime.Serialization;

namespace WebService.TIR.Models
{
    [DataContract]
    public class ProdutoTir
    {
        [DataMember]
        public int CodigoDoGrupo { get; set; }
        
        [DataMember]
        public int CodigoDoProduto { get; set; }

        [DataMember]
        public int MediaMensalAno1 { get; set; }

        [DataMember]
        public int MediaMensalAno2 { get; set; }

        [DataMember]
        public int MediaMensalAno3 { get; set; }

        [DataMember]
        public float PrazoEmDias { get; set; }
    }
}