﻿using TIR.Entidades;
using TIR.Entidades.Encaixes;

namespace TIR.Bo.Calculos
{
    public interface ICalculo
    {
        double[] Calcular(Encaixe enc, TIRNegociacao tir);
    }
}
