﻿using System.Data;
////using System.Diagnostics.Contracts;
using daobase.Enumerados;

public class ConexaoBase
    {
        #region Atributos
        private static ConexaoBase _instancia = null;
        private IDbConnection _conexao;
        private IDbTransaction _transacao;
        #endregion

        #region Propriedades
        /// <summary>
        /// Tipo da base de dados utilizada no sistema
        /// </summary>
        public BaseDadosEnum TipoBaseDados { get; set; }

        /// <summary>
        /// String de conexão para a base de dados
        /// </summary>
        public string StringConexao { get; set; }

        /// <summary>
        /// Conexão com a base de dados
        /// </summary>
        public IDbConnection Conexao
        {
            get
            {
                if (_conexao == null) _conexao = ConexaoFactory.ObterConexao();
                return _conexao;
            }
        }

        /// <summary>
        /// Transação ativa
        /// </summary>
        public IDbTransaction TransacaoAtiva
        {
            get { return _transacao; }
        }
        #endregion

        #region Métodos
        /// <summary>
        /// Método que retorna a instância singleton da configuração
        /// </summary>
        /// <returns>Configuração base</returns>
        public static ConexaoBase GetInstance()
        {
            if (_instancia == null) _instancia = new ConexaoBase();
            return _instancia;
        }

        /// <summary>
        /// Método que abre a conexão com a base de dados
        /// </summary>
        public void Abrir()
        {
            //#region Contracts
            ////Contract.Requires(GetInstance().Conexao != null, Mensagens.VAL_CONEXAO_NULA);
            //Contract.Ensures(GetInstance().Conexao.State == ConnectionState.Open, Mensagens.ERR_CONEXAO_NAO_ABRIU);
            //#endregion

            if (GetInstance().Conexao.State == ConnectionState.Closed)
                GetInstance().Conexao.Open();
        }

        /// <summary>
        /// Método que encerra e destrói a conexão
        /// </summary>
        public void Fechar()
        {
            //#region Contracts
            ////Contract.Requires(GetInstance().Conexao != null, Mensagens.VAL_CONEXAO_INICIALIZADA);
            //Contract.Ensures(_instancia == null, Mensagens.ERR_CONEXAO_NAO_ENCERROU);
            //#endregion

            GetInstance().Conexao.Close();
            _instancia = null;
        }

        /// <summary>
        /// Método que inicia uma transação
        /// </summary>
        public void IniciarTransacao()
        {
            //#region Contracts
            ////Contract.Requires(GetInstance().Conexao != null, Mensagens.VAL_CONEXAO_NULA);
            ////Contract.Requires(GetInstance().Conexao.State == ConnectionState.Open, Mensagens.VAL_CONEXAO_ATIVA);
            //Contract.Ensures(_transacao != null, Mensagens.ERR_TRANSACAO_NAO_CRIOU);
            //Contract.Assert(_transacao == null, Mensagens.ERR_TRANSACAO_JA_EXISTE);
            //#endregion

            _transacao = GetInstance().Conexao.BeginTransaction();
        }

        /// <summary>
        /// Método que realiza o commit da transação
        /// </summary>
        public void Commit()
        {
            //#region Contracts
            ////Contract.Requires(GetInstance().TransacaoAtiva != null, Mensagens.VAL_TRANSACAO_ATIVA);
            //Contract.Ensures(GetInstance().TransacaoAtiva == null, Mensagens.ERR_TRANSACAO_COMMIT);
            //#endregion

            _transacao.Commit();
            _transacao = null;
        }

        /// <summary>
        /// Método que realiza o rollback da transação
        /// </summary>
        public void Rollback()
        {
            //#region Contracts
            ////Contract.Requires(GetInstance().TransacaoAtiva != null, "Transação deve estar ativa");
            //Contract.Ensures(GetInstance().TransacaoAtiva == null, "Rollback da transação não pôde ser realizado corretamente");
            //#endregion

            if (_transacao != null && _transacao.Connection != null)
            {
                _transacao.Rollback();
                _transacao = null;
            }
        }
        #endregion

        #region Construtor
        private ConexaoBase() { }
        #endregion
    }