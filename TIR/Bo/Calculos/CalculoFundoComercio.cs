﻿using System;
using tir.dao;
using tir.dao.Entidades;
using TIR.Entidades;
using TIR.Entidades.Encaixes;

namespace TIR.Bo.Calculos
{
    public class CalculoFundoComercio : ICalculo
    {
        public double[] Calcular(Encaixe enc, TIRNegociacao tir)
        {
            try
            {
                //converte encaixe para fundo de comércio
                FundoComercio fundo = (FundoComercio)enc;

                //define prazo total e redimensiona fluxo
                //int prazoTotal = tir.ContrAdicional + tir.ContrExistente + 1;
                //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
                int prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 1;
                //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
                //fundo.Fluxo = new double[prazoTotal + 1];
                fundo.Fluxo = new double[prazoTotal + tir.MesesCompVolume + 1];

                //verifica se há prazo suficiente para o cálculo
                if (prazoTotal < 2)
                {
                    throw new Exception("CalculoFundoComercio.calcular: Erro ao calcular fundo de comércio. Os prazos dos contratos precisam ser especificados.");
                }

                //verifica se os meses de compra e venda inicial e final estão coerentes
                if (fundo.MesCompraFinal + fundo.MesCompraInicial - 1 > prazoTotal - 1 ||
                    fundo.MesVendaFinal + fundo.MesVendaInicial - 1 > prazoTotal - 1)
                {
                    throw new Exception("CalculoFundoComercio.calcular: Erro ao calcular fundo de comércio. Os meses finais de compra e venda de fundo de comércio não podem exceder o prazo total do projeto.");
                }

                //verifica se o mês de venda está coerente com o prazo da negociação
                if (fundo.MesVendaVista > prazoTotal - 1)
                {
                    throw new Exception("CalculoFundoComercio.calcular: Erro ao calcular fundo de comércio. O mês de venda à vista do fundo de comércio não pode ser maior que o prazo total do projeto.");
                }

                //busca imposto de renda
                OutrosParamDao outrosDAO = new OutrosParamDao();
                OutrosParam outros = outrosDAO.ObterParametro("PercIR");
                double percIr = (double)outros.ValorParametroNumerico;
                                
                int mes = 0;
                //se tiver venda à vista - deve sair no mês da venda
                if (fundo.ValorVendaVista > 0)
                {
                    //deve sair no mês da venda
                    mes = fundo.MesVendaVista;
                }
                else
                {
                    //se tiver venda financiada
                    if (fundo.ValorVenda > 0)
                    {
                        //deve sair no último mês de carência
                        mes = fundo.MesVendaInicial;
                    }
                }

                //--- novo cálculo - conforme Sivirino em junho/2006
                double perdaLucro = (fundo.ValorCompra - fundo.ValorVenda - fundo.ValorVendaVista) * (percIr / 100.0);

                //calcula compra
                double valorFluxoPrestCompra = fundo.ValorCompra / (fundo.MesCompraFinal - fundo.MesCompraInicial + 1.0) * (-1.0);

                //--- distribuir valores
                for (int i = fundo.MesCompraInicial; i <= fundo.MesCompraFinal; i++)
                {
                    fundo.Fluxo[i] += valorFluxoPrestCompra / tir.Inflacao[i] / tir.ValorFAO;
                }

                //saída do IR conforme cálculo acima da variável mes (em 18/07/2007 Sivirino e Felipe)
                if (!mes.Equals(fundo.MesVendaVista))
                {
                    //cálculo da saída de IR (sem venda à vista)
                    fundo.Fluxo[mes] = perdaLucro / tir.ValorFAO / tir.Inflacao[mes];
                }
                else
                {
                    //cálculo de saída do IR mais venda à vista
                    //mFluxo(mMesVendaVista) = (mValorVendaVista + tPerdaLucro) / mPROJ.valorFao / mPROJ.Inflacao(mMesVendaVista)
                    //a linha acima foi comentada em 29/11/2007 pois o valor de compra estava se perdendo quando caía no mesmo mês
                    //do valor de venda
                    fundo.Fluxo[fundo.MesVendaVista] += (fundo.ValorVendaVista + perdaLucro) / tir.ValorFAO / tir.Inflacao[fundo.MesVendaVista];
                }

                //retorno com sucesso
                return fundo.Fluxo;
            }
            catch (Exception ex)
            {
                throw new Exception("CalculoFundoComercio.calcular: Erro ao calcular fundo de comércio.\n" + ex.Message, ex);
            }
        }
    }
}
