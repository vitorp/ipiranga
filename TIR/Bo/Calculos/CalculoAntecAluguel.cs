﻿using System;
using tir.dao;
using tir.dao.Entidades;
using TIR.Constantes;
using TIR.Entidades;
using TIR.Entidades.Componentes;
using TIR.Entidades.Encaixes;

namespace TIR.Bo.Calculos
{
    public class CalculoAntecAluguel : ICalculo
    {
        public double[] Calcular(Encaixe enc, TIRNegociacao tir)
        {
            //converte encaixe para antecipação de aluguel
            AntecAluguel aluguel = (AntecAluguel)enc;

            //define valor em FAO, em reais e prazo total
            double valorEmFao = aluguel.Valor / tir.ValorFAO;
            double valorEmReais = aluguel.Valor;
            //int prazoTotal = tir.ContrAdicional + tir.ContrExistente + 1;            
            //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
            //int prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 1;
            //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
            int prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 1;

            //redimensiona vetor do fluxo
            aluguel.Fluxo = new double[prazoTotal + 1];

            //verifica se foi fornecido o código do encaixe
            if (aluguel.CodEcxDcx == 0)
            {
                throw new Exception("CalculoAntecAluguel.calcular: Erro ao calcular antecipação de aluguel. O código do encaixe não foi informado.");
            }

            //verifica se o prazo está coerente
            if (prazoTotal < 2)
            {
                throw new Exception("CalculoAntecAluguel.calcular: Erro ao calcular antecipação de aluguel. Os prazos dos contratos precisam ser especificados.");
            }

            //busca percentual de imposto de renda
            OutrosParamDao outrosDao = new OutrosParamDao();
            OutrosParam outros = outrosDao.ObterParametro("PercIR");
            double Ir = (double)outros.ValorParametroNumerico;

            //***Teste comentado por Luiz Felipe a pedido de Marketing - set/2015
            //se não for bonificação postecipada
            if (!aluguel.CodEcxDcx.Equals(TipoCodEcxDcx.BONIFICACAO_POSTECIPADA))
            {
                //define meses de antecipação
                aluguel.MesesAntecip = tir.ContrAdicional + tir.ContrExistente - aluguel.MesConcessao;
                //Alterado em abr/2015 para incluir os meses sem venda
                if (tir.PeriodoMatura > 0)
                {
                    if (aluguel.MesConcessao > tir.PeriodoMatura)
                    {
                        aluguel.MesesAntecip = tir.ContrAdicional + tir.ContrExistente - (aluguel.MesConcessao - tir.PeriodoMatura);
                    }
                    else
                    {
                        aluguel.MesesAntecip = tir.ContrAdicional + tir.ContrExistente;
                    }
                }
            }

            //define fluxo no mês de concessão
            aluguel.Fluxo[aluguel.MesConcessao] = valorEmFao * (-1.0) / tir.Inflacao[aluguel.MesConcessao];

            //--- Aluguel Antecipado CTF deverá ser amortizado em 20 anos - janeiro/2006 - Calmeida/siva
            if (aluguel.CodEcxDcx.Equals(TipoCodEcxDcx.ALUGUEL_ANTECIPADO_CTF))
            {
                double ex80ValorTotal = 0.0;
                //Só retira os meses sem venda - incluído em ago/2014 por Luiz Felipe
                int iniAlug = aluguel.MesConcessao + 1;
                if (tir.PeriodoMatura > 0)
                {
                    if (tir.PeriodoMatura + 1 > aluguel.MesConcessao + 1) iniAlug = tir.PeriodoMatura + 1;
                }

                //Monta o fluxo, retirando os meses sem venda se necessário - por Luiz Felipe em ago/2014
                //for (int i = aluguel.MesConcessao + 1; i <= prazoTotal - 1; i++)
                //for (int i = iniAlug; i <= aluguel.MesConcessao + aluguel.MesesAntecip; i++)
                for (int i = iniAlug; i <= prazoTotal - 1; i++)
                {
                    aluguel.Fluxo[i] = valorEmFao * Ir / 100.0 / 240.0 / tir.Inflacao[i];  //incluida o não inflacionamento - agosto/2005 - sivirino
                    ex80ValorTotal += aluguel.Fluxo[i];
                }

                valorEmReais = valorEmReais * Ir / 100.0;
                valorEmReais = valorEmReais - (valorEmReais / 240.0 * (prazoTotal - aluguel.MesConcessao - 1.0));
                aluguel.Fluxo[prazoTotal] = valorEmReais / tir.ValorFAO / tir.Inflacao[prazoTotal];
            }
            else
            {
                //pedido do Mauricio Macedo - bonificação postecipada parcelada - 18/03/2008 - parte comentada com ('') e criado loop
                //somente bonif. postecipada - siva em agosto/2005
                if (aluguel.CodEcxDcx.Equals(TipoCodEcxDcx.BONIFICACAO_POSTECIPADA))
                {
                    //define valor calculado do fluxo no mês de concessão
                    double dblBonifPost = (valorEmFao / (aluguel.MesesAntecip - aluguel.MesConcessao + 1.0)) * (-1.0);

                    //define o imposto de renda sobre o valor acima
                    double dblIr = dblBonifPost * (Ir / 100.0);

                    //Calculando o valor da bonificação postecipada
                    double volumeInicial = aluguel.VolumeInicial;
                    double volumeUtilizado = 0.0;
                    double volumeReferencial = aluguel.VolumeInicial;
                    //redimensiona vetor do fluxo
                    aluguel.Fluxo = new double[prazoTotal + 1];
                    int qtdeParc = 1;
                    int diferenca = 0;
                    int saldo = 0;

                    //***Criado em jun/2014 para garantiar que não haverá
                    //Bonificação Postecipada em meses sem volume
                    int iniAlug = aluguel.MesConcessao;
                    if (iniAlug <= 0)
                    {
                        //Se existe volume incremental
                        //if (tir.IsIncremental)
                        //{
                            //Só retira os meses sem venda
                            iniAlug = tir.PeriodoMatura > 0 ? tir.PeriodoMatura + 1 : 1;
                        //}
                        //Não existe volume incremental
                        //else
                        //{
                            //Retira meses existentes e meses sem venda
                            //iniAlug = (tir.ContrExistente + tir.PeriodoMatura) > 0 ? tir.ContrExistente + tir.PeriodoMatura + 1 : 1;
                        //}
                    }

                    //for (int i = (aluguel.MesConcessao > 0 ? aluguel.MesConcessao : 1); i < prazoTotal; i++)
                    //***

                    for (int i = iniAlug; i < prazoTotal; i++)
                    {
                        if (qtdeParc <= aluguel.QuantidadeParcelas)
                        {
                            //volumeUtilizado += AreaAbastecimento.TotalVolumeMesAno1;
                            //Alterado em jul/2012 por Luiz Felipe, para excluir os lubrificantes
                            //volumeUtilizado += AreaAbastecimento.TotalVolumeMesAno1SemLubes;

                            //***Alterado por Luiz Felipe em jun/2015, para incluir a Maturação
                            //***de Vendas no cálculo da Bonificação Postecipada e adequar o
                            //***volume ao ano (1, 2 ou 3) que estiver sendo processado
                            //Determina o volume de acordo com o ano (descontando os meses sem venda)
                            double VolumeAno = 0;
                            //if (i - tir.PeriodoMatura <= 12)    //Ano 1
                            //Alterado por Luiz Felipe em jul/2015, para considerar os meses existentes no prazo
                            if (i - tir.PeriodoMatura <= (12 + tir.ContrExistente))    //Ano 1
                            {
                                VolumeAno = AreaAbastecimento.TotalVolumeMesAno1SemLubes;
                            }
                            else
                            {
                                //if (i - tir.PeriodoMatura <= 24)    //Ano 2
                                //Alterado por Luiz Felipe em jul/2015, para considerar os meses existentes no prazo
                                if (i - tir.PeriodoMatura <= (24 + tir.ContrExistente))    //Ano 2
                                {
                                    VolumeAno = AreaAbastecimento.TotalVolumeMesAno2SemLubes;
                                }
                                //Ano 3
                                else
                                {
                                    VolumeAno = AreaAbastecimento.TotalVolumeMesAno3SemLubes;
                                }
                            }

                            //Se não houver Maturação de Vendas
                            if (tir.IsMaturaVenda == false)
                            {
                                volumeUtilizado += VolumeAno;
                            }
                            //Existe Maturação
                            else
                            {
                                //Aplica o percentual de Maturação de acordo
                                //com o mês, descontando os meses sem venda
                                float PercMatura = tir.PercMaturaVenda[tir.MesMaturaVenda.Length - 1] / 100;
                                for (int j = 0; j < tir.MesMaturaVenda.Length; j++)
                                {
                                    if ((i - tir.PeriodoMatura) <= tir.MesMaturaVenda[j])
                                    {
                                        PercMatura = tir.PercMaturaVenda[j] / 100;
                                        break;
                                    }
                                }

                                volumeUtilizado += VolumeAno * PercMatura;
                            }
                            //***

                            if (volumeInicial > 0)
                            {
                                //if (volumeUtilizado >= aluguel.VolumeInicial)
                                //***Alterado em jul/2015 por Luiz Felipe, para dar
                                //***1ª saída em (volume inicial + primeira parcela)
                                if (volumeUtilizado >= (aluguel.VolumeInicial + aluguel.VolumeParcial))
                                {
                                    //volumeReferencial = volumeInicial;
                                    //***Alterado por Luiz Felipe em jul/2015 para o acerto da 1ª saída
                                    volumeReferencial = volumeInicial + aluguel.VolumeParcial;
                                    volumeInicial = 0;
                                    aluguel.Fluxo[i] = (dblBonifPost - dblIr);// / tir.Inflacao[i];
                                    qtdeParc++;
                                }
                            }

                            if (volumeInicial == 0)
                            {
                                if (aluguel.VolumeParcial > 0)
                                {
                                    saldo = (int)(volumeUtilizado - volumeReferencial);
                                    diferenca = (int)(saldo / aluguel.VolumeParcial);

                                    if (diferenca > 0)
                                    {
                                        aluguel.Fluxo[i] = (dblBonifPost - dblIr) * diferenca; // / tir.Inflacao[i] * diferenca;
                                        qtdeParc += diferenca;
                                        volumeReferencial += (aluguel.VolumeParcial * diferenca);
                                    }
                                }
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                else
                {
                    //Só retira os meses sem venda - incluído em ago/2014 por Luiz Felipe
                    int iniAlug = aluguel.MesConcessao + 1;
                    if (tir.PeriodoMatura > 0)
                    {
                        if (tir.PeriodoMatura + 1 > aluguel.MesConcessao + 1) iniAlug = tir.PeriodoMatura + 1;
                    }

                    /*Excluído por Luiz Felipe em abr/2015 a pedido da área financeira
                    //***Incluído por Luiz Felipe em mar/2015
                    //Define por quantos meses deve ser considerada a antecipação
                    //(só se aplica se houver compensação de volume)
                    if (tir.MesesCompVolume > 0)
                    {
                        aluguel.MesesAntecip += tir.MesesCompVolume;
                        //Se o período de antecipação for maior que o contrato, restringe ao tempo do contrato
                        if (aluguel.MesesAntecip > (tir.ContrAdicional + tir.ContrExistente)) 
                            aluguel.MesesAntecip = tir.ContrAdicional + tir.ContrExistente;
                    }
                    //***
                    */

                    //Monta o fluxo, retirando os meses sem venda se necessário - por Luiz Felipe em ago/2014
                    //for (int i = aluguel.MesConcessao + 1; i <= aluguel.MesConcessao + aluguel.MesesAntecip; i++)
                    //for (int i = iniAlug; i <= aluguel.MesConcessao + aluguel.MesesAntecip; i++)
                    for (int i = iniAlug; i < iniAlug + aluguel.MesesAntecip; i++)
                    {
                        aluguel.Fluxo[i] = valorEmFao * Ir / 100.0 / aluguel.MesesAntecip / tir.Inflacao[i];  //incluida o não inflacionamento - agosto/2005 - sivirino
                    }
                }
            }

            //retorno com sucesso
            return aluguel.Fluxo;
        }
    }
}
