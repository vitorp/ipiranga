﻿
namespace TIR.Constantes
{
    public static class TipoCodProduto
    {
        public const int GRAXAS = 1;
        public const int GASOLINA_COMUM = 2;
        public const int OLEO_DIESEL_COMUM = 3;
        public const int ALCOOL = 4;
        public const int GAS_NATURAL = 5;
        public const int QUEROSENE = 6;
        public const int OLEOS_COMBUSTIVEIS = 7;
        public const int GASOLINA_PREMIUM = 9;
        public const int GASOLINA_F1_MASTER = 10;
        public const int OLEO_DIESEL_ADITIVADO = 11;
        public const int OLEOS_LUBRIFICANTES = 12;
    }
}
