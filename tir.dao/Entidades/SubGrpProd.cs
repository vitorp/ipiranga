﻿
namespace tir.dao.Entidades
{
    public class SubGrpProd
    {
        #region Propriedades
        public decimal CodigoSubgrupoProduto { get; set; }
        public string NomeSubgrupoProduto { get; set; }
        #endregion
    }
}
