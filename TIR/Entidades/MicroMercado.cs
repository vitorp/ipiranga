﻿
namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que representa o Micro Mercado.
    /// </summary>
    public class MicroMercado
    {
        #region Propriedades

        public string Codigo { get; set; }
        public string Descricao { get; set; }

        #endregion
    }
}
