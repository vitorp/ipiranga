﻿
namespace TIR.Entidades.Previa
{
    public class Sumario
    {
        public string Descricao { get; set; }
        public double Valor { get; set; }
        public double ValorInstalacao{ get; set; }
        public double PercentPartOperador { get; set; }
        public double ValorPartOperador { get; set; }
        public double ValorFinalInstal { get; set; }
        public double ValorOriginalInstal { get; set; }
        public string CaminhoArquivo { get; set; }
        public string NomeArquivo { get; set; }
    }
}
