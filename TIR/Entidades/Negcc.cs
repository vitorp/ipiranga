﻿
namespace TIR.Entidades
{
    public class Negcc
    {
        #region Propriedades

        //Propriedades simples
        /*public int NumTIR { get; set; }
        public int NumCliente { get; set; }
        public string Nome { get; set; }
        public string Objetivo { get; set; }
        public string Tipo { get; set; }
        public string DataCriacao { get; set; }
        public string DataUltimaAlteracao { get; set; }
        public string Empresa { get; set; }*/
        public int Categoria { get; set; }      //Tipo de negociação
        public string NomeNegociacao { get; set; }
        /*public string Responsavel { get; set; }
        public bool IsClienteDragged { get; set; }
        public bool IsTIRDragged { get; set; }
        public bool IsConcluida { get; set; }
        public string Path { get; set; }
        public string IdRevisao { get; set; }
        public string NumeroPropostaRevisao { get; set; }
        public string GVDPPI { get; set; }      //Obter a GV da DPPI antes de obter a GN
        public string Versao { get; set; }*/

        //valores da TIR total da Negociação
        /*public double ValorFAO {get; set;}
        public double ValorTIR { get; set; }
        public double ValorNPV { get; set; }
        public int PayBack { get; set; }
        public double[] Fluxo { get; set; }

        //Previa
        public string Previa { get; set; }
        public int NumPrevia { get; set; }
        public List<Previa> Previas { get; set; }*/

        //Propriedades tipo classe
        //public Cliente[] Clientes { get; set; }
        public TIRNegociacao TIR { get; set; }
        //public GrupoEconomico GrupoEconomico { get; set; }
        //public GerenciaNacional GerenciaNacional { get; set; }
        //public Elaborador Elaborador { get; set; }

        #endregion
    }
}
