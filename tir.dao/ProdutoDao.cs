﻿using System;
using System.Data;
using tir.dao.Entidades;
using System.Text;
using daobase;

namespace tir.dao
{
    public class ProdutoDao : DaoBase<Produto>
    {
        #region Métodos Públicos

        /// <summary>
        /// Obtem os dados de um Produto pelo seu código
        /// </summary>
        /// <param name="codProdu"></param>
        /// <returns></returns>
        public Produto ObterProduto(string codProdu)
        {
            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select Cd_Prod, ");
                sb.Append("Nm_Prod, ");
                sb.Append("Cd_Grupo_Prod, ");
                sb.Append("Cd_Subgrp_Prod, ");
                sb.Append("Tp_Prod ");
                sb.Append("from Produtos ");
                sb.AppendFormat("where Cd_Prod = {0} ", DaoUtil.FormatarParametro("codProdu"));

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codProdu"), codProdu, typeof(String), 3));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //se há resultado
                if (dr.Read())
                {
                    //valor de retorno
                    return MontarObjeto(dr);
                }
                else
                {
                    //valor de retorno nulo
                    return null;
                }
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("GrupoProdDao.ObterGrupoProduto",
                //"Erro ao obter o Grupo do Produto pelo código do Grupo.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        #endregion

        #region Overrides

        protected override Produto MontarObjeto(IDataReader dr)
        {
            Produto ent = new Produto();
            try
            {
                ent.CodigoProduto = (string)dr["Cd_Prod"];
                ent.NomeProduto = (string)dr["Nm_Prod"];
                ent.CodigoGrupoProduto = (string)dr["Cd_Grupo_Prod"];
                //ent.CodigoSubgrupoProduto = (decimal)dr["Cd_Subgrp_Prod"];
                //ent.CodigoTipoProduto = (decimal)dr["Tp_Prod"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
