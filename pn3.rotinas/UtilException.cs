﻿using System;

namespace pn3.rotinas
{
    /// <summary>
    /// Classe de exceção das rotinas.
    /// </summary>
    public class UtilException : Exception
    {
        /// <summary>
        /// Construtor da classe.
        /// </summary>
        /// <param name="message">Mensagem a ser passada.</param>
        /// <param name="innerException">Erro de origem.</param>
        public UtilException(string source, string message, Exception innerException)
            : base(message, innerException)
        {
            //define propriedades
            this.Source = source;
        }
    }
}
