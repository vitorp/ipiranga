﻿using System.Collections.Generic;
using tir.dao;
using tir.dao.Entidades;
using TIR.Constantes;
using TIR.Entidades;
using TIR.Entidades.Componentes;
using TIR.Entidades.Encaixes;
using TIR.Entidades.ItensFranquias;

namespace TIR.Bo.Calculos
{
    public class CalculoRoyaltyFixo : ICalculo
    {
        #region Propriedades
        public Franquia Franquia { get; set; }
        #endregion

        public double[] Calcular(Encaixe enc, TIRNegociacao tir)
        {
            //converte encaixe
            RoyaltyFixo royalty = (RoyaltyFixo)enc;

            royalty.Descricao = "Royalty Fixo";

            //define prazo total e redimensiona fluxo
            //int prazoTotal = Franquia.TempoContrato + 1;
            //Incluídos os meses sem venda por Luiz Felipe - out/2014
            //int prazoTotal = Franquia.TempoContrato + tir.PeriodoMatura + 1;
            //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
            int prazoTotal = Franquia.TempoContrato + tir.PeriodoMatura + tir.MesesCompVolume + 1;

            //royalty.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + 2];
            //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
            //royalty.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 2];
            //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
            royalty.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 2];

            bool isFranquiaRenovacao = false;
            //--- Alteração asseguramento de franquias - Agosto/2002
            if (Franquia.TpNegFranquia.Equals(2) || Franquia.TpNegFranquia.Equals(1))
            {
                isFranquiaRenovacao = true;
            }

            //busca alíquotas de imposto de renda e pis
            OutrosParamDao outrosDAO = new OutrosParamDao();
            OutrosParam outros = outrosDAO.ObterParametro("PercIR");
            OutrosParam outrosPis = outrosDAO.ObterParametro("ReceitaPIS");
            float percIr = (float)outros.ValorParametroNumerico;
            double aliqPisCof = (double)outrosPis.ValorParametroNumerico;

            //--- Gerar o fluxo de RoyaltyFixo
            //--- Este cálculo não é válido para AmPm, ela não admite
            //--- valores em cascata. É por tempo de contrato: se até
            //--- 5 anos valor x ou se 8 anos valor y.

            //busca royalties
            RoyalCbpiDao cbpiDAO = new RoyalCbpiDao();
            List<RoyalCbpi> royalties = cbpiDAO.ListarPorComponente(Franquia.CodPadraoComp, Franquia.CodPerfilComp, Franquia.CodTipoComp);
            if (royalties.Count > 0)
            {
                Franquia.FaixaRoyaltyCBPI = new int[royalties.Count];
                for (int i = 0; i < royalties.Count; i++)
                {
                    Franquia.FaixaRoyaltyCBPI[i] = royalties[i].NumeroMesFaixaRoyalties;
                }
            }

            int mesInicio = 0;
            //migração de franquias maio/2004
            if (isFranquiaRenovacao.Equals(true) && !tir.Categoria.Equals(41))
            {
                //só calcula se tiver prazo proposto
                if (tir.ContrAdicional > 0)
                {
                    mesInicio = 1 + tir.ContrExistente;
                }
                else
                {
                    //não precisa calcular o fluxo
                    return royalty.Fluxo;
                }
            }
            else
            {
                mesInicio = 1;
            }

            //busca quantidade de elementos de royalties
            int max = Franquia.QtElementosFaixa;

            double valorRoy = 0.0;
            for (int i = mesInicio + tir.PeriodoMatura; i <= (prazoTotal - 1 + (mesInicio - 1)); i++)
            {
                //--- acha o valor conforme o período
                for (int j = 0; j <= max; j++)
                {
                    if (Franquia.FaixaRoyaltyCBPI[j].Equals(0))
                    {
                        break;
                    }

                    //somente AmPm
                    if (Franquia.CodTipoComp.Equals(TipoComponente.AMPM))
                    {
                        if (Franquia.TempoContrato <= Franquia.FaixaRoyaltyCBPI[j] || j == max)
                        {
                            valorRoy = Franquia.ValorRoyaltyFixo[j] / tir.ValorFAO;
                            break;
                        }
                    }
                    else
                    {
                        if (Franquia.TpNegFranquia > 0)
                        {
                            if (j.Equals(max))
                            {
                                valorRoy = Franquia.ValorRoyaltyFixo[j] / tir.ValorFAO;
                                break;
                            }
                        }
                        else
                        {
                            if (i <= Franquia.FaixaRoyaltyCBPI[j] || j.Equals(max))
                            {
                                valorRoy = Franquia.ValorRoyaltyFixo[j] / tir.ValorFAO;
                            }
                        }
                    }
                }

                royalty.Fluxo[i] = valorRoy * (1.0 - percIr / 100.0) * (1.0 - aliqPisCof / 100.0);
            }

            //retorno com sucesso
            return royalty.Fluxo;
        }
    }
}
