﻿using System;
using System.Collections.Generic;
using Microsoft.VisualBasic;
using tir.dao;
using tir.dao.Entidades;
using TIR.Entidades;
using TIR.Entidades.Componentes;

namespace TIR.Bo.Calculos
{
    public class CalculoTIRNegociacao
    {
        #region Métodos Públicos

        public double[] Calcular(TIRNegociacao tir)
        {
            //define o tipo de negociação e perfil
            string TpNeg = tir.Cliente.segmento;
            int Localiz = Convert.ToInt32(tir.Cliente.Perfil);

            //verifica parâmetros de tipo de negociação
            ParamNegDao parNegDAO = new ParamNegDao();
            ParamNeg parNeg = parNegDAO.ObterParametro(tir.Categoria, TpNeg, Localiz);

            //verificar se tem maturação de vendas
            if (parNeg.IndicadorMaturacao == true)
            {
                //define variável booleana como verdadeiro
                tir.IsMaturaVenda = true;

                //busca o percentual de volume
                PegaPercVolume(tir.Categoria, tir);

                //Calcula os meses que terão compensação de Volume
                CalculaMesesCompensacaoVolume(tir);

            }
            else
            {
                tir.IsMaturaVenda = false;

                //redimensiona vetores
                tir.VendasMes = new int[1];
                tir.PercentualMes = new float[1];
            }

            //-- prazos ------------

            //verifica se os prazos estão condizentes ao tipo de negociação
            if (tir.ContrAdicional + tir.ContrExistente > parNeg.QuantidadeMaximaMesesProjeto)
            {
                throw new Exception("CalculoTIRNegociacao: Erro ao calcular TIR. " +
                    "O tempo de contrato adicional + existente não pode ser maior que " +
                    Convert.ToString(parNeg.QuantidadeMaximaMesesProjeto) +
                    " meses, que é o máximo permitido para o tipo de negociação selecionado.");
            }

            //verifica se a negociação permite prazo para contrato existente
            if (parNeg.IndicadorContratoExistente == false && tir.ContrExistente > 0)
            {
                throw new Exception("CalculoTIRNegociacao: Erro ao calcular TIR. O tipo de negociação não admite prazo para contratos existentes.");
            }

            //int meses = tir.ContrExistente + tir.ContrAdicional + 2;
            //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
            //int meses = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 2;
            //Alterado em fev/2015 para incluir o período de compensação de Volume devido à Maturação
            int meses = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 2;

            //dimensiona vetor de fluxo
            tir.Fluxo = new double[meses];

            //define variáveis booleanas
            //bool calcularTIR = false;
            //bool indCalculo = false;

            //Calcula a inflação para todo o fluxo
            //CalculaInflacao(tir.ContrExistente + tir.ContrAdicional, tir);
            //Alterado em jun/2014 para incluir os meses sem venda
            //CalculaInflacao(tir.ContrExistente + tir.ContrAdicional + tir.PeriodoMatura, tir);
            //Alterado em fev/2015 para incluir o período de compensação de Volume devido à Maturação
            CalculaInflacao(tir.ContrExistente + tir.ContrAdicional + tir.PeriodoMatura + tir.MesesCompVolume, tir);

            //instancia componente DEFAULT
            AreaAbastecimento abast = tir.Cliente.Posto; //(AreaAbastecimento)tir.Cliente.Componentes.Find(c => c is AreaAbastecimento);

            //verifica se existem encaixes ou produtos na área de abastecimento
            if ((abast.Encaixes != null && abast.Encaixes.Count > 0) || 
                (abast.Produtos != null && abast.Produtos.Count > 0))
            {
                //verifica se foi possível calcular a TIR da área de abastecimento
                CalculoTIRAreaAbast calcTIRAbast = new CalculoTIRAreaAbast();
                abast.Fluxo = calcTIRAbast.Calcular(abast, tir);
                if (abast.Fluxo == null)
                {
                    //se não foi possível limpa fluxo total
                    tir.Fluxo = new double[meses];

                    //propaga erro e sai da rotina
                    throw new Exception("CalculoTIRNegociacao: Erro ao calcular TIR. Dados insuficientes para o cálculo da TIR para a área de abastecimento.");
                }
                //se foi possível calcular a TIR
                else
                {
                    //varre o fluxo total
                    for (int i = 0; i < meses; i++)
                    {
                        tir.Fluxo[i] += abast.Fluxo[i];
                    }
                }
            }

            //------ coleção de outros componentes

            CalculoTIRFranquias calcFranq = new CalculoTIRFranquias();

            //AmPm
            if (tir.Cliente.AmPm != null)
            {
                //instancia objeto do ampm
                AmPm AmPm = tir.Cliente.AmPm; //(AmPm)comp;

                //verifica se existe tempo de contrato
                if (AmPm.TempoContrato > 0)
                {
                    //verifica validade do tempo de contrato da AmPm
                    if (AmPm.TempoContrato > tir.ContrAdicional + tir.ContrExistente)
                    {
                        //se for inválido levanta erro
                        throw new Exception("CalculoTIRNegociacao: Erro ao calcular TIR. O tempo de contrato especificado para AmPm não pode ser maior que o contrato adicional + existente.");
                    }

                    //verifica se foi possível calcular a TIR da AmPm
                    AmPm.Fluxo = calcFranq.Calcular(AmPm, tir);
                    if (AmPm.Fluxo == null)
                    {
                        //se não foi possível levanta erro
                        throw new Exception("CalculoTIRNegociacao: Erro ao calcular TIR. O cálculo do fluxo do componente AmPm falhou.");
                    }

                    //varre fluxo total
                    for (int i = 0; i < meses; i++)
                    {
                        //adiciona o fluxo da AmPm ao fluxo total
                        tir.Fluxo[i] += AmPm.Fluxo[i];
                    }
                }
            }

            //JetOil
            if (tir.Cliente.JetOil != null)
            {
                //instancia objeto do jetoil
                JetOil jet = tir.Cliente.JetOil; //(JetOil)comp;

                //verifica se existe tempo de contrato
                if (jet.TempoContrato > 0)
                {
                    //verifica validade do tempo de contrato do JetOil
                    if (jet.TempoContrato > tir.ContrAdicional + tir.ContrExistente)
                    {
                        //se for inválido levanta erro
                        throw new Exception("CalculoTIRNegociacao: Erro ao calcular TIR. O tempo de contrato especificado para JetOil não pode ser maior que o contrato adicional + existente.");
                    }

                    //verifica se foi possível calcular a TIR do JetOil
                    jet.Fluxo = calcFranq.Calcular(jet, tir);
                    if (jet.Fluxo == null)
                    {
                        //se não foi possível levanta erro
                        throw new Exception("CalculoTIRNegociacao: Erro ao calcular TIR. O cálculo do fluxo do componente JetOil falhou.");
                    }

                    //varre fluxo total
                    for (int i = 0; i < meses; i++)
                    {
                        //adiciona o fluxo do JetOil ao fluxo total
                        tir.Fluxo[i] += jet.Fluxo[i];
                    }
                }
            }

            //JetOil Motos
            if (tir.Cliente.JetMotos != null)
            {
                //instancia objeto do JetOil Motos
                JetOilMotos motos = tir.Cliente.JetMotos; //(JetOilMotos)comp;

                //verifica se existe tempo de contrato
                if (motos.TempoContrato > 0)
                {
                    //verifica validade do tempo de contrato do JetOil
                    if (motos.TempoContrato > tir.ContrAdicional + tir.ContrExistente)
                    {
                        //se for inválido levanta erro
                        throw new Exception("CalculoTIRNegociacao: Erro ao calcular TIR. O tempo de contrato especificado para JetOil Motos não pode ser maior que o contrato adicional + existente.");
                    }

                    //verifica se foi possível calcular a TIR do JetOil
                    motos.Fluxo = calcFranq.Calcular(motos, tir);
                    if (motos.Fluxo == null)
                    {
                        //se não foi possível levanta erro
                        throw new Exception("CalculoTIRNegociacao: Erro ao calcular TIR. O cálculo do fluxo do componente JetOil Motos falhou.");
                    }

                    //varre fluxo total
                    for (int i = 0; i < meses; i++)
                    {
                        //adiciona o fluxo do JetOilMotos ao fluxo total
                        tir.Fluxo[i] += motos.Fluxo[i];
                    }
                }
            }

            //---- A tir é calculada por ano, por isso a fórmula abaixo

            //guarda valor para verificar depois se houve alteração
            double verificaTIR = tir.TIR;

            //calcula o valor da tir
            double[] fluxo = tir.Fluxo;
            try
            {
                tir.TIR = (Math.Pow(1.0 + Financial.IRR(ref fluxo, 1.15151291201983E-02), 12.0) - 1.0) * 100.0;
            }
            catch
            {
                //se tem erro na fórmula, define valor padrão para a TIR
                tir.TIR = -999999.777777;
            }

            try
            {
                //***Inserido por Luiz Felipe em setembro/2011
                //***A TIR considerada passa a ser a nominal
                InflacaoDao inflaDAO = new InflacaoDao();
                Double inflaTIRNominal = inflaDAO.ObterInflacaoMedia();

                tir.TIR = (((1.0 + tir.TIR / 100.0) * (1.0 + inflaTIRNominal / 100.0)) - 1.0) * 100.0;

 
                //verifica se houve alteração
                if (tir.TIR != verificaTIR)
                {
                    //se houve alteração, marca a TIR como editada
                    tir.IsEditada = true;
                }

                //Descrição: Alteração pedida pelo Mauricio Macedo em 21/11/2007;
                //           Quando a TIR for negativa colocá-la como indefinida.
                //           Tarefa: 005F8747.
                if (tir.TIR < 0.0)
                {
                    tir.TIR = -999999.777777;
                }

                //---- Cálculo do NPV (valor presente)

                //variáveis de apoio
                double[] Awork = new double[tir.Fluxo.GetUpperBound(0) + 1];
                double CustoAno;

                //varre fluxo
                for (int i = 1; i <= tir.Fluxo.GetUpperBound(0); i++)
                {
                    //copia fluxo no vetor de trabalho (atenção aos índices)
                    Awork[i - 1] = tir.Fluxo[i];
                }


                //busca custo de oportunidade
                OutrosParamDao outrosDAO = new OutrosParamDao();
                OutrosParam outros = outrosDAO.ObterParametro("CustoOport");

                //Inserido em setembro/2011 por Luiz Felipe,
                //para passar como parâmetro para a PNE
                tir.CustoOportunidade = outros.ValorParametroNumerico;

                //calcula custo anual
                CustoAno = Math.Pow((1.0 + (double)outros.ValorParametroNumerico / 100.0), (1.0 / 12.0)) - 1.0;

                //calcula npv
                tir.NPV = (Financial.NPV(CustoAno, ref Awork) + tir.Fluxo[0]) * tir.ValorFAO;

                //formata valor da tir para o caso em que não tenha sido possível calcular (extremos)
                if (Math.Abs(tir.TIR) == 999999.777777 && tir.NPV > 0.0)
                {
                    tir.TIR = 999999.777777;
                }


                //----- Cálculo do PayBack ----------

                //declaração de variáveis
                double wTaxa;
                double wSaldo;
                int wMaior;
                bool wFlag;

                //começa cálculo do saldo e da taxa
                wSaldo = tir.Fluxo[0];
                wTaxa = Math.Pow((1.0 + CustoAno / 100.0), (1.0 / 12.0)) - 1.0;
                wMaior = -1;

                //seta flag como falso
                wFlag = false;

                //varre fluxo
                for (int i = 1; i < meses; i++)
                {
                    //cálculo do saldo
                    wSaldo += tir.Fluxo[i] / Math.Pow((1.0 + wTaxa), i);

                    if (wFlag == false)
                    {
                        if (wSaldo >= 0.0)
                        {
                            wMaior = i;
                            wFlag = true;
                        }
                    }
                    else
                    {
                        if (wSaldo < 0.0)
                        {
                            wFlag = false;
                        }
                    }
                }

                //define payback
                tir.Payback = wMaior;

                if (wFlag == false)
                {
                    tir.Payback = -1;
                }
            }
            catch
            {
                //se tem erro na fórmula, define valor padrão para a TIR
                tir.TIR = -999999.777777;
            }

            //retorno com sucesso
            return tir.Fluxo;
        }

        #endregion

        #region Métodos Privados

        /// <summary>
        ///Método privado que lê e retorna os meses e os percentuais de
        ///Maturação de Vendas para um determinado código de categoria. 
        /// </summary>
        /// <param name="codCategoria">Código da Categoria a pesquisar.</param>
        /// <param name="tir">TIR de Negociação a ser atualizada.</param>
        private void PegaPercVolume(int codCategoria, TIRNegociacao tir)
        {
            //------ ler meses e percentuais
            MaturaVendaDao matVendaDAO = new MaturaVendaDao();
            List<MaturaVenda> matVenda = matVendaDAO.ListarPorTipoNegociacao(codCategoria);

            //redimensiona os arrays
            tir.VendasMes = new int[matVenda.Count];
            tir.PercentualMes = new float[matVenda.Count];
            tir.MesMaturaVenda = new int[matVenda.Count];
            tir.PercMaturaVenda = new float[matVenda.Count];

            //atualiza os valores da TIR
            for (int i = 0; i < matVenda.Count; i++)
            {
                tir.VendasMes[i] = matVenda[i].QuantidadeMesesMaturacao;
                tir.PercentualMes[i] = (float)matVenda[i].PercentualVolumeVendas;
                tir.MesMaturaVenda[i] = matVenda[i].QuantidadeMesesMaturacao;
                tir.PercMaturaVenda[i] = (float)matVenda[i].PercentualVolumeVendas;
            }

            return;
        }

        /// <summary>
        /// Método privado que calcula a inflação mês a mês.
        /// </summary>
        /// <param name="meses">Quantidade de meses a serem calculados.</param>
        /// <param name="tir">TIR de Negociação a ser atualizada.</param>
        private void CalculaInflacao(int meses, TIRNegociacao tir)
        {
            if (meses <= 0)
            {
                throw new Exception("CalculoTIRNegociacao::CalculaInflacao: Erro ao calcular inflação. A negociação precisa ter prazo de contrato.");
            }

            tir.Inflacao = new double[meses + 2];

            double inflacaoAno = 0.0;
            double inflacaoMes = 0.0;

            //---- calcula inflacao mes a mes
            int Ano = -1;

            tir.Inflacao[0] = 1;

            for (int i = 1; i <= meses + 1; i++)
            {
                if (Ano != Convert.ToInt32((i - 1) / 12) + 1)
                {
                    InflacaoDao inflaDAO = new InflacaoDao();
                    Inflacao infla = inflaDAO.ObterPorAno(Convert.ToInt32((i - 1) / 12) + 1);
                    if (infla == null)
                    {
                        if (Ano == -1)
                        {
                            throw new Exception("CalculoTIRNegociacao::CalculaInflacao: Erro ao calcular inflação. Não tem inflação cadastrada para nenhum ano de projeto. Verifique tabela de inflações.");
                        }
                    }
                    else
                    {
                        inflacaoAno = infla.PercentualInflacaoAnual;
                        inflacaoMes = Math.Pow(1.0 + inflacaoAno / 100.0, (1.0 / 12.0)) - 1.0;

                    }

                    Ano = (int)((i - 1) / 12) + 1;
                }

                if (i == 1)
                {
                    tir.Inflacao[i] = 1.0 + inflacaoMes;
                }
                else
                {
                    tir.Inflacao[i] = tir.Inflacao[i - 1] * (1.0 + inflacaoMes);
                }
            }
        }

        /// <summary>
        ///Método privado que verifica se existe volume de Produto a ser
        ///compensado ao final do contrato devido a Maturação de Vendas.
        ///Se houver, calcula a quantidade máxima de meses onde haverá a
        ///compensação para extender o fluxo por esse período.
        /// </summary>
        /// <param name="tir">TIR de Negociação a ser atualizada.</param>
        private void CalculaMesesCompensacaoVolume(TIRNegociacao tir)
        {
            //instancia componente DEFAULT
            AreaAbastecimento abast = tir.Cliente.Posto;

            double Volume = 0.0;

            //Volumes anuais (variáveis locais)
            double volAno1 = 0.0;
            double volAno2 = 0.0;
            double volAno3 = 0.0;

            //Volume a ser compensado (para um produto)
            double volComp = 0.0;

            //Meses durante os quais haverá compensação de volume
            int mesesComp = 0;

            //Excluídas as negociações de Novo Negócio Próprio/Controlado
            //e de Embandeiramento (nesse caso se o tipo de negócio do
            //Cliente for posto próprio (020) ou controlado (061))
            //a pedido da Área Financeira - por Luiz Felipe em ago/2015
            if (tir.Categoria == 7 ||
                (tir.Categoria == 33 && (tir.Cliente.TipoNegocio.Codigo == "020" ||
                                         tir.Cliente.TipoNegocio.Codigo == "061")))
            {
                tir.MesesCompVolume = 0;
            }
            else
            {
                //Só calcula se houver produtos
                if (abast.Produtos != null && abast.Produtos.Count > 0)
                {
                    //Calcula o tamanho do fluxo sem compensação de volume
                    int prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura; // +2;

                    //Loop de Produtos na Área de Abastecimento
                    foreach (TIR.Entidades.Produto p in abast.Produtos)
                    {
                        //Transfere Volumes anuais para variáveis locais
                        volAno1 = p.MediaMesAno1;
                        volAno2 = p.MediaMesAno2;
                        volAno3 = p.MediaMesAno3;

                        //Se for lubrificante, transforma volume de litros para m3
                        if (p.CodProduto == 12)
                        {
                            volAno1 = p.MediaMesAno1 / 1000;
                            volAno2 = p.MediaMesAno2 / 1000;
                            volAno3 = p.MediaMesAno3 / 1000;
                        }

                        volComp = 0.0;

                        //----- Propagação de Volume
                        if (volAno2 == 0.0 && volAno1 != 0.0 && tir.IsIncremental == false)
                        {
                            volAno2 = volAno1;
                        }

                        if (volAno3 == 0.0 && volAno2 != 0.0)
                        {
                            volAno3 = volAno2;
                        }

                        //Calcula o volume do Produto a ser compensado após o fim do contrato
                        for (int i = 0; i < prazoTotal; i++)
                        {

                            if (tir.PeriodoMatura < i)
                            {
                                Volume = volAno1;
                                if (volAno2 > 0.0 && (i - tir.PeriodoMatura) > 12) Volume = volAno2;
                                if (volAno3 > 0.0 && (i - tir.PeriodoMatura) > 24) Volume = volAno3;

                                for (int j = 0; j < tir.MesMaturaVenda.Length; j++)
                                {
                                    if (tir.MesMaturaVenda[j] == 0)
                                    {
                                        break;
                                    }

                                    if ((i - tir.PeriodoMatura) <= tir.MesMaturaVenda[j])
                                    {
                                        volComp += Volume * (100 - tir.PercMaturaVenda[j]) / 100;
                                        break;
                                    }
                                }
                            }
                        }

                        //Calcula o número de meses onde haverá compensação de volume
                        Volume = volAno1;
                        if (prazoTotal > 12) Volume = volAno2;
                        if (prazoTotal > 24) Volume = volAno3;
                        mesesComp = Convert.ToInt16(Math.Truncate(volComp / Volume));
                        //Se houver fração de meses, incluir um mês a mais
                        if (mesesComp * Volume != volComp) mesesComp++;

                        //Se o total de meses com compensação desse Produto for maior
                        //que os da Negociação, atualizar
                        if (mesesComp > tir.MesesCompVolume) tir.MesesCompVolume = mesesComp;

                        //Preenche os meses de compensação de volume
                        p.volMensalComp = new double[mesesComp];
                        //Se for apenas um mês, usa todo o volume
                        if (mesesComp == 1)
                        {
                            p.volMensalComp[0] = volComp;
                        }
                        //Se for mais de um mês
                        else
                        {
                            //Loop de meses de compensação  
                            for (int i = 0; i < mesesComp; i++)
                            {
                                //Se o volume a compensar for maior que o volume total mensal
                                if (volComp >= Volume)
                                {
                                    //Valor do mês é o total mensal
                                    p.volMensalComp[i] = Volume;
                                    //Decrementa volume a compensar
                                    volComp -= Volume;
                                }
                                //Volume a compensar é menor que o total mensal
                                else
                                {
                                    //Valor do mês é o volume a compensar                           
                                    p.volMensalComp[i] = volComp;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            //Alteração para testar fluxo sem compensação
            //tir.MesesCompVolume = 0;
        }

        #endregion
    }
}
