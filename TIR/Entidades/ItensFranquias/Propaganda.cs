﻿using TIR.Entidades.Encaixes;

namespace TIR.Entidades.ItensFranquias
{
    /// <summary>
    /// Item de franquia que representa propaganda.
    /// </summary>
    public class Propaganda : Encaixe
    {
    }
}
