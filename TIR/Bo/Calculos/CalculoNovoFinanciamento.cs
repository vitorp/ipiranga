﻿using System;
using tir.dao;
using tir.dao.Entidades;
using TIR.Constantes;
using TIR.Entidades;
using TIR.Entidades.Componentes;
using TIR.Entidades.Encaixes;
using Microsoft.VisualBasic;

namespace TIR.Bo.Calculos
{
    public class CalculoNovoFinanciamento : FechamentoAno, ICalculo
    {
        #region Propriedades
        public Franquia Franquia { get; set; }
        #endregion

        public double[] Calcular(Encaixe enc, TIRNegociacao tir)
        {
            try
            {
                //converte encaixe para financiamento
                Financiamento fin = (Financiamento)enc;

                int contratoFranquia = 0;

                //verifica se passou o objeto franquia
                if (Franquia != null)
                {
                    //se passou busca o tempo de contrato da franquia
                    contratoFranquia = Franquia.TempoContrato;

                    bool isFranquiaRenovacao = false;
                    //verificar pelo tipo se é renovação
                    if (Franquia.TpNegFranquia.Equals(2) || Franquia.TpNegFranquia.Equals(1))
                    {
                        isFranquiaRenovacao = true;
                    }
                }
                else
                {
                    //não passou o objeto franquia, logo não tem tempo de contrato
                    contratoFranquia = 0;
                }

                //---- Acessar a alíquota de IOF - criado em 24/02/99
                //---- Ver na classe de gravação do Oracle, quando gravar
                //---- o financiamento, somar o valor de IOF ao valor de financ.

                float aliqIof = 0;

                //instancia dao e busca alíquota de IOF
                AliqIOFDao aliqDAO = new AliqIOFDao();
                AliqIOF iof = aliqDAO.ObterPorMes(fin.Amortizacao);

                //se não encontrou alíquota ou se o encaixe é "46) Fundo de Comércio - Financiado"
                if (iof == null || fin.CodEcxDcx.Equals(TipoCodEcxDcx.FUNDO_DE_COMERCIO_FINANCIADO))
                {
                    //não utilizar IOF
                    aliqIof = 0;
                }
                else
                {
                    //verifica se é pessoa física ou jurídica
                    if (fin.IndPessoaFisica.Equals(true))
                    {
                        //define alíquota de IOF para pessoa física
                        aliqIof = (float)iof.PercentualIOFPessoaFisica;
                    }
                    else
                    {
                        //define alíquota de IOF para pessoa jurídica
                        aliqIof = (float)iof.PercentualIOFPessoaJuridica;
                    }
                }

                //----- Todos os calculos são em Reais (moeda corrente)
                //----- Somente ao final os valores serão convertidos para FAO.

                double Fao = tir.ValorFAO;
                int prazoTotal = 0;

                //-- 31) Migração Franquias maio/2004
                if (contratoFranquia.Equals(0) || tir.Categoria.Equals(41))
                {
                    //define o prazo total para migração de franquias
                    //prazoTotal = tir.ContrAdicional + tir.ContrExistente + 1;
                    //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
                    prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 1;
                }
                else
                {
                    //define o prazo total
                    prazoTotal = contratoFranquia + 1;
                }

                //---- em junho/2004 a Assessoria de Planejamento de MK
                //---- através de Paula, Andrea e Sivirino, decidiram
                //---- que o financiamento para Franquias deveria começar
                //---- sempre do início do contr. existente se houver.

                // TODO: Analisar aqui com o Felipe, pois o prazo total é alterado!!!

                //redefine prazo total
                //prazoTotal = tir.ContrAdicional + tir.ContrExistente + 1;
                //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
                prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 1;

                //redimensiona vetores
                //fin.Fluxo = new double[prazoTotal + 1];
                //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
                fin.Fluxo = new double[prazoTotal + tir.MesesCompVolume + 1];

                //se não tem valor de financiamento no encaixe
                if (fin.ValorFinanc <= 0.0)
                {
                    //sai do método
                    return fin.Fluxo;
                }

                //salva valor
                double valorOriginal = fin.ValorFinanc;

                //verifica se há amortização
                if (fin.Amortizacao <= 0)
                {
                    throw new Exception("CalculoFinanciamento.calcular: Erro ao calcular financiamento. É preciso informar o número de meses para amortização.");
                }

                //Define limites dos Períodos de Liberação, Carência e Amortização
                int iniLiber = -1;
                int fimLiber = 0;
                int fimCarencia = 0;
                int iniAmort = 0;
                int fimAmort = 0;

                float sngWork = 0;

                for (int i = 0; i <= fin.PercLiberacao.GetUpperBound(0); i++)
                {
                    sngWork += fin.PercLiberacao[i];
                    if (fin.PercLiberacao[i] > 0.0)
                    {
                        if (iniLiber == -1)
                        {
                            iniLiber = i;   //início da Liberação
                        }

                        fimLiber = i;    //fim da Liberação
                    }
                }

                //só permite 100% de percentual total de liberação
                if (!sngWork.Equals(100))
                {
                    throw new Exception("CalculoFinanciamento.calcular: Erro ao calcular financiamento. O percentual total de liberação de financiamento tem que ser de 100%.");
                }

                //verifica se os prazos estão coerentes
                if (fin.Amortizacao + fin.Carencia + fimLiber > prazoTotal)
                {
                    throw new Exception("CalculoFinanciamento.calcular: Erro ao calcular financiamento. A soma dos prazos de amortização, carência e liberação da última parcela não pode ultrapassar a soma dos contratos adicionais e existentes.");
                }

                //O fim da Carência é o mês de saída da última
                //parcela mais o período de Carência
                fimCarencia = fimLiber + fin.Carencia;

                //A Amortização vai do fim do período de Carência
                //até (fim da Carência + tempo de Amortização)
                iniAmort = fimCarencia + 1;
                fimAmort = fimCarencia + fin.Amortizacao;

                //--- obter alíquota de IR e PIS/COFINS
                OutrosParamDao outrosDAO = new OutrosParamDao();
                OutrosParam ir = outrosDAO.ObterParametro("PercIR");
                //Alterado em nov/2015 por Luiz Felipe, para pegar a
                //alíquota de Pis/Cofins exclusiva dos Financiamentos
                //OutrosParam pis = outrosDAO.ObterParametro("ReceitaPIS");
                OutrosParam pis = outrosDAO.ObterParametro("FinancPIS");
                float aliqIr = (float)ir.ValorParametroNumerico;
                double aliqPisCof = (double)pis.ValorParametroNumerico;

                //***Valores para o Fluxo
                double[] juros = new double[prazoTotal + 1];
                double[] corrMon = new double[prazoTotal + 1];

                double[] jurosMaisCM = new double[prazoTotal + 1];
                double[] amortCM = new double[prazoTotal + 1];

                //Liberação/Carência
                double[] prestLiber = new double[prazoTotal + 1];
                double[] amortLiber = new double[prazoTotal + 1];

                //Financiamento
                double[] prestFin = new double[prazoTotal + 1];
                double[] amortFin = new double[prazoTotal + 1];
                //***

                //Tributos
                double despesasIOF = 0;
                double pisCofins = 0;
                double irCSLL = 0;

                //EBIT e NOPAT
                double ebit = 0;
                double nopat = 0;

                //Atribui juros e CM mensais, de acordo com o período
                for (int i = 0; i <= prazoTotal; i++)
                {
                    //Juros e CM do Financiamento
                    juros[i] = fin.PercJuros / 100;
                    corrMon[i] = fin.PercCorMonet / 100;

                    //Se existe Carência e está dentro dela,
                    //usa e CM juros da Carência
                    if (fin.Carencia > 0 && i <= fimCarencia)
                    {
                        juros[i] = fin.PercJurosCarencia / 100;
                        corrMon[i] = fin.PercCorMonCarencia / 100;
                    }
                }

                //*** Cálculo do Fluxo na Liberação/Carência
                double valorAtual = 0;
                double saldoLiber = 0;

                double jur = 0;
                double car = 0;

                //Calcula para o período de Liberação/Carência
                for (int i = iniLiber; i <= fimCarencia; i++)
                {
                    //Para cada parcela da Liberação
                    if (i <= fimLiber && fin.PercLiberacao[i] > 0.0)
                    {
                        valorAtual = fin.ValorFinanc * fin.PercLiberacao[i] / 100;

                        //Loop de meses de Carência; ainda que não haja
                        //Carência, o final dela será o mesmo da Liberação
                        for (int j = i + 1; j <= fimCarencia; j++)
                        {
                            jur = valorAtual * juros[i];
                            car = valorAtual * (tir.Inflacao[j] / tir.Inflacao[j - 1] - 1) * corrMon[i];
                            valorAtual += jur + car;
                            jurosMaisCM[j] += jur + car;
                        }
                        saldoLiber += valorAtual;
                    }
                }

                //Monta o fluxo para o período de Liberação/Carência
                for (int i = iniLiber; i <= fimCarencia; i++)
                {
                    //Se houver parcela de Liberação no mês
                    if (i <= fimLiber && fin.PercLiberacao[i] > 0.0)
                    {
                        valorAtual = fin.ValorFinanc * fin.PercLiberacao[i] / 100;
                        despesasIOF = valorAtual * aliqIof / 100;
                    }
                    else
                    {
                        valorAtual = 0;
                        despesasIOF = 0;
                    }

                    pisCofins = jurosMaisCM[i] * aliqPisCof / 100;
                    ebit = jurosMaisCM[i] - (despesasIOF + pisCofins);
                    irCSLL = ebit * aliqIr / 100;
                    nopat = ebit - irCSLL;
                    fin.Fluxo[i] = nopat - valorAtual - jurosMaisCM[i];
                }
                //***

                double valFin = new double();
                int amorFin = new int();
                int mesesAmort = new int();
                int mesesAposCar = new int();
                double amortMes = new int();
                double saldoInicial = new int();
                bool isAmortCM = true;
                int ano = 0;
                double somaAmortCM = 0;
                int inicioSomaCM = 0;
                double saldoFinal = new double();
    
                double inflacao = 0;
                double mes = 0;

                //===============Liberação/Carência após a Carência (com e sem Amort CM)===============
                //***Cálculo do saldo de Liberação/Carência após a Carência
                //Se houve repactuação
                if (fin.JurosCarRepact == true)
                {
                    //Calcula com e sem Amort CM (começa com)
                    for (int k = 0; k < 2; k++)
                    {
                        valFin = 0;
                        amorFin = fin.Amortizacao;
                        mesesAposCar = 0;
                        mesesAmort = 0;
                        amortMes = 0;

                        saldoInicial = 0;
                        //Remove o valor do Financiamento do saldo, para calcular
                        //apenas o que sobrar após Liberação/Carência
                        saldoFinal = saldoLiber - fin.ValorFinanc;
                        if (saldoFinal == 0) break;

                        //Calcula para a Amortização
                        for (int i = iniAmort; i <= fimAmort; i++)
                        {
                            saldoInicial = saldoFinal;

                            //Obtem a inflação acumulada para o ano
                            ano = (i + 1) / 12 + 1;
                            if (ano > 5) ano = 5;
                            InflacaoDao inflaDAO = new InflacaoDao();
                            Inflacao inflaAno = new Inflacao();
                            inflaAno = inflaDAO.ObterPorAno(ano);
                            inflacao = inflaAno.PercentualInflacaoAnual / 100;
                            mes = Math.Pow((1.0 + inflacao), (1.0 / 12.0)) - 1;

                            //Se for o começo do cálculo
                            if (i == iniAmort)
                            {
                                mesesAmort = 0;
                                valFin = saldoInicial;
                            }
                            else
                            {
                                somaAmortCM = 0;
                                //Se for o fechamento de um ano
                                //if ((mesesAposCar) % 12 == 0)
                                if ((i - fimLiber - 1) % 12 == 0)
                                {
                                    amorFin -= mesesAmort;
                                    mesesAmort = 0;
                                    //Se estiver calculando com Amort CM
                                    if (isAmortCM == true)
                                    {
                                        if (i - 12 < iniAmort)
                                        {
                                            inicioSomaCM = iniAmort;
                                        }
                                        else
                                        {
                                            inicioSomaCM = i - 12;
                                        }

                                        for (int j = inicioSomaCM; j < i; j++)
                                        {
                                            somaAmortCM += amortCM[j];
                                        }
                                        saldoInicial = saldoFinal + somaAmortCM;
                                    }
                                    valFin = saldoInicial;
                                }
                            }

                            //---cálculo da prestação
                            prestacao = -Financial.Pmt(juros[i], amorFin, valFin, 0.0, 0);

                            jur = saldoInicial * juros[i];
                            amortMes = prestacao - jur;
                            saldoFinal = saldoInicial - amortMes;
                            
                            //Se for com CM, calcula
                            if (isAmortCM)
                            {
                                //if ((mesesAposCar) % 12 == 0)
                                if ((i - fimLiber - 1) % 12 == 0)
                                {
                                    amortCM[i] = (saldoInicial - somaAmortCM - amortMes) * mes * corrMon[i];
                                }
                                else
                                {
                                    amortCM[i] = saldoFinal * mes * corrMon[i];
                                }
                            }
                            //Sem CM; zera
                            else
                            {
                                amortCM[i] = 0;
                            }

                            mesesAmort++;

                            //Salva valores para o fluxo
                            if (isAmortCM)
                            {
                                prestLiber[i] = prestacao;  //Se for com CM, salva a prestação
                            }
                            else
                            {
                                amortLiber[i] = amortMes;   //Se for sem CM, salva o valor amortizado
                            }

                            //fin.Fluxo[i] = amortLiber[i];
                            mesesAposCar++;
                        }

                        //Prepara para calcular sem Amort CM
                        isAmortCM = false;
                    }
                }
                //Não repactuou; pagamento do saldo no último mês da Carência
                else
                {
                    fin.Fluxo[fimCarencia] += saldoLiber - fin.ValorFinanc;
                }
                //***
                //=====================================================================================

                //=================Financiamento após a Carência (com e sem Amort CM)=================
                //***Cálculo do Financiamento após a Carência
                //Calcula com e sem Amort CM (começa com)
                isAmortCM = true;
                for (int k = 0; k < 2; k++)
                {
                    valFin = 0;
                    amorFin = fin.Amortizacao;
                    mesesAposCar = 0;
                    mesesAmort = 0;
                    amortMes = 0;

                    saldoInicial = 0;
                    saldoFinal = fin.ValorFinanc;

                    somaAmortCM = 0;
                    inicioSomaCM = 0;

                    //Calcula para a Amortização
                    for (int i = iniAmort; i <= fimAmort; i++)
                    {
                        saldoInicial = saldoFinal;

                        //Obtem a inflação acumulada para o ano
                        ano = (i + 1) / 12 + 1;
                        if (ano > 5) ano = 5;
                        InflacaoDao inflaDAO = new InflacaoDao();
                        Inflacao inflaAno = new Inflacao();
                        inflaAno = inflaDAO.ObterPorAno(ano);
                        inflacao = inflaAno.PercentualInflacaoAnual / 100;
                        mes = Math.Pow((1.0 + inflacao), (1.0 / 12.0)) - 1;

                        //Se for o começo do cálculo
                        if (i == iniAmort)
                        {
                            mesesAmort = 0;
                            valFin = saldoInicial;
                        }
                        else
                        {
                            somaAmortCM = 0;
                            //Se for o fechamento de um ano
                            //if ((mesesAposCar) % 12 == 0)
                            if ((i - fimLiber - 1) % 12 == 0)
                            {
                                amorFin -= mesesAmort;
                                mesesAmort = 0;
                                //Se estiver calculando com Amort CM
                                if (isAmortCM == true)
                                {
                                    if (i - 12 < iniAmort)
                                    {
                                        inicioSomaCM = iniAmort;
                                    }
                                    else
                                    {
                                        inicioSomaCM = i - 12;
                                    }

                                    for (int j = inicioSomaCM; j < i; j++)
                                    {
                                        somaAmortCM += amortCM[j];
                                    }
                                    //saldoInicial = saldoFinal + amortMes - somaAmortCM;
                                    saldoInicial = saldoFinal + somaAmortCM;
                                }
                                valFin = saldoInicial;
                            }
                        }

                        //---cálculo da prestação
                        prestacao = -Financial.Pmt(juros[i], amorFin, valFin, 0.0, 0);

                        jur = saldoInicial * juros[i];
                        amortMes = prestacao - jur;
                        saldoFinal = saldoInicial - amortMes;

                        //Se for com CM, calcula
                        if (isAmortCM)
                        {
                            amortCM[i] = (saldoInicial - somaAmortCM - amortMes) * mes * corrMon[i];
                        }
                        //Sem CM; zera
                        else
                        {
                            amortCM[i] = 0;
                        }

                        mesesAmort++;

                        //Salva valores para o fluxo
                        if (isAmortCM)
                        {
                            prestFin[i] = prestacao;  //Se for com CM, salva a prestação
                        }
                        else
                        {
                            amortFin[i] = amortMes;   //Se for sem CM, salva o valor amortizado
                        }

                        //fin.Fluxo[i] = amortFin[i];
                        mesesAposCar++;
                    }

                    //Prepara para calcular sem Amort CM
                    isAmortCM = false;
                }
                //***
                //====================================================================================
                
                //***Monta o fluxo
                for (int i = iniAmort; i <= fimAmort; i++)
                {
                    //Calcula os valores para o EBIT
                    dblWork = prestFin[i] + prestLiber[i] - amortFin[i];
                    pisCofins = dblWork * aliqPisCof / 100;
                    ebit = dblWork - amortLiber[i] - pisCofins;
                    irCSLL = ebit * aliqIr / 100;

                    //Calcula NOPAT
                    nopat = ebit - irCSLL;

                    //Calcula os valores para o fluxo
                    fin.Fluxo[i] = nopat + amortFin[i] + amortLiber[i];
                }

                //Transforma o fluxo em FAO
                for (int i = 0; i <= prazoTotal; i++)
                {
                    fin.Fluxo[i] = fin.Fluxo[i] / tir.ValorFAO / tir.Inflacao[i];
                }

                //retorno com sucesso
                return fin.Fluxo;
            }
            catch (Exception ex)
            {
                throw new Exception("CalculoFinanciamento.calcular: Erro ao calcular o fluxo para financiamento.\n" + ex.Message, ex);
            }
        }
    }
}
