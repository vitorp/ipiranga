﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WebService.TIR.Models.Encaixes
{

    [DataContract]
    public class BonificacaoPostecipadaTir
    {
        [DataMember]
        public int MesConcessao { get; set; }

        [DataMember]
        public double VolumeInicial { get; set; }

        [DataMember]
        public double VolumeParcial { get; set; }

        [DataMember]
        public int QuantidadeParcelas { get; set; }

        [DataMember]
        public double Valor { get; set; }
    }
}