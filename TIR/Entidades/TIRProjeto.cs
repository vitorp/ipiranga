﻿using System.Collections.Generic;

namespace TIR.Entidades
{
    public class TIRProjeto : TIRResultado
    {
        public List<TIRNegociacao> TIRNegociacoes { get; set; }
    }
}
