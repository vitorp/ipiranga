﻿using System;
using System.Collections.Generic;
using System.Data;
//using System.Diagnostics.Contracts;
using System.Text;
using daobase;
using tir.dao.Entidades;

namespace tir.dao
{
    public class CustoTercDao : DaoBase<CustoTerc>
    {
        #region Métodos Públicos
        /// <summary>
        /// Lista os custos de Terceirização para um Componente.
        /// </summary>
        /// <param name="codTipo">Tipo de componente.</param>
        /// <param name="codPerfil">Perfil de componente.</param>
        /// <param name="codPadrao">Padrão de componente.</param>
        /// <returns>Lista de objetos CustoTerc.</returns>
        public List<CustoTerc> ListarPorComponente(long codTipo, long codPerfil, long codPadrao)
        {
            #region Contracts
            //Contract.Requires(codPadrao > 0, Mensagens.CustoTerc.VAL_CODIGO_PADRAO);
            //Contract.Requires(codPerfil > 0, Mensagens.CustoTerc.VAL_CODIGO_PERFIL);
            //Contract.Requires(codTipo > 0, Mensagens.CustoTerc.VAL_CODIGO_TIPO);
            #endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select c.cd_tipo_com, ");
                sb.Append("c.cd_padrao_com, ");
                sb.Append("c.cd_perfil_com, ");
                sb.Append("c.no_mes_faixa, ");
                sb.Append("c.vl_custo_terc ");
                sb.Append("from CustoTerc c ");
                sb.AppendFormat("where c.cd_padrao_com = {0} ", DaoUtil.FormatarParametro("codPadrao"));
                sb.AppendFormat("and c.cd_perfil_com = {0} ", DaoUtil.FormatarParametro("codPerfil"));
                sb.AppendFormat("and c.cd_tipo_com = {0} ", DaoUtil.FormatarParametro("codTipo"));
                sb.Append("order by c.no_mes_faixa");

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codPadrao"), codPadrao, typeof(Int16)));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codPerfil"), codPerfil, typeof(Int16)));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codTipo"), codTipo, typeof(Int16)));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //valor de retorno
                return MontarLista(dr);
  
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("CustoTercDao.ListarPorComponente",
                //"Erro ao listar custo de Terceirização para um Componente.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }
        #endregion

        #region Overrides

        protected override CustoTerc MontarObjeto(IDataReader dr)
        {
            CustoTerc ent = new CustoTerc();
            try
            {
                ent.CodigoTipoComponente = (Int32)dr["cd_tipo_com"];
                ent.CodigoPadraoComponente = (Int32)dr["cd_padrao_com"];
                ent.CodigoPerfilComponente = (Int32)dr["cd_perfil_com"];
                ent.NumeroMesFaixa = (Int16)dr["no_mes_faixa"];
                ent.ValorCustoTerceirizacao = (double)dr["vl_custo_terc"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
