﻿using System;
using System.Collections.Generic;
using System.Data;
//using System.Diagnostics.Contracts;
using System.Text;
using daobase;
using tir.dao.Entidades;

namespace tir.dao
{
    public class RoyalCbpiDao : DaoBase<RoyalCbpi>
    {
        #region Métodos Públicos
        /// <summary>
        /// Lista royalties CBPI por Componente.
        /// </summary>
        /// <param name="codPadrao">Padrão de componente.</param>
        /// <param name="codPerfil">Perfil de componente.</param>
        /// <param name="codTipo">Tipo de componente.</param>
        /// <returns>Lista de objetos RoyalCbpi.</returns>
        public List<RoyalCbpi> ListarPorComponente(long codPadrao, long codPerfil, long codTipo)
        {
            #region Contracts
            //Contract.Requires(codPadrao > 0, Mensagens.RoyalCbpi.VAL_CODIGO_PADRAO);
            //Contract.Requires(codPerfil > 0, Mensagens.RoyalCbpi.VAL_CODIGO_PERFIL);
            //Contract.Requires(codTipo > 0, Mensagens.RoyalCbpi.VAL_CODIGO_TIPO);
            #endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select r.cd_tipo_com, ");
                sb.Append("r.cd_padrao_com, ");
                sb.Append("r.cd_perfil_com, ");
                sb.Append("r.no_mes_faixa, ");
                sb.Append("r.vl_roy_cbpi, ");
                sb.Append("r.pc_roy_cbpi ");
                sb.Append("from RoyaltiesCbpi r ");
                sb.AppendFormat("where r.cd_padrao_com = {0} ", DaoUtil.FormatarParametro("codPadrao"));
                sb.AppendFormat("and r.cd_perfil_com = {0} ", DaoUtil.FormatarParametro("codPerfil"));
                sb.AppendFormat("and r.cd_tipo_com = {0} ", DaoUtil.FormatarParametro("codTipo"));
                sb.Append("order by r.cd_padrao_com, r.cd_perfil_com, r.cd_tipo_com");

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codPadrao"), codPadrao, typeof(Int16)));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codPerfil"), codPerfil, typeof(Int16)));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codTipo"), codTipo, typeof(Int16)));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //valor de retorno
                return MontarLista(dr);
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("RoyalCbpiDao.ListarPorComponente",
                //"Erro ao listar royalties CBPI para um Componente.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }
        #endregion

        #region Overrides

        protected override RoyalCbpi MontarObjeto(IDataReader dr)
        {
            RoyalCbpi ent = new RoyalCbpi();
            try
            {
                ent.CodigoTipoComponente = (Int32)dr["cd_tipo_com"];
                ent.CodigoPadraoComponente = (Int32)dr["cd_padrao_com"];
                ent.CodigoPerfilComponente = (Int32)dr["cd_perfil_com"];
                ent.NumeroMesFaixaRoyalties = (Int16)dr["no_mes_faixa"];
                ent.ValorRoyaltiesCbpi = (double)dr["vl_roy_cbpi"];
                ent.PercentualRoyaltiesCbpi = (Single)dr["pc_roy_cbpi"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
