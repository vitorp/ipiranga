﻿
namespace tir.dao.Entidades
{
    public class Unidade
    {
        #region Propriedades
        public string CodigoUnidade { get; set; }
        public string NomeUnidade { get; set; }
        public decimal CodigoRegiao { get; set; }
        public string SiglaUF { get; set; }
        public double TaxaMov { get; set; } //todo: ver com Felipe
        #endregion
    }
}
