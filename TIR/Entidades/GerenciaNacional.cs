﻿
namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que represena a Gerência Nacional
    /// </summary>
    public class GerenciaNacional
    {
        #region Propriedades

        public string Codigo { get; set; }
        public string Nome { get; set; }
        public string Sigla { get; set; }
        public decimal Segmento { get; set; }

        #endregion
    }
}
