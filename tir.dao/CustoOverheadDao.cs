﻿using System;
using System.Data;
//using System.Diagnostics.Contracts;
using System.Text;
using daobase;
using tir.dao.Entidades;
using System.Collections.Generic;

namespace tir.dao
{
    public class CustoOverheadDao : DaoBase<CustoOverhead>
    {
        #region Métodos Públicos
        /// <summary>
        /// Obtem custo de Overhead.
        /// </summary>
        /// <param name="codGV">Código da unidade organizacional.</param>
        /// <param name="siglaUF">Sigla da UF a que pertence o Cliente.</param>
        /// <param name="codPadrao">Padrão da franquia.</param>
        /// <param name="codPerfil">Perfil da franquia.</param>
        /// <param name="codTipo">Tipo da franquia.</param>
        /// <returns>Lista de objetos CustoOverhead.</returns>
        public List<CustoOverhead> ObterCustoOverhead(string codGV, string siglaUF, long codPadrao, long codPerfil, long codTipo)
        {
            #region Contracts
            //Contract.Requires(codGV != null, Mensagens.CustoOverhead.VAL_CODIGO_GV);
            //Contract.Requires(codPadrao > 0, Mensagens.CustoOverhead.VAL_CODIGO_PADRAO);
            //Contract.Requires(codPerfil > 0, Mensagens.CustoOverhead.VAL_CODIGO_PERFIL);
            //Contract.Requires(codTipo > 0, Mensagens.CustoOverhead.VAL_CODIGO_TIPO);
            #endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select c.cd_tipo_com, ");
                sb.Append("c.cd_padrao_com, ");
                sb.Append("c.cd_perfil_com, ");
                sb.Append("c.cd_gv, ");
                sb.Append("c.faixa_ate_mes, ");
                sb.Append("c.Sg_UF, ");
                sb.Append("c.vl_overhead ");
                sb.Append("from CustoOverhead_UF c ");
                sb.AppendFormat("where c.cd_gv = {0} ", DaoUtil.FormatarParametro("codGV"));
                sb.AppendFormat("and c.Sg_UF = {0} ", DaoUtil.FormatarParametro("siglaUF"));
                sb.AppendFormat("and c.cd_padrao_com = {0} ", DaoUtil.FormatarParametro("codPadrao"));
                sb.AppendFormat("and c.cd_perfil_com = {0} ", DaoUtil.FormatarParametro("codPerfil"));
                sb.AppendFormat("and c.cd_tipo_com = {0} ", DaoUtil.FormatarParametro("codTipo"));
                sb.Append("order by c.faixa_ate_mes");

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codGV"), codGV, typeof(String), 6));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("siglaUF"), siglaUF, typeof(String), 2));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codPadrao"), codPadrao, typeof(Int16)));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codPerfil"), codPerfil, typeof(Int16)));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codTipo"), codTipo, typeof(Int16)));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //valor de retorno - faixas de Overhead
                return MontarLista(dr);
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("CustoOverheadDao.ObterCustoOverhead",
                //"Erro ao buscar custo de Overhead para uma Franquia.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }
        #endregion

        #region Overrides
        protected override CustoOverhead MontarObjeto(IDataReader dr)
        {
            CustoOverhead ent = new CustoOverhead();
            try
            {
                ent.CodigoTipoComponente = (Int32)dr["cd_tipo_com"];
                ent.CodigoPadraoComponente = (Int32)dr["cd_padrao_com"];
                ent.CodigoPerfilComponente = (Int32)dr["cd_perfil_com"];
                ent.CodigoGV = (string)dr["cd_gv"];
                ent.SiglaUF = (string)dr["Sg_UF"];
                ent.FaixaAteMes = (Int32)dr["faixa_ate_mes"];
                ent.ValorCustoOverhead = (double)dr["vl_overhead"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
