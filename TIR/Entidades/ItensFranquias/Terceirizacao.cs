﻿using TIR.Entidades.Encaixes;

namespace TIR.Entidades.ItensFranquias
{
    /// <summary>
    /// Item de franquia que representa terceirização.
    /// </summary>
    public class Terceirizacao : Encaixe
    {
    }
}
