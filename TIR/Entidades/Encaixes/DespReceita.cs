﻿
namespace TIR.Entidades.Encaixes
{
    /// <summary>
    /// Encaixe que representa uma despesa ou receita.
    /// </summary>
    public class DespReceita : Encaixe
    {
        #region Propriedades

        public int MesInicial { get; set; }
        public int MesFinal { get; set; }

        #endregion
    }
}
