﻿using System;
using TIR.Entidades;
using TIR.Entidades.Componentes;
using TIR.Entidades.Encaixes;
using TIR.Entidades.ItensFranquias;
using tir.dao;
using tir.dao.Entidades;

namespace TIR.Bo.Calculos
{
    public class CalculoCapGiroFranquia : ICalculo
    {
        #region Propriedades
        public Franquia Franquia { get; set; }
        #endregion

        public double[] Calcular(Encaixe enc, TIRNegociacao tir)
        {
            try
            {
                //converte encaixe
                CapitalGiroFranquia capGiroFranquia = (CapitalGiroFranquia)enc;

                capGiroFranquia.Descricao = "Capital de Giro Franquia";

                int prazoTotal = 0;

                //prazoTotal = Franquia.TempoContrato + tir.PeriodoMatura + 1;
                //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
                prazoTotal = Franquia.TempoContrato + tir.PeriodoMatura + tir.MesesCompVolume + 1;
                //capGiroFranquia.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 2];
                //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
                capGiroFranquia.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 2];

                //busca prazo médio de recebimento de Royalties
                ParamGeraisDao paramDAO = new ParamGeraisDao();
                ParamGeral param = new ParamGeral();

                //obtém parâmetro de acordo com o tipo de Franquia
                switch (Franquia.CodTipoComp)
                {
                    case 3:     //AmPm
                        param = paramDAO.ObterParametro(7);
                        break;

                    case 4:     //JetOil Motos
                        param = paramDAO.ObterParametro(9);
                        break;

                    case 5:     //Jet Oil
                        param = paramDAO.ObterParametro(8);
                        break;
                }

                Franquia.PrazoMedioRecebRoyalties = Convert.ToInt32(param.ValorNumerico);

                bool isFranquiaRenovacao = false;
                //--- Alteração asseguramento de franquias - Agosto/2002
                if (Franquia.TpNegFranquia.Equals(2) || Franquia.TpNegFranquia.Equals(1))
                {
                    isFranquiaRenovacao = true;
                }
                int mesInicio = 0;

                //--- Gerar o fluxo de RoyaltyVar - migração franquias maio/2004
                if (isFranquiaRenovacao.Equals(true) && !tir.Categoria.Equals(41))
                {
                    //só calcula se tiver prazo proposto
                    if (tir.ContrAdicional > 0)
                    {
                        mesInicio = tir.ContrExistente + 1;
                    }
                    else
                    {
                        //não precisa calcular
                        return capGiroFranquia.Fluxo;
                    }
                }
                else
                {
                    mesInicio = 1;
                }

                //Cria contas a receber
                //double[] contaReceb = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 2];
                //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
                double[] contaReceb = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 2];

                //Cria Royalty diário
                //double[] royaltyDiario = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 2];
                //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
                double[] royaltyDiario = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 2];

                //Cria e inicializa Royalty mensal
                //double[] royaltyMensal = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 2];
                //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
                double[] royaltyMensal = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 2];

                //Obtém valores do fluxo de Royalty variável em reais
                for (int i = 0; i < tir.FluxoAuxiliar.Length; i++)
                {
                    royaltyMensal[i] = tir.FluxoAuxiliar[i];
                }

                //Cria e inicializa Capital de Giro Nominal
                double capGiroNominal = 0;

                //Cria e inicializa Capital de Giro Constante
                double capGiroConstante = 0;

                //Para o retorno
                double valorTotal = 0;

                //Monta o fluxo de Capital de Giro Franquia
                for (int i = mesInicio + tir.PeriodoMatura; i <= (prazoTotal - 1 + (mesInicio - 1)); i++)
                {
                    royaltyDiario[i] = royaltyMensal[i] / 30;

                    if (Franquia.PrazoMedioRecebRoyalties > 30)
                    {
                        if (i > mesInicio + tir.PeriodoMatura)
                        {
                            contaReceb[i] = ((royaltyDiario[i - 1] * (Franquia.PrazoMedioRecebRoyalties - 30)) + royaltyMensal[i]) * -1;
                        }
                        else
                        {
                            contaReceb[i] = royaltyMensal[i] * -1;
                        }
                    }
                    else
                    {
                        contaReceb[i] = royaltyDiario[i] * royaltyMensal[i];
                    }

                    //Calcula Capital de Giro Nominal
                    if (i > mesInicio + tir.PeriodoMatura)
                    {
                        capGiroNominal = contaReceb[i] - contaReceb[i - 1];
                    }
                    else
                    {
                        capGiroNominal = contaReceb[i];
                    }

                    //Calcula Capital de Giro Constante
                    capGiroConstante = capGiroNominal / tir.Inflacao[i];

                    //Atualiza o fluxo do Capital de Giro Franquia
                    capGiroFranquia.Fluxo[i] = capGiroConstante / tir.ValorFAO; // / tir.Inflacao[i];

                    //Acumula para o valor de retorno
                    valorTotal += capGiroConstante / tir.ValorFAO * tir.Inflacao[i];
                }

                //Retorno
                //capGiroFranquia.Fluxo[prazoTotal - 1 + mesInicio] = valorTotal * (-1.0) / tir.Inflacao[prazoTotal];
                capGiroFranquia.Fluxo[prazoTotal - 1 + mesInicio] = valorTotal * (-1.0) / tir.Inflacao[prazoTotal - 1 + mesInicio];

                //retorno com sucesso
                return capGiroFranquia.Fluxo;
            }
            catch (Exception ex)
            {
                throw new Exception("CalculoCapGiroFranquia.calcular: Erro ao calcular Capital de Giro de Franquia.\n" + ex.Message, ex);
            }
        }
    }
}
