﻿
using System;
namespace tir.dao.Entidades
{
    public class Inflacao
    {
        #region Propriedades
        public Int16 NumeroAno { get; set; }
        public double PercentualInflacaoAnual { get; set; }
        public double InflacaoMedia { get; set; }
        #endregion
    }
}
