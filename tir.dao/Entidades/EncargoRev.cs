﻿
namespace tir.dao.Entidades
{
    public class EncargoRev
    {
        #region Propriedades
        public string SiglaUF { get; set; }
        public string CodigoProduto { get; set; }
        public decimal ValorMargem { get; set; }
        #endregion
    }
}
