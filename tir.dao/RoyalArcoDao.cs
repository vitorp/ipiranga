﻿using System;
using System.Collections.Generic;
using System.Data;
//using System.Diagnostics.Contracts;
using System.Text;
using daobase;
using tir.dao.Entidades;

namespace tir.dao
{
    public class RoyalArcoDao : DaoBase<RoyalArco>
    {
        #region Métodos Públicos
        /// <summary>
        /// Lista os Royalties ARCO de um Componente.
        /// </summary>
        /// <param name="codPadrao">Padrão de componente.</param>
        /// <param name="codPerfil">Perfil de componente.</param>
        /// <param name="codTipo">Tipo de componente.</param>
        /// <returns>Lista de objetos RoyalArco.</returns>
        public List<RoyalArco> ListarPorComponente(long codPadrao, long codPerfil, long codTipo)
        {
            #region Contracts
            //Contract.Requires(codPadrao > 0, Mensagens.RoyalArco.VAL_CODIGO_PADRAO);
            //Contract.Requires(codPerfil > 0, Mensagens.RoyalArco.VAL_CODIGO_PERFIL);
            //Contract.Requires(codTipo > 0, Mensagens.RoyalArco.VAL_CODIGO_TIPO);
            #endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select r.cd_tipo_com, ");
                sb.Append("r.cd_padrao_com, ");
                sb.Append("r.cd_perfil_com, ");
                sb.Append("r.no_mes_faixa, ");
                sb.Append("r.pc_roy_arco ");
                sb.Append("from RoyaltiesArco r ");
                sb.AppendFormat("where r.cd_padrao_com = {0} ", DaoUtil.FormatarParametro("codPadrao"));
                sb.AppendFormat("and r.cd_perfil_com = {0} ", DaoUtil.FormatarParametro("codPerfil"));
                sb.AppendFormat("and r.cd_tipo_com = {0} ", DaoUtil.FormatarParametro("codTipo"));
                sb.Append("order by r.no_mes_faixa");

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codPadrao"), codPadrao, typeof(Int16)));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codPerfil"), codPerfil, typeof(Int16)));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codTipo"), codTipo, typeof(Int16)));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //valor de retorno
                return MontarLista(dr);
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("RoyalArcoDao.ListarPorComponente",
                //"Erro ao listar royalties ARCO para um Componente.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }
        #endregion

        #region Overrides

        protected override RoyalArco MontarObjeto(IDataReader dr)
        {
            RoyalArco ent = new RoyalArco();
            try
            {
                ent.CodigoTipoComponente = (Int32)dr["cd_tipo_com"];
                ent.CodigoPadraoComponente = (Int32)dr["cd_padrao_com"];
                ent.CodigoPerfilComponente = (Int32)dr["cd_perfil_com"];
                ent.NumeroMesFaixaRoyalties = (Int16)dr["no_mes_faixa"];
                ent.PercentualRoyaltiesArco = (Single)dr["pc_roy_arco"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
