﻿
namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que representa aluguel de margem.
    /// </summary>
    public class AluguelMargem
    {
        #region Propriedades

        public double[] VrEncargo { get; set; }
        public string[] NomeProd { get; set; }
        public long[] CodProd { get; set; }
        public float PercAluguel { get; set; }
        public int Carencia { get; set; }
        public int Periodo { get; set; }
        public float PercVolume { get; set; }

        #endregion
    }
}
