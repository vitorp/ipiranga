﻿using TIR.Entidades.ItensFranquias;

namespace TIR.Entidades.Componentes
{
    /// <summary>
    /// Componente que representa uma franquia (AmPm, JetOil ou JetOil Motos).
    /// </summary>
    public class Franquia : Componente
    {
        #region Propriedades

        public double ValorPadraoTxFranquia { get; set; }
        public int TpNegFranquia { get; set; }
        public int QtElementosFaixa { get; set; }
        public float[] PcRoyaltyVariavel { get; set; }
        public int[] FaixaRoyaltyCBPI { get; set; }
        public double[] ValorRoyaltyFixo { get; set; }
        public bool IndParceladoTxFranquia { get; set; }
        public int NoParcelaTxFranquia { get; set; }
        public double VlTxFranquia { get; set; }
        public long CodPerfilComp { get; set; }
        public long CodPadraoComp { get; set; }
        public double ValorFaturamento { get; set; }
        public int TempoContrato { get; set; }
        public int[] TipoRenovacao { get; set; }    //Por Luiz Felipe - out/2014
        public int PrazoMedioRecebRoyalties { get; set; }   //Por Luiz Felipe - out/2014

        //itens de franquias
        public Propaganda Propaganda { get; set; }
        public Terceirizacao Terceirizacao { get; set; }
        public Overhead Overhead { get; set; }
        public RoyaltyVar RoyaltyVar { get; set; }
        public TaxaFranquia TaxaFranquia { get; set; }
        public CapitalGiroFranquia CapitalGiroFranquia { get; set; }    //Por Luiz Felipe - out/2014

        #endregion
    }
}
