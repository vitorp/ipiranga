﻿using System;

namespace tir.dao.Entidades
{
    public class TipoCategoria
    {
        #region Propriedades
        public decimal TpoCategoria { get; set; }
        public string NomeCategoria { get; set; }
        public decimal QuantidadeMaximaMeses { get; set; }
        public double VolumeTotalMinimo { get; set; }
        public Boolean IndicadorMaturacao { get; set; }
        public Boolean IndicadorDepreciacao25Anos { get; set; }
        #endregion
    }
}
