﻿using System;
using System.Data;
using tir.dao.Entidades;

namespace tir.dao
{
    public class PadraoComDao : DaoBase<PadraoCom>
    {
        #region Métodos Públicos

        #endregion

        #region Overrides

        protected override PadraoCom MontarObjeto(IDataReader dr)
        {
            PadraoCom ent = new PadraoCom();
            try
            {
                ent.CodigoTipoComponente = (int)dr["cd_tipo_com"];
                ent.CodigoPadraoComponente = (int)dr["cd_padrao_com"];
                ent.NomePadraoComponente = (string)dr["nm_padrao_com"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
