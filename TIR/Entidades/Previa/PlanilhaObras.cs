﻿
namespace TIR.Entidades.Previa
{
    /// <summary>
    /// Entidade que representa a planilha de obras.
    /// </summary>
    public class PlanilhaObras
    {
        #region Propriedades

//      jul/2011 - criados atributos para refletir os novos encaixes
//                 de Obras, Equipamentos e Elementos de Imagem.
//                 Alterações marcadas com "NP".
//                 Por Luiz Felipe Gomes Soutinho

        //Atributos
        //Obras Civis
        public double PlanValorObras { get; set; }
        public double PlanPercentPartOperPrevia { get; set; }
        public double PlanValorPartOperPrevia { get; set; }
        //Obras Civis - Posto (NP)
        public double PlanValObrasPosto { get; set; }
        public double PlanPercPartOperObrasPosto { get; set; }
        public double PlanValPartOperObrasPosto { get; set; }
        //Obras Civis - AmPm (NP)
        public double PlanValObrasAmPm { get; set; }
        public double PlanPercPartOperObrasAmPm { get; set; }
        public double PlanValPartOperObrasAmPm { get; set; }
        //Obras Civis - JetOil (NP)
        public double PlanValObrasJetOil { get; set; }
        public double PlanPercPartOperObrasJetOil { get; set; }
        public double PlanValPartOperObrasJetOil { get; set; }
        //Obras Civis - JetOIl Motos (NP)
        public double PlanValObrasJetOilMotos { get; set; }
        public double PlanPercPartOperObrasJetOilMotos { get; set; }
        public double PlanValPartOperObrasJetOilMotos { get; set; }
        //Obras Civis - Parceiros (NP)
        public double PlanValObrasParceiros { get; set; }
        public double PlanPercPartOperObrasParceiros { get; set; }
        public double PlanValPartOperObrasParceiros { get; set; }

        //Equipamentos
        public double PlanValorFornecEquip { get; set; }
        public double PlanValorInstalEquip { get; set; }
        public double PlanPercOperInstalEquip { get; set; }
        public double PlanValorOperInstalEquip { get; set; }
        //Equipamentos - Bombas (NP)
        public double PlanValFornecEquipBombas { get; set; }
        public double PlanValInstalEquipBombas { get; set; }
        public double PlanPercOperInstalEquipBombas { get; set; }
        public double PlanValOperInstalEquipBombas { get; set; }
        //Equipamentos - Tanques (NP)
        public double PlanValFornecEquipTanques { get; set; }
        public double PlanValInstalEquipTanques { get; set; }
        public double PlanPercOperInstalEquipTanques { get; set; }
        public double PlanValOperInstalEquipTanques { get; set; }

        //Elementos de Imagem
        public double PlanValorFornecElem { get; set; }
        public double PlanValorInstalElem { get; set; }
        public double PlanPercOperInstalElem { get; set; }
        public double PlanValorOperInstalElem { get; set; }
        //Elementos de Imagem - Posto (NP)
        public double PlanValFornecElemPosto { get; set; }
        public double PlanValInstalElemPosto { get; set; }
        public double PlanPercOperInstalElemPosto { get; set; }
        public double PlanValOperInstalElemPosto { get; set; }
        //Elementos de Imagem - AmPm (NP)
        public double PlanValFornecElemAmPm { get; set; }
        public double PlanValInstalElemAmPm { get; set; }
        public double PlanPercOperInstalElemAmPm { get; set; }
        public double PlanValOperInstalElemAmPm { get; set; }
        //Elementos de Imagem - JetOil (NP)
        public double PlanValFornecElemJetOil { get; set; }
        public double PlanValInstalElemJetOil { get; set; }
        public double PlanPercOperInstalElemJetOil { get; set; }
        public double PlanValOperInstalElemJetOil { get; set; }
        //Elementos de Imagem - JetOil Motos (NP)
        public double PlanValFornecElemJetOilMotos { get; set; }
        public double PlanValInstalElemJetOilMotos { get; set; }
        public double PlanPercOperInstalElemJetOilMotos { get; set; }
        public double PlanValOperInstalElemJetOilMotos { get; set; }

        //Estoque Pulmão de Elementos de Imagem
        public double PlanValorEstPulmao { get; set; }
        //Estoque Pulmão de Elementos de Imagem - Posto (NP)
        public double PlanValEstPulmaoPosto { get; set; }
        //Estoque Pulmão de Elementos de Imagem - AmPm (NP)
        public double PlanValEstPulmaoAmPm { get; set; }
        //Estoque Pulmão de Elementos de Imagem - JetOil (NP)
        public double PlanValEstPulmaoJetOil { get; set; }
        //Estoque Pulmão de Elementos de Imagem - Jet OIl Motos (NP)
        public double PlanValEstPulmaoJetOilMotos { get; set; }

        //Estoque Pulmão de Equipamentos
        public double PlanValorEstPulmaoEquipamento { get; set; }
        //Estoque Pulmão de Equipamentos - Bombas (NP)
        public double PlanValEstPulmaoEquipBombas { get; set; }
        //Estoque Pulmão de Equipamentos - Tanques (NP)
        public double PlanValEstPulmaoEquipTanques { get; set; }

        public double PlanValorFornecCTF { get; set; }
        public double PlanValorInstalCTF { get; set; }
        public long PlanPadraoImagem { get; set; }
        public string PlanCheckPolitica { get; set; }
        public string PlanComentarios { get; set; }
        public double PlanValorInstalGNV { get; set; }
        public double PlanValorFornecGNV { get; set; }
        public double PlanValorDespesa { get; set; }
        public string Planilha { get; set; }
        public string Nome { get; set; }
        public string DataHoraArquivo { get; set; }
        public int PlanCodDocumento { get; set; }

        #endregion
    }
}
