﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;

public abstract class DaoBase<T>
{
    #region Atributos
    protected IDbConnection _conn = ConexaoBase.GetInstance().Conexao;

    protected IDbTransaction TransacaoCorrente() { return ConexaoBase.GetInstance().TransacaoAtiva; }
    #endregion

    #region Métodos
    protected List<T> MontarLista(IDataReader dr)
    {
        List<T> lista = new List<T>();
        while (dr.Read()) { lista.Add(MontarObjeto(dr)); }
        return lista;
    }
    protected IDataParameter CriarParametro(string nomeParametro, object valor, Type tipo, int tamanho = 0)
    {
        IDataParameter p = null;

        switch (ConexaoBase.GetInstance().TipoBaseDados)
        {
            case daobase.Enumerados.BaseDadosEnum.Access:
                OleDbParameter oleParam = new OleDbParameter();
                oleParam.ParameterName = nomeParametro;
                oleParam.Value = valor;
                oleParam.IsNullable = (valor == null);

                switch (tipo.Name.ToLower())
                {
                    case "string":
                        if (tamanho == 1)
                            oleParam.OleDbType = OleDbType.Char;
                        else
                            oleParam.OleDbType = OleDbType.VarChar;
                        oleParam.Size = tamanho;
                        break;
                        
                    case "int":
                    case "int16":
                    case "int32":
                        oleParam.OleDbType = OleDbType.Integer;
                        break;

                    case "decimal":
                        oleParam.OleDbType = OleDbType.Decimal;
                        break;

                    case "datetime":
                        oleParam.OleDbType = OleDbType.Date;
                        break;
                }
                p = oleParam;
                break;

            case daobase.Enumerados.BaseDadosEnum.SQLServer:
                SqlParameter sqlParam = new SqlParameter();
                sqlParam.ParameterName = nomeParametro;
                sqlParam.Value = (valor == null ? DBNull.Value : valor);
                sqlParam.IsNullable = (valor == null);

                switch (tipo.Name.ToLower())
                {
                    case "string":
                        if (tamanho == 1)
                            sqlParam.SqlDbType = SqlDbType.Char;
                        else
                            sqlParam.SqlDbType = SqlDbType.VarChar;
                        sqlParam.Size = tamanho;
                        break;

                    case "int":
                    case "int32":
                        sqlParam.SqlDbType = SqlDbType.Int;
                        break;

                    case "decimal":
                        sqlParam.SqlDbType = SqlDbType.Decimal;
                        sqlParam.Precision = 10;
                        sqlParam.Scale = 2;
                        break;

                    case "datetime":
                        sqlParam.SqlDbType = SqlDbType.DateTime;
                        break;

                    case "guid":
                        sqlParam.SqlDbType = SqlDbType.UniqueIdentifier;
                        break;
                }
                p = sqlParam;
                break;

            case daobase.Enumerados.BaseDadosEnum.Oracle:
                throw new NotImplementedException();
        }

        return p;
    }
    #endregion

    #region Métodos Abstratos
    protected abstract T MontarObjeto(IDataReader dr);
    #endregion
}

