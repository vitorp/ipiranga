﻿
using System;
namespace tir.dao.Entidades
{
    public class TipoCom
    {
        #region Propriedades
        public Int32 CodigoTipoComponente { get; set; }
        public string NomeTipoComponente { get; set; }
        public string CodigoTipoComponenteNovo { get; set; }
        public Int16? NumeroMinimoMesesTerceiro { get; set; }
        public Int16? NumeroMaximoMesesTerceiro { get; set; }
        public Int16? NumeroMinimoMesesProprio { get; set; }
        public Int16? NumeroMaximoMesesProprio { get; set; }
        #endregion
    }
}
