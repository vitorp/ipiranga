﻿using System;
using System.Data;
//using System.Diagnostics.Contracts;
using System.Text;
using daobase;
using tir.dao.Entidades;

namespace tir.dao
{
    public class InflacaoDao : DaoBase<Inflacao>
    {
        #region Métodos Públicos
        /// <summary>
        /// Obtem a Inflação no ano.
        /// </summary>
        /// <param name="noAno">Ano a buscar inflação.</param>
        /// <returns>Objeto Inflaçao.</returns>
        public Inflacao ObterPorAno(int noAno)
        {
            #region Contracts
            //Contract.Requires(noAno > 0, Mensagens.Inflacao.VAL_NO_ANO);
            #endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select i.No_Ano, ");
                sb.Append("i.Pc_Inflacao ");
                sb.Append("from Inflacao i ");
                sb.AppendFormat("where i.No_Ano = {0} ", DaoUtil.FormatarParametro("noAno"));

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("noAno"), noAno, typeof(Int16)));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //se há resultado
                if (dr.Read())
                {
                    //valor de retorno
                    return MontarObjeto(dr);
                }
                else
                {
                    //valor de retorno nulo
                    return null;
                }
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("InflacaoDao.ObterPorAno",
                //"Erro ao obter a Inflação para um ano.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        /// <summary>
        /// Obtem a inflação média.
        /// </summary>
        /// <returns>Valor da Inflação média.</returns>
        public double ObterInflacaoMedia()
        {
            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();           

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select AVG(i.Pc_Inflacao) as infla_media ");
                sb.Append("from Inflacao i ");

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                object valor = cmd.ExecuteScalar();

                if (valor != null)
                    return (double)valor;
                else
                    return 0;
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("InflacaoDao.ObterInflacaoMedia",
                //"Erro ao obter a Inflação média.", ex);
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        #endregion

        #region Overrides

        protected override Inflacao MontarObjeto(IDataReader dr)
        {
            Inflacao ent = new Inflacao();
            try
            {
                ent.NumeroAno = (Int16)dr["No_Ano"];
                ent.PercentualInflacaoAnual = (double)dr["Pc_Inflacao"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }

        #endregion
    }
}
