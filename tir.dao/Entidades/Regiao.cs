﻿
namespace tir.dao.Entidades
{
    public class Regiao
    {
        #region Propriedades
        public string SiglaRegiao { get; set; }
        public string SiglaRegiaoAlcool { get; set; }
        public decimal CodigoRegiao { get; set; }
        public string NomeRegiao { get; set; }
        #endregion
    }
}
