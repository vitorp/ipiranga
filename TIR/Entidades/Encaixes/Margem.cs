﻿
namespace TIR.Entidades.Encaixes
{
    /// <summary>
    /// Encaixe que representa uma margem.
    /// </summary>
    public class Margem : Encaixe
    {
        #region Propriedades

        public long CodProduto { get; set; }

        #endregion
    }
}
