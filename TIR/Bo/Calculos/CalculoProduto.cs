﻿using System;
using TIR.Entidades;

namespace TIR.Bo.Calculos
{
    public class CalculoProduto
    {
        public double[] Calcular(Produto prod, TIRNegociacao tir)
        {
            //int Prazo = tir.ContrAdicional + tir.ContrExistente + 2;    // 1;
            //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
            //int Prazo = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 2;
            //Alterado em jun/2014 para incluir os meses de compensação de volume no dimensionamento do fluxo
            int Prazo = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 2;

            prod.Fluxo = new double[Prazo];

            if (prod.MediaMesAno1 + prod.MediaMesAno2 + prod.MediaMesAno3 > 0.0)
            {
                //---- Calculo do Aluguel do volume
                CalculoAluguelVolume calcAlugVol = new CalculoAluguelVolume();
                prod.AluguelVol.Fluxo = calcAlugVol.Calcular(prod.AluguelVol, tir);
                if (prod.AluguelVol.Fluxo == null)
                {
                    throw new Exception("Produto::Calculo: Erro ao calcular produto. O cálculo do aluguel do produto " + Convert.ToString(prod.CodProduto) + " falhou.");
                }

                for (int i = 0; i < Prazo; i++)
                {
                    prod.Fluxo[i] += prod.AluguelVol.Fluxo[i];
                }

                //--- Calculo do capital de Giro
                //      Não calcula para o Gás Natural (23/03/2000)
                CalculoCapitalGiro calcCapGiro = new CalculoCapitalGiro();
                prod.CapitalGiro.Fluxo = calcCapGiro.Calcular(prod.CapitalGiro, tir);
                if (prod.CapitalGiro.Fluxo == null)
                {
                    throw new Exception("Produto::Calculo: Erro ao calcular produto. O cálculo do capital de giro do produto " + Convert.ToString(prod.CodProduto) + " falhou.");
                }

                for (int i = 0; i < Prazo; i++)
                {
                    prod.Fluxo[i] += prod.CapitalGiro.Fluxo[i];
                }

                //--- Calculo das Margens do produto
                //       23/03/2000 - Se gás natural e compressor próprio utilizar um redutor de 0,6
                //                    na margem da distribuidora.
                CalculoMargem calcMargem = new CalculoMargem();
                prod.Margem.Fluxo = calcMargem.Calcular(prod.Margem, tir);
                if (prod.Margem.Fluxo == null)
                {
                    throw new Exception("Produto::Calculo: Erro ao calcular produto. O cálculo das margens do produto " + Convert.ToString(prod.CodProduto) + " falhou.");
                }

                for (int i = 0; i < Prazo; i++)
                {
                    prod.Fluxo[i] += prod.Margem.Fluxo[i];
                }

                //--- Estoque Técnico do produto
                CalculoEstoqTecnico calcEstTec = new CalculoEstoqTecnico();
                prod.EstoqTecnico.Fluxo = calcEstTec.Calcular(prod.EstoqTecnico, tir);
                if (prod.EstoqTecnico.Fluxo == null)
                {
                    throw new Exception("Produto::Calculo: Erro ao calcular produto. O cálculo do estoque técnico do produto " + Convert.ToString(prod.CodProduto) + " falhou.");
                }

                for (int i = 0; i < Prazo; i++)
                {
                    prod.Fluxo[i] += prod.EstoqTecnico.Fluxo[i];
                }
            }

            return prod.Fluxo;
        }
    }
}
