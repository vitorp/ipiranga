﻿
namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que representa a coordenadoria do cliente.
    /// </summary>
    public class Coordenadoria
    {
        #region Propriedades

        public string Codigo { get; set; }
        public string Nome { get; set; }
        public string Sigla { get; set; }

        #endregion 
    }
}
