﻿using System;
using System.Data;
//using System.Diagnostics.Contracts;
using System.Text;
using daobase;
using tir.dao.Entidades;

namespace tir.dao
{
    public class DescontoMinDao : DaoBase<DescontoMin>
    {
        #region Métodos Públicos
        /// <summary>
        /// Obtem Desconto Mínimo.
        /// </summary>
        /// <param name="sgTipoNegcc">Tipo de negociação.</param>
        /// <param name="codProdu">Código do produto.</param>
        /// <returns>Objeto DescontoMin.</returns>
        public DescontoMin ObterDescontoMinimo(string sgTipoNegcc, string codProdu)
        {
            #region Contracts
            //Contract.Requires(sgTipoNegcc != null, Mensagens.DescontoMin.VAL_TIPO_NEGOCIACAO);
            //Contract.Requires(codProdu != null, Mensagens.DescontoMin.VAL_CODIGO_PRODUTO);
            #endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select d.Sg_Tpneg, ");
                sb.Append("d.cd_prod, ");
                sb.Append("d.Pc_Desc_Min ");
                sb.Append("from DescontoMin d ");
                sb.AppendFormat("where d.Sg_Tpneg = {0} ", DaoUtil.FormatarParametro("sgTipoNegcc"));
                sb.AppendFormat("and d.cd_prod = {0} ", DaoUtil.FormatarParametro("codProdu"));

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("sgTipoNegcc"), sgTipoNegcc, typeof(String), 3));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codProdu"), codProdu, typeof(String), 3));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //se há resultado
                if (dr.Read())
                {
                    //valor de retorno
                    return MontarObjeto(dr);
                }
                else
                {
                    //valor de retorno nulo
                    return null;
                }
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("DescontoMinDao.ObterDescontoMinimo",
                //"Erro ao obter Desconto Mímino por Tipo de Negociação e Código do Produto.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }
        #endregion

        #region Overrides

        protected override DescontoMin MontarObjeto(IDataReader dr)
        {
            DescontoMin ent = new DescontoMin();
            try
            {
                ent.SegmentoTipoNegocio = (string)dr["Sg_Tpneg"];
                ent.CodigoProduto = (string)dr["Cd_Prod"];
                ent.PrecoDescontoMinimo = (Single)dr["Pc_Desc_Min"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
