﻿using System.ServiceModel;
using WebService.TIR.Models;

namespace WebService.TIR
{
    [ServiceContract]
    public interface ICalculadoraTirService
    {
        [OperationContract]
        ResultadoTir Calcular(ParametrosTir composite);
    }
}
