﻿using System.Collections.Generic;
using TIR.Bo;
using TIR.Entidades.Componentes;

namespace TIR.Entidades
{
    public class Negociacao
    {
        public List<Componente> Componentes { get; set; }
        public TIRResultado Tir { get; set; }

        public void Calcular()
        {
            ComponenteBo bo = new ComponenteBo();

            foreach (Componente comp in this.Componentes)
            {
                bo.Calcular(comp, Tir);
            }
        }
    }
}
