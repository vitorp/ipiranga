﻿
namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que representa os tipos de garantia.
    /// </summary>
    public class TipoGarantia
    {
        #region Propriedades

        public string Codigo { get; set; }
        public string Descricao { get; set; }

        #endregion
    }
}
