﻿using System;

namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que representa o hitórico da PNE.
    /// </summary>
    public class HistoricosPNE
    {
        #region Propriedades

        public string TipoNegoc { get; set; }
        public string Numero { get; set; }
        public float Valor { get; set; }
        public DateTime DataAprovacao { get; set; }
        public string Objetivo { get; set; }
        public string IndAprovado { get; set; }

        #endregion
    }
}
