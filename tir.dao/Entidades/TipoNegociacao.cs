﻿
using System;
namespace tir.dao.Entidades
{
    public class TipoNegociacao
    {
        #region Propriedades
        public Int16 CodigoTipoNegociacao { get; set; }
        public string NomeTipoNegociacao { get; set; }
        public Int16 StTipoNegociacao { get; set; }
        public Int16 CodigoGrupoNegociacao { get; set; }
        public string IdTipoNegociacao { get; set; }
        #endregion
    }
}
