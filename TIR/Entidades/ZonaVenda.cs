﻿
namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que represena a Zona de Venda
    /// </summary>
    public class ZonaVenda
    {
        #region Propriedades

        public string Codigo { get; set; }
        public string Nome { get; set; }
        public string Perfil { get; set; }

        #endregion
    }
}
