﻿using System.Runtime.Serialization;
using WebService.TIR.Models.Base;

namespace WebService.TIR.Models.Franquias
{
    [DataContract]
    public class JetOilTir
    {
        [DataMember(Order = 0)]
        public int CodTipoComp { get; set; }

        [DataMember(Order = 1)]
        public long CodPadraoComp { get; set; }

        [DataMember(Order = 2)]
        public long CodPerfilComp { get; set; }

        [DataMember(Order = 3)]
        public double VlTxFranquia { get; set; }

        [DataMember(Order = 4)]
        public double ValorFaturamento { get; set; }

        [DataMember(Order = 5)]
        public int TempoContrato { get; set; }

        [DataMember(Order = 6)]
        public bool IndParceladoTxFranquia { get; set; }

        [DataMember(Order = 6)]
        public int NoParcelaTxFranquia { get; set; }
    }
}