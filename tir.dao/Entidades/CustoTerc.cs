﻿
using System;
namespace tir.dao.Entidades
{
    public class CustoTerc
    {
        #region Propriedades
        public Int32 CodigoTipoComponente { get; set; }
        public Int32 CodigoPadraoComponente { get; set; }
        public Int32 CodigoPerfilComponente { get; set; }
        public Int16 NumeroMesFaixa { get; set; }
        public double ValorCustoTerceirizacao { get; set; }
        #endregion
    }
}
