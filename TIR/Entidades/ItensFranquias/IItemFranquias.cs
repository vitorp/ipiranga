﻿
namespace TIR.Entidades.ItensFranquias
{
    /// <summary>
    /// Interface que representa um item de uma franquia,
    /// como RoyaltyFixo, RoyaltiesARCO, Propaganda, Terceirizacao,
    /// Overhead, RoyaltyVar e TaxaFranquia.
    /// </summary>
    public interface IItemFranquias
    {
    }
}
