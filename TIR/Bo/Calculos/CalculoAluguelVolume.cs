﻿using System;
using tir.dao;
using tir.dao.Entidades;
using TIR.Entidades;
using TIR.Entidades.Componentes;
using TIR.Entidades.Encaixes;

namespace TIR.Bo.Calculos
{
    public class CalculoAluguelVolume : ICalculo
    {
        public double[] Calcular(Encaixe enc, TIRNegociacao tir)
        {
            try
            {
                //converte encaixe para aluguel de volume
                AluguelVol aluguel = (AluguelVol)enc;

                //define o componente Área de Abastecimento
                AreaAbastecimento areaAbast = tir.Cliente.Posto; // (AreaAbastecimento)tir.Cliente.Componentes[0];

                //define código do Produto e Período de Maturação
                int indProd = areaAbast.Produtos.FindIndex(p => p.CodProduto == aluguel.CodProduto);
                int mesesSemVenda = tir.PeriodoMatura;

                //define prazo total
                //int prazoTotal = tir.ContrAdicional + tir.ContrExistente + 1;
                //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
                //int prazoTotal = tir.ContrAdicional + tir.ContrExistente + mesesSemVenda + 1;
                //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
                int prazoTotal = tir.ContrAdicional + tir.ContrExistente + mesesSemVenda + tir.MesesCompVolume + 1;

                //redimensiona vetor de fluxo
                aluguel.Fluxo = new double[prazoTotal + 1];

                //define indicador de receita de aluguel
                ParamNegDao paramDAO = new ParamNegDao();
                ParamNeg paramNeg = paramDAO.ObterParametro(tir.Categoria, tir.Cliente.segmento, Convert.ToInt32(tir.Cliente.Perfil));
                bool indRecAluguelExist = (bool)paramNeg.IndicadorRecebimentoAluguelExistente;

                if (areaAbast.AluguelMargem.PercAluguel == 0)
                {
                    //não precisa calcular
                    return aluguel.Fluxo;
                }

                if (indRecAluguelExist == false)
                {
                    if (areaAbast.AluguelMargem.Carencia + areaAbast.AluguelMargem.Periodo > tir.ContrAdicional)
                    {
                        throw new Exception("CalculoAluguelVol.calcular: Erro ao calcular aluguel de volume. O período + carência do aluguel sobre produto não pode ser maior que o prazo do contrato adicional.");
                    }
                }
                else
                {
                    if (areaAbast.AluguelMargem.Carencia + areaAbast.AluguelMargem.Periodo > tir.ContrAdicional + tir.ContrExistente)
                    {
                        throw new Exception("CalculoAluguelVol.calcular: Erro ao calcular aluguel de volume. O período + carência do aluguel sobre produto não pode ser maior que o prazo dos contratos adicional mais existente.");
                    }
                }

                //Alíquota de imposto de renda e receita PIS/COFINS
                OutrosParamDao outrosDAO = new OutrosParamDao();
                OutrosParam outrosIR = outrosDAO.ObterParametro("PercIR");
                OutrosParam outrosPIS = outrosDAO.ObterParametro("ReceitaPIS");

                //define alíquota de imposto de renda
                double aliqIR = (double)outrosIR.ValorParametroNumerico;

                //define a receita de PIS
                double aliqPISCOFINS = (double)outrosPIS.ValorParametroNumerico;

                //Encargo da Revenda
                double encRev = 0.0;

                for (int i = 0; i < areaAbast.AluguelMargem.CodProd.Length; i++)
                {
                    if (aluguel.CodProduto == areaAbast.AluguelMargem.CodProd[i])
                    {
                        encRev = areaAbast.AluguelMargem.VrEncargo[i];
                        break;
                    }
                }

                //se não há informação de encargo
                if (encRev == 0.0)
                {
                    //busca encargo de Revenda
                    EncargoRevDao encDao = new EncargoRevDao();
                    EncargoRev encargo = encDao.ObterEncargo(tir.Cliente.Endereco.UF, string.Format("{0:000}", aluguel.CodProduto));
                    encRev = (double)encargo.ValorMargem;
                }

                int contExist = tir.ContrExistente;
                float percAlu = areaAbast.AluguelMargem.PercAluguel;
                float percVol = areaAbast.AluguelMargem.PercVolume;
                int contAdic = tir.ContrAdicional;
                int carencia = areaAbast.AluguelMargem.Carencia;
                int cobranca = areaAbast.AluguelMargem.Periodo;

                int ind = 0;
                int mesesCobranca = 0;

                //***Incrementa saldo de volume (em caso de Maturação de Vendas)
                //***Por Luiz Felipe - jul/2015
                double saldoVolume = 0;
                //Determina o volume esperado para o mês
                double volumeEsperado = 0;
                for (int i = 1 + mesesSemVenda; i < prazoTotal - 1; i++)
                {
                    if (i - mesesSemVenda <= 12)
                    {
                        volumeEsperado = areaAbast.Produtos[indProd].MediaMesAno1 * percVol / 100.0;
                    }
                    else
                    {
                        if (i - mesesSemVenda <= 24)
                        {
                            volumeEsperado = areaAbast.Produtos[indProd].MediaMesAno2 * percVol / 100.0;
                        }
                        else
                        {
                            volumeEsperado = areaAbast.Produtos[indProd].MediaMesAno3 * percVol / 100.0;
                        }
                    }

                    //Incrementa o saldo de volume
                    saldoVolume += volumeEsperado - (areaAbast.Produtos[indProd].FluxoVol[i] * percVol / 100.0);
                }
                //***

                if (indRecAluguelExist == false)
                {
                    //for (int i = 1; i < prazoTotal; i++)
                    //***Alterado por Luiz Felipe para considerar os meses sem venda - jul/2015
                    for (int i = 1 + mesesSemVenda; i < prazoTotal; i++)
                    {
                        double valor = encRev * areaAbast.Produtos[indProd].FluxoVol[i] * percAlu / 100.0 * percVol / 100.0;
                        valor = valor * (1.0 - aliqIR / 100.0) * (1.0 - aliqPISCOFINS / 100.0);

                        //alterado em 14/08/2001 - Alex Grimberg - incremental negativo
                        if (areaAbast.Produtos[indProd].FluxoVol[i] != 0.0)
                        {
                            ind++;

                            if (ind > carencia)
                            {
                                mesesCobranca++;

                                if (mesesCobranca <= cobranca)
                                {
                                    aluguel.Fluxo[i] = valor;
                                }
                               //Trata o saldo de volume, se houver 
                                else
                                {
                                    //Se houver saldo de volume
                                    if (saldoVolume > 0)
                                    {
                                        //Se o saldo for maior ou igual ao valor de volume do mês
                                        if (saldoVolume >= areaAbast.Produtos[indProd].FluxoVol[i] * percVol / 100.0)
                                        {
                                            //Dá saída no mês
                                            aluguel.Fluxo[i] = valor;
                                            //Decrementa o saldo
                                            saldoVolume -= areaAbast.Produtos[indProd].FluxoVol[i] * percVol / 100.0;
                                        }
                                        //Saldo menor que o valor do mês
                                        else
                                        {
                                            //Calcula o valor do mês de acordo com o saldo
                                            valor = encRev * saldoVolume * percAlu / 100.0;
                                            valor = valor * (1.0 - aliqIR / 100.0) * (1.0 - aliqPISCOFINS / 100.0);
                                            //Dá saída no mês
                                            aluguel.Fluxo[i] = valor;
                                            //Zera o saldo de volume
                                            saldoVolume = 0;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    //--> Tem cobrança de aluguel no periodo de contrato existente
                    //obtém o primeiro volume
                    double volExistente = 0.0;
                    if (areaAbast.Produtos[indProd].MediaMesAno3 > 0.0)
                    {
                        volExistente = areaAbast.Produtos[indProd].MediaMesAno3;
                    }
                    else
                    {
                        volExistente = areaAbast.Produtos[indProd].MediaMesAno1;
                    }

                    //cálculo periodo de aluguel de volume
                    for (int i = 1 + mesesSemVenda; i < prazoTotal; i++)
                    {
                        double baseVolume = volExistente;

                        double valor = encRev * baseVolume * percAlu / 100.0 * percVol / 100.0;
                        valor = valor * (1.0 - aliqIR / 100.0) * (1.0 - aliqPISCOFINS / 100.0);
                        ind++;

                        if (ind > carencia)
                        {
                            mesesCobranca++;

                            if (mesesCobranca <= cobranca)
                            {
                                aluguel.Fluxo[i] = valor;
                            }
                        }
                    }
                }

                //retorna o fluxo
                return aluguel.Fluxo;

            }
            catch (Exception ex)
            {
                throw new Exception("CalculoAluguelVol.calcular: Erro ao calcular aluguel de volume.\n" + ex.Message, ex);
            }
        }
    }
}
