﻿using System;
using System.Data;
using System.Text;
using daobase;
//using System.Diagnostics.Contracts;
using tir.dao.Entidades;

namespace tir.dao
{
    public class AliqIOFDao : DaoBase<AliqIOF>
    {
        #region Métodos Públicos

        /// <summary>
        /// Busca uma alíquota de IOF cujo número do mês
        /// limite máximo seja maior ou igual a "NoMes".
        /// </summary>
        /// <param name="NoMes">Número do mês para busca da alíquota.</param>
        /// <returns>Objeto AliqIOF.</returns>
        public AliqIOF ObterPorMes(int NoMes)
        {
            #region Contracts
            //Contract.Requires(NoMes > 0, Mensagens.AliqIOF.VAL_NO_MES);
            #endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select a.No_Mes_Lim_Max, ");
                sb.Append("a.Pc_IOF_Fisica, ");
                sb.Append("a.Pc_IOF_Jurid ");
                sb.Append("from AliqIOF a ");
                sb.AppendFormat("where a.No_Mes_Lim_Max >= {0} ", DaoUtil.FormatarParametro("NoMes"));

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("NoMes"), NoMes, typeof(Int16)));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //se há resultado
                if (dr.Read())
                {
                    //valor de retorno - alíquota de IOF
                    return MontarObjeto(dr);
                }
                else
                {
                    //valor de retorno nulo
                    return null;
                }
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("AliqIOFDao.ObterPorMes",
                                        //"Erro ao buscar alíquota de IOF pelo número máximo do mês.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }
        #endregion

        #region Overrides
        protected override AliqIOF MontarObjeto(IDataReader dr)
        {
            AliqIOF ent = new AliqIOF();

            try
            {
                ent.NumeroMesLimitemaximo = (Int16)dr["No_Mes_Lim_Max"];
                ent.PercentualIOFPessoaJuridica = (Single)dr["Pc_IOF_Fisica"];
                ent.PercentualIOFPessoaJuridica = (Single)dr["Pc_IOF_Jurid"];
            }
            catch (Exception)
            {                
                throw;
            }

            return ent;
        }
        #endregion
    }
}
