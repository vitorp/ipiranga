﻿
namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que representa uma base supridora.
    /// </summary>
    public class BaseSupridora
    {
        #region Propriedades

        public string Codigo { get; set; }
        public string Nome { get; set; }

        #endregion
    }
}
