﻿
namespace TIR.Entidades.Encaixes
{
    /// <summary>
    /// Interface que define as classes que podem herdar as funcionalidades de fechamento de ano.
    /// </summary>
    public interface IFechamentoAno
    {
        #region Propriedades

        float PercCorMonSaldo { get; set; }

        float PercCorMonCarencia { get; set; }

        float PercCorMonet { get; set; }

        int Carencia { get; set; }

        double ValorFinanc { get; set; }

        float PercJurosCarencia { get; set; }

        float PercJuros { get; set; }

        int Amortizacao { get; set; }

        #endregion
    }
}
