﻿
namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que representa o percentual de maturação de vendas em determinado mês.
    /// Esta entidade é utilizada como uma lista de maturação do documento tir.
    /// Não confundir com a entidade MaturacaoVenda do componente cbpi.mkt.dalc que 
    /// representa apenas o objeto dto para acesso à tabela de maturação.
    /// </summary>
    public class MaturacaoVendaTir
    {
        #region Propriedades

        public int Mes { get; set; }
        public float Percentual { get; set; }

        #endregion
    }
}
