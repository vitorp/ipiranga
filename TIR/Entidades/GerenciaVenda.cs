﻿
namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que represena a Gerência de Vendas
    /// </summary>
    public class GerenciaVenda
    {
        #region Propriedades

        public string Codigo { get; set; }
        public string Nome { get; set; }
        public string Sigla { get; set; }

        #endregion
    }
}
