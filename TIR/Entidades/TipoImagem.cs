﻿
namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que representa o tipo de imagem do cliente.
    /// </summary>
    public class TipoImagem
    {
        #region Propriedades

        public string Codigo { get; set; }
        public string Descricao { get; set; }

        #endregion
    }
}
