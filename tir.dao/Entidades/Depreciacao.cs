﻿using System;

namespace tir.dao.Entidades
{
    public class Depreciacao
    {
        #region Propriedades
        public Int16 CodigoECX { get; set; }
        public Int16 MesesDepreciacao { get; set; }
        public Int16 MesesReinvestimento { get; set; }
        public Single PercReinvestimento { get; set; }
        public string VendaFinal { get; set; }
        #endregion
    }
}
