﻿
using System;
namespace tir.dao.Entidades
{
    public class AliqIOF
    {
        #region Propriedades
        public Int16 NumeroMesLimitemaximo { get; set; }
        public Single PercentualIOFPessoaFisica { get; set; }
        public Single PercentualIOFPessoaJuridica { get; set; }
        #endregion
    }
}
