﻿using System;
using System.Data;
////using System.Diagnostics.Contracts;
using System.Text;
using daobase;
using tir.dao.Entidades;

namespace tir.dao
{
    public class EncargoRevDao : DaoBase<EncargoRev>
    {
        #region Métodos Públicos
        /// <summary>
        /// Obtem os parâmetros de encargo de revenda.
        /// </summary>
        /// <param name="siglaUF">Unidade Federativa.</param>
        /// <param name="codProdu">Código do produto.</param>
        /// <returns>Objeto EncargoRev.</returns>
        public EncargoRev ObterEncargo(string siglaUF, string codProdu)
        {
            #region Contracts
            //Contract.Requires(siglaUF != null, Mensagens.EncargoRev.VAL_SIGLA_UF);
            //Contract.Requires(codProdu != null, Mensagens.EncargoRev.VAL_CODIGO_PRODUTO);
            #endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select e.Sg_UF, ");
                sb.Append("e.Cd_Prod, ");
                sb.Append("e.Vr_MrgR ");
                sb.Append("from EncargoRev e ");
                sb.AppendFormat("where e.Sg_UF = {0} ", DaoUtil.FormatarParametro("siglaUF"));
                sb.AppendFormat("and e.Cd_Prod = {0} ", DaoUtil.FormatarParametro("codProdu"));

                //#region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("siglaUF"), siglaUF, typeof(String), 2));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codProdu"), codProdu, typeof(String), 3));
                //#endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //se há resultado
                if (dr.Read())
                {
                    //valor de retorno - alíquota de IOF
                    return MontarObjeto(dr);
                }
                else
                {
                    //valor de retorno nulo
                    return null;
                }
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("EncargoRevDao.ObterEncargo",
                //"Erro ao obter parâmetros de encargo de revenda por UF e código do Produto.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }
        #endregion

        #region Overrides

        protected override EncargoRev MontarObjeto(IDataReader dr)
        {
            EncargoRev ent = new EncargoRev();
            try
            {
                ent.SiglaUF = (string)dr["Sg_UF"];
                ent.CodigoProduto = (string)dr["Cd_Prod"];
                ent.ValorMargem = (decimal)dr["Vr_MrgR"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
