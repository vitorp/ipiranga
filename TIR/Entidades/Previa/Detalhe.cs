﻿
using System.Collections.Generic;
namespace TIR.Entidades.Previa
{
    public class Detalhe
    {
        //Propriedades simples.
        public string Descricao { get; set; }
        public double Total { get; set; }

        //Atributos tipo coleção.
        public List<Servico> Servicos { get; set; }
        public List<EquipamentoPrevia> EquipamentosPrevia { get; set; }
    }
}
