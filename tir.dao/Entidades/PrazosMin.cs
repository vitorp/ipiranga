﻿
using System;
namespace tir.dao.Entidades
{
    public class PrazosMin
    {
        #region Propriedades
        public string UnidadeNegocio { get; set; }
        public string SiglaUF { get; set; }
        public string SiglaTipoNegocio { get; set; }
        public Int16 CodigoLocalizacaoPosto { get; set; }
        public string CodigoProduto { get; set; }
        public double VolumeFaixa { get; set; }
        public Single QuantidadeDiasMinimo { get; set; }
        #endregion
    }
}
