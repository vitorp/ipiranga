﻿
namespace TIR.Entidades.Encaixes
{
    /// <summary>
    /// Encaixe que representa um recebimento de luvas.
    /// </summary>
    public class RecebLuvas : Encaixe, IFechamentoAno
    {
        #region Propriedades

        public int MesSaidaVista { get; set; }
        public double ValorVista { get; set; }
        public int Amortizacao { get; set; }
        public int Carencia { get; set; }
        public float PercCorMonet { get; set; }
        public float PercJuros { get; set; }
        public float PercCorMonSaldo { get; set; }
        public float PercCorMonCarencia { get; set; }
        public float PercJurosCarencia { get; set; }
        public double ValorFinanc { get; set; }
        public bool JurosCarRepact { get; set; }

        #endregion
    }
}
