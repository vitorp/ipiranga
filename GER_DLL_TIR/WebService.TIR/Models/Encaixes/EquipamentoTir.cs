﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WebService.TIR.Models.Encaixes
{
    [DataContract]
    public class EquipamentoTir
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public double Valor { get; set; }

        [DataMember]
        public int MesInstalacao { get; set; }
    }
}