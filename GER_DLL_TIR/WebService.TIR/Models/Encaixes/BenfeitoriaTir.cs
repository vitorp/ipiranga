﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using WebService.TIR.Models.Base;

namespace WebService.TIR.Models.Encaixes
{
    public class BenfeitoriaTir 
    {
        [DataMember]
        public double Valor { get; set; }

        [DataMember]
        public int MesConcessao { get; set; }
    }
}