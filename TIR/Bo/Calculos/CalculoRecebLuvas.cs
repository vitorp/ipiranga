﻿using System;
using Microsoft.VisualBasic;
using tir.dao;
using tir.dao.Entidades;
using TIR.Entidades;
using TIR.Entidades.Componentes;
using TIR.Entidades.Encaixes;

namespace TIR.Bo.Calculos
{
    public class CalculoRecebLuvas : FechamentoAno, ICalculo
    {
        #region Propriedades
        public Franquia Franquia { get; set; }
        #endregion

        public double[] Calcular(Encaixe enc, TIRNegociacao tir)
        {
            //Converte encaixe para RecebLuvas
            RecebLuvas recebLuvas = (RecebLuvas)enc;

            int contratoFranquia = 0;
            bool isFranquiaRenovacao = false;

            //--- Verificar tipo de franquia pelo objeto
            if (Franquia != null)
            {
                //se tiver franquia define o tempo de contrato da franquia
                contratoFranquia = Franquia.TempoContrato;

                //de acordo com o tipo de negociação da franquia
                if (Franquia.TpNegFranquia.Equals(2) || Franquia.TpNegFranquia.Equals(1))
                {
                    //define que é renovação
                    isFranquiaRenovacao = true;
                }
            }
            else
            {
                //não tem franquia - zera o tempo de contrato da franquia
                contratoFranquia = 0;
            }

            //----- Todos os calculos são em Reais (moeda corrente)
            //----- Somente ao final os valores serão convertidos para FAO

            //define o valor da FAO
            double fao = tir.ValorFAO;

            int prazoTotal = 0;

            //se não tem franquia
            if (contratoFranquia.Equals(0))
            {
                //define o tempo de contrato pelos contratos adicional + existente
                //prazoTotal = tir.ContrAdicional + tir.ContrExistente + 1;
                //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
                prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 1;
            }
            else
            {
                //se tem franquia  - define o contrato pelo contrato da franquia
                prazoTotal = contratoFranquia + 1;
            }

            //redimensiona vetores de acordo com o tempo de contrato
            //recebLuvas.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + 2];
            //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
            //recebLuvas.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 2];
            //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
            recebLuvas.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 2];
            saldo = new double[prazoTotal + 1];
            juros = new double[prazoTotal + 1];
            amort = new double[prazoTotal + 1];
            prest = new double[prazoTotal + 1];
            saldoSub = new double[prazoTotal + 1];
            recDesp = new double[prazoTotal + 1];
            valorJurCar = new double[prazoTotal + 1];

            //salva o valor original do valor financiado
            double valorOriginal = recebLuvas.ValorFinanc;

            //instancia dao e busca percentual de IR e receita PIS/COFINS
            OutrosParamDao outrosDAO = new OutrosParamDao();
            OutrosParam ir = outrosDAO.ObterParametro("PercIR");
            OutrosParam pis = outrosDAO.ObterParametro("ReceitaPIS");
            float aliqIr = (float)ir.ValorParametroNumerico;
            double aliqPisCof = (double)pis.ValorParametroNumerico;

            //se tem financiamento vai seguir a rotina abaixo
            //se não, encerra o calculo
            if (recebLuvas.ValorFinanc <= 0)
            {
                //verifica se segue o cálculo ou sai da função
                if (recebLuvas.ValorVista > 0)
                {
                    //se tiver valor à vista segue para outro fluxo de cálculo
                    calcularValorAvista(enc, tir, aliqIr, aliqPisCof, fao);
                }

                //retorno com sucesso
                return recebLuvas.Fluxo;
            }

            //verifica se a amortização está coerente
            if (recebLuvas.Amortizacao <= 0)
            {
                throw new Exception("CalculoRecebLuvas.calcular: Erro ao calcular recebimento de luvas. É preciso informar o número de meses de amortização.");
            }

            //mês início do recebimento de luvas parcelada
            ultimoMes = 0;

            //verifica se os prazos estão coerentes
            if (recebLuvas.Amortizacao + recebLuvas.Carencia + ultimoMes > prazoTotal)
            {
                throw new Exception("CalculoRecebLuvas.calcular: Erro ao calcular recebimento de luvas. A soma dos prazos de amortização, carência e liberação da última parcela não pode ultrapassar a soma dos contratos proposto e existente.");
            }

            //--- Cálculo da liberação e calculo correcao saldo devedor
            recebLuvas.Fluxo[0] = recebLuvas.ValorFinanc * (-1.0);
            recebLuvas.Fluxo[0] = (recebLuvas.Fluxo[0] + recebLuvas.Fluxo[0] * (1.0 - aliqIr / 100.0)) / fao;

            //--- juros total de carência, se tiver
            //--- vê correção monetária da carência
            if (recebLuvas.Carencia > 0 && recebLuvas.PercJurosCarencia > 0)
            {
                dblWork = (recebLuvas.ValorFinanc + cMsaldoDev) * (((tir.Inflacao[ultimoMes + recebLuvas.Carencia]
                        / tir.Inflacao[ultimoMes]) - 1.0) * recebLuvas.PercCorMonCarencia / 100.0 + 1.0);
                jurCarTotal = dblWork * (Math.Pow((recebLuvas.PercJurosCarencia / 100.0 + 1.0), recebLuvas.Carencia) - 1.0);
            }

            if (recebLuvas.JurosCarRepact.Equals(true))
            {
                recebLuvas.ValorFinanc += jurCarTotal;
            }

            dblWork = 0;

            //--- cálculo da prestação
            prestacao = Financial.Pmt(recebLuvas.PercJuros / 100.0, recebLuvas.Amortizacao, (-1.0) * recebLuvas.ValorFinanc, 0.0, 0);
            //int intWork = 0;

            for (int i = 1; i <= ultimoMes + recebLuvas.Carencia + recebLuvas.Amortizacao; i++)
            {
                if (i > ultimoMes + recebLuvas.Carencia)
                {
                    //---- período de recebimento das prestações
                    prest[i] = prestacao / tir.Inflacao[i];

                    if (saldo[i - 1] == 0)
                    {
                        saldo[i - 1] = recebLuvas.ValorFinanc;
                    }

                    juros[i] = saldo[i - 1] * recebLuvas.PercJuros / 100.0;
                    amort[i] = prestacao - juros[i];
                    saldo[i] = saldo[i - 1] - amort[i];

                    if (saldo[i] < 0.0000001)
                    {
                        saldo[i] = 0.0;
                    }
                }

                if (((i / 12.0 - i / 12) * 12) == 0)
                {
                    fecharAno(recebLuvas, tir, i);
                }
            }

            for (int i = 1; i <= ultimoMes + recebLuvas.Carencia + recebLuvas.Amortizacao; i++)
            {
                if (i > ultimoMes + recebLuvas.Carencia)
                {
                    //---> pis/cofins nos juros 24/02/99
                    recebLuvas.Fluxo[i] = prest[i] * (1.0 - aliqIr / 100.0) * (1.0 - aliqPisCof / 100.0)
                                  - (juros[i] + recDesp[i]) / tir.Inflacao[i] * aliqIr / 100.0
                                  - juros[i] / tir.Inflacao[i] * (1.0 - aliqIr / 100.0) * aliqPisCof / 100.0
                                  - recDesp[i] / tir.Inflacao[i] * (1.0 - aliqIr / 100.0) * aliqPisCof / 100.0;
                }
                else if (i <= ultimoMes)
                {
                    //--- liberação
                    recebLuvas.Fluxo[i] = recebLuvas.Fluxo[i] - recDesp[i] / fao / tir.Inflacao[i] * aliqIr / 100.0
                                          - recDesp[i] / fao / tir.Inflacao[i] * (1.0 - aliqIr / 100.0) * aliqPisCof / 100.0;
                }
                else
                {
                    //--- carência ---> piscofins nos juros 24/02/99
                    recebLuvas.Fluxo[i] = recebLuvas.Fluxo[i] - (recDesp[i] + valorJurCar[i])
                                  / tir.Inflacao[i] * aliqIr / 100.0
                                  - valorJurCar[i] / tir.Inflacao[i] * (1.0 - aliqIr / 100.0) * aliqPisCof / 100.0
                                  - recDesp[i] / tir.Inflacao[i] * (1.0 - aliqIr / 100.0) * aliqPisCof / 100.0;
                }

                if (i > ultimoMes)
                {
                    recebLuvas.Fluxo[i] /= fao;
                }
            }

            //--- Juros de carência não repactuados
            if (recebLuvas.JurosCarRepact == false && (ultimoMes + recebLuvas.Carencia) > 0)
            {
                recebLuvas.Fluxo[ultimoMes + recebLuvas.Carencia] += jurCarTotal / tir.Inflacao[ultimoMes + recebLuvas.Carencia] / fao;
            }

            recebLuvas.ValorFinanc = valorOriginal;
            recebLuvas.Fluxo[0] = 0.0;    //para recebimento de luvas não tem saída de verba  

            if (recebLuvas.ValorVista > 0.0)
            {
                calcularValorAvista(enc, tir, aliqIr, aliqPisCof, fao);
            }

            //retorno com sucesso
            return recebLuvas.Fluxo;
        }

        #region Métodos Privados

        /// <summary>
        /// Método privado que calcula o fluxo quando existe apenas valor à vista
        /// </summary>
        private void calcularValorAvista(Encaixe enc, TIRResultado tir, float aliqIr, double aliqPisCof, double fao)
        {
            //converte o encaixe para recebimento de luvas
            RecebLuvas recebLuvas = (RecebLuvas)enc;

            //calcula o fluxo do valor à vista
            recebLuvas.Fluxo[recebLuvas.MesSaidaVista] += recebLuvas.ValorVista * (1.0 - aliqIr / 100.0)
                 * (1.0 - aliqPisCof / 100.0) / fao / tir.Inflacao[recebLuvas.MesSaidaVista];
        }

        #endregion
    }
}
