﻿using TIR.Entidades.Encaixes;

namespace TIR.Entidades.ItensFranquias
{
    /// <summary>
    /// Item de franquia que representa Royalty variável.
    /// </summary>
    public class RoyaltyVar : Encaixe
    {
    }
}
