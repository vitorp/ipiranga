﻿using System;

namespace tir.dao.Entidades
{
    public class RotinaCalculoTIR
    {
        #region Propriedades
        public Int16 CodigoCalculoTIR { get; set; }
        public string DescricaoCalculoTIR { get; set; }
        #endregion
    }
}
