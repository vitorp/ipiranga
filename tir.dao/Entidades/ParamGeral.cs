﻿using System;

namespace tir.dao.Entidades
{
    public class ParamGeral
    {
        #region Propriedades

        public int CodigoParametro { get; set; }
        public string NomeParametro { get; set; }
        public string DescricaoParametro { get; set; }
        public string CodigoTipoParametro { get; set; }
        public string ValorAlfa { get; set; }
        public string ValorNumerico { get; set; }
        public DateTime? ValorData { get; set; }

        #endregion
    }
}
