﻿using System;
using tir.dao;
using tir.dao.Entidades;
using TIR.Entidades;
using TIR.Entidades.Componentes;
using TIR.Entidades.Encaixes;
using TIR.Entidades.ItensFranquias;
using System.Collections.Generic;

namespace TIR.Bo.Calculos
{
    public class CalculoOverhead : ICalculo
    {
        #region Propriedades
        public Franquia Franquia { get; set; }
        #endregion

        public double[] Calcular(Encaixe enc, TIRNegociacao tir)
        {
            try
            {
                //converte encaixe
                Overhead overhead = (Overhead)enc;

                overhead.Descricao = "OverHead";

                int prazoTotal = 0;
                bool isFranquiaRenovacao = false;

                //define prazo total e redimensiona fluxo
                //prazoTotal = Franquia.TempoContrato + 1;
                //Incluídos os meses sem venda por Luiz Felipe - out/2014
                //prazoTotal = Franquia.TempoContrato + tir.PeriodoMatura + 1;
                //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
                prazoTotal = Franquia.TempoContrato + tir.PeriodoMatura + tir.MesesCompVolume + 1;

                //overhead.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + 2];
                //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
                //overhead.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 2];
                //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
                overhead.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 2];

                //--- Alteração asseguramento de franquias - Agosto/2002
                if (Franquia.TpNegFranquia == 2 || Franquia.TpNegFranquia == 1)
                {
                    isFranquiaRenovacao = true;
                }

                //busca alíquota de imposto de renda
                OutrosParamDao outrosDAO = new OutrosParamDao();
                OutrosParam outros = outrosDAO.ObterParametro("PercIR");
                float percIr = (float)outros.ValorParametroNumerico;

                //busca custo de overhead
                CustoOverheadDao custoDAO = new CustoOverheadDao();
                List<CustoOverhead> custo = custoDAO.ObterCustoOverhead(tir.UnidOrg, tir.Cliente.Endereco.UF, Franquia.CodPadraoComp, Franquia.CodPerfilComp, Franquia.CodTipoComp);
                //Se não encontrou dados para GV/componente
                if (custo.Count == 0)
                {
                    throw new Exception("CalculoOverhead.calcular: Erro ao calcular overhead.\n Não foram encontrados dados para GV/Componente.");
                }
                double valorOverHead = 0;

                int mesInicio = 0;
                //--- migração franquias maio/2004  
                //Tarefa 17928: Overhead de franquias -> Alteração para considerar Reforma/Ativação Controlado
                if (isFranquiaRenovacao.Equals(true) && !tir.Categoria.Equals(41) && !tir.Categoria.Equals(8))
                {
                    //só calcula se tiver prazo proposto
                    if (tir.ContrAdicional > 0)
                    {
                        mesInicio = 1 + tir.ContrExistente;
                    }
                    else
                    {
                        //não precisa calcular
                        return overhead.Fluxo;
                    }
                }
                else
                {
                    mesInicio = 1;

                    //***Nova lógica: se for Renovação, mas tiver informação de tipo, não calcula
                    //***Por Luiz Felipe - out/2014
                    if (isFranquiaRenovacao.Equals(true) && Franquia.TipoRenovacao.Length > 0)
                    {
                        //não precisa calcular
                        return overhead.Fluxo;
                    }
                    //***
                }

                //--- Gerar o fluxo de OverHead
                for (int i = mesInicio + tir.PeriodoMatura; i <= (prazoTotal - 1 + (mesInicio - 1)); i++)
                {
                    //Busca custo de OverHead de acordo com o mês
                    for (int j = 0; j < custo.Count; j++)
                    {
                        valorOverHead = (double)custo[custo.Count - 1].ValorCustoOverhead;
                        //Alterado por Luiz Felipe em dez/2015 - Finaceiro não quer mais
                        //usar os meses sem venda como offset
                        //if ((i - tir.PeriodoMatura) <= custo[j].FaixaAteMes)
                        if (i <= custo[j].FaixaAteMes)
                        {
                            valorOverHead = (double)custo[j].ValorCustoOverhead;
                            break;
                        }
                    }

                    overhead.Fluxo[i] = valorOverHead * (1 - percIr / 100.0) * (-1.0);
                }

                //retorno com sucesso
                return overhead.Fluxo;
            }
            catch (Exception ex)
            {
                throw new Exception("CalculoOverhead.calcular: Erro ao calcular overhead.\n" + ex.Message, ex);
            }
        }
    }
}
