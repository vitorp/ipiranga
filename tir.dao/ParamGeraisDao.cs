﻿using System;
using System.Text;
using tir.dao.Entidades;
using System.Data;
using daobase;

namespace tir.dao
{
    public class ParamGeraisDao : DaoBase<ParamGeral>
    {
        #region Métodos Públicos
        /// <summary>
        /// Método que obtem um Parâmetro Geral.
        /// </summary>
        /// <param name="codParam">Código do parâmetro.</param>
        /// <returns>Objeto ParamGeral.</returns>
        public ParamGeral ObterParametro(int codParam)
        {

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select p.vr_alfa, ");
                sb.Append("p.vr_no, ");
                sb.Append("p.vr_dt ");
                sb.Append("from parametros_gerais p ");
                sb.AppendFormat("where p.cd_param = {0} ", DaoUtil.FormatarParametro("codParam"));

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codParam"), codParam, typeof(Int16)));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //se há resultado
                if (dr.Read())
                {
                    //valor de retorno - parâmetro
                    return MontarObjeto(dr);
                }
                else
                {
                    //valor de retorno nulo
                    return null;
                }
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("ParamNegDao.ObterParametro",
                //"Erro ao obter Parâmetro por Tipo da Categoria, Sigla do Segmento de Mercado e Perfil.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        #endregion

        #region Overrides

        protected override ParamGeral MontarObjeto(IDataReader dr)
        {
            ParamGeral ent = new ParamGeral();
            try
            {
                ent.ValorAlfa = dr["vr_alfa"] != DBNull.Value ? (string)dr["vr_alfa"] : null;
                ent.ValorNumerico = dr["vr_no"] != DBNull.Value ? (string)dr["vr_no"] : null;
                ent.ValorData = dr["vr_dt"] != DBNull.Value ? (DateTime?)dr["vr_dt"] : null;
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }

        #endregion
    }
}
