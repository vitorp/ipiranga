﻿
namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que representa o tipo de compromisso.
    /// </summary>
    public class TipoCompromisso
    {
        #region Propriedades

        public string Codigo { get; set; }
        public string Descricao { get; set; }

        #endregion
    }
}
