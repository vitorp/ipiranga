﻿
namespace tir.dao.Entidades
{
    public class UF
    {
        #region Propriedades
        public string SiglaUF { get; set; }
        public string NomeUF { get; set; }
        #endregion
    }
}
