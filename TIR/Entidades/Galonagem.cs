﻿using System;

namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que representa os valores de galonagem.
    /// </summary>
    public class Galonagem
    {
        #region Propriedades

        public string AnoMes { get; set; }
        public DateTime DtIni { get; set; }
        public DateTime DtFim { get; set; }
        public long Meses { get; set; }
        public double VolumeContratado { get; set; }
        public double VolAteMes { get; set; }
        public double GalonagemDevedora { get; set; }
        public string Previsao { get; set; }

        #endregion
    }
}
