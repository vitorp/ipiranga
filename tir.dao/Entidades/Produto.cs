﻿
namespace tir.dao.Entidades
{
    public class Produto
    {
        #region Propriedades
        public string CodigoProduto { get; set; }
        public string NomeProduto { get; set; }
        public string CodigoGrupoProduto {get; set; }
        public decimal CodigoSubgrupoProduto { get; set; }
        public decimal CodigoTipoProduto { get; set; }
        #endregion
    }
}
