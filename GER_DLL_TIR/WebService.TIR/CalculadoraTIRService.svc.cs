﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.ServiceModel.Web;
using TIR.Bo.Calculos;
using TIR.Constantes;
using TIR.Entidades;
using TIR.Entidades.Componentes;
using TIR.Entidades.Encaixes;
using TIR.Entidades.ItensFranquias;
using WebService.TIR.Models;

namespace WebService.TIR
{
    public class CalculadoraTirService : ICalculadoraTirService
    {
        private const string ERRO_VALOR_MENOR_OU_IGUAL_A_ZERO = "Não é possível calcular a TIR com valor {0} menor ou igual a zero!";
        private const string ERRO_VALOR_NULO_OU_EM_BRANCO = "Não é possível calcular a TIR com o valor {0} em branco ou nulo!";
        private const string SUCCESS_MESSAGE = "Success!";

        public ResultadoTir Calcular(ParametrosTir parametros)
        {
            var stopwatch = System.Diagnostics.Stopwatch.StartNew();

            try
            {
                ValidateXmlProperties(parametros);

                #region ... Dados Base : Cliente ...

                // Configurações básicas do cliente
                var cliente = new Cliente
                {
                    Endereco = new Endereco
                    {
                        Bairro = string.Empty,
                        CEP = string.Empty,
                        UF = parametros.UF,
                        Logradouro = string.Empty,
                        Municipio = string.Empty
                    },
                    Perfil = parametros.Perfil, 
                    segmento = parametros.Segmento,
                    razaoSocial = string.Empty,
                    Posto = new AreaAbastecimento
                    {
                        AluguelMargem = new AluguelMargem
                        {
                            PercAluguel = 0
                        },
                        Encaixes = new List<Encaixe>()
                    },

                    // Configuração de base Supridora
                    BasesSupridoras = new List<BaseSupridora>
                    {
                        new BaseSupridora
                        {
                            Codigo = parametros.CodigoBaseSupridora
                        }
                    },

                    TipoNegocio = new TipoNegocio()
                };

                cliente.Posto.Produtos = new List<Produto>();
                foreach (var produtoTir in parametros.Produtos)
                {
                    // Configurações básicas de Produto
                    var produto = new Produto
                    {
                        CodGrupo = produtoTir.CodigoDoGrupo,
                        CodProduto = produtoTir.CodigoDoProduto, 
                        MediaMesAno1 = produtoTir.MediaMensalAno1,
                        MediaMesAno2 = produtoTir.MediaMensalAno2,
                        MediaMesAno3 = produtoTir.MediaMensalAno3,
                        AluguelVol = new AluguelVol
                        {
                            CodProduto = produtoTir.CodigoDoProduto
                        },
                        CapitalGiro = new CapitalGiro
                        {
                            CodProduto = produtoTir.CodigoDoProduto
                        },
                        Margem = new Margem
                        {
                            CodProduto = produtoTir.CodigoDoProduto
                        },
                        EstoqTecnico = new EstoqTecnico
                        {
                            CodProduto = produtoTir.CodigoDoProduto
                        },
                        PrazoDias = produtoTir.PrazoEmDias
                    };

                    cliente.Posto.Produtos.Add(produto);
                }
                
                cliente.Posto.AluguelMargem.CodProd = new[] { 0L };
                cliente.Posto.AluguelMargem.VrEncargo = new[] { 0.00 };

                #endregion

                #region ... Dados Franquia : AmPm ...

                if (parametros.HasAmPm)
                {
                    cliente.AmPm = new AmPm
                    {
                        PcRoyaltyVariavel = new float[] { 0 },
                        ValorRoyaltyFixo = new double[] { 0 },
                        IndParceladoTxFranquia = parametros.AmPm.IndParceladoTxFranquia,
                        NoParcelaTxFranquia = parametros.AmPm.NoParcelaTxFranquia,
                        VlTxFranquia = parametros.AmPm.VlTxFranquia,
                        CodTipoComp = parametros.AmPm.CodTipoComp,
                        CodPerfilComp = parametros.AmPm.CodPerfilComp,
                        CodPadraoComp = parametros.AmPm.CodPadraoComp,
                        ValorFaturamento = parametros.AmPm.ValorFaturamento,
                        TempoContrato = parametros.AmPm.TempoContrato,
                    };

                    cliente.AmPm.Propaganda = new Propaganda();
                    cliente.AmPm.Terceirizacao = new Terceirizacao();
                    cliente.AmPm.Overhead = new Overhead();
                    cliente.AmPm.RoyaltyVar = new RoyaltyVar();
                    cliente.AmPm.TaxaFranquia = new TaxaFranquia();
                    cliente.AmPm.CapitalGiroFranquia = new CapitalGiroFranquia();
                }

                #endregion

                #region ... Dados Franquia : JetOil

                if (parametros.HasJetOil)
                {
                    cliente.JetOil = new JetOil
                    {
                        PcRoyaltyVariavel = new float[] { 0 },
                        ValorRoyaltyFixo = new double[] { 0 },
                        IndParceladoTxFranquia = parametros.JetOil.IndParceladoTxFranquia,
                        NoParcelaTxFranquia = parametros.JetOil.NoParcelaTxFranquia,
                        VlTxFranquia = parametros.JetOil.VlTxFranquia,
                        CodTipoComp = parametros.JetOil.CodTipoComp,
                        CodPerfilComp = parametros.JetOil.CodPerfilComp,
                        CodPadraoComp = parametros.JetOil.CodPadraoComp,
                        ValorFaturamento = parametros.JetOil.ValorFaturamento,
                        TempoContrato = parametros.JetOil.TempoContrato,
                    };

                    cliente.JetOil.Propaganda = new Propaganda();
                    cliente.JetOil.Terceirizacao = new Terceirizacao();
                    cliente.JetOil.Overhead = new Overhead();
                    cliente.JetOil.RoyaltyVar = new RoyaltyVar();
                    cliente.JetOil.TaxaFranquia = new TaxaFranquia();
                    cliente.JetOil.CapitalGiroFranquia = new CapitalGiroFranquia();
                }

                #endregion

                #region ... Dados Franquia : JetOilMotos

                if (parametros.HasJetOilMotos)
                {
                    cliente.JetMotos = new JetOilMotos
                    {
                        PcRoyaltyVariavel = new float[] { 0 },
                        ValorRoyaltyFixo = new double[] { 0 },
                        IndParceladoTxFranquia = parametros.JetOilMotos.IndParceladoTxFranquia,
                        NoParcelaTxFranquia = parametros.JetOilMotos.NoParcelaTxFranquia,
                        VlTxFranquia = parametros.JetOilMotos.VlTxFranquia,
                        CodTipoComp = parametros.JetOilMotos.CodTipoComp,
                        CodPerfilComp = parametros.JetOilMotos.CodPerfilComp,
                        CodPadraoComp = parametros.JetOilMotos.CodPadraoComp,
                        ValorFaturamento = parametros.JetOilMotos.ValorFaturamento,
                        TempoContrato = parametros.JetOilMotos.TempoContrato,
                    };

                    cliente.JetMotos.Propaganda = new Propaganda();
                    cliente.JetMotos.Terceirizacao = new Terceirizacao();
                    cliente.JetMotos.Overhead = new Overhead();
                    cliente.JetMotos.RoyaltyVar = new RoyaltyVar();
                    cliente.JetMotos.TaxaFranquia = new TaxaFranquia();
                    cliente.JetMotos.CapitalGiroFranquia = new CapitalGiroFranquia();
                }

                #endregion

                #region ... Encaixes : Financiamentos ...

                if(parametros.Financiamentos != null && parametros.Financiamentos.Length > 0)
                {
                    foreach(var financiamento in parametros.Financiamentos)
                    {
                        cliente.Posto.Encaixes.Add(new Financiamento
                        {
                           //<web2:Amortizacao>60</web2:Amortizacao>
                           Amortizacao = financiamento.Amortizacao,
                           
                           //<web2:Carencia>3</web2:Carencia>
                           Carencia = financiamento.Carencia,

                           //<web2:Codigo>9</web2:Codigo>
                           CodEcxDcx = financiamento.Codigo,

                           //<web2:IndPessoaFisica>false</web2:IndPessoaFisica>
                           IndPessoaFisica = financiamento.IndPessoaFisica,

                           //<web2:JurosDeCarenciaRepactuados>true</web2:JurosDeCarenciaRepactuados>
                           JurosCarRepact = financiamento.JurosDeCarenciaRepactuados,

                           //<web2:MesConcessao>0</web2:MesConcessao>
                           MesConcessao = financiamento.MesConcessao,

                           //<web2:PercCorMonCarencia>1</web2:PercCorMonCarencia>
                           PercCorMonCarencia = financiamento.PercCorMonCarencia,

                           //<web2:PercCorMonSaldo>1</web2:PercCorMonSaldo>
                           PercCorMonSaldo = financiamento.PercCorMonSaldo,

                           //<web2:PercCorMonet>1</web2:PercCorMonet>
                           PercCorMonet = financiamento.PercCorMonet,

                           //<web2:PercJuros>1</web2:PercJuros>
                           PercJuros = financiamento.PercJuros,

                           //<web2:PercJurosCarencia>1</web2:PercJurosCarencia>
                           PercJurosCarencia = financiamento.PercJurosCarencia,

                           //<web2:PercLiberacao>
                           //  <arr:float>100</arr:float>
                           //</web2:PercLiberacao>
                           PercLiberacao = financiamento.PercLiberacao,

                           //<web2:ValorFinanc>100000</web2:ValorFinanc>
                           ValorFinanc = financiamento.ValorFinanc
                        });
                    }
                    
                }

                #endregion

                #region ... Encaixes : Recebimento de Luvas ...

                if (parametros.RecebimentoDeLuvas != null && parametros.RecebimentoDeLuvas.Length > 0)
                {
                    foreach (var recebimentoDeLuvas in parametros.RecebimentoDeLuvas)
                    {
                        cliente.Posto.Encaixes.Add(new RecebLuvas
                        {
                            //<web2:Amortizacao></web2:Amortizacao>
                            Amortizacao = recebimentoDeLuvas.Amortizacao,

                            //<web2:Carencia></web2:Carencia>
                            Carencia = recebimentoDeLuvas.Carencia,

                            //<web2:JurosCarRepact></web2:JurosCarRepact>
                            JurosCarRepact = recebimentoDeLuvas.JurosCarRepact,

                            //<web2:MesSaidaVista></web2:MesSaidaVista>
                            MesSaidaVista = recebimentoDeLuvas.MesSaidaVista,

                            //<web2:PercCorMonCarencia></web2:PercCorMonCarencia>
                            PercCorMonCarencia = recebimentoDeLuvas.PercCorMonCarencia,

                            //<web2:PercCorMonSaldo></web2:PercCorMonSaldo>
                            PercCorMonSaldo = recebimentoDeLuvas.PercCorMonSaldo,

                            //<web2:PercCorMonet></web2:PercCorMonet>
                            PercCorMonet = recebimentoDeLuvas.PercCorMonet,

                            //<web2:PercJuros></web2:PercJuros>
                            PercJuros = recebimentoDeLuvas.PercJuros,

                            //<web2:PercJurosCarencia></web2:PercJurosCarencia>
                            PercJurosCarencia = recebimentoDeLuvas.PercJurosCarencia,

                            //<web2:ValorFinanc></web2:ValorFinanc>
                            ValorFinanc = recebimentoDeLuvas.ValorFinanc,

                            //<web2:ValorVista></web2:ValorVista>
                            ValorVista = recebimentoDeLuvas.ValorVista
                        });
                    }

                }

                #endregion

                #region ... Encaixes : Benfeitorias ...

                if (parametros.Benfeitorias != null && parametros.Benfeitorias.Length > 0)
                {
                    foreach(var benfeitoria in parametros.Benfeitorias)
                    {
                        cliente.Posto.Encaixes.Add(new Benfeitoria
                        {
                            Valor = benfeitoria.Valor,
                            MesConcessao = benfeitoria.MesConcessao
                        });
                    }
                }

                #endregion

                #region ... Encaixes : Receitas e Despesas 

                if (parametros.DespesasReceitas != null && parametros.DespesasReceitas.Length > 0)
                {
                    foreach (var despesasReceitas in parametros.DespesasReceitas)
                    {
                        if (despesasReceitas.Codigo != 17 && despesasReceitas.Codigo != 61)
                        {
                            despesasReceitas.Valor = despesasReceitas.Valor * -1;
                        }

                        cliente.Posto.Encaixes.Add(new DespReceita
                        {
                            CodEcxDcx = despesasReceitas.Codigo,
                            Valor = despesasReceitas.Valor,
                            MesInicial = despesasReceitas.MesInicial,
                            MesFinal = despesasReceitas.MesFinal,
                        });
                    }
                }

                #endregion

                #region ... Encaixes : Antecipações e Postecipações ...

                // Antecipação de Aluguel
                if(parametros.AntecipacaoAluguel != null && parametros.AntecipacaoAluguel.Length > 0)
                {
                    foreach(var antecipacao in parametros.AntecipacaoAluguel)
                    {
                        cliente.Posto.Encaixes.Add(new AntecAluguel
                        {
                            CodEcxDcx = TipoCodEcxDcx.PGTO_ANT_DE_ALUGUEL,
                            MesConcessao = antecipacao.MesConcessao,
                            Valor = antecipacao.Valor
                        });
                    }
                }

                // Antecipação de Aluguel CTF
                if (parametros.AntecipacaoAluguelCtf != null && parametros.AntecipacaoAluguelCtf.Length > 0)
                {
                    foreach (var antecipacao in parametros.AntecipacaoAluguelCtf)
                    {
                        cliente.Posto.Encaixes.Add(new AntecAluguel
                        {
                            CodEcxDcx = TipoCodEcxDcx.ALUGUEL_ANTECIPADO_CTF,
                            MesConcessao = antecipacao.MesConcessao,
                            Valor = antecipacao.Valor
                        });
                    }
                }

                // Bonificação Antecipada
                if (parametros.BonificacaoAntecipadaTir != null && parametros.BonificacaoAntecipadaTir.Length > 0)
                {
                    foreach (var bonificacao in parametros.BonificacaoAntecipadaTir)
                    {
                        cliente.Posto.Encaixes.Add(new AntecAluguel
                        {
                            CodEcxDcx = TipoCodEcxDcx.BONIFICACAO_ANTECIPADA,
                            MesConcessao = bonificacao.MesConcessao,
                            Valor = bonificacao.Valor
                        });
                    }
                }

                // Bonificação Postecipada
                if (parametros.BonificacaoPostecipadaTir != null && parametros.BonificacaoPostecipadaTir.Length > 0)
                {
                    foreach (var bonificacao in parametros.BonificacaoPostecipadaTir)
                    {
                        cliente.Posto.Encaixes.Add(new AntecAluguel
                        {
                            CodEcxDcx = TipoCodEcxDcx.BONIFICACAO_POSTECIPADA,
                            Valor = bonificacao.Valor,
                            VolumeInicial = bonificacao.VolumeInicial,
                            VolumeParcial = bonificacao.VolumeParcial,
                            QuantidadeParcelas = bonificacao.QuantidadeParcelas,
                            MesConcessao = bonificacao.MesConcessao
                        });
                    }
                }

                #endregion

                #region ... Encaixes : Equipamento 

                // Equipamentos
                if (parametros.Equipamentos != null && parametros.Equipamentos.Length > 0)
                {
                    foreach (var equipamento in parametros.Equipamentos)
                    {
                        cliente.Posto.Encaixes.Add(new Equipamento
                        {
                            CodEcxDcx = equipamento.Codigo,
                            MesConcessao = equipamento.MesInstalacao,
                            Valor = equipamento.Valor
                        });
                    }
                }

                #endregion
                
                #region ... Dados Base : Negociação ...

                // Criando negociação
                var negociacao = new Negcc
                {
                    NomeNegociacao = string.Empty,
                    TIR = new TIRNegociacao
                    {
                        Cliente = cliente,
                        Categoria = parametros.Categoria,
                        ValorFAO = parametros.ValorFao
                    },
                };

                // Tempo de Contrato
                negociacao.TIR.ContrExistente = parametros.TempoDeContratoExistente;
                negociacao.TIR.ContrAdicional = parametros.TempoDeContratoAdicional;
                negociacao.TIR.PeriodoMatura = parametros.TempoDePeriodoMatura;

                // Adiciona código de Gerencia de Vendas
                negociacao.TIR.UnidOrg = parametros.CodigoDeGerenciaDeVendas;

                #endregion

                var proposta = new Proposta
                {
                    Negociacoes = new List<Negcc>()
                    {
                        negociacao
                    }
                };

                // Calcular TIR
                var connectionString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
                var calculadora = new CalculoTIRProposta(connectionString);
                var resultado = calculadora.CalcularTIRProposta(proposta);

                stopwatch.Stop();

                return new ResultadoTir
                {
                    Npv = resultado.NPV,
                    Payback = resultado.Payback,
                    Tir = resultado.TIR,
                    TimeElapsed = stopwatch.Elapsed.Milliseconds,
                    Success = true,
                    Message = SUCCESS_MESSAGE
                };
            }
            catch (KeyNotFoundException ex)
            {
                if (stopwatch.IsRunning)
                {
                    stopwatch.Stop();
                }

                var ctx = WebOperationContext.Current;
                if (ctx != null) ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.BadRequest;

                return new ResultadoTir
                {
                    Success = false,
                    Message = ex.Message,
                    TimeElapsed = stopwatch.Elapsed.Milliseconds
                };
            }
            catch (Exception ex)
            {
                if (stopwatch.IsRunning)
                {
                    stopwatch.Stop();
                }

                var ctx = WebOperationContext.Current;
                if (ctx != null) ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError;

                return new ResultadoTir
                {
                    Success = false,
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                    TimeElapsed = stopwatch.Elapsed.Milliseconds
                };
            }
        }

        private static void ValidateXmlProperties(ParametrosTir parametros)
        {
            #region ... Validação dados Base ...

            if (parametros.ValorFao <= 0)
            {
                throw new KeyNotFoundException(string.Format(ERRO_VALOR_MENOR_OU_IGUAL_A_ZERO,
                    Helpers.Helpers.GetPropertyName(() => parametros.ValorFao)));
            }

            if (string.IsNullOrWhiteSpace(parametros.UF))
            {
                throw new KeyNotFoundException(string.Format(ERRO_VALOR_NULO_OU_EM_BRANCO,
                    Helpers.Helpers.GetPropertyName(() => parametros.UF)));
            }

            if (string.IsNullOrWhiteSpace(parametros.Segmento))
            {
                throw new KeyNotFoundException(string.Format(ERRO_VALOR_NULO_OU_EM_BRANCO,
                    Helpers.Helpers.GetPropertyName(() => parametros.Segmento)));
            }

            if (string.IsNullOrWhiteSpace(parametros.Perfil))
            {
                throw new KeyNotFoundException(string.Format(ERRO_VALOR_NULO_OU_EM_BRANCO, 
                    Helpers.Helpers.GetPropertyName(() => parametros.Perfil)));
            }

            if (parametros.Produtos == null)
            {
                throw new KeyNotFoundException(string.Format(ERRO_VALOR_NULO_OU_EM_BRANCO,
                    Helpers.Helpers.GetPropertyName(() => parametros.Produtos)));
            }

            if (parametros.Categoria <= 0)
            {
                throw new KeyNotFoundException(string.Format(ERRO_VALOR_MENOR_OU_IGUAL_A_ZERO,
                    Helpers.Helpers.GetPropertyName(() => parametros.Categoria)));
            }

            if (string.IsNullOrWhiteSpace(parametros.CodigoDeGerenciaDeVendas))
            {
                throw new KeyNotFoundException(string.Format(ERRO_VALOR_NULO_OU_EM_BRANCO, 
                    Helpers.Helpers.GetPropertyName(() => parametros.CodigoDeGerenciaDeVendas)));
            }

            if (string.IsNullOrWhiteSpace(parametros.CodigoBaseSupridora))
            {
                throw new KeyNotFoundException(string.Format(ERRO_VALOR_NULO_OU_EM_BRANCO, 
                    Helpers.Helpers.GetPropertyName(() => parametros.CodigoBaseSupridora)));
            }

            #endregion


            #region ... Validação : Franquias - AmPm ...

            if (parametros.HasAmPm)
            {
                if(parametros.AmPm == null)
                    throw new KeyNotFoundException(string.Format(ERRO_VALOR_NULO_OU_EM_BRANCO, 
                        Helpers.Helpers.GetPropertyName(() => parametros.AmPm)));
            }

            #endregion

            #region ... Validação : Franquias - JetOil ...

            if (parametros.HasJetOil)
            {
                if (parametros.JetOil == null)
                    throw new KeyNotFoundException(string.Format(ERRO_VALOR_NULO_OU_EM_BRANCO,
                        Helpers.Helpers.GetPropertyName(() => parametros.JetOil)));
            }

            #endregion

            #region ... validação : Franquias - JetOilMotos ...

            if (parametros.HasJetOilMotos)
            {
                if (parametros.JetOilMotos == null)
                    throw new KeyNotFoundException(string.Format(ERRO_VALOR_NULO_OU_EM_BRANCO,
                        Helpers.Helpers.GetPropertyName(() => parametros.JetOilMotos)));
            }

            #endregion

        }
    }
}
