﻿
using System;
namespace tir.dao.Entidades
{
    public class MargemPrz
    {
        #region Propriedades
        public string CodigoGV { get; set; }
        public string SiglaUF { get; set; }
        public string SiglaTipoNegocio { get; set; }
        public Int16 PerfilPosto { get; set; }
        public string CodigoProduto { get; set; }
        public Int16 NumeroMesFaixa { get; set; }
        public double ValorMargemDistribuicao { get; set; }
        public decimal ValorMargemDGasEquip { get; set; } //todo: ver com Felipe
        public decimal ValorPrecoCompra { get; set; }
        public decimal ValorPrecoVenda { get; set; }
        public Single? QuantidadeDiasEstoque { get; set; }
        public Single QuantidadeDiasCompra { get; set; }
        public double? ValorAjustePrecoVenda { get; set; }
        #endregion
    }
}
