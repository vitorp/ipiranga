﻿using System;

namespace tir.dao.Entidades
{
    public class ExDxComp
    {
        #region Propriedades
        public decimal CodigoEncaixeDesencaixe { get; set; }
        public string NomeEncaixeDesencaixe { get; set; }
        public Boolean IndicadorVerbaInvestimento { get; set; }
        #endregion
    }
}
