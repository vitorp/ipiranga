﻿using System;
using System.Collections.Generic;
using tir.dao;
using tir.dao.Entidades;
using TIR.Constantes;
using TIR.Entidades;
using TIR.Entidades.Componentes;
using TIR.Entidades.Encaixes;

namespace TIR.Bo.Calculos
{
    public class CalculoMargem : ICalculo
    {
        public double[] Calcular(Encaixe enc, TIRNegociacao tir)
        {
            try
            {
                //converte encaixe e define posto
                Margem margem = (Margem)enc;
                AreaAbastecimento areaAbast = tir.Cliente.Posto; // (AreaAbastecimento)tir.Cliente.Componentes[0];

                //define variáveis            
                string indProd = String.Format("{0:000}", margem.CodProduto);
                int indiceProd = areaAbast.Produtos.FindIndex(p => p.CodProduto == margem.CodProduto);
                bool indCompressProp = areaAbast.Produtos[indiceProd].IndCompressProp;
                float prazoDias = areaAbast.Produtos[indiceProd].PrazoDias;
                //int prazoTotal = tir.ContrAdicional + tir.ContrExistente + 1;
                //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
                //int prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 1;
                //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
                int prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 1;
                int contExist = tir.ContrExistente;
                string tipoNegocio = tir.Cliente.segmento;
                int localiz = Convert.ToInt32(tir.Cliente.Perfil);
                string codGerVenda = tir.UnidOrg;
                string codGV = codGerVenda;
                string codBase = string.Empty;

                //define a base supridora
                if (tir.Cliente.BasesSupridoras.Count > 0)
                {
                    codBase = tir.Cliente.BasesSupridoras[0].Codigo;
                }

                //redimensiona fluxo
                margem.Fluxo = new double[prazoTotal + 1];

                //--- busca parâmetros de negociação para definir flag de verificação de prazo mínimo
                ParamNegDao paramDAO = new ParamNegDao();
                ParamNeg paramNeg = paramDAO.ObterParametro(tir.Categoria, tipoNegocio, localiz);
                bool isVerificaPrazoMinimo = (bool)paramNeg.IndicadorPrazoMinimo;

                //--- obtendo o desconto mínimo
                float descMin = areaAbast.Produtos[indiceProd].DescMargem;

                //somente para descontos positivos
                if (descMin > 0.0)
                {
                    //busca desconto mínimo na tabela
                    DescontoMinDao descDAO = new DescontoMinDao();
                    DescontoMin desc = descDAO.ObterDescontoMinimo(tipoNegocio, indProd);

                    //se há resultado
                    if (desc != null)
                    {
                        if (desc.PrecoDescontoMinimo > 0)
                        {
                            descMin = (float)desc.PrecoDescontoMinimo;
                        }
                    }
                }

                //--- busca prazos mínimos por faixa de volume e faixas de preços na tabela de margens e prazos
                PrazosMinDao prazosDAO = new PrazosMinDao();
                MargemPrzDao margemDAO = new MargemPrzDao();
                //Alterado por Luiz Felipe - mar/2015
                //Passa a usar o código da GV (se for Revenda) ou da Coordenadoria (se for Consumo) no método
                //List<PrazosMin> prazos = prazosDAO.ListarPorFaixaVolume(tipoNegocio, localiz, indProd);
                List<PrazosMin> prazos = prazosDAO.ListarPorFaixaVolume(codGV, tir.Cliente.Endereco.UF, tipoNegocio, localiz, indProd);
                List<MargemPrz> margens = margemDAO.ListarMargemPrazo(codGV, tir.Cliente.Endereco.UF, tipoNegocio, localiz, indProd);

                //busca informações na tabela de parâmetros
                OutrosParamDao outrosDAO = new OutrosParamDao();
                OutrosParam outrosPis = outrosDAO.ObterParametro("PrazoPis");
                OutrosParam outrosFin = outrosDAO.ObterParametro("PrazoFin");
                OutrosParam outrosAliqPis = outrosDAO.ObterParametro("AliqPis");
                OutrosParam outrosAliqFin = outrosDAO.ObterParametro("AliqFin");
                OutrosParam outrosPercIr = outrosDAO.ObterParametro("PercIR");
                int prazoPis = (int)outrosPis.ValorParametroNumerico;
                int prazoFin = (int)outrosFin.ValorParametroNumerico;
                float aliqPis = (float)outrosAliqPis.ValorParametroNumerico;
                float aliqFin = (float)outrosAliqFin.ValorParametroNumerico;
                double aliqIr = (double)outrosPercIr.ValorParametroNumerico;
                
                double fator = 0.0;
                //se o produto for gás natural (cod=116), lê o fator de redução
                if (indCompressProp.Equals(false))
                {
                    if (margem.CodProduto.Equals(TipoCodProduto.GAS_NATURAL))
                    {
                        //lê fator de redução
                        FatorReducaoGNVDao fatorDAO = new FatorReducaoGNVDao();
                        FatorReducaoGNV gnv = fatorDAO.ObterFatorReducaoGNV(codGV, tir.Cliente.Endereco.UF);
                        fator = (double)gnv.FatReducaoGNV;
                    }
                }

                //--- verifica prazos mínimos por faixa de volume - lê tabela
                float diasTabela = 0;

                for (int i = 0; i < prazos.Count; i++)
                {
                    if (prazos[i].VolumeFaixa.Equals(0.0))
                    {
                        break;
                    }

                    if (areaAbast.TodosVolumes[prazoTotal - 1] <= prazos[i].VolumeFaixa || i.Equals(prazos.Count - 1))
                    {
                        diasTabela = (float)prazos[i].QuantidadeDiasMinimo;
                        break;
                    }
                }

                //--- alteração em 06/10/2005 - felipe e siva
                if (isVerificaPrazoMinimo.Equals(true))
                {
                    if (diasTabela <= prazoDias)
                    {
                        diasTabela = prazoDias;
                    }
                    else
                    {
                        areaAbast.Produtos[indiceProd].PrazoDias = diasTabela;
                    }
                }
                else
                {
                    diasTabela = prazoDias;
                }

                //--- Cálculo das margens --------------------
                int mesValido = 0;

                for (int i = 1; i < prazoTotal; i++)
                {
                    mesValido++;

                    //--- custo por GV e taxa de movimentação (custo da base)
                    CustosDao custoDAO = new CustosDao();
                    Custos custo = custoDAO.ObterFaixaCusto(codGV, tir.Cliente.Endereco.UF, tipoNegocio, indProd, localiz, i);
                    double custoProd = 0;
                    if (custo != null)
                    {
                        custoProd = (double)custo.ValorCusto;
                    }
                    CustoDaBaseDao baseDAO = new CustoDaBaseDao();
                    CustoDaBase custoBase = baseDAO.ObterFaixaCustoBase(codBase, tipoNegocio, indProd, i);
                    double tCustoBase = (double)custoBase.ValorTaxaMovimentacao;
                    double custoTotal = areaAbast.Produtos[indiceProd].FluxoVol[i] * (custoProd + tCustoBase);

                    double precoVenda = 0.0;
                    double precoCompra = 0.0;
                    float prazoCompra = 0;
                    double margemDist1 = 0.0;

                    //--- acha valores conforme a faixa
                    for (int j = 0; j < margens.Count; j++)
                    {
                        if (margens[j].NumeroMesFaixa.Equals(0))
                        {
                            break;
                        }

                        if (i <= margens[j].NumeroMesFaixa || j.Equals(margens.Count - 1))
                        {
                            precoVenda = (double)margens[j].ValorPrecoVenda;
                            precoCompra = (double)margens[j].ValorPrecoCompra;
                            prazoCompra = (float)margens[j].QuantidadeDiasCompra;
                            margemDist1 = (double)margens[j].ValorMargemDistribuicao;

                            break;
                        }
                    }

                    //Se Gás Natural (cod=116) e não tiver compressor, usar redução
                    if (margem.CodProduto.Equals(TipoCodProduto.GAS_NATURAL) && indCompressProp.Equals(false))
                    {
                        margemDist1 = margemDist1 * fator;
                    }

                    //--- Margem tributada
                    double margemTrib = (margemDist1 * (1.0 - descMin / 100.0) * areaAbast.Produtos[indiceProd].FluxoVol[i]) - custoTotal;

                    //--- Dias de exposição
                    double diasExpo = (precoCompra / precoVenda * prazoCompra) - diasTabela;

                    float inflacao = 0;
                    //--- Cálculo da inflação mensal
                    if (tir.Inflacao[i - 1].Equals(0.0))
                    {
                        inflacao = (float)tir.Inflacao[i];
                    }
                    else
                    {
                        inflacao = (float)(tir.Inflacao[i] / tir.Inflacao[i - 1]);
                    }

                    //--- Margem líquida
                    /*double margemLiq = ((margemDist1 * (1.0 - descMin / 100.0) + precoVenda *
                                 (1.0 - (1.0 / (Math.Pow(inflacao, (diasExpo / 30.0))))) + precoVenda * (aliqPis *
                                 (1.0 - (1.0 / (Math.Pow(inflacao, (prazoPis / 30.0)))))) + precoVenda * (aliqFin *
                                 (1.0 - (1.0 / (Math.Pow(inflacao, (prazoFin / 30.0))))))) * areaAbast.Produtos[indiceProd].FluxoVol[i]) - custoTotal;*/
                    //Alterado por Luiz Felipe em nov/2014, a pedido da área financeira
                    double margemLiq = ((margemDist1 * (1.0 - descMin / 100.0) * areaAbast.Produtos[indiceProd].FluxoVol[i])) - custoTotal;
                    margem.Fluxo[i] = margemLiq - margemTrib * aliqIr / 100.0;
                }

                //retorno com sucesso
                return margem.Fluxo;
            }
            catch (Exception ex)
            {
                throw new Exception("CalculoMargem.calcular: Erro ao calcular margem.\n" + ex.Message, ex);
            }
        }
    }
}
