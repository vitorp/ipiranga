﻿using System;
using System.Data;
//using System.Diagnostics.Contracts;
using System.Text;
using daobase;
using tir.dao.Entidades;

namespace tir.dao
{
    public class GrupoProdDao : DaoBase<GrupoProd>
    {
        #region Métodos Públicos
        public GrupoProd ObterGrupoProduto(string codGrupoProdu)
        {
            //#region Contracts
            ////Contract.Requires(codGrupoProdu != null, Mensagens.GrupoProduto.VAL_CODIGO_GRUPO);
            //#endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select g.Cd_Grupo_Prod, ");
                sb.Append("g.Nm_Grupo_Prod ");
                sb.Append("from GrupoProduto g ");
                sb.AppendFormat("where g.Cd_Grupo_Prod = {0} ", DaoUtil.FormatarParametro("codGrupoProdu"));

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codGrupoProdu"), codGrupoProdu, typeof(String), 3));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //se há resultado
                if (dr.Read())
                {
                    //valor de retorno
                    return MontarObjeto(dr);
                }
                else
                {
                    //valor de retorno nulo
                    return null;
                }
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("GrupoProdDao.ObterGrupoProduto",
                //"Erro ao obter o Grupo do Produto pelo código do Grupo.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }
        #endregion

        #region Overrides

        protected override GrupoProd MontarObjeto(IDataReader dr)
        {
            GrupoProd ent = new GrupoProd();
            try
            {
                ent.CodigoGrupoProduto = (string)dr["Cd_Grupo_Prod"];
                ent.NomeGrupoProduto = (string)dr["Nm_Grupo_Prod"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
