﻿using System;
using tir.dao;
using tir.dao.Entidades;
using TIR.Constantes;
using TIR.Entidades;
using TIR.Entidades.Componentes;
using TIR.Entidades.Encaixes;

namespace TIR.Bo.Calculos
{
    public class CalculoEquipamento : ICalculo
    {
        #region Propriedades
        public Franquia Franquia { get; set; }
        #endregion

        public double[] Calcular(Encaixe enc, TIRNegociacao tir)
        {
            int contratoFranquia = 0;
            bool isFranquiaRenovacao = false;

            //--- verificar tipo de franquia pelo objeto
            if (Franquia != null)
            {
                //define o tempo de contrato
                contratoFranquia = Franquia.TempoContrato;

                //de acordo com o tipo de negociação da franquia
                if (Franquia.TpNegFranquia.Equals(2) || Franquia.TpNegFranquia.Equals(1))
                {
                    //marca renovação
                    isFranquiaRenovacao = true;
                }
            }
            else
            {
                //zera o contrato da franquia
                contratoFranquia = 0;
            }

             //--- busca percentual de imposto de renda e percentual de reinvestimento
            OutrosParamDao outrosDAO = new OutrosParamDao();
            OutrosParam outros = outrosDAO.ObterParametro("PercIR");
            double percIr = (double)outros.ValorParametroNumerico;

            //***Alterado por Luiz Felipe em fev/2015
            //***Passa a pegar valores relativos à Depreciação de tabela específica
            //outros = outrosDAO.ObterParametro("PercReinvest");
            //double percReinvest = (double)outros.ValorParametroNumerico;

            //Valores default para Equipamento
            double percReinvest = 25;
            int numMesesDeprec = 120;
            int numMesesReinvest = 120;
            string vendaFinal = "S";
            //Busca parâmetros de Depreciação
            DepreciacaoDao deprecDAO = new DepreciacaoDao();
            Depreciacao deprec = deprecDAO.ObterDepreciacao((Int16)enc.CodEcxDcx);           
            //Se encontrou parâmetros de Depreciação no banco,
            //usa esses valores; caso contrário, usa o default
            if (deprec != null)
            {
                percReinvest = deprec.PercReinvestimento;
                numMesesDeprec = deprec.MesesDepreciacao;
                numMesesReinvest = deprec.MesesReinvestimento;
                vendaFinal = deprec.VendaFinal;
            }
            //***

            //define o valor em FAO
            double valorEmFao = enc.Valor / tir.ValorFAO; 

            int prazoTotal = 0;

            //se não tiver franquia
            if (contratoFranquia.Equals(0))
            {
                //define o prazo total
                //prazoTotal = tir.ContrAdicional + tir.ContrExistente + 1;
                //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
                prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 1;
                //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
                //if (tir.ContrAdicional < 120)
                    prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 1;

            }
            else
            {
                //define o prazo quando tem franquia
                prazoTotal = contratoFranquia + 1;
            }

            //Junho/2004 - retirado o fluxo de equipamento começando a partir
            //do término do contrato existente. Agora sempre começa junto com
            //o contrato existente.
            //prazoTotal = tir.ContrAdicional + tir.ContrExistente + 1;
            //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
            prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 1;
            //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
            //if (tir.ContrAdicional < 120)
                prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 1;

            //--- Migração de Franquias maio/2004 Paula/Andrea/Carlos
            if (tir.Categoria.Equals(41))
            {
                //redefine o prazo total
                //prazoTotal = tir.ContrAdicional + tir.ContrExistente + 1;
                //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
                prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 1;
                //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
                //if (tir.ContrAdicional < 120)
                    prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 1;
            }

            //redimensiona o vetor de fluxo
            //enc.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + 2];
            //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
            //enc.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 2];
            //Alterado em fev/2015 para incluir os meses sem venda no dimensionamento do fluxo
            enc.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 2];

            //verifica se o número de meses mínimo é obedecido
            if (prazoTotal < 2)
            {
                throw new Exception("CalculoEquipamento.calcular: Erro ao calcular equipamento. Os prazos dos contratos precisam ser especificados.");
            }

            //define meses de início e de saída
            int mesSaida = enc.MesConcessao;
            int mesInicio = mesSaida + 1;

            /* Comentado em jul/2014 - Equipamentos devem sair no mês de concessão
            //verifica se tem período de maturação
            if (tir.PeriodoMatura > 0 && enc.MesConcessao <= 0)
            {
                //redefine meses de início e de saída
                mesSaida = tir.PeriodoMatura;
                mesInicio = mesSaida + 1;
            }*/

            //verifica se o mês de concessão está coerente com os contratos
            if (mesSaida > prazoTotal - 1 || mesInicio > prazoTotal - 1)
            {
                throw new Exception("CalculoEquipamento.calcular: Erro ao calcular equipamento. O mês de concessão para equipamento está maior que a soma dos contratos.");
            }

            double valorDeprec = 0.0;

            /*//dependendo do encaixe
            if (enc.CodEcxDcx.Equals(TipoCodEcxDcx.AQ_EQTO_SISTEMA_AUTOMACAO_CS) || 
                enc.CodEcxDcx.Equals(TipoCodEcxDcx.INSTAL_EQUIPAMENTO_CTF_HW_SW) || 
                enc.CodEcxDcx.Equals(TipoCodEcxDcx.INST_EQTO_SISTEMA_AUTOMACAO_CS) || 
                enc.CodEcxDcx.Equals(TipoCodEcxDcx.EQUIPAMENTO_CTF_HW_SW))
            {
                //define depreciação em 5 anos
                //alt incluida em fev/2002 Carlos Almeida(ass plan mkt)
                valorDeprec = valorEmFao / 60.0;    //5 anos
            }
            else
            {
                //demais encaixes - define depreciação em 10 anos
                valorDeprec = valorEmFao / 120.0;   //10 anos
            }*/
            //Alterado por Luiz Felipe - fev/2015
            //Valores de relativos à Depreciação agora são lidos do banco
            //valorDeprec = valorEmFao / numMesesDeprec;
            double valDeprec = valorEmFao / numMesesDeprec;

            //--- Acerto em julho/2005 para não inflacionar a saida do valor de equipamento - sivirino
            //'mFluxo(tMesSaida) = tValoremFAO * (-1)
            enc.Fluxo[mesSaida] = valorEmFao * (-1.0) / tir.Inflacao[mesSaida];

            if (!enc.CodEcxDcx.Equals(TipoCodEcxDcx.RE_INSTALACAO))
            {
                //fev/2002 quando reinstalação de equipto usado
                //não tem receita, somente custo
                //double valorResidual = enc.Fluxo[mesSaida] * (-1.0);
                //Alterado em mar/2015 para não deflacionar o valor original
                double valorResidual = valorEmFao;
                double residuoDeprec = valorEmFao;
                double residuoReinvest = 0.0;
                double valReinvest = 0.0;
                int mesesDeprec = 0;
                double valorReinvest = 0.0;

                //varre os meses
                for (int i = mesInicio; i <= prazoTotal - 1; i++)
                {
                    //***Teste incluído em jul/2014 por Luiz Felipe:
                    //Só começa a depreciar a partir do final dos meses sem venda
                    if (i > tir.PeriodoMatura)
                    {
                        //contador da depreciação
                        mesesDeprec++;

                        /*//de acordo com o encaixe calcula a depreciação
                        if (enc.CodEcxDcx.Equals(TipoCodEcxDcx.AQ_EQTO_SISTEMA_AUTOMACAO_CS) ||
                            enc.CodEcxDcx.Equals(TipoCodEcxDcx.INSTAL_EQUIPAMENTO_CTF_HW_SW) ||
                            enc.CodEcxDcx.Equals(TipoCodEcxDcx.INST_EQTO_SISTEMA_AUTOMACAO_CS) ||
                            enc.CodEcxDcx.Equals(TipoCodEcxDcx.EQUIPAMENTO_CTF_HW_SW))
                        {
                            //equip com depreciação em 5 anos - fev/2002
                            if (mesesDeprec.Equals(61) || mesesDeprec.Equals(121) || mesesDeprec.Equals(181) || mesesDeprec.Equals(241))
                            {
                                valorReinvest = valorEmFao * (-1.0) * percReinvest / 100.0;
                                enc.Fluxo[i - 1] += valorReinvest;
                                valorResidual = valorReinvest * (-1.0);
                                //valorDeprec = valorReinvest * (-1.0) / 60.0;  //5 anos
                                valorDeprec = valorReinvest * (-1.0) / numMesesDeprec;  //5 anos
                            }
                        }
                        else
                        {
                            //equipamentos com depreciação em 10 anos
                            if (mesesDeprec.Equals(121) || mesesDeprec.Equals(241))
                            {
                                valorReinvest = valorEmFao * (-1.0) * percReinvest / 100.0;
                                enc.Fluxo[i - 1] += valorReinvest;
                                valorResidual = valorReinvest * (-1.0);
                                //valorDeprec = valorReinvest * (-1.0) / 120.0; //10 anos
                                valorDeprec = valorReinvest * (-1.0) / numMesesDeprec; //10 anos
                            }
                        }*/

                        //enc.Fluxo[i] = valorDeprec / tir.Inflacao[i] * percIr / 100.0;
                        //valorResidual -= valorDeprec;
                        enc.Fluxo[i] = (valDeprec + valReinvest) / tir.Inflacao[i] * percIr / 100.0;
                        residuoDeprec -= valDeprec;
                        residuoReinvest -= valReinvest;

                        //Alterado por Luiz Felipe - fev/2015
                        //Calcula de acordo com os meses de Depreciação e inflaciona
                        //if ((mesesDeprec) > 0 && ((mesesDeprec) % numMesesDeprec) == 0)
                        //Alterado por Luiz Felipe - abr/2015
                        //Calcula de acordo com os meses de Reinvestimento e inflaciona
                        if ((mesesDeprec) > 0 && ((mesesDeprec) % numMesesReinvest) == 0)
                        {
                            valorReinvest = valorEmFao * (-1.0) * percReinvest / 100.0;
                            enc.Fluxo[i] += valorReinvest;
                            residuoReinvest = valorReinvest * (-1.0) * tir.Inflacao[i];
                            valReinvest = valorReinvest * (-1.0) / numMesesDeprec * tir.Inflacao[i];
                            //valorDeprec = valorReinvest * (-1.0) / numMesesDeprec * tir.Inflacao[i];
                        }

                        //Incluído por Luiz Felipe - abr/2015
                        //Calcula de acordo com a Depreciação e inflaciona
                        if ((mesesDeprec) > 0 && ((mesesDeprec) % numMesesDeprec) == 0)
                        {
                            //Novo valor de Depreciação é calculado pelo Reinvestimento
                            residuoDeprec = 0;
                            valDeprec = 0;
                            //valorDeprec = valorReinvest * (-1.0) / numMesesDeprec *tir.Inflacao[i];
                        }

                    }

                    if (!enc.CodEcxDcx.Equals(TipoCodEcxDcx.INSTAL_EQUIPAMENTO_CTF_HW_SW) &&
                        !enc.CodEcxDcx.Equals(TipoCodEcxDcx.INST_EQTO_SISTEMA_AUTOMACAO_CS))
                    {
                        //alterado em 20/04/2000 para não retornar o resíduo em instalações
                        //alterado em fev/2002, a pedido de Carlos Almeida(mk), somente
                        // não retornar para inst CTF e  inst.Sist.Autom
                        valorResidual = residuoDeprec + residuoReinvest;
                        enc.Fluxo[prazoTotal] = valorResidual / tir.Inflacao[prazoTotal];
                        //Incluido por Luiz Felipe - mar/2015
                        //Se o equipamento não for vendido ao final, aplica IR
                        if (vendaFinal == "N")
                        {
                            enc.Fluxo[prazoTotal] = valorResidual / tir.Inflacao[prazoTotal] * percIr / 100.0;
                        }
                    }
                    //Se está dentro dos meses sem venda, saída = 0
                    else
                    {
                        enc.Fluxo[i] = 0;
                    }
                }                
            }

            //retorno com sucesso
            return enc.Fluxo;
        }
    }
}
