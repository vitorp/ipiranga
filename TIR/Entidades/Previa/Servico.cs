﻿
namespace TIR.Entidades.Previa
{
    public class Servico
    {
        public string Nome { get; set; }
        public double Quantidade { get; set; }
        public string Valor { get; set; }
        public double ValorUnitario { get; set; }
        public string UnidadeMedida { get; set; }
    }
}
