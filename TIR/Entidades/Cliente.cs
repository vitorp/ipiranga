﻿using System.Collections.Generic;
using TIR.Entidades.Componentes;

namespace TIR.Entidades
{
    public class Cliente
    {
        #region Propriedades

        //Propriedades simples.
        public string segmento { get; set; }
        public string Perfil { get; set; }
        //public string cnpj { get; set; }
        public string razaoSocial { get; set; }
        //public string tipoCliente { get; set; }
        //public string PontoVenda { get; set; }
        //public string CompJDE { get; set; }
        //public string codGrupo { get; set; }
        //public string nomeGrupo { get; set; }
        //public string dataInclusao { get; set; }
        //public bool isProprTerreno { get; set; }
        //public string codTerreno { get; set; }
        //public bool isCompra5Meses { get; set; }
        //public DateTime dataBaixa { get; set; }
        //public string DV { get; set; }

        //Propriedades tipo classe.
        //public PlanilhaObras PlanilhaObras { get; set; }
        //public Bandeira Bandeira { get; set; }
        public Endereco Endereco { get; set; }
        //public Coordenadoria Coordenadoria { get; set; }
        //public TipoImagem TipoImagem { get; set; }
        //public MicroMercado MicroMercado { get; set; }
        public TipoNegocio TipoNegocio { get; set; }
        //public GerenciaVenda GerenciaVenda { get; set; }
        public GerenciaNacional GerenciaNacional { get; set; }
        //public ZonaVenda ZonaVenda { get; set; }
        //public Garantidor Garantidor { get; set; }
        //public Galonagem Galonagem { get; set; }

        //Atributos tipo coleção.
        //public List<Componente> Componentes { get; set; }
        public AreaAbastecimento Posto { get; set; }
        public AmPm AmPm { get; set; }
        public JetOil JetOil { get; set; }
        public JetOilMotos JetMotos {get; set;}
        public List<BaseSupridora> BasesSupridoras { get; set; }
        public List<DadosVolume> DadosVolume { get; set; }
        public List<Contratos> Contratos { get; set; }
        public List<FaixasDia> FaixasDia { get; set; }
        public List<Fiancas> Fiancas { get; set; }
        public List<Hipotecas> Hipotecas { get; set; }
        public List<Garantias> Garantias { get; set; }
        //public List<Justificativas> Justificativas { get; set; }

        #endregion
    }
}
