﻿using System;
using System.Data;
using tir.dao.Entidades;
using System.Text;
using System.Collections.Generic;

namespace tir.dao
{
    public class UnidadeDao : DaoBase<Unidade>
    {
        #region Métodos Públicos

        #endregion

        #region Overrides

        protected override Unidade MontarObjeto(IDataReader dr)
        {
            Unidade ent = new Unidade();
            try
            {
                ent.CodigoUnidade = (string)dr["cd_unid"];
                ent.CodigoRegiao = (decimal)dr["Cd_Regiao"];
                ent.NomeUnidade = (string)dr["Nm_unid"];
                ent.SiglaUF = (string)dr["Sg_UF"];
                ent.TaxaMov = (double)dr["Vr_Taxa_Mov"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
