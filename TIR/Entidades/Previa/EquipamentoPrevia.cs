﻿
namespace TIR.Entidades.Previa
{
    public class EquipamentoPrevia
    {
        public string Nome { get; set; }
        public int Quantidade { get; set; }
        public string Valor { get; set; }
        public double ValorUnitario { get; set; }
        public string UnidadeMedida { get; set; }
    }
}
