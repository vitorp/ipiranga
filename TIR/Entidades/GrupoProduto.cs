﻿
namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que representa um grupo de produtos.
    /// </summary>
    public class GrupoProduto
    {
        #region Propriedades

        public string Codigo { get; set; }
        public string Nome { get; set; }

        #endregion
    }
}
