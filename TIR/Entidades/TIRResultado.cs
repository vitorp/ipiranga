﻿using System.Collections.Generic;
using System;

namespace TIR.Entidades
{
    [Serializable()]
    public class TIRResultado
    {
        #region Propriedades

        public Cliente Cliente { get; set; }
        //public Elaborador Elaborador { get; set; }
        public double TIR { get; set; }
        public double NPV { get; set; }
        public int Payback { get; set; }
        public string Empresa { get; set; }
        public string UnidOrg { get; set; }
        public double[] Fluxo { get; set; }
        public double[] Inflacao { get; set; }
        //public string NomeProj { get; set; }
        //public string Versao { get; set; }
        //public string VersaoSistema { get; set; }
        //public string Responsavel { get; set; }
        //public string Observacao { get; set; }
        //public DateTime DataCriacao { get; set; }
        //public DateTime DataAlteracao { get; set; }
        public int Categoria { get; set; }
        public int[] MesMaturaVenda { get; set; }
        public float[] PercMaturaVenda { get; set; }
        public int ContrExistente { get; set; }
        public int ContrAdicional { get; set; }
        public int MesesCompVolume { get; set; }    //Criado por Luiz Felipe em fev/2015
        public double ValorFAO { get; set; }
        public int PeriodoMatura { get; set; }
        //public int TpInvest { get; set; }
        //public bool IsClienteDragged { get; set; }
        public bool IsMaturaVenda { get; set; }
        //public bool IsCalculo { get; set; }
        public bool IsEditada { get; set; }
        //public bool IsErro { get; set; }
        public bool IsIncremental { get; set; }
        //public bool IsTravada { get; set; }

        public List<MaturacaoVendaTir> Maturacao { get; set; }

        #endregion
    }
}
