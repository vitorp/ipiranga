﻿using System;

namespace tir.dao.Entidades
{
    public class DencxUnidNegcc
    {
        #region Propriedades

        public Nullable<Int16> CodigoUnidadeNegociacao { get; set; }
        public Nullable<Int16> CodigoTipoEncaixeDesencaixe { get; set; }

        #endregion
    }
}
