﻿using System;
using tir.dao;
using tir.dao.Entidades;
using TIR.Constantes;
using TIR.Entidades;
using TIR.Entidades.Componentes;
using TIR.Entidades.Encaixes;

namespace TIR.Bo.Calculos
{
    public class CalculoBenfeitoria : ICalculo
    {
        #region Propriedades
        public Franquia Franquia { get; set; }
        #endregion

        public double[] Calcular(Encaixe enc, TIRNegociacao tir)
        {
            try
            {
                int contratoFranquia = 0;
                bool isFranquiaRenovacao = false;

                //verificar o tipo de franquia pelo objeto
                if (Franquia != null)
                {
                    contratoFranquia = Franquia.TempoContrato;

                    if (Franquia.TpNegFranquia.Equals(2) || Franquia.TpNegFranquia.Equals(1))
                    {
                        isFranquiaRenovacao = true;
                    }
                }
                else
                {
                    contratoFranquia = 0;
                }

                //--- busca percentual de imposto de renda
                OutrosParamDao outrosDAO = new OutrosParamDao();
                OutrosParam ir = outrosDAO.ObterParametro("PercIR");

                //define o valor em FAO
                double valorEmFao = enc.Valor / tir.ValorFAO;

                int prazoTotal = 0;

                //se não tem franquia
                if (contratoFranquia.Equals(0))
                {
                    //define o prazo total pelos contratos adicional + existente
                    //prazoTotal = tir.ContrAdicional + tir.ContrExistente + 1;
                    //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
                    prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 1;
                    //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
                    //prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 1;
                }
                else
                {
                    //define o prazo total pelo tempo de contrato da franquia
                    //prazoTotal = contratoFranquia + 1;
                    //Alterado em jan/2015 por Luiz Felipe para considerar os meses sem venda
                    prazoTotal = contratoFranquia + tir.PeriodoMatura + 1;
                    //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
                    //prazoTotal = contratoFranquia + tir.PeriodoMatura + tir.MesesCompVolume + 1;
                }

                //--- Migração de Franquias maio/2004 Paula/Andrea/Carlos
                if (tir.Categoria.Equals(41))
                {
                    //redefine o prazo total
                    //prazoTotal = tir.ContrAdicional + tir.ContrExistente + 1;
                    //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
                    prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 1;
                    //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
                    //prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 1;
                }

                //redimensiona o vetor de fluxo de acordo com o tempo de contrato
                //enc.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + 2];
                //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
                //enc.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 2];
                //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
                enc.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 2];

                //verifica se o tempo de contrato está coerente para o cálculo
                if (prazoTotal < 2)
                {
                    throw new Exception("CalculoBenfeitoria.calcular: Erro ao calcular benfeitoria. Os prazos dos contratos precisam ser especificados.");
                }

                //define os meses de início e saída
                int mesSaida = enc.MesConcessao;
                int mesInicio = mesSaida + 1;

                //***Inserido em nov/2014 por Luiz Felipe a pedido do Financeiro
                //Acerto do início da depreciação com meses sem venda
                if (mesSaida < tir.PeriodoMatura)
                {
                    mesInicio = tir.PeriodoMatura + 1;
                }
                //***

                /* Comentado em jul/2014 - Benfeitorias devem começar no mês de concessão
               //verifica se tem período de maturação
               if (tir.PeriodoMatura > 0 && enc.MesConcessao <= 0 && !enc.CodEcxDcx.Equals(TipoCodEcxDcx.COMPRA_DE_BENFEITORIA))
               {
                   //redefine os meses de início e saída
                   mesSaida = tir.PeriodoMatura;
                   mesInicio = mesSaida + 1;
               }

               //de acordo com o mês de concessão
               if (enc.MesConcessao >= 0 && enc.MesConcessao <= tir.PeriodoMatura && !enc.CodEcxDcx.Equals(TipoCodEcxDcx.COMPRA_DE_BENFEITORIA))
               {
                   //se o mês de concessão for no mês zero
                   if (enc.MesConcessao.Equals(0))
                   {
                       //redefine os meses de início e saída
                       mesSaida = (int)(tir.PeriodoMatura / 2.0 + 0.5);
                       mesInicio = tir.PeriodoMatura + 1;
                   }
                   else
                   {
                       //redefine os meses de início e saída
                       mesSaida = (int)(enc.MesConcessao / 2.0 + 0.5);
                       mesInicio = enc.MesConcessao + 1;
                   }
               }*/

                //Obtém o indicador de depreciação em 25 anos
                ParamNegDao parNegDAO = new ParamNegDao();
                ParamNeg parNeg = parNegDAO.ObterParametro(tir.Categoria, tir.Cliente.segmento, Convert.ToInt16(tir.Cliente.Perfil));

                double valorDeprec = 0.0;
                //----- Se indicador de 25 anos=true é Posto Próprio
                if (parNeg.IndicadorDepreciacao25 == true)
                {
                    //Apenas nesse caso, considera os meses de compensação de volume no prazo
                    if (contratoFranquia.Equals(0))
                    {
                        prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 1;
                    }
                    else
                    {
                        prazoTotal = contratoFranquia + tir.PeriodoMatura + tir.MesesCompVolume + 1;
                    }

                    //deprecia em 25 anos (300 meses)
                    //valorDeprec = valorEmFao / 300.0;
                    //Alterado por Luiz Felipe - abr/2015
                    //Se flag de depreciação em 25 anos for verdadeiro,
                    //passa a ler o valor do banco de dados
                    //Valores default para Benfeitoria
                    double percReinvest = 0;
                    int numMesesDeprec = 324;
                    int numMesesReinvest = 0;
                    string vendaFinal = "N";
                    //Busca parâmetros de Depreciação
                    DepreciacaoDao deprecDAO = new DepreciacaoDao();
                    Depreciacao deprec = deprecDAO.ObterDepreciacao((Int16)enc.CodEcxDcx);  
                    //Se encontrou parâmetros de Depreciação no banco,
                    //usa esses valores; caso contrário, usa o default
                    if (deprec != null)
                    {
                        percReinvest = deprec.PercReinvestimento;
                        numMesesDeprec = deprec.MesesDepreciacao;
                        numMesesReinvest = deprec.MesesReinvestimento;
                        vendaFinal = deprec.VendaFinal;
                    }
                    valorDeprec = valorEmFao / numMesesDeprec;
                }
                else
                {
                    //***Incluído por Luiz Felipe em mar/2015
                    //Define por quantos meses deve ser considerada a depreciação
                    //(só se aplica se houver compensação de volume)
                    int mesesDeprec = 0;
                    if (tir.MesesCompVolume > 0)
                    {
                        mesesDeprec = prazoTotal - mesInicio;
                        //Se o período de depreciação for maior que o contrato, restringe ao tempo do contrato
                        if (mesesDeprec > (tir.ContrAdicional + tir.ContrExistente))
                        {
                            mesesDeprec = tir.ContrAdicional + tir.ContrExistente;
                            prazoTotal = mesInicio + mesesDeprec;
                        }
                    }
                    //***
                
                    //define o valor de depreciação
                    valorDeprec = valorEmFao / (prazoTotal - 1.0 - (mesInicio - 1.0));
                }

                //---- Alteração de renovação de franquia agosto/2002 e maio/2004 (migração)
                if (isFranquiaRenovacao.Equals(true) && !tir.Categoria.Equals(41))
                {
                    //redefine meses de início e saída e prazo total
                    mesSaida += tir.ContrExistente;
                    mesInicio += tir.ContrExistente;
                    prazoTotal += tir.ContrExistente;

                    if (enc.Fluxo.Length < prazoTotal)
                    {
                        throw new Exception("CalculoBenfeitoria.calcular: Meses de contrato das franquias está maior que os meses propostos preenchidos.");
                    }
                }

                //define fluxo do mês de saída
                enc.Fluxo[mesSaida] = valorEmFao * (-1.0) / tir.Inflacao[mesSaida];

                //varre meses
                for (int i = mesInicio; i <= prazoTotal - 1; i++)
                {
                    //***Teste incluído em jul/2014 por Luiz Felipe:
                    //Só começa a depreciar a partir do final dos meses sem venda
                    if (i > tir.PeriodoMatura)
                    {
                        //calcula fluxo do mês
                        enc.Fluxo[i] = valorDeprec / tir.Inflacao[i] * (double)ir.ValorParametroNumerico / 100.0;
                    }
                    else
                    {
                        enc.Fluxo[i] = 0;
                    }
                }

                //define fluxo do mês ao final do prazo
                //***Alterado por Luiz Felipe em mar/2015 para incluir IR e deflacionar
                //enc.Fluxo[prazoTotal] = valorEmFao - (valorDeprec * (prazoTotal - mesInicio));
                enc.Fluxo[prazoTotal] = (valorEmFao - (valorDeprec * (prazoTotal - mesInicio))) * 
                                        (double)ir.ValorParametroNumerico / 100.0 / tir.Inflacao[prazoTotal];

                //retorno com sucesso
                return enc.Fluxo;
            }
            catch (Exception ex)
            {
                throw new Exception("CalculoBenfeitoria.calcular: Erro ao calcular o fluxo para benfeitoria.\n" + ex.Message, ex);
            }
        }
    }
}
