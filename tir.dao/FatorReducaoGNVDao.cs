﻿using System;
using System.Data;
//using System.Diagnostics.Contracts;
using System.Text;
using daobase;
using tir.dao.Entidades;

namespace tir.dao
{
    public class FatorReducaoGNVDao : DaoBase<FatorReducaoGNV>
    {
        #region Métodos Públicos
        /// <summary>
        /// Obtem Fator de Redução GNV.
        /// </summary>
        /// <param name="codGV">Código da gerência de vendas.</param>
        /// <param name="siglaUF">Sigla da UF do Cliente.</param>
        /// <returns>Objeto FaotrReducaoGNV.</returns>
        public FatorReducaoGNV ObterFatorReducaoGNV(string codGV, string siglaUF)
        {
            #region Contracts
            //Contract.Requires(codGV != null, Mensagens.FatorReducaoGNV.VAL_CODIGO_GV);
            #endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select f.cd_gv, ");
                sb.Append("f.Sg_UF, ");
                sb.Append("f.fatorRedGNV ");
                sb.Append("from FatorReducaoGNV_UF f ");
                sb.AppendFormat("where f.cd_gv = {0} ", DaoUtil.FormatarParametro("codGV"));
                sb.AppendFormat("and f.Sg_UF = {0} ", DaoUtil.FormatarParametro("siglaUF"));

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codGV"), codGV, typeof(String), 6));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("siglaUF"), siglaUF, typeof(String), 2));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //se há resultado
                if (dr.Read())
                {
                    //valor de retorno
                    return MontarObjeto(dr);
                }
                else
                {
                    //valor de retorno nulo
                    return null;
                }
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("FatorReducaoGNVDao.ObterFatorReducaoGNV",
                //"Erro ao obter Fator de Redução GNV pelo código da GV.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }
        #endregion

        #region Overrides

        protected override FatorReducaoGNV MontarObjeto(IDataReader dr)
        {
            FatorReducaoGNV ent = new FatorReducaoGNV();
            try
            {
                ent.CodigoGV = (string)dr["cd_gv"];
                ent.SiglaUF = (string)dr["Sg_UF"];
                ent.FatReducaoGNV = (double)dr["fatorRedGNV"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
