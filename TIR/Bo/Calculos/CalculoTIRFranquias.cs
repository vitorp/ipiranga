﻿using System;
using Microsoft.VisualBasic;
using tir.dao;
using tir.dao.Entidades;
using TIR.Constantes;
using TIR.Entidades;
using TIR.Entidades.Componentes;
using TIR.Entidades.Encaixes;
using TIR.Entidades.ItensFranquias;
using TIR.Fabricas;

namespace TIR.Bo.Calculos
{
    public class CalculoTIRFranquias
    {
        public double[] Calcular(Componente comp, TIRNegociacao tir)
        {
            Franquia franquia = (Franquia)comp;

            //Determina o nome da Franquia
            string nomeFranquia = string.Empty;

            switch (franquia.CodTipoComp)
            {
                case TipoComponente.AMPM:

                    nomeFranquia = "AmPm";
                    break;

                case TipoComponente.JET_OIL:

                    nomeFranquia = "JetOil";
                    break;

                case TipoComponente.JET_OIL_MOTOS:

                    nomeFranquia = "JetOil Motos";
                    break;
            }

            bool calculouAlgo = false;

            string tpNeg = tir.Cliente.segmento;
            int localiz = Convert.ToInt32(tir.Cliente.Perfil);

            //int meses = tir.ContrExistente + tir.ContrAdicional + 2;
            //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
            //int meses = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 2;
            //Alterado em fev/2015 para incluir o período de compensação de Volume devido à Maturação
            int meses = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 2;

            franquia.Fluxo = new double[meses];

            //--- Verifica prazos dos contratos
            ParamNegDao paramDAO = new ParamNegDao();
            ParamNeg param = paramDAO.ObterParametro(tir.Categoria, tpNeg, localiz);

            TipoComDao tipoDAO = new TipoComDao();
            TipoCom tipo = tipoDAO.ObterPorCodigo(franquia.CodTipoComp);

            if (param.IndicadorDepreciacao25 == true)
            {
                //---Posto próprio
                if (franquia.TempoContrato < tipo.NumeroMinimoMesesProprio)
                {
                    throw new Exception(nomeFranquia + "::CalculoAntecAluguel.calcular: Erro ao calcular " + nomeFranquia + ". O tempo mínimo de contrato para " + nomeFranquia + " é de " + tipo.NumeroMinimoMesesProprio + " meses.");
                }

                if (franquia.TempoContrato > tipo.NumeroMaximoMesesProprio)
                {
                    throw new Exception(nomeFranquia + "::CalculoAntecAluguel.calcular: Erro ao calcular " + nomeFranquia + ". O tempo máximo de contrato para " + nomeFranquia + " é de " + tipo.NumeroMaximoMesesProprio + " meses.");
                }
            }
            else
            {
                //---Posto de Terceiros
                if (franquia.TempoContrato < tipo.NumeroMinimoMesesTerceiro)
                {
                    throw new Exception(nomeFranquia + "::CalculoAntecAluguel.calcular: Erro ao calcular " + nomeFranquia + ". O tempo mínimo de contrato para " + nomeFranquia + " é de " + tipo.NumeroMinimoMesesTerceiro + " meses.");
                }

                if (franquia.TempoContrato > tipo.NumeroMaximoMesesTerceiro)
                {
                    throw new Exception(nomeFranquia + "::CalculoAntecAluguel.calcular: Erro ao calcular " + nomeFranquia + ". O tempo máximo de contrato para " + nomeFranquia + " é de " + tipo.NumeroMaximoMesesTerceiro + " meses.");
                }
            }

            if (franquia.Encaixes != null && franquia.Encaixes.Count > 0)
            {
                foreach (Encaixe ecxDcx in franquia.Encaixes)
                {
                    //não calcula p/encaixes de produto
                    if (ecxDcx.CodEcxDcx < 90 || ecxDcx.CodEcxDcx > 92)
                    {
                        ecxDcx.Fluxo = CalculoFactory.Calcular(ecxDcx, comp, tir);
                        if (ecxDcx.Fluxo != null) calculouAlgo = true;

                        for (int i = 0; i < meses; i++)
                        {
                            franquia.Fluxo[i] = franquia.Fluxo[i] + ecxDcx.Fluxo[i];
                        }
                    }
                }
            }
            
            //--- Propaganda 1 por Franquia
            CalculoPropaganda cProp = new CalculoPropaganda();
            //Seta a franquia para o cálculo
            cProp.Franquia = franquia;
            franquia.Propaganda = new Propaganda();
            franquia.Propaganda.Fluxo = cProp.Calcular(franquia.Propaganda, tir);
            if (franquia.Propaganda.Fluxo != null)calculouAlgo = true;

            for (int i = 0; i < meses; i++)
            {
                franquia.Fluxo[i] = franquia.Fluxo[i] + franquia.Propaganda.Fluxo[i];
            }

            //--- Custo Terceirização
            CalculoTerceirizacao cTerc = new CalculoTerceirizacao();
            //Seta a franquia para o cálculo
            cTerc.Franquia = franquia;
            franquia.Terceirizacao = new Terceirizacao();
            franquia.Terceirizacao.Fluxo = cTerc.Calcular(franquia.Terceirizacao, tir);
            if (franquia.Terceirizacao.Fluxo != null)calculouAlgo = true;

            for (int i = 0; i < meses; i++)
            {
                franquia.Fluxo[i] = franquia.Fluxo[i] + franquia.Terceirizacao.Fluxo[i];
            }

            //--- Overhead
            CalculoOverhead cOver = new CalculoOverhead();
            //Seta a franquia para o cálculo
            cOver.Franquia = franquia;
            franquia.Overhead = new Overhead();
            franquia.Overhead.Fluxo = cOver.Calcular(franquia.Overhead, tir);
            if (franquia.Overhead.Fluxo != null) calculouAlgo = true;

            for (int i = 0; i < meses; i++)
            {
                franquia.Fluxo[i] = franquia.Fluxo[i] + franquia.Overhead.Fluxo[i];
            }

            //Apenas para AmPm
            if (nomeFranquia == "AmPm")
            {
                AmPm AmPm = (AmPm)franquia;

                //--- Royalty ARCO
                CalculoRoyaltiesARCO cRoyArco = new CalculoRoyaltiesARCO();
                cRoyArco.Franquia = franquia;
                AmPm.RoyaltiesArco = new RoyaltiesArco();
                AmPm.RoyaltiesArco.Fluxo = cRoyArco.Calcular(AmPm.RoyaltiesArco, tir);
                if (AmPm.RoyaltiesArco.Fluxo != null) calculouAlgo = true;

                for (int i = 0; i < meses; i++)
                {
                    franquia.Fluxo[i] = franquia.Fluxo[i] + AmPm.RoyaltiesArco.Fluxo[i];
                }

                //--- Royalty fixo
                CalculoRoyaltyFixo cRoyFixo = new CalculoRoyaltyFixo();
                cRoyFixo.Franquia = franquia;
                AmPm.RoyaltyFixo = new RoyaltyFixo();
                AmPm.RoyaltyFixo.Fluxo = cRoyFixo.Calcular(AmPm.RoyaltyFixo, tir);
                if (AmPm.RoyaltyFixo.Fluxo != null) calculouAlgo = true;

                for (int i = 0; i < meses; i++)
                {
                    franquia.Fluxo[i] = franquia.Fluxo[i] + AmPm.RoyaltyFixo.Fluxo[i];
                }
            }

            //--- Royalty Variável
            CalculoRoyaltyVar cRoyVar = new CalculoRoyaltyVar();
            cRoyVar.Franquia = franquia;
            franquia.RoyaltyVar = new RoyaltyVar();
            franquia.RoyaltyVar.Fluxo = cRoyVar.Calcular(franquia.RoyaltyVar, tir);
            if (franquia.RoyaltyVar.Fluxo != null) calculouAlgo = true;

            for (int i = 0; i < meses; i++)
            {
                franquia.Fluxo[i] = franquia.Fluxo[i] + franquia.RoyaltyVar.Fluxo[i];
            }

            //--- Capital de Giro Franquia - criado em out/2014 por Luiz Felipe
            CalculoCapGiroFranquia cCapGiroFranq = new CalculoCapGiroFranquia();
            cCapGiroFranq.Franquia = franquia;
            franquia.CapitalGiroFranquia = new CapitalGiroFranquia();
            franquia.CapitalGiroFranquia.Fluxo = cCapGiroFranq.Calcular(franquia.CapitalGiroFranquia, tir);
            if (franquia.CapitalGiroFranquia.Fluxo != null) calculouAlgo = true;

            for (int i = 0; i < meses; i++)
            {
                franquia.Fluxo[i] = franquia.Fluxo[i] + franquia.CapitalGiroFranquia.Fluxo[i];
            }

            //--- Taxa de Franquia
            CalculoTaxaFranquia cTaxa = new CalculoTaxaFranquia();
            cTaxa.Franquia = franquia;
            franquia.TaxaFranquia = new TaxaFranquia();
            franquia.TaxaFranquia.Fluxo = cTaxa.Calcular(franquia.TaxaFranquia, tir);
            if (franquia.TaxaFranquia.Fluxo != null) calculouAlgo = true;

            for (int i = 0; i < meses; i++)
            {
                franquia.Fluxo[i] = franquia.Fluxo[i] + franquia.TaxaFranquia.Fluxo[i];
            }

            if (calculouAlgo == false)
            {
                franquia.TIR = 0.0;
                franquia.NPV = 0.0;
                franquia.Payback = 0;

                return franquia.Fluxo;
            }

            //---- A tir é calculada por ano, por isso a fórmula abaixo

            try
            {
                //calcula o valor da tir
                double[] fluxo = franquia.Fluxo;
                franquia.TIR = (Math.Pow((1.0 + Financial.IRR(ref fluxo, 1.15151291201983E-02)), 12.0) - 1.0) * 100.0;
            }
            catch
            {
                //se tem erro na fórmula, define valor padrão para a TIR
                franquia.TIR = -999999.777777;
            }

            //---- Cálculo do NPV (valor presente)

            //variáveis de apoio
            double[] Awork = new double[franquia.Fluxo.GetUpperBound(0) + 1];
            double CustoAno;

            //varre fluxo
            for (int i = 1; i <= franquia.Fluxo.GetUpperBound(0); i++)
            {
                //copia fluxo no vetor de trabalho (atenção aos índices)
                Awork[i - 1] = franquia.Fluxo[i];
            }

            //busca custo de oportunidade
            OutrosParamDao outrosDAO = new OutrosParamDao();
            OutrosParam outros = outrosDAO.ObterParametro("CustoOport");

            //calcula custo anual
            CustoAno = CustoAno = Math.Pow((1.0 + (double)outros.ValorParametroNumerico / 100.0), (1.0 / 12.0)) - 1.0;

            //calcula npv
            franquia.NPV = (Financial.NPV(CustoAno, ref Awork) + franquia.Fluxo[0]) * tir.ValorFAO;

            //formata valor da tir para o caso em que não tenha sido possível calcular (extremos)
            if (Math.Abs(franquia.TIR) == 999999.777777 && franquia.NPV > 0.0)
            {
                franquia.TIR = 999999.777777;
            }

            //----- Cálculo do PayBack ----------

            //declaração de variáveis
            double wTaxa;
            double wSaldo;
            int wMaior;
            bool wFlag;

            //começa cálculo do saldo e da taxa
            wSaldo = franquia.Fluxo[0];
            wTaxa = Math.Pow((1.0 + CustoAno / 100.0), (1.0 / 12.0)) - 1.0;
            wMaior = -1;

            //seta flag como falso
            wFlag = false;

            //varre fluxo
            for (int i = 1; i < tir.Fluxo.GetUpperBound(0) + 1; i++)
            {
                if (i < franquia.Fluxo.GetUpperBound(0))
                {
                    //cálculo do saldo
                    wSaldo += franquia.Fluxo[i] / Math.Pow((1.0 + wTaxa), i);

                    if (wFlag == false)
                    {
                        if (wSaldo >= 0.0)
                        {
                            wMaior = i;
                            wFlag = true;
                        }
                    }
                    else
                    {
                        if (wSaldo < 0.0)
                        {
                            wFlag = false;
                        }
                    }
                }
            }

            //define payback
            franquia.Payback = wMaior;

            if (wFlag == false)
            {
                franquia.Payback = -1;
            }

            //retorno com sucesso
            return franquia.Fluxo;
        }
    }
}
