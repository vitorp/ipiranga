﻿
namespace tir.dao.Entidades
{
    public class FatorReducaoGNV
    {
        #region Propriedades
        public string CodigoGV { get; set; }
        public string SiglaUF { get; set; }
        public double FatReducaoGNV { get; set; }
        #endregion
    }
}
