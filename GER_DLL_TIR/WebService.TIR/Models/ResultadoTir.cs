﻿using System.Runtime.Serialization;

namespace WebService.TIR.Models
{
    [DataContract]
    public class ResultadoTir
    {
        [DataMember(Order = 0)]
        public double Tir { get; set; }

        [DataMember(Order = 1)]
        public double Npv { get; set; }

        [DataMember(Order = 2)]
        public double Payback { get; set; }

        [DataMember(Order = 3)]
        public int TimeElapsed { get; set; }

        [DataMember(Order = 4)]
        public bool Success { get; set; }

        [DataMember(Order = 5)]
        public string Message { get; set; }

        [DataMember(Order = 6)]
        public string StackTrace { get; set; }
    }
}