﻿using System;
using System.Collections.Generic;
using tir.dao;
using tir.dao.Entidades;
using TIR.Entidades;
using TIR.Entidades.Componentes;
using TIR.Entidades.Encaixes;
using TIR.Entidades.ItensFranquias;

namespace TIR.Bo.Calculos
{        
    public class CalculoRoyaltiesARCO : ICalculo
    {
        #region Propriedades
        public Franquia Franquia { get; set; }
        #endregion

        public double[] Calcular(Encaixe enc, TIRNegociacao tir)
        {
            try
            {
                //converte encaixe
                RoyaltiesArco royalty = (RoyaltiesArco)enc;

                royalty.Descricao = "Royalty Arco";

                //define prazo total e redimensiona fluxo
                //int prazoTotal = Franquia.TempoContrato + 1;
                //Incluídos os meses sem venda por Luiz Felipe - out/2014
                //int prazoTotal = Franquia.TempoContrato + tir.PeriodoMatura + 1;
                //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
                int prazoTotal = Franquia.TempoContrato + tir.PeriodoMatura + tir.MesesCompVolume + 1;

                //royalty.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + 2];
                //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
                //royalty.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 2];
                //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
                royalty.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 2];

                bool isFranquiaRenovacao = false;

                //--- Alteração asseguramento de franquias - Agosto/2002
                if (Franquia.TpNegFranquia.Equals(2) || Franquia.TpNegFranquia.Equals(1))
                {
                    isFranquiaRenovacao = true;
                }

                //busca alíquota de imposto de renda
                OutrosParamDao outrosDAO = new OutrosParamDao();
                OutrosParam outros = outrosDAO.ObterParametro("PercIR");
                float percIr = (float)outros.ValorParametroNumerico;

                //busca faixa de redutores e royalties
                RedutorFatDao redutorDAO = new RedutorFatDao();
                List<RedutorFat> redutores = redutorDAO.ListarPorComponente(Franquia.CodPadraoComp, Franquia.CodPerfilComp, Franquia.CodTipoComp);
                RoyalArcoDao royalDAO = new RoyalArcoDao();
                List<RoyalArco> royalties = royalDAO.ListarPorComponente(Franquia.CodPadraoComp, Franquia.CodPerfilComp, Franquia.CodTipoComp);

                int mesInicio = 0;

                //--- Gerar o fluxo de RoyaltiesARCO
                if (isFranquiaRenovacao.Equals(true) && !tir.Categoria.Equals(41))
                {
                    //só calcula se tiver prazo proposto
                    if (tir.ContrAdicional > 0)
                    {
                        mesInicio = 1 + tir.ContrExistente;
                    }
                    else
                    {
                        //não precisa calcular
                        return royalty.Fluxo;
                    }
                }
                else
                {
                    mesInicio = 1;
                }

                double redutor = 0.0;

                for (int i = mesInicio + tir.PeriodoMatura; i <= (prazoTotal - 1 + (mesInicio - 1)); i++)
                {
                    //acha o redutor de vendas
                    for (int j = 0; j <= redutores.Count - 1; j++)
                    {
                        if (redutores[j].NumeroMesFaixaRedutores.Equals(0))
                        {
                            break;
                        }

                        if (Franquia.TpNegFranquia > 0)
                        {
                            if (j.Equals(redutores.Count - 1))
                            {
                                redutor = (double)redutores[j].PercentualReducaoFaturamento;
                            }
                        }
                        else
                        {
                            if (i <= redutores[j].NumeroMesFaixaRedutores || j.Equals(redutores.Count - 1))
                            {
                                redutor = (double)redutores[j].PercentualReducaoFaturamento;

                                break;
                            }
                        }
                    }

                    float percentRoy = 0;

                    //--- acha o percentual de royalty ARCO
                    for (int j = 0; j <= royalties.Count - 1; j++)
                    {
                        if (royalties[j].NumeroMesFaixaRoyalties.Equals(0))
                        {
                            break;
                        }

                        if (Franquia.TpNegFranquia > 0)
                        {
                            if (j == royalties.Count - 1)
                            {
                                percentRoy = (float)royalties[j].PercentualRoyaltiesArco;
                            }
                        }
                        else
                        {
                            if (i <= royalties[j].NumeroMesFaixaRoyalties || j.Equals(royalties.Count - 1))
                            {
                                percentRoy = (float)royalties[j].PercentualRoyaltiesArco;
                            }
                        }
                    }

                    double vlRoyaltyFao = percentRoy / 100.0 * Franquia.ValorFaturamento * (1.0 - redutor / 100.0) / tir.ValorFAO;
                    royalty.Fluxo[i] = vlRoyaltyFao * (1.0 - percIr / 100) * (-1.0);
                }

                //retorno com sucesso
                return royalty.Fluxo;
            }
            catch (Exception ex)
            {
                throw new Exception("CalculoToyaltiesARCO.calcular: Erro ao calcular royalties ARCO.\n" + ex.Message, ex);
            }
        }
    }
}
