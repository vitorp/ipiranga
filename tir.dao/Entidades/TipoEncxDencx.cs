﻿using System;

namespace tir.dao.Entidades
{
    public class TipoEncxDencx
    {
        #region Propriedades
        public Nullable<Int16> CodigoTipoEncaixeDesencaixe{ get; set; }
        public string NomeTipoEnciaxeDesencaixe{ get; set; }
        public Nullable<Int16> CodigoCalculoTIR{ get; set; }
        public string IdVerbaInvestimento{ get; set; }
        public string CodigoEmpresa{ get; set; }
        public Nullable<Int16> CodigoGrupoEncaixeDesencaixe{ get; set; }
        #endregion
    }
}
