using System.Runtime.Serialization;
using WebService.TIR.Models.Encaixes;

namespace WebService.TIR.Models.Base
{
    [DataContract]
    public abstract class FranquiaTir
    {
        [DataMember]
        public int TpNegFranquia { get; internal set; }

        [DataMember]
        public int CodTipoComp { get; set; }

        [DataMember]
        public bool IndParceladoTxFranquia { get; set; }

        [DataMember]
        public int NoParcelaTxFranquia { get; set; }

        [DataMember]
        public double VlTxFranquia { get; set; }

        [DataMember]
        public long CodPerfilComp { get; set; }

        [DataMember]
        public long CodPadraoComp { get; set; }

        [DataMember]
        public double ValorFaturamento { get; set; }

        [DataMember]
        public int TempoContrato { get; set; }
        
    }
}