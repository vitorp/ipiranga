﻿using System;

namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que representa o elaborador da proposta.
    /// </summary>
    public class Elaborador
    {
        #region Propriedades

        public String Codigo { get; set; }
        public String Nome { get; set; }
        public String Mneumonico { get; set; }
        public String Nivel { get; set; }
        public String QueryString { get; set; }
        public bool IsMultiEmpresa { get; set; }
        public String GC { get; set; }

        #endregion
    }
}
