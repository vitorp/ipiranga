﻿
namespace tir.dao.Entidades
{
    public class OutrosParam
    {
        #region Propriedades
        public string CodigoParametro { get; set; }
        public string NomeParametro { get; set; }
        public double ValorParametroNumerico { get; set; }
        public string ValorParametroTexto { get; set; }
        #endregion
    }
}
