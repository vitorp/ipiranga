﻿using System;
using System.Data;
using tir.dao.Entidades;

namespace tir.dao
{
    public class InicDeprecDao : DaoBase<InicDeprec>
    {
        #region Métodos Públicos

        #endregion

        #region Overrides

        protected override InicDeprec MontarObjeto(IDataReader dr)
        {
            InicDeprec ent = new InicDeprec();
            try
            {
                ent.CodigoTipoInvestimento = (decimal)dr["Tp_Invest"];
                ent.QuantidadeMesesMaturacao = (decimal)dr["Qt_Mes_Maturac"];
                ent.NumeroMesSaidaCaixa = (decimal)dr["No_Mes_Saida"];
                ent.NumeroMesInicioDepreciacao = (decimal)dr["No_mes_deprec"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
