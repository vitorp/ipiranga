﻿
namespace TIR.Entidades.Encaixes
{
    /// <summary>
    /// Encaixe que representa um financiamento.
    /// </summary>
    public class Financiamento : Encaixe, IFechamentoAno
    {
        #region Propriedades

        public int Amortizacao { get; set; }
        public int Carencia { get; set; }
        public float PercCorMonet { get; set; }
        public float PercJuros { get; set; }
        public float PercCorMonSaldo { get; set; }
        public float PercCorMonCarencia { get; set; }
        public float PercJurosCarencia { get; set; }
        public double ValorFinanc { get; set; }
        public bool JurosCarRepact { get; set; }
        public double ValorIOF { get; set; }
        public double ValorPrestacao { get; set; }
        public float[] PercLiberacao { get; set; }
        public bool IndPessoaFisica { get; set; }

        #endregion
    }
}
