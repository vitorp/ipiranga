﻿using System;
using System.Data;
//using System.Diagnostics.Contracts;
using System.Text;
using daobase;
using tir.dao.Entidades;

namespace tir.dao
{
    public class TipoNegociacaoDao : DaoBase<TipoNegociacao>
    {
        #region Métodos Públicos
        /// <summary>
        /// Método que retorna um item.
        /// </summary>
        /// <param name="codTipoNegcc">Código do Tipo de Negociação.</param>
        /// <returns>Objeto TipoNegociacao.</returns>
        public TipoNegociacao ObterPorCodigoTipoNegociacao(int codTipoNegcc)
        {
            #region Contracts
            ////Contract.Requires(codTipoNegcc > 0, Mensagens.TipoNegociacao.VAL_CODIGO_TIPO);
            #endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select n.CD_TP_NEGCC, ");
                sb.Append("n.NM_TP_NEGCC, ");
                sb.Append("n.ST_TP_NEGCC, ");
                sb.Append("n.CD_GRUPO_NEGCC, ");
                sb.Append("n.ID_TP_NEGCC ");
                sb.Append("from NEGCLI_TIPO_NEGOCIACAO n ");
                sb.AppendFormat("where n.CD_TP_NEGCC = {0} ", DaoUtil.FormatarParametro("codTipoNegcc"));

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codTipoNegcc"), codTipoNegcc, typeof(Int16)));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //se há resultado
                if (dr.Read())
                {
                    //valor de retorno - alíquota de IOF
                    return MontarObjeto(dr);
                }
                else
                {
                    //valor de retorno nulo
                    return null;
                }
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("TipoEncxDencxDao.ObterPorCodigoEncaixe",
                //"Erro ao obter Encaixe/Desencaixe por código do Tipo.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }
        #endregion

        #region Overrides
        protected override TipoNegociacao MontarObjeto(IDataReader dr)
        {
            TipoNegociacao ent = new TipoNegociacao();

            try
            {
                ent.CodigoTipoNegociacao = (Int16)dr["CD_TP_NEGCC"];
                ent.NomeTipoNegociacao = (string)dr["NM_TP_NEGCC"];
                ent.StTipoNegociacao = (Int16)dr["ST_TP_NEGCC"];
                ent.CodigoGrupoNegociacao = (Int16)dr["CD_GRUPO_NEGCC"];
                ent.IdTipoNegociacao = (string)dr["ID_TP_NEGCC"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
