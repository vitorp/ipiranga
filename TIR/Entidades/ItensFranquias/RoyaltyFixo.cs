﻿using TIR.Entidades.Encaixes;

namespace TIR.Entidades.ItensFranquias
{
    /// <summary>
    /// Item de franquia que representa royalty fixo.
    /// </summary>
    public class RoyaltyFixo : Encaixe
    {
    }
}
