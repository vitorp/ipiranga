﻿using System.Collections.Generic;

namespace TIR.Entidades.Componentes
{
    public class AreaAbastecimento : Componente
    {
        #region Propriedades

        public AluguelMargem AluguelMargem { get; set; }
        public List<Produto> Produtos { get; set; }
        public double[] TodosVolumes { get; set; }
        public static double TotalVolumeMesAno1 {get; set;}
        public static double TotalVolumeMesAno1SemLubes { get; set; }
        //*** Criados por Luiz Felipe - jun/2015
        public static double TotalVolumeMesAno2SemLubes { get; set; }
        public static double TotalVolumeMesAno3SemLubes { get; set; }
        //***

        #endregion
    }
}
