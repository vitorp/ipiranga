﻿
namespace TIR.Entidades
{
    /// <summary>
    /// 
    /// </summary>
    public class DadosVolume
    {
        #region Propriedades

        public float VolPotencial { get; set; }
        public float VolMedio3meses { get; set; }
        public float VolMedio6meses { get; set; }
        public float VolMedio12meses { get; set; }
        public float VolProposto { get; set; }
        public float VolMaior { get; set; }
        public float Preco { get; set; }
        public int Prazo { get; set; }
        public string TipoPrazo { get; set; }
        public float MediaReal { get; set; }
        public float VolContratado { get; set; }

        public GrupoProduto GrupoProduto { get; set; }

        #endregion
    }
}
