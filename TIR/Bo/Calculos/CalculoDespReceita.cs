﻿using System;
using tir.dao;
using tir.dao.Entidades;
using TIR.Entidades;
using TIR.Entidades.Componentes;
using TIR.Entidades.Encaixes;

namespace TIR.Bo.Calculos
{
    public class CalculoDespReceita : ICalculo
    {
        #region Propriedades
        public Franquia Franquia { get; set; }
        #endregion

        public double[] Calcular(Encaixe enc, TIRNegociacao tir)
        {
            try
            {
                //converte o encaixe para o tipo DespReceita
                DespReceita despRec = (DespReceita)enc;

                int contratoFranquia = 0;
                bool isFranquiaRenovacao = false;

                //verificar o tipo de franquia pelo objeto
                if (Franquia != null)
                {
                    contratoFranquia = Franquia.TempoContrato;

                    if (Franquia.TpNegFranquia.Equals(2) || Franquia.TpNegFranquia.Equals(1))
                    {
                        isFranquiaRenovacao = true;
                    }
                }

                //define valor em FAO
                double valorEmFao = despRec.Valor / tir.ValorFAO;

                int prazoTotal = 0;

                //define prazo total
                if (contratoFranquia.Equals(0))
                {
                    //prazoTotal = tir.ContrAdicional + tir.ContrExistente + 1;
                    //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
                    prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 1;
                }
                else if (tir.Categoria.Equals(41))
                {
                    //Migração de Franquias maio/2004 Paula/Andrea/Carlos
                    //prazoTotal = tir.ContrAdicional + tir.ContrExistente + 1;
                    //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
                    prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 1;
                }
                else
                {
                    prazoTotal = contratoFranquia + 1;
                }

                //redimensiona vetor de fluxo
                //despRec.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + 2];
                //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
                //despRec.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 2];
                //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
                despRec.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 2];

                //verifica se o prazo está coerente
                if (prazoTotal < 2)
                {
                    throw new Exception("CalculoDespReceita.calcular: Erro ao calcular despesa/receita. Os prazos dos contratos precisam ser especificados.");
                }

                //verifica se os meses estão coerentes
                if (despRec.MesInicial > despRec.MesFinal)
                {
                    throw new Exception("CalculoDespReceita.calcular: Erro ao calcular despesa/receita. O mês inicial não pode ser maior que o mês final.");
                }

                //--- Aliq PisCofins para Receita - criado em 24/2/1999
                OutrosParamDao outrosDAO = new OutrosParamDao();
                OutrosParam outros = null;
                double aliqPisCof = 0.0;

                if (valorEmFao < 0)
                {
                    aliqPisCof = 0.0;
                }
                else
                {
                    //--- busca receita PIS
                    outros = outrosDAO.ObterParametro("ReceitaPIS");
                    aliqPisCof = (double)outros.ValorParametroNumerico;
                }

                //--- busca o percentual de imposto de renda
                outros = outrosDAO.ObterParametro("PercIR");
                double percIr = (double)outros.ValorParametroNumerico;

                //--- Alteração de renovação de franquia agosto/2002 e maio/2004(migração)
                if (isFranquiaRenovacao && !tir.Categoria.Equals(41))
                {
                    despRec.MesFinal += tir.ContrExistente;
                    despRec.MesInicial += tir.ContrExistente;
                    prazoTotal += tir.ContrExistente;
                }

                //cálculo do fluxo
                for (int i = despRec.MesInicial; i <= despRec.MesFinal; i++)
                {
                    despRec.Fluxo[i] = valorEmFao * (1.0 - percIr / 100.0) * (1 - aliqPisCof / 100.0) / tir.Inflacao[i];
                }

                //valor de retorno positivo
                return despRec.Fluxo;
            }
            catch (Exception ex)
            {
                throw new Exception("CalculoDespReceita.calcular: Erro ao calcular despesa/receita.\n" + ex.Message, ex);
            }
        }
    }
}
