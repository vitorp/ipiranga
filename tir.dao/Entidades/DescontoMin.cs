﻿
using System;
namespace tir.dao.Entidades
{
    public class DescontoMin
    {
        #region Propriedades
        public string SegmentoTipoNegocio { get; set; }
        public string CodigoProduto { get; set; }
        public Single PrecoDescontoMinimo { get; set; }
        #endregion
    }
}
