﻿
using System;
namespace tir.dao.Entidades
{
    public class CustoDaBase
    {
        #region Propriedades
        public string CodigoUnidade { get; set; }
        public string SegmentoTipoNegocio { get; set; }
        public string CodigoProduto { get; set; }
        public Int16 NumeroMesFaixa { get; set; }
        public double ValorTaxaMovimentacao { get; set; }//todo: ver com Felipe
        #endregion
    }
}
