﻿using System;
using System.Collections.Generic;
using tir.dao;
using tir.dao.Entidades;
using TIR.Constantes;
using TIR.Entidades;
using TIR.Entidades.Componentes;
using TIR.Entidades.Encaixes;

namespace TIR.Bo.Calculos
{
    public class CalculoCapitalGiro : ICalculo
    {
        public double[] Calcular(Encaixe enc, TIRNegociacao tir)
        {
            //converte encaixe para capital de giro
            CapitalGiro cap = (CapitalGiro)enc;

            //instancia área de abastecimento
            AreaAbastecimento posto = tir.Cliente.Posto; // (AreaAbastecimento)tir.Cliente.Componentes[0];
            
            float inflacao = 0;

            //define variáveis
            string tpNeg = tir.Cliente.segmento;
            int localiz = Convert.ToInt32(tir.Cliente.Perfil);
            string indProd = String.Format("{0:000}", cap.CodProduto);
            int indiceProd = posto.Produtos.FindIndex(p => p.CodProduto == cap.CodProduto);

            //Inserido por Luiz Felipe - mar/2015
            bool indCompressProp = posto.Produtos[indiceProd].IndCompressProp;

            string grpProd = String.Format("{0:000}", cap.CodGrupo);
            float prazoDias = posto.Produtos[indiceProd].PrazoDias;
            int contExist = tir.ContrExistente;
            //int prazoTotal = tir.ContrAdicional + tir.ContrExistente + 1;
            //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
            //int prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 1;
            //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
            int prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 1;
            string codGV = tir.UnidOrg;

            //dimensiona fluxo
            cap.Fluxo = new double[prazoTotal + 1];

            //***Comentado por Luiz Felipe - mar/2015
            //***Agora GNV tem cálculo de Capital de Giro
            /*//se o produto for Gas Natural (cod=116), não tem Cap.Giro
            if (cap.CodProduto.Equals(TipoCodProduto.GAS_NATURAL))
            {
                //não precisa calcular fluxo
                return cap.Fluxo;
            }*/

            //--- busca parâmetros de negociação para definir flag de verificação de prazo mínimo
            ParamNegDao paramDAO = new ParamNegDao();
            ParamNeg paramNeg = paramDAO.ObterParametro(tir.Categoria, tpNeg, localiz);
            bool isVerificaPrazoMinimo = (bool)paramNeg.IndicadorPrazoMinimo;

            //--- busca prazos mínimos por faixa de volume e faixas de preços na tabela de margens e prazos
            PrazosMinDao prazosDAO = new PrazosMinDao();
            //Alterado por Luiz Felipe - mar/2015
            //Passa a usar o código da GV (se for Revenda) ou da Coordenadoria (se for Consumo) no método
            //List<PrazosMin> prazos = prazosDAO.ListarPorFaixaVolume(tpNeg, localiz, indProd);
            List<PrazosMin> prazos = prazosDAO.ListarPorFaixaVolume(codGV, tir.Cliente.Endereco.UF, tpNeg, localiz, indProd);
            MargemPrzDao margensDAO = new MargemPrzDao();
            List<MargemPrz> margens = margensDAO.ListarMargemPrazo(codGV, tir.Cliente.Endereco.UF, tpNeg, localiz, indProd);

            //--- busca prazos e preços por faixa de meses na tabela de Capital de Giro
            //--- (menos Lubrificantes e GNV), que vêm da MargensPrazos
            Capital_GiroDao capGiroDAO = new Capital_GiroDao();
            List<Capital_Giro> lisCapGiro = capGiroDAO.ObterCompraFrete(tir.Cliente.BasesSupridoras[0].Codigo, indProd);

            //--- acesso a tabela de alíquotas icms
            float aliquotaIcms = 0;
            if (grpProd != "000")
            {
                AliquotaDao aliqDAO = new AliquotaDao();
                Aliquota aliquota = aliqDAO.ObterAliquota(tir.Cliente.Endereco.UF, grpProd);
                aliquotaIcms = (float)aliquota.PercentualICMS;
            }

            //busca informações na tabela de parâmetros
            OutrosParamDao outrosDAO = new OutrosParamDao();
            OutrosParam outrosPis = outrosDAO.ObterParametro("PrazoPis");
            OutrosParam outrosFin = outrosDAO.ObterParametro("PrazoFin");
            OutrosParam outrosAliqPis = outrosDAO.ObterParametro("AliqPis");
            OutrosParam outrosAliqFin = outrosDAO.ObterParametro("AliqFin");
            float prazoPis = (float)outrosPis.ValorParametroNumerico;
            float prazoFin = (float)outrosFin.ValorParametroNumerico;
            float aliqPis = (float)outrosAliqPis.ValorParametroNumerico;
            float aliqFin = (float)outrosAliqFin.ValorParametroNumerico;

            //--- Acessa encargo da revenda - calcula preco bomba
            //--- Encargo da Revenda

            double encRev = 0.0;

            for (int i = 0; i < posto.AluguelMargem.CodProd.Length; i++)
            {
                if (cap.CodProduto == posto.AluguelMargem.CodProd[i])
                {
                    encRev = posto.AluguelMargem.VrEncargo[i];
                    break;
                }
            }

            //se não encontrou o encargo
            if (encRev.Equals(0))
            {
                //busca encargo de revenda
                EncargoRevDao encargoDAO = new EncargoRevDao();
                EncargoRev encargo = encargoDAO.ObterEncargo(tir.Cliente.Endereco.UF, indProd);
                encRev = (double)encargo.ValorMargem;
            }

            //--- Se cliente for consumidor, não tem encargo.
            if (tir.Cliente.segmento.Equals("CON"))
            {
                encRev = 0.0;
            }

            //--- Verifica valores por mês
            double valorTotal = 0.0;
            double valorAnte = -987654321.0;
            double valor = 0.0;

            //--- verifica prazos mínimos por faixa de volume - lê tabela
            float diasTabela = 0;

            for (int i = 0; i < prazos.Count; i++)
            {
                if (prazos[i].VolumeFaixa == 0)
                {
                    break;
                }

                if (posto.TodosVolumes[prazoTotal - 1] <= prazos[i].VolumeFaixa || i == prazos.Count - 1)
                {
                    diasTabela = (float)prazos[i].QuantidadeDiasMinimo;
                    break;
                }
            }

            //--- alteração em 06/10/2005 - felipe e siva
            if (isVerificaPrazoMinimo.Equals(true))
            {
                if (diasTabela <= prazoDias)
                {
                    diasTabela = prazoDias;
                }
                else
                {
                    posto.Produtos[indiceProd].PrazoDias = diasTabela;
                }
            }
            else
            {
                diasTabela = prazoDias;
            }

            double fluxoVolume = 0.0;
            double precoVenda = 0.0;
            double precoCompra = 0.0;
            float prazoCompra = 0;
            float diasEstoque = 0;
            //***Criados por Luiz Felipe em fev/2015
            double frete = 0;
            double margemComDesconto = 0;
            //***

            /*double proxPrecoVenda = 0.0;
            double proxPrecoCompra = 0.0;
            float proxPrazoCompra = 0;
            float proxDiasEstoque = 0;*/

            bool primVolume = true;

            for (int i = 0; i <= prazoTotal - 1; i++)
            {
                //alterado em 14/08/2001 - alex grimberg - incremental negativo
                fluxoVolume = posto.Produtos[indiceProd].FluxoVol[i + 1];

                if (!fluxoVolume.Equals(0.0))
                {
                    //--- contas a receber
                    //--- acha valores conforme a faixa


                    for (int j = 0; j < margens.Count; j++)
                    {
                        if (margens[j].NumeroMesFaixa.Equals(0))
                        {
                            break;
                        }

                        if (i <= margens[j].NumeroMesFaixa || j.Equals(margens.Count - 1))
                        {
                            /*//Pega valores do próximo período para usar em contas a pagar
                            //e giro de estoque - por Luiz Felipe em dez/2014
                            //***
                            if (j.Equals(margens.Count - 1))
                            {
                                proxPrecoVenda = (double)margens[j].ValorPrecoVenda;
                                proxPrecoCompra = (double)margens[j].ValorPrecoCompra;
                                proxPrazoCompra = (float)margens[j].QuantidadeDiasCompra;
                                proxDiasEstoque = (float)margens[j].QuantidadeDiasEstoque;
                            }
                            else
                            {
                                proxPrecoVenda = (double)margens[j+1].ValorPrecoVenda;
                                proxPrecoCompra = (double)margens[j + 1].ValorPrecoCompra;
                                proxPrazoCompra = (float)margens[j + 1].QuantidadeDiasCompra;
                                proxDiasEstoque = (float)margens[j + 1].QuantidadeDiasEstoque;
                            }
                            //***
                            */

                            //***Teste inserido por Luiz Felipe em dez/2015
                            //Se for Lubrificante ou GNV, usa o valor da tabela MargensPrazos
                            if (cap.CodProduto == TipoCodProduto.OLEOS_LUBRIFICANTES ||
                                cap.CodProduto == TipoCodProduto.GAS_NATURAL)
                            {
                            precoVenda = (double)margens[j].ValorPrecoVenda;
                            precoCompra = (double)margens[j].ValorPrecoCompra;
                            prazoCompra = (float)margens[j].QuantidadeDiasCompra;
                            diasEstoque = (float)margens[j].QuantidadeDiasEstoque;
                            }
                            //Não é Lubrificante nem GNV; usa a tabela Capital_Giro
                            else
                            {
                                for (int k = 0; k < lisCapGiro.Count; k++)
                                {
                                    if (lisCapGiro[k].NumeroMesFaixa.Equals(0))
                                    {
                                        break;
                                    }

                                    if (i <= lisCapGiro[k].NumeroMesFaixa || k.Equals(lisCapGiro.Count - 1))
                                    {
                                        precoVenda = margens[j].ValorMargemDistribuicao + 
                                                     (double)lisCapGiro[k].ValorPrecoCompra + lisCapGiro[k].FreteEntrega
                                                     + (margens[j].ValorAjustePrecoVenda.HasValue ? margens[j].ValorAjustePrecoVenda.Value : 0); //alteração Jesiel 04/01/16: adicionado novo campo no cálculo do preço de venda a pedido do Financeiro
                                        precoCompra = (double)lisCapGiro[k].ValorPrecoCompra;
                                        prazoCompra = (float)lisCapGiro[k].QuantidadeDiasCompra;
                                        diasEstoque = (float)lisCapGiro[k].QuantidadeDiasEstoque;

                                        break;
                                    }
                                }
                            }

                            //***Se for um Cliente de Consumo - por Luiz Felipe em fev/2015
                            if (tir.Cliente.segmento == "CON")
                            {
                                //Cálculo do Frete
                                frete = precoVenda - margens[j].ValorMargemDistribuicao - precoCompra;
                                //Cálculo da Margem com desconto
                                margemComDesconto = margens[j].ValorMargemDistribuicao * 
                                                   (100 - posto.Produtos[indiceProd].DescMargem) / 100;
                                //Cálculo do novo Preço de Venda
                                precoVenda = precoCompra + margemComDesconto + frete;
                            }
                            //***

                            break;
                        }
                    }

                    //Incluído por Luiz Felipe em out/2012,
                    //a pedido da área financeira
                    //***
                    
                    //No primeiro mês não há inflação
                    //if (i > 0)
                    //{
                    //    //--- Cálculo da inflação mensal
                    //    if (tir.Inflacao[i - 1].Equals(0.0))
                    //    {
                    //        inflacao = (float)tir.Inflacao[i];
                    //    }
                    //    else
                    //    {
                    //        inflacao = (float)(tir.Inflacao[i] / tir.Inflacao[i - 1]);
                    //    }
                    //}
                    //else
                    //{
                        inflacao = (float)tir.Inflacao[i];
                    //}
                    //***

                    double precoBomba = encRev + precoVenda;
                    //double contReceber = fluxoVolume * diasTabela * precoVenda / 30.0 * (-1.0);
                    //double contPagar = fluxoVolume * prazoCompra * precoCompra / 30.0;
                    //double giroEstoque = fluxoVolume * (1.0 - aliquotaIcms / 100.0) * precoCompra * diasEstoque / 30.0 * (-1.0);
                    //Alterado por Luiz Felipe em out/2012 para inflacionar
                    //os valores, a pedido da área financeira
                    //double contReceber = fluxoVolume * diasTabela * precoVenda / 30.0 * (-1.0) * inflacao;
                    //Alterado por Luiz Felipe em dez/2014 para usar o valor do mês em lugar do do mês seguinte
                    double contReceber = posto.Produtos[indiceProd].FluxoVol[i] * diasTabela * precoVenda / 30.0 * (-1.0) * inflacao;
                    double giroEstoque = 0;
                    double contPagar = 0;

                    //***Alterado por Luiz Felipe - ago/2015
                    //***Voltam a ser usados os valores do mês para o cálculo
                    /*
                    //Não calcula Contas a Pagar para o último mês
                    if (i != prazoTotal - 1)
                    {
                        //Se for o penúltimo mês, usa o volume do último mês
                        if (i == prazoTotal - 2)
                        {
                            contPagar = posto.Produtos[indiceProd].FluxoVol[i + 1] * prazoCompra * precoCompra / 30.0 * inflacao;
                        }
                        else
                        {
                            contPagar = fluxoVolume * prazoCompra * precoCompra / 30.0 * inflacao;
                        }
                    }

                    //Não calcula Giro de Estoque para o último mês
                    if (i != prazoTotal - 1)
                    {
                        //Se for o penúltimo mês, usa o volume do último mês
                        if (i == prazoTotal - 2)
                        {
                            giroEstoque = posto.Produtos[indiceProd].FluxoVol[i + 1] * precoCompra * diasEstoque / 30.0 * (-1.0) * inflacao;
                        }
                        else
                        {
                            giroEstoque = fluxoVolume * precoCompra * diasEstoque / 30.0 * (-1.0) * inflacao;
                        }
                    }*/

                    contPagar = posto.Produtos[indiceProd].FluxoVol[i] * prazoCompra * precoCompra / 30.0 * inflacao;
                    giroEstoque = posto.Produtos[indiceProd].FluxoVol[i] * precoCompra * diasEstoque / 30.0 * (-1.0) * inflacao;
                    //***

                    double pisFin = ((precoVenda + precoBomba) * fluxoVolume * prazoPis * aliqPis +
                              (precoVenda + precoBomba) * fluxoVolume * prazoFin * aliqFin) / 30.0;
                    //valor = contReceber + contPagar + giroEstoque + pisFin;
                    //***Inserido o cálculo para GNV por Luiz Felipe - mar/2015
                    //Se o Produto for GNV e o compressor de terceiros
                    if (cap.CodProduto.Equals(TipoCodProduto.GAS_NATURAL) &&
                        indCompressProp == false)
                    {
                        if (primVolume)
                        {
                            valor = 0;
                            primVolume = false;
                        }
                        else
                        {
                            valor = contReceber;
                        }
                    }
                    //Produto não é GNV ou é GNV e o compressor é próprio
                    else
                    {
                    //Alterado por Luiz Felipe em out/2012,
                    //a pedido da área financeira
                    if (primVolume)
                    {
                        valor = contPagar + giroEstoque;
                        primVolume = false;
                    }
                    else
                    {
                        valor = contReceber + contPagar + giroEstoque;
                    }
                    }
                    //***

                    if (!valor.Equals(valorAnte))
                    {
                        if (valorAnte.Equals(-987654321.0))
                        {
                            valorAnte = 0.0;
                        }

                        //cap.Fluxo[i] = valor - valorAnte;
                        //Alterado por Luiz Felipe em out/2012, para deflacionar o valor
                        //a pedido da área financeira
                        cap.Fluxo[i] = (valor - valorAnte) / inflacao;

                        valorTotal += (valor - valorAnte);

                        valorAnte = valor;
                        
                    }
                }
            }

            cap.Fluxo[prazoTotal] = valorTotal * (-1.0) / tir.Inflacao[prazoTotal];

            //retorno com sucesso
            return cap.Fluxo;
        }
    }
}
