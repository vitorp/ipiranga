﻿
using System;
namespace tir.dao.Entidades
{
    public class RoyalCbpi
    {
        #region Propriedades
        public Int32 CodigoTipoComponente { get; set; }
        public Int32 CodigoPadraoComponente { get; set; }
        public Int32 CodigoPerfilComponente { get; set; }
        public Int16 NumeroMesFaixaRoyalties { get; set; }
        public double ValorRoyaltiesCbpi { get; set; }
        public Single PercentualRoyaltiesCbpi { get; set; }
        #endregion
    }
}
