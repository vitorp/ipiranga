﻿using System;
using System.Collections.Generic;
using System.Data;
//using System.Diagnostics.Contracts;
using System.Text;
using daobase;
using tir.dao.Entidades;

namespace tir.dao
{
    public class MargemPrzDao : DaoBase<MargemPrz>
    {
        #region Métodos Públicos
        /// <summary>
        /// Método que lista Margens e Prazos.
        /// </summary>
        /// <param name="codGV">Código da gerência de venda.</param>
        /// <param name="siglaUF">Sigla da UF do Cliente.</param>
        /// <param name="TpNegcc">Tipo de negociação.</param>
        /// <param name="idPerfilPosto">Perfil.</param>
        /// <param name="codProdu">Código do produto.</param>
        /// <returns>Lista de objetos MargemPrz.</returns>
        public List<MargemPrz> ListarMargemPrazo(string codGV, string siglaUF, string TpNegcc, int idPerfilPosto, string codProdu)
        {
            #region Contracts
            //Contract.Requires(codGV != null, Mensagens.MargemPrazo.VAL_CODIGO_GV);
            //Contract.Requires(TpNegcc != null, Mensagens.MargemPrazo.VAL_TIPO_NEGOCIACAO);
            //Contract.Requires(idPerfilPosto > 0, Mensagens.MargemPrazo.VAL_ID_PERFIL);
            //Contract.Requires(codProdu != null, Mensagens.MargemPrazo.VAL_CODIGO_PRODUTO);
            #endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select m.Cd_GV, ");
                sb.Append("m.Sg_UF, ");
                sb.Append("m.sg_tpneg, ");
                sb.Append("m.id_perfil_posto, ");
                sb.Append("m.Cd_Prod, ");
                sb.Append("m.no_mes_faixa, ");
                sb.Append("m.Vr_MrgDistr, ");
                sb.Append("m.Vr_MrgD_Gas_Equip, ");
                sb.Append("m.Vr_Prc_Compra, ");
                sb.Append("m.Vr_Prc_Venda, ");
                sb.Append("m.Qt_Dias_Estoque, ");
                sb.Append("m.Qt_Dias_Compra, ");
                sb.Append("m.Vr_Ajus_Prc_Vnd ");
                sb.Append("from MargensPrazos_UF m ");
                sb.AppendFormat("where m.Cd_GV = {0} ", DaoUtil.FormatarParametro("codGV"));
                sb.AppendFormat("and m.Sg_UF = {0} ", DaoUtil.FormatarParametro("siglaUF"));
                sb.AppendFormat("and m.sg_tpneg = {0} ", DaoUtil.FormatarParametro("TpNegcc"));
                sb.AppendFormat("and m.id_perfil_posto = {0} ", DaoUtil.FormatarParametro("idPerfilPosto"));
                sb.AppendFormat("and m.Cd_Prod = {0} ", DaoUtil.FormatarParametro("codProdu"));
                sb.Append("order by m.no_mes_faixa");

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codGV"), codGV, typeof(String), 6));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("siglaUF"), siglaUF, typeof(String), 2));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("TpNegcc"), TpNegcc, typeof(String), 3));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("idPerfilPosto"), idPerfilPosto, typeof(Int16)));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codProdu"), codProdu, typeof(String), 3));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //se há resultado
                //valor de retorno - alíquota de IOF
                return MontarLista(dr);
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("MargemPrzDao.ListarMargemPrazo",
                //"Erro ao listar Margens e Prazos.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }
        #endregion

        #region Overrides

        protected override MargemPrz MontarObjeto(IDataReader dr)
        {
            MargemPrz ent = new MargemPrz();
            try
            {
                ent.CodigoGV = (string)dr["Cd_GV"];
                ent.SiglaUF = (string)dr["Sg_UF"];
                ent.SiglaTipoNegocio = (string)dr["sg_tpneg"];
                ent.PerfilPosto = (Int16)dr["id_perfil_posto"];
                ent.CodigoProduto = (string)dr["Cd_Prod"];
                ent.NumeroMesFaixa = (Int16)dr["no_mes_faixa"];
                ent.ValorMargemDistribuicao = (double)dr["Vr_MrgDistr"];
                ent.ValorMargemDGasEquip = (decimal)dr["Vr_MrgD_Gas_Equip"];
                ent.ValorPrecoCompra = (decimal)dr["Vr_Prc_Compra"];
                ent.ValorPrecoVenda = (decimal)dr["Vr_Prc_venda"];
                ent.QuantidadeDiasEstoque = dr["Qt_Dias_Estoque"] != DBNull.Value ? (Single?)dr["Qt_Dias_Estoque"] : null;
                ent.QuantidadeDiasCompra = (Single)dr["Qt_Dias_Compra"];
                ent.ValorAjustePrecoVenda = dr["vr_ajus_prc_vnd"] != DBNull.Value ? (double?)dr["vr_ajus_prc_vnd"] : null;
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
