﻿using System;

namespace tir.dao.Entidades
{
    public class ParamNeg
    {
        #region Propriedades
        public Int16 TipoCategoriaProjeto { get; set; }
        public string SiglaSegmentoMercado {get; set;}
        public Int16 PerfilPosto { get; set; }
        public Int16 QuantidadeMaximaMesesProjeto { get; set; }
        public double VolumeTotalMinimoPositivo { get; set; }
        public Boolean IndicadorMaturacao { get; set; }
        public Boolean IndicadorDepreciacao25 { get; set; }
        public Boolean IndicadorContratoExistente { get; set; }
        public Boolean IndicadorRecebimentoAluguelExistente { get; set; }
        public Boolean? IndicadorPrazoMinimo { get; set; }
        #endregion
    }
}
