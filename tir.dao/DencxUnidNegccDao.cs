﻿using System;
using System.Collections.Generic;
using System.Data;
//using System.Diagnostics.Contracts;
using System.Text;
using daobase;
using tir.dao.Entidades;

namespace tir.dao
{
    public class DencxUnidNegccDao : DaoBase<DencxUnidNegcc>
    {
        #region Métodos Públicos
        /// <summary>
        /// Método que retorna uma lista filtrada de itens.
        /// </summary>
        /// <param name="codUnidade">Código da Unidade de Negociação.</param>
        /// <returns>Lista de objetos DencxUnidNegcc.</returns>
        public List<DencxUnidNegcc> ListarPorUnidade(int codUnidade)
        {
            #region Contracts
            //Contract.Requires(codUnidade > 0, Mensagens.DencxUnidNegcc.VAL_CODIGO_UNIDADE);
            #endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select e.cd_unid_negcc, ");
                sb.Append("e.cd_tp_encx_dencx ");
                sb.Append("from NEGCLI_DENCX_UNID_NEGCC e ");
                sb.AppendFormat("where e.cd_unid_negcc = {0} ", DaoUtil.FormatarParametro("codUnidade"));

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codUnidade"), codUnidade, typeof(Int16)));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //valor de retorno 
                return MontarLista(dr);

            }
            catch //(Exception ex)
            {
                throw; //new DaoException("DencxUnidNegcc.ListarPorUnidade",
                //"Erro ao listar os Encaixes/Desencaixes de uma Unidade de Negociação.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }
        #endregion

        #region Overrides
        protected override DencxUnidNegcc MontarObjeto(IDataReader dr)
        {
            DencxUnidNegcc ent = new DencxUnidNegcc();

            try
            {
                ent.CodigoUnidadeNegociacao = (Int16?)dr["cd_unid_negcc"];
                ent.CodigoTipoEncaixeDesencaixe = (Int16?)dr["cd_tp_encx_dencx"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
