﻿using System;
using System.Collections.Generic;
using tir.dao;
using tir.dao.Entidades;
using TIR.Entidades;
using TIR.Entidades.Componentes;
using TIR.Entidades.Encaixes;
using TIR.Entidades.ItensFranquias;

namespace TIR.Bo.Calculos
{
    public class CalculoTerceirizacao : ICalculo
    {
        #region Propriedades
        public Franquia Franquia { get; set; }
        #endregion

        public double[] Calcular(Encaixe enc, TIRNegociacao tir)
        {
            try
            {
                //converte encaixe
                Terceirizacao terc = (Terceirizacao)enc;

                terc.Descricao = "Terceirização";

                //define prazo total e dimensiona Fluxo
                int prazoTotal = Franquia.TempoContrato + 1;
                //terc.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + 2];
                //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
                terc.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 2];
                //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
                terc.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 2];

                bool isFranquiaRenovacao = false;
                if (Franquia.TpNegFranquia.Equals(2) || Franquia.TpNegFranquia.Equals(1))
                {
                    isFranquiaRenovacao = true;
                }

                //busca alíquota de IR
                OutrosParamDao outrosDAO = new OutrosParamDao();
                OutrosParam outros = outrosDAO.ObterParametro("PercIR");
                float percIr = (float)outros.ValorParametroNumerico;

                //buscar custo de terceirização
                CustoTercDao custoDAO = new CustoTercDao();
                List<CustoTerc> custos = custoDAO.ListarPorComponente(Franquia.CodTipoComp, Franquia.CodPerfilComp, Franquia.CodPadraoComp);

                int mesInicio = 0;

                //--- Gerar o Fluxo de Terceirizacao - migração franquias maio/2004
                if (isFranquiaRenovacao && !tir.Categoria.Equals(41))
                {
                    //só calcula se tiver prazo proposto
                    if (tir.ContrAdicional > 0)
                    {
                        mesInicio = tir.ContrExistente + 1;
                    }
                    else
                    {
                        //não precisa calcular
                        return terc.Fluxo;
                    }
                }
                else
                {
                    mesInicio = 1;
                }

                double valorCustoTerc = 0.0;

                for (int i = mesInicio + tir.PeriodoMatura; i <= (prazoTotal - 1 + (mesInicio - 1)); i++)
                {
                    //acha o custo conforme o período
                    for (int j = 0; j <= custos.Count - 1; j++)
                    {
                        if (custos[j].NumeroMesFaixa.Equals(0))
                        {
                            break;
                        }

                        if (Franquia.TpNegFranquia > 0)
                        {
                            if (j.Equals(custos.Count - 1))
                            {
                                valorCustoTerc = (double)custos[j].ValorCustoTerceirizacao / tir.ValorFAO;
                                break;
                            }
                        }
                        else
                        {
                            if (i <= custos[j].NumeroMesFaixa || j.Equals(custos.Count - 1))
                            {
                                valorCustoTerc = (double)custos[j].ValorCustoTerceirizacao / tir.ValorFAO;
                                break;
                            }
                        }
                    }

                    terc.Fluxo[i] = valorCustoTerc * (1.0 - percIr / 100.0) * (-1.0);
                }

                //retorno com sucesso
                return terc.Fluxo;
            }
            catch (Exception ex)
            {
                throw new Exception("CalculoTerceirizacao.calcular: Erro ao calcular terceirização.\n" + ex.Message, ex);
            }
        }
    }
}
