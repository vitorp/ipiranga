﻿using System;

namespace tir.dao.Entidades
{
    public class Capital_Giro
    {
        #region Propriedades
        public string CodigoUnidade { get; set; }
        public string CodigoProduto { get; set; }
        public Int16 NumeroMesFaixa { get; set; }
        public decimal ValorPrecoCompra { get; set; }
        public double FreteEntrega { get; set; }
        public decimal ValorPrecoVenda { get; set; }
        public Single? QuantidadeDiasEstoque { get; set; }
        public Single QuantidadeDiasCompra { get; set; }
        #endregion
    }
}
