﻿
namespace TIR.Entidades
{
    public class TIRNegociacao : TIRResultado
    {
        //#region Atributos
        //private Cliente _cliente;
        //#endregion

        #region Propriedades

        /*public Cliente Cliente
        {
            get
            {
                return _cliente;
            }
            set
            {
                Cliente c = (Cliente)value;

                // não permite arrastar posto próprio se a negociação
                   já tiver sido preenchida e for asseguramento ou embandeiramento 
                if (c.isProprTerreno)   //verifica se é posto próprio
                {
                    //verifica se a negociação já foi preenchida e é embandeiramento ou asseguramento
                    if (this.Categoria.Equals(1) || this.Categoria.Equals(33))
                    {
                        throw new Exception("TIRNegociacao: ATENÇÃO: O cliente não foi anexado!\n Não é permitido embandeiramento ou asseguramento para posto próprio.\n Este cliente apenas poderá ser arrastado para a TIR se a negociação for alterada.");
                    }
                }

                //verifica se a tir está travada
                if (this.IsTravada)
                {
                    throw new Exception("TIRNegociacao: ATENÇÃO: O cliente não foi anexado!\n Esta TIR já foi anexada à uma PNE e portanto encontra-se travada.\n É preciso destravá-la antes de anexar o cliente.");
                }

                //verifica se já possui cliente anexado
                if (this.IsClienteDragged)
                {
                    //verifica se a tir já tem um cliente anexado com gerenia nacional diferente
                    if (!c.GerenciaNacional.Sigla.Equals(this.Cliente.GerenciaNacional.Sigla))
                    {
                        throw new Exception("TIRNegociacao: ATENÇÃO: Esta TIR já possui um cliente anexado com gerência nacional diferente. Você deve criar uma nova TIR para usar este cliente.");
                    }
                }

                //cria referência p/TIR apontando para objeto oCliente original
                _cliente = c;
                this.IsClienteDragged = true;

                //zera perfil e segmento (obriga a ser escolhido)
                this.Cliente.Perfil = String.Empty;
                this.Cliente.segmento = String.Empty;
            }
        }*/

        public double CustoOportunidade { get; set; }

        public int[] VendasMes { get; set; }
        public float[] PercentualMes { get; set; }
        //Criado por Luiz Felipe em out/2014, para ser usado no cálculo
        //de Capital de Giro Franquia, que usa o fluxo de Royalty variável
        public double[] FluxoAuxiliar { get; set; }

        #endregion
    }
}
