﻿
namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que representa uma margem mensal.
    /// </summary>
    public class MargensMensal
    {
        #region Propriedades

        public string AnoMes { get; set; }
        public string TipoProduto { get; set; }
        public float Volume { get; set; }
        public float MargemTot { get; set; }
        public float MargemUnit { get; set; }
        public float PrazoMedio { get; set; }

        #endregion
    }
}
