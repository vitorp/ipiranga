﻿
namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que representa a bandeira do cliente.
    /// </summary>
    public class Bandeira
    {
        #region Propriedades

        public string Codigo { get; set; }
        public string Nome { get; set; }

        #endregion
    }
}
