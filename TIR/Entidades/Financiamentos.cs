﻿
namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que representa o financiamento.
    /// </summary>
    public class Financiamentos
    {
        #region Propriedades

        public string Tipo { get; set; }
        public string Descricao { get; set; }
        public float Principal { get; set; }
        public float Saldo { get; set; }
        public string Numero { get; set; }
        public string Data { get; set; }
        public string CodTipo { get; set; }
        public string CodCategoria { get; set; }
        public int MesAmort { get; set; }
        public string Vencimento { get; set; }
        public float PercJuros { get; set; }
        public float PercCM { get; set; }
        public float PercJurosCar { get; set; }
        public float PercCMCar { get; set; }

        #endregion
    }
}
