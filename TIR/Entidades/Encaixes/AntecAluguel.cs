﻿
namespace TIR.Entidades.Encaixes
{
    /// <summary>
    /// Encaixe que representa uma antecipação de aluguel.
    /// </summary>
    public class AntecAluguel : Encaixe
    {
        #region Propriedades

        public int MesesAntecip { get; set; }
        public double VolumeInicial { get; set; }
        public double VolumeParcial { get; set; }
        public int QuantidadeParcelas { get; set; }

        #endregion
    }
}
