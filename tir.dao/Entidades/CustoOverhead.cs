﻿
using System;
namespace tir.dao.Entidades
{
    public class CustoOverhead
    {
        #region Propriedades
        public Int32 CodigoTipoComponente { get; set; }
        public Int32 CodigoPadraoComponente { get; set; }
        public Int32 CodigoPerfilComponente { get; set; }
        public string CodigoGV { get; set; }
        public string SiglaUF { get; set; }
        public Int32 FaixaAteMes { get; set; }
        public double ValorCustoOverhead { get; set; }
        #endregion
    }
}
