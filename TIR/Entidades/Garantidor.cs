﻿
namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que representa o garantidor.
    /// </summary>
    public class Garantidor
    {
        #region Propriedades

        public string Cnpj { get; set; }
        public string TipoCliente { get; set; }
        public string Componente { get; set; }
        public string Nome { get; set; }
        public double Lastro { get; set; }
        public int NumClientes { get; set; }
        public double LastroRural { get; set; }
        public double LastroUrbano { get; set; }

        #endregion
    }
}
