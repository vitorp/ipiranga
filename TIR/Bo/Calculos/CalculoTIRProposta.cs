﻿using System;
using System.Collections.Generic;
using Microsoft.VisualBasic;
using tir.dao;
using tir.dao.Entidades;
using TIR.Base;
using TIR.Entidades;

namespace TIR.Bo.Calculos
{
    public class CalculoTIRProposta : TIRBase
    {
        public CalculoTIRProposta() : base() { }

        /// <summary>
        /// Construtor alternativo com passagem de conexão no parâmetro.
        /// </summary>
        /// <param name="conexao">Conexão alternativa.</param>
        public CalculoTIRProposta(string conexao) : base(conexao) { }        

        /// <summary>
        /// Calcula as TIRs de Proposta, Negociações, Componentes e Encaixes.
        /// </summary>
        /// <param name="Prop">Objeto Proposta.</param>
        /// <returns>Objeto ResultadoProposta.</returns>
        public ResultadoProposta CalcularTIRProposta(Proposta Prop)
        {
            ResultadoProposta resProp = new ResultadoProposta();

            //Negociações da Proposta
            resProp.ResultadosNegociacoes = new List<ResultadoNegociacao>();
            for (int i = 0; i < Prop.Negociacoes.Count; i++)
            {
                CalculoTIRNegociacao calcNeg = new CalculoTIRNegociacao();

                //Atualiza propriedades para o resultado da Negociação
                ResultadoNegociacao resNeg = new ResultadoNegociacao();

                resNeg.Identificador = Prop.Negociacoes[i].TIR.Cliente.razaoSocial + " - " + Prop.Negociacoes[i].NomeNegociacao;
                resNeg.Fluxo = calcNeg.Calcular(Prop.Negociacoes[i].TIR);
                resNeg.TIR = Prop.Negociacoes[i].TIR.TIR;
                resNeg.NPV = Prop.Negociacoes[i].TIR.NPV;
                resNeg.Payback = Prop.Negociacoes[i].TIR.Payback;
                resNeg.Inflacao = Prop.Negociacoes[i].TIR.Inflacao;

                //Instancia DAO do Tipo de Componente
                TipoComDao daoComp = new TipoComDao();

                //Componentes da Negociação
                resNeg.ResultadosComponentes = new List<ResultadoComponente>();

                //Posto
                if (Prop.Negociacoes[i].TIR.Cliente.Posto != null)
                {
                    ResultadoComponente resComp = new ResultadoComponente();
                    //Obtem objeto Tipo de Componente
                    TipoCom tpComp = daoComp.ObterPorCodigo(Prop.Negociacoes[i].TIR.Cliente.Posto.CodTipoComp);

                    if (tpComp != null)
                    {
                        resComp.Identificador = tpComp.NomeTipoComponente;
                        resComp.Fluxo = Prop.Negociacoes[i].TIR.Cliente.Posto.Fluxo;
                        resComp.TIR = Prop.Negociacoes[i].TIR.Cliente.Posto.TIR;
                        resComp.NPV = Prop.Negociacoes[i].TIR.Cliente.Posto.NPV;
                        resComp.Payback = Prop.Negociacoes[i].TIR.Cliente.Posto.Payback;
                        resComp.Inflacao = Prop.Negociacoes[i].TIR.Inflacao;

                        //Encaixes do Posto (só geram Fluxo)
                        resComp.ResultadosEncaixes = new List<Resultado>();
                        for (int k = 0; k < Prop.Negociacoes[i].TIR.Cliente.Posto.Encaixes.Count; k++)
                        {
                            Resultado resEnc = new Resultado();

                            //Atualiza propriedades para o resultado do Encaixe
                            resEnc.codEcxDcx = Prop.Negociacoes[i].TIR.Cliente.Posto.Encaixes[k].CodEcxDcx;
                            resEnc.Identificador = Prop.Negociacoes[i].TIR.Cliente.Posto.Encaixes[k].Descricao;
                            resEnc.Fluxo = Prop.Negociacoes[i].TIR.Cliente.Posto.Encaixes[k].Fluxo;

                            //Adiciona o resultado do Encaixe ao do Componente
                            resComp.ResultadosEncaixes.Add(resEnc);

                        }
                    }

                    //Adiciona o resultado do Componente ao da Negociação
                    resNeg.ResultadosComponentes.Add(resComp);
                }

                //AmPm
                if (Prop.Negociacoes[i].TIR.Cliente.AmPm != null)
                {
                    ResultadoComponente resComp = new ResultadoComponente();
                    //Obtem objeto Tipo de Componente
                    TipoCom tpComp = daoComp.ObterPorCodigo(Prop.Negociacoes[i].TIR.Cliente.AmPm.CodTipoComp);

                    if (tpComp != null)
                    {
                        resComp.Identificador = tpComp.NomeTipoComponente;
                        resComp.Fluxo = Prop.Negociacoes[i].TIR.Cliente.AmPm.Fluxo;
                        resComp.TIR = Prop.Negociacoes[i].TIR.Cliente.AmPm.TIR;
                        resComp.NPV = Prop.Negociacoes[i].TIR.Cliente.AmPm.NPV;
                        resComp.Payback = Prop.Negociacoes[i].TIR.Cliente.AmPm.Payback;
                        resComp.Inflacao = Prop.Negociacoes[i].TIR.Inflacao;

                        //Encaixes da AmPm (só geram Fluxo)
                        resComp.ResultadosEncaixes = new List<Resultado>();

                        Resultado resEnc = new Resultado();

                        //Overhead
                        if (Prop.Negociacoes[i].TIR.Cliente.AmPm.Overhead != null)
                        {
                            resEnc.codEcxDcx = Prop.Negociacoes[i].TIR.Cliente.AmPm.Overhead.CodEcxDcx;
                            resEnc.Identificador = Prop.Negociacoes[i].TIR.Cliente.AmPm.Overhead.Descricao;
                            resEnc.Fluxo = Prop.Negociacoes[i].TIR.Cliente.AmPm.Overhead.Fluxo;

                            //Adiciona o resultado ao do Componente
                            resComp.ResultadosEncaixes.Add(resEnc);
                        }

                        //Royalty Variável
                        if (Prop.Negociacoes[i].TIR.Cliente.AmPm.RoyaltyVar != null)
                        {
                            resEnc = new Resultado();
                            resEnc.codEcxDcx = Prop.Negociacoes[i].TIR.Cliente.AmPm.RoyaltyVar.CodEcxDcx;
                            resEnc.Identificador = Prop.Negociacoes[i].TIR.Cliente.AmPm.RoyaltyVar.Descricao;
                            resEnc.Fluxo = Prop.Negociacoes[i].TIR.Cliente.AmPm.RoyaltyVar.Fluxo;

                            //Adiciona o resultado ao do Componente
                            resComp.ResultadosEncaixes.Add(resEnc);
                        }

                        //Capital Giro Franquia - criado por Luiz Felipe em out/2014
                        if (Prop.Negociacoes[i].TIR.Cliente.AmPm.CapitalGiroFranquia != null)
                        {
                            resEnc = new Resultado();
                            resEnc.codEcxDcx = Prop.Negociacoes[i].TIR.Cliente.AmPm.CapitalGiroFranquia.CodEcxDcx;
                            resEnc.Identificador = Prop.Negociacoes[i].TIR.Cliente.AmPm.CapitalGiroFranquia.Descricao;
                            resEnc.Fluxo = Prop.Negociacoes[i].TIR.Cliente.AmPm.CapitalGiroFranquia.Fluxo;

                            //Adiciona o resultado ao do Componente
                            resComp.ResultadosEncaixes.Add(resEnc);
                        }

                        //Taxa de Franquia
                        if (Prop.Negociacoes[i].TIR.Cliente.AmPm.TaxaFranquia != null)
                        {
                            resEnc = new Resultado();
                            resEnc.codEcxDcx = Prop.Negociacoes[i].TIR.Cliente.AmPm.TaxaFranquia.CodEcxDcx;
                            resEnc.Identificador = Prop.Negociacoes[i].TIR.Cliente.AmPm.TaxaFranquia.Descricao;
                            resEnc.Fluxo = Prop.Negociacoes[i].TIR.Cliente.AmPm.TaxaFranquia.Fluxo;

                            //Adiciona o resultado ao do Componente
                            resComp.ResultadosEncaixes.Add(resEnc);
                        }

                        //Propaganda
                        if (Prop.Negociacoes[i].TIR.Cliente.AmPm.Propaganda != null)
                        {
                            //Só inclui o Fluxo se houver pelo menos um elemento diferente de zero
                            double valor = Array.Find(Prop.Negociacoes[i].TIR.Cliente.AmPm.Propaganda.Fluxo, element => element != 0.0);

                            if (valor > 0.0)
                            {
                                resEnc = new Resultado();
                                resEnc.codEcxDcx = Prop.Negociacoes[i].TIR.Cliente.AmPm.Propaganda.CodEcxDcx;
                                resEnc.Identificador = Prop.Negociacoes[i].TIR.Cliente.AmPm.Propaganda.Descricao;
                                resEnc.Fluxo = Prop.Negociacoes[i].TIR.Cliente.AmPm.Propaganda.Fluxo;

                                //Adiciona o resultado ao do Componente
                                resComp.ResultadosEncaixes.Add(resEnc);
                            }
                        }

                        //Terceirização
                        if (Prop.Negociacoes[i].TIR.Cliente.AmPm.Terceirizacao != null)
                        {
                            //Só inclui o Fluxo se houver pelo menos um elemento diferente de zero
                            double valor = Array.Find(Prop.Negociacoes[i].TIR.Cliente.AmPm.Terceirizacao.Fluxo, element => element != 0.0);

                            if (valor > 0.0)
                            {
                                resEnc = new Resultado();
                                resEnc.codEcxDcx = Prop.Negociacoes[i].TIR.Cliente.AmPm.Terceirizacao.CodEcxDcx;
                                resEnc.Identificador = Prop.Negociacoes[i].TIR.Cliente.AmPm.Terceirizacao.Descricao;
                                resEnc.Fluxo = Prop.Negociacoes[i].TIR.Cliente.AmPm.Terceirizacao.Fluxo;

                                //Adiciona o resultado ao do Componente
                                resComp.ResultadosEncaixes.Add(resEnc);
                            }
                        }

                        //Royalty Arco (só para AmPm)
                        if (Prop.Negociacoes[i].TIR.Cliente.AmPm.RoyaltiesArco != null)
                        {
                            //Só inclui o Fluxo se houver pelo menos um elemento diferente de zero
                            double valor = Array.Find(Prop.Negociacoes[i].TIR.Cliente.AmPm.RoyaltiesArco.Fluxo, element => element != 0.0);

                            if (valor > 0.0)
                            {
                                resEnc = new Resultado();
                                resEnc.codEcxDcx = Prop.Negociacoes[i].TIR.Cliente.AmPm.RoyaltiesArco.CodEcxDcx;
                                resEnc.Identificador = Prop.Negociacoes[i].TIR.Cliente.AmPm.RoyaltiesArco.Descricao;
                                resEnc.Fluxo = Prop.Negociacoes[i].TIR.Cliente.AmPm.RoyaltiesArco.Fluxo;

                                //Adiciona o resultado ao do Componente
                                resComp.ResultadosEncaixes.Add(resEnc);
                            }
                        }

                        //Royalty Fixo (só para AmPm)
                        if (Prop.Negociacoes[i].TIR.Cliente.AmPm.RoyaltyFixo != null)
                        {
                            //Só inclui o Fluxo se houver pelo menos um elemento diferente de zero
                            double valor = Array.Find(Prop.Negociacoes[i].TIR.Cliente.AmPm.RoyaltyFixo.Fluxo, element => element != 0.0);

                            if (valor > 0.0)
                            {
                                resEnc = new Resultado();
                                resEnc.codEcxDcx = Prop.Negociacoes[i].TIR.Cliente.AmPm.RoyaltyFixo.CodEcxDcx;
                                resEnc.Identificador = Prop.Negociacoes[i].TIR.Cliente.AmPm.RoyaltyFixo.Descricao;
                                resEnc.Fluxo = Prop.Negociacoes[i].TIR.Cliente.AmPm.RoyaltyFixo.Fluxo;

                                //Adiciona o resultado ao do Componente
                                resComp.ResultadosEncaixes.Add(resEnc);
                            }
                        }

                        //Demais Encaixes
                        if (Prop.Negociacoes[i].TIR.Cliente.AmPm.Encaixes != null &&
                            Prop.Negociacoes[i].TIR.Cliente.AmPm.Encaixes.Count > 0)
                        {
                            for (int k = 0; k < Prop.Negociacoes[i].TIR.Cliente.AmPm.Encaixes.Count; k++)
                            {
                                Resultado resEnca = new Resultado();

                                //Atualiza propriedades para o resultado do Encaixe
                                resEnca.codEcxDcx = Prop.Negociacoes[i].TIR.Cliente.AmPm.Encaixes[k].CodEcxDcx;
                                resEnca.Identificador = Prop.Negociacoes[i].TIR.Cliente.AmPm.Encaixes[k].Descricao;
                                resEnca.Fluxo = Prop.Negociacoes[i].TIR.Cliente.AmPm.Encaixes[k].Fluxo;

                                //Adiciona o resultado do Encaixe ao do Componente
                                resComp.ResultadosEncaixes.Add(resEnca);
                            }
                        }
                    }

                    //Adiciona o resultado do Componente ao da Negociação
                    resNeg.ResultadosComponentes.Add(resComp);
                }

                //Jet Oil
                if (Prop.Negociacoes[i].TIR.Cliente.JetOil != null)
                {
                    ResultadoComponente resComp = new ResultadoComponente();
                    //Obtem objeto Tipo de Componente
                    TipoCom tpComp = daoComp.ObterPorCodigo(Prop.Negociacoes[i].TIR.Cliente.JetOil.CodTipoComp);

                    if (tpComp != null)
                    {
                        resComp.Identificador = tpComp.NomeTipoComponente;
                        resComp.Fluxo = Prop.Negociacoes[i].TIR.Cliente.JetOil.Fluxo;
                        resComp.TIR = Prop.Negociacoes[i].TIR.Cliente.JetOil.TIR;
                        resComp.NPV = Prop.Negociacoes[i].TIR.Cliente.JetOil.NPV;
                        resComp.Payback = Prop.Negociacoes[i].TIR.Cliente.JetOil.Payback;
                        resComp.Inflacao = Prop.Negociacoes[i].TIR.Inflacao;

                        //Encaixes da AmPm (só geram Fluxo)
                        resComp.ResultadosEncaixes = new List<Resultado>();

                        Resultado resEnc = new Resultado();

                        //Overhead
                        if (Prop.Negociacoes[i].TIR.Cliente.JetOil.Overhead != null)
                        {
                            resEnc.codEcxDcx = Prop.Negociacoes[i].TIR.Cliente.JetOil.Overhead.CodEcxDcx;
                            resEnc.Identificador = Prop.Negociacoes[i].TIR.Cliente.JetOil.Overhead.Descricao;
                            resEnc.Fluxo = Prop.Negociacoes[i].TIR.Cliente.JetOil.Overhead.Fluxo;

                            //Adiciona o resultado ao do Componente
                            resComp.ResultadosEncaixes.Add(resEnc);
                        }

                        //Royalty Variável
                        if (Prop.Negociacoes[i].TIR.Cliente.JetOil.RoyaltyVar != null)
                        {
                            resEnc = new Resultado();
                            resEnc.codEcxDcx = Prop.Negociacoes[i].TIR.Cliente.JetOil.RoyaltyVar.CodEcxDcx;
                            resEnc.Identificador = Prop.Negociacoes[i].TIR.Cliente.JetOil.RoyaltyVar.Descricao;
                            resEnc.Fluxo = Prop.Negociacoes[i].TIR.Cliente.JetOil.RoyaltyVar.Fluxo;

                            //Adiciona o resultado ao do Componente
                            resComp.ResultadosEncaixes.Add(resEnc);
                        }

                        //Capital Giro Franquia - criado por Luiz Felipe em out/2014
                        if (Prop.Negociacoes[i].TIR.Cliente.JetOil.CapitalGiroFranquia != null)
                        {
                            resEnc = new Resultado();
                            resEnc.codEcxDcx = Prop.Negociacoes[i].TIR.Cliente.JetOil.CapitalGiroFranquia.CodEcxDcx;
                            resEnc.Identificador = Prop.Negociacoes[i].TIR.Cliente.JetOil.CapitalGiroFranquia.Descricao;
                            resEnc.Fluxo = Prop.Negociacoes[i].TIR.Cliente.JetOil.CapitalGiroFranquia.Fluxo;

                            //Adiciona o resultado ao do Componente
                            resComp.ResultadosEncaixes.Add(resEnc);
                        }

                        //Taxa de Franquia
                        if (Prop.Negociacoes[i].TIR.Cliente.JetOil.TaxaFranquia != null)
                        {
                            resEnc = new Resultado();
                            resEnc.codEcxDcx = Prop.Negociacoes[i].TIR.Cliente.JetOil.TaxaFranquia.CodEcxDcx;
                            resEnc.Identificador = Prop.Negociacoes[i].TIR.Cliente.JetOil.TaxaFranquia.Descricao;
                            resEnc.Fluxo = Prop.Negociacoes[i].TIR.Cliente.JetOil.TaxaFranquia.Fluxo;

                            //Adiciona o resultado ao do Componente
                            resComp.ResultadosEncaixes.Add(resEnc);
                        }

                        //Propaganda
                        if (Prop.Negociacoes[i].TIR.Cliente.JetOil.Propaganda != null)
                        {
                            //Só inclui o Fluxo se houver pelo menos um elemento diferente de zero
                            double valor = Array.Find(Prop.Negociacoes[i].TIR.Cliente.JetOil.Propaganda.Fluxo, element => element != 0.0);

                            if (valor > 0.0)
                            {
                                resEnc = new Resultado();
                                resEnc.codEcxDcx = Prop.Negociacoes[i].TIR.Cliente.JetOil.Propaganda.CodEcxDcx;
                                resEnc.Identificador = Prop.Negociacoes[i].TIR.Cliente.JetOil.Propaganda.Descricao;
                                resEnc.Fluxo = Prop.Negociacoes[i].TIR.Cliente.JetOil.Propaganda.Fluxo;

                                //Adiciona o resultado ao do Componente
                                resComp.ResultadosEncaixes.Add(resEnc);
                            }
                        }

                        //Terceirização
                        if (Prop.Negociacoes[i].TIR.Cliente.JetOil.Terceirizacao != null)
                        {
                            //Só inclui o Fluxo se houver pelo menos um elemento diferente de zero
                            double valor = Array.Find(Prop.Negociacoes[i].TIR.Cliente.JetOil.Terceirizacao.Fluxo, element => element != 0.0);

                            if (valor > 0.0)
                            {
                                resEnc = new Resultado();
                                resEnc.codEcxDcx = Prop.Negociacoes[i].TIR.Cliente.JetOil.Terceirizacao.CodEcxDcx;
                                resEnc.Identificador = Prop.Negociacoes[i].TIR.Cliente.JetOil.Terceirizacao.Descricao;
                                resEnc.Fluxo = Prop.Negociacoes[i].TIR.Cliente.JetOil.Terceirizacao.Fluxo;

                                //Adiciona o resultado ao do Componente
                                resComp.ResultadosEncaixes.Add(resEnc);
                            }
                        }

                        //Demais Encaixes
                        if (Prop.Negociacoes[i].TIR.Cliente.JetOil.Encaixes != null &&
                            Prop.Negociacoes[i].TIR.Cliente.JetOil.Encaixes.Count > 0)
                        {
                            for (int k = 0; k < Prop.Negociacoes[i].TIR.Cliente.JetOil.Encaixes.Count; k++)
                            {
                                Resultado resEnca = new Resultado();

                                //Atualiza propriedades para o resultado do Encaixe
                                resEnca.codEcxDcx = Prop.Negociacoes[i].TIR.Cliente.JetOil.Encaixes[k].CodEcxDcx;
                                resEnca.Identificador = Prop.Negociacoes[i].TIR.Cliente.JetOil.Encaixes[k].Descricao;
                                resEnca.Fluxo = Prop.Negociacoes[i].TIR.Cliente.JetOil.Encaixes[k].Fluxo;

                                //Adiciona o resultado do Encaixe ao do Componente
                                resComp.ResultadosEncaixes.Add(resEnca);
                            }
                        }
                    }

                    //Adiciona o resultado do Componente ao da Negociação
                    resNeg.ResultadosComponentes.Add(resComp);
                }

                //Jet Oil Motos
                if (Prop.Negociacoes[i].TIR.Cliente.JetMotos != null)
                {
                    ResultadoComponente resComp = new ResultadoComponente();
                    //Obtem objeto Tipo de Componente
                    TipoCom tpComp = daoComp.ObterPorCodigo(Prop.Negociacoes[i].TIR.Cliente.JetMotos.CodTipoComp);

                    if (tpComp != null)
                    {
                        resComp.Identificador = tpComp.NomeTipoComponente;
                        resComp.Fluxo = Prop.Negociacoes[i].TIR.Cliente.JetMotos.Fluxo;
                        resComp.TIR = Prop.Negociacoes[i].TIR.Cliente.JetMotos.TIR;
                        resComp.NPV = Prop.Negociacoes[i].TIR.Cliente.JetMotos.NPV;
                        resComp.Payback = Prop.Negociacoes[i].TIR.Cliente.JetMotos.Payback;
                        resComp.Inflacao = Prop.Negociacoes[i].TIR.Inflacao;

                        //Encaixes da AmPm (só geram Fluxo)
                        resComp.ResultadosEncaixes = new List<Resultado>();

                        //Overhead
                        Resultado resEnc = new Resultado();
                        if (Prop.Negociacoes[i].TIR.Cliente.JetMotos.Overhead != null)
                        {
                            resEnc.codEcxDcx = Prop.Negociacoes[i].TIR.Cliente.JetMotos.Overhead.CodEcxDcx;
                            resEnc.Identificador = Prop.Negociacoes[i].TIR.Cliente.JetMotos.Overhead.Descricao;
                            resEnc.Fluxo = Prop.Negociacoes[i].TIR.Cliente.JetMotos.Overhead.Fluxo;

                            //Adiciona o resultado ao do Componente
                            resComp.ResultadosEncaixes.Add(resEnc);
                        }

                        //Royalty Variável
                        if (Prop.Negociacoes[i].TIR.Cliente.JetMotos.RoyaltyVar != null)
                        {
                            resEnc = new Resultado();
                            resEnc.codEcxDcx = Prop.Negociacoes[i].TIR.Cliente.JetMotos.RoyaltyVar.CodEcxDcx;
                            resEnc.Identificador = Prop.Negociacoes[i].TIR.Cliente.JetMotos.RoyaltyVar.Descricao;
                            resEnc.Fluxo = Prop.Negociacoes[i].TIR.Cliente.JetMotos.RoyaltyVar.Fluxo;

                            //Adiciona o resultado ao do Componente
                            resComp.ResultadosEncaixes.Add(resEnc);
                        }

                        //Capital Giro Franquia - criado por Luiz Felipe em out/2014
                        if (Prop.Negociacoes[i].TIR.Cliente.JetMotos.CapitalGiroFranquia != null)
                        {
                            resEnc = new Resultado();
                            resEnc.codEcxDcx = Prop.Negociacoes[i].TIR.Cliente.JetMotos.CapitalGiroFranquia.CodEcxDcx;
                            resEnc.Identificador = Prop.Negociacoes[i].TIR.Cliente.JetMotos.CapitalGiroFranquia.Descricao;
                            resEnc.Fluxo = Prop.Negociacoes[i].TIR.Cliente.JetMotos.CapitalGiroFranquia.Fluxo;

                            //Adiciona o resultado ao do Componente
                            resComp.ResultadosEncaixes.Add(resEnc);
                        }

                        //Taxa de Franquia
                        if (Prop.Negociacoes[i].TIR.Cliente.JetMotos.TaxaFranquia != null)
                        {
                            resEnc = new Resultado();
                            resEnc.codEcxDcx = Prop.Negociacoes[i].TIR.Cliente.JetMotos.TaxaFranquia.CodEcxDcx;
                            resEnc.Identificador = Prop.Negociacoes[i].TIR.Cliente.JetMotos.TaxaFranquia.Descricao;
                            resEnc.Fluxo = Prop.Negociacoes[i].TIR.Cliente.JetMotos.TaxaFranquia.Fluxo;

                            //Adiciona o resultado ao do Componente
                            resComp.ResultadosEncaixes.Add(resEnc);
                        }

                        //Propaganda
                        if (Prop.Negociacoes[i].TIR.Cliente.JetMotos.Propaganda != null)
                        {
                            //Só inclui o Fluxo se houver pelo menos um elemento diferente de zero
                            double valor = Array.Find(Prop.Negociacoes[i].TIR.Cliente.JetMotos.Propaganda.Fluxo, element => element != 0.0);

                            if (valor > 0.0)
                            {
                                resEnc = new Resultado();
                                resEnc.codEcxDcx = Prop.Negociacoes[i].TIR.Cliente.JetMotos.Propaganda.CodEcxDcx;
                                resEnc.Identificador = Prop.Negociacoes[i].TIR.Cliente.JetMotos.Propaganda.Descricao;
                                resEnc.Fluxo = Prop.Negociacoes[i].TIR.Cliente.JetMotos.Propaganda.Fluxo;

                                //Adiciona o resultado ao do Componente
                                resComp.ResultadosEncaixes.Add(resEnc);
                            }
                        }

                        //Terceirização
                        if (Prop.Negociacoes[i].TIR.Cliente.JetMotos.Terceirizacao != null)
                        {
                            //Só inclui o Fluxo se houver pelo menos um elemento diferente de zero
                            double valor = Array.Find(Prop.Negociacoes[i].TIR.Cliente.JetMotos.Terceirizacao.Fluxo, element => element != 0.0);

                            if (valor > 0.0)
                            {
                                resEnc = new Resultado();
                                resEnc.codEcxDcx = Prop.Negociacoes[i].TIR.Cliente.JetMotos.Terceirizacao.CodEcxDcx;
                                resEnc.Identificador = Prop.Negociacoes[i].TIR.Cliente.JetMotos.Terceirizacao.Descricao;
                                resEnc.Fluxo = Prop.Negociacoes[i].TIR.Cliente.JetMotos.Terceirizacao.Fluxo;

                                //Adiciona o resultado ao do Componente
                                resComp.ResultadosEncaixes.Add(resEnc);
                            }
                        }

                        //Demais Encaixes
                        if (Prop.Negociacoes[i].TIR.Cliente.JetMotos.Encaixes != null &&
                            Prop.Negociacoes[i].TIR.Cliente.JetMotos.Encaixes.Count > 0)
                        {
                            for (int k = 0; k < Prop.Negociacoes[i].TIR.Cliente.JetMotos.Encaixes.Count; k++)
                            {
                                Resultado resEnca = new Resultado();

                                //Atualiza propriedades para o resultado do Encaixe
                                resEnca.Identificador = Prop.Negociacoes[i].TIR.Cliente.JetMotos.Encaixes[k].Descricao;
                                resEnca.codEcxDcx = Prop.Negociacoes[i].TIR.Cliente.JetMotos.Encaixes[k].CodEcxDcx;
                                resEnca.Fluxo = Prop.Negociacoes[i].TIR.Cliente.JetMotos.Encaixes[k].Fluxo;

                                //Adiciona o resultado do Encaixe ao do Componente
                                resComp.ResultadosEncaixes.Add(resEnca);
                            }
                        }
                    }

                    //Adiciona o resultado do Componente ao da Negociação
                    resNeg.ResultadosComponentes.Add(resComp);
                }

                //Adiciona o resultado da Negociação ao da Proposta
                resProp.ResultadosNegociacoes.Add(resNeg);
            }

            //Obtem a maior dimensão dos fluxos de TIRs de Negociações
            int maxFluxo = 0;
            for (int i = 0; i < resProp.ResultadosNegociacoes.Count; i++)
            {
                if (maxFluxo < resProp.ResultadosNegociacoes[i].Fluxo.GetUpperBound(0))
                {
                    maxFluxo = resProp.ResultadosNegociacoes[i].Fluxo.GetUpperBound(0);
                }
            }

            //dimensiona o fluxo da Proposta
            resProp.Fluxo = new double[maxFluxo + 1];

            //inclui os fluxos das TIRs de Negociações
            //no fluxo da TIR da Proposta
            for (int i = 0; i < resProp.ResultadosNegociacoes.Count; i++)
            {
                for (int j = 0; j <= resProp.ResultadosNegociacoes[i].Fluxo.GetUpperBound(0); j++)
                {
                    resProp.Fluxo[j] += resProp.ResultadosNegociacoes[i].Fluxo[j];
                }
            }

            //Se houver apenas uma Negociação na Proposta, os valores 
            //da TIR da Proposta são os da TIR dessa Negociação
            if (resProp.ResultadosNegociacoes.Count == 1)
            {
                resProp.Inflacao = resProp.ResultadosNegociacoes[0].Inflacao;
                resProp.TIR = resProp.ResultadosNegociacoes[0].TIR;
                resProp.NPV = resProp.ResultadosNegociacoes[0].NPV;
                resProp.Payback = resProp.ResultadosNegociacoes[0].Payback;
            }
            //Existe mais de uma Negociação na Proposta;
            //é necessário calcular TIR, NPV e Payback
            else
            {
                //---- A tir é calculada por ano, por isso a fórmula abaixo

                //calcula o valor da tir
                double[] fluxo = resProp.Fluxo;
                try
                {
                    resProp.TIR = (Math.Pow((1.0 + Microsoft.VisualBasic.Financial.IRR(ref fluxo, 1.15151291201983E-02)), 12.0) - 1.0) * 100.0;
                }
                catch
                {
                    //se tem erro na fórmula, define valor padrão para a TIR
                    resProp.TIR = -999999.777777;
                }
                
                try
                {
                    //***Inserido por Luiz Felipe em setembro/2011
                    //***A TIR considerada passa a ser a nominal
                    InflacaoDao inflaDAO = new InflacaoDao();
                    Double inflaTIRNominal = inflaDAO.ObterInflacaoMedia();

                    resProp.TIR = (((1.0 + resProp.TIR / 100.0) * (1.0 + inflaTIRNominal / 100.0)) - 1.0) * 100.0;

                    //Descrição: Alteração pedida pelo Mauricio Macedo em 21/11/2007;
                    //           Quando a TIR for negativa colocá-la como indefinida.
                    //           Tarefa: 005F8747.
                    if (resProp.TIR < 0.0)
                    {
                        resProp.TIR = -999999.777777;
                    }

                    //---- Cálculo do NPV (valor presente)

                    //variáveis de apoio
                    double[] Awork = new double[resProp.Fluxo.GetUpperBound(0)];
                    double CustoAno;

                    //varre fluxo
                    for (int i = 1; i <= resProp.Fluxo.GetUpperBound(0); i++)
                    {
                        //copia fluxo no vetor de trabalho (atenção aos índices)
                        Awork[i - 1] = resProp.Fluxo[i];
                    }

                    //busca custo de oportunidade
                    OutrosParamDao outrosDAO = new OutrosParamDao();
                    OutrosParam outros = outrosDAO.ObterParametro("CustoOport");

                    //calcula custo anual
                    CustoAno = Math.Pow((1.0 + (double)outros.ValorParametroNumerico / 100.0), (1.0 / 12.0)) - 1.0;

                    //calcula npv
                    resProp.NPV = (Financial.NPV(CustoAno, ref Awork) + resProp.Fluxo[0]) * Prop.ValorFAO;

                    //formata valor da tir para o caso em que não tenha sido possível calcular (extremos)
                    if (Math.Abs(resProp.TIR) == 999999.777777 && resProp.NPV > 0.0)
                    {
                        resProp.TIR = 999999.777777;
                    }

                    //----- Cálculo do PayBack ----------

                    //declaração de variáveis
                    double wTaxa;
                    double wSaldo;
                    int wMaior;
                    bool wFlag;

                    //começa cálculo do saldo e da taxa
                    wSaldo = resProp.Fluxo[0];
                    wTaxa = Math.Pow((1.0 + CustoAno / 100.0), (1.0 / 12.0)) - 1.0;
                    wMaior = -1;

                    //seta flag como falso
                    wFlag = false;

                    //varre fluxo
                    for (int i = 1; i <= maxFluxo; i++)
                    {
                        //cálculo do saldo
                        wSaldo += resProp.Fluxo[i] / Math.Pow((1.0 + wTaxa), i);

                        if (wFlag == false)
                        {
                            if (wSaldo >= 0.0)
                            {
                                wMaior = i;
                                wFlag = true;
                            }
                        }
                        else
                        {
                            if (wSaldo < 0.0)
                            {
                                wFlag = false;
                            }
                        }
                    }

                    //define payback
                    resProp.Payback = wMaior;

                    if (wFlag == false)
                    {
                        resProp.Payback = -1;
                    }
                }
                catch
                {
                    //se tem erro na fórmula, define valor padrão para a TIR
                    resProp.TIR = -999999.777777;
                }
            }

        //retorno com sucesso    
        return resProp;
        }
    }
}
