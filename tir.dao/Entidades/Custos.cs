﻿
namespace tir.dao.Entidades
{
    public class Custos
    {
        #region Propriedades
        public string CodigoGV { get; set; }
        public string SiglaUF { get; set; }
        public string SegmentoTipoNegociacao { get; set; }
        public int IdPerfil { get; set; }
        public string CodigoProduto { get; set; }
        public int NumeroMesFaixa { get; set; } //todo: ver com Felipe
        public double ValorCusto {get; set;}
        #endregion
    }
}
