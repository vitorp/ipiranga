﻿
namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que representa ...
    /// </summary>
    public class FaixasDia
    {
        #region Propriedades

        public string CodNumDias { get; set; }
        public string NumDias { get; set; }
        public string AnoMes { get; set; }
        public float ValorTotal { get; set; }
        public float Percentual { get; set; }

        #endregion
    }
}
