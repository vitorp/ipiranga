﻿using TIR.Entidades.ItensFranquias;

namespace TIR.Entidades.Componentes
{
    /// <summary>
    /// Componente que representa um AmPm.
    /// </summary>
    public class AmPm : Franquia
    {
        #region Propriedades

        public RoyaltyFixo RoyaltyFixo { get; set; }
        public RoyaltiesArco RoyaltiesArco { get; set; }

        #endregion
    }
}
