﻿
namespace TIR.Entidades.Encaixes
{
    /// <summary>
    /// Encaixe que representa um capital de giro.
    /// </summary>
    public class CapitalGiro : Encaixe
    {
        #region Propriedades

        public long CodProduto { get; set; }
        public long CodGrupo { get; set; }

        #endregion
    }
}
