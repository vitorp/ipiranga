﻿
using System;
namespace tir.dao.Entidades
{
    public class RedutorFat
    {
        #region Propriedades
        public long CodigoTipoComponente { get; set; }
        public long CodigoPadraoComponente { get; set; }
        public long CodigoPerfilComponente { get; set; }
        public int NumeroMesFaixaRedutores { get; set; }
        public Single PercentualReducaoFaturamento { get; set; }
        #endregion
    }
}
