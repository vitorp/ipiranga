﻿using System;
using System.Collections.Generic;
using System.IO;

namespace TIR.Entidades.Previa
{
    public class Previa
    {
        #region Propriedades

        //Propriedades simples.
        public int Codigo { get; set; }
        public string CodigoPV { get; set; }
        public DateTime DataElaboracao { get; set; }
        public DateTime DataEnvio { get; set; }
        public string Descricao { get; set; }
        public DateTime DataValidade { get; set; }
        public string Obra { get; set; }
        public string BaseEquipamento { get; set; }
        public string BaseServico { get; set; }
        public string EngenheiroResp { get; set; }
        public string VolVendasPrevisto { get; set; }
        public string Area { get; set; }
        public int ClasseRisco { get; set; }
        public string ConceitoImagem { get; set; }
        public string Politica { get; set; }
        public string Versao { get; set; }
        public string Elaborador { get; set; }
        public string ResponsavelEnvio { get; set; }
        public string SegmentoMercado { get; set; }
        public string TotalOrcamento { get; set; }
        public double PercentPartOperador { get; set; }
        public double ValorPartOperador { get; set; }
        public double ValorFinalObra { get; set; }
        public double ValorOriginalObra { get; set; }
        public string CaminhoArquivo { get; set; }
        public string NomeArquivo { get; set; }

        //Atributos tipo coleção.
        public List<Sumario> Sumarios { get; set; }
        public List<Detalhe> Detalhes { get; set; }
        #endregion

        #region Métodos Públicos

        /// <summary>
        /// Método público que calcula o valor final das obras civis.
        /// </summary>
        /// <param name="isPerc">Se verdadeiro, indica que "partOperador" é um percentual.</param>
        /// <param name="partOperador">Participação do operador na obra; pode ser percentual ou valor.</param>
        /// <returns>Valor final das obras civis.</returns>
        public double CalculaValorFinalObra(bool isPerc, double partOperador)
        {
            double valorAtual = 0.0;

            //Localiza as obras no sumário
            int pos = Sumarios.FindIndex(s => s.Descricao == "OBRAS CIVIS");

            //Calcula o valor total da obra
            ValorOriginalObra = Sumarios[pos].Valor + Sumarios[pos].ValorInstalacao;
            valorAtual = Sumarios[pos].Valor + Sumarios[pos].ValorInstalacao;

            //Se for calcular pelo percentual, atribui o valor escolhido ao atributo e
            //calcula o valor final da obra com o desconto do percentual do operador
            if (isPerc == true)
            {
                PercentPartOperador = Convert.ToDouble(String.Format("###,###,##0.00", partOperador));

                //Calcula o valor do operador em função do percentual da participação
                ValorPartOperador = Convert.ToDouble(String.Format("###,###,##0.00", valorAtual * (partOperador / 100.0)));
                valorAtual = valorAtual - (valorAtual * (partOperador / 100.0));
            }
            //Não é pelo percentual
            else
            {
                ValorPartOperador = Convert.ToDouble(String.Format("###,###,##0.00", partOperador));

                //Calcula o percentual do operador em função do valor da participação
                if (valorAtual > 0.0)
                {
                    PercentPartOperador = Convert.ToDouble(String.Format("###,###,##0.00", partOperador / valorAtual * 100.0));
                }
                else
                {
                    PercentPartOperador = 0.00;
                }

                //Abate diretamente a participação do operador
                valorAtual = valorAtual - partOperador;
            }

            //Atribui o valor final calculado ao atributo
            ValorFinalObra = valorAtual;

            //Retorna o valor calculado
            return valorAtual;
        }

        /// <summary>
        /// Método público que calcula o valor final de instalação
        /// para um item da Prévia.
        /// </summary>
        /// <param name="descricao">Descrição do item.</param>
        /// <param name="isPerc">Se verdadeiro, indica que "partOperador" é um percentual.</param>
        /// <param name="partOperador">Participação do operador no item; pode ser percentual ou valor.</param>
        /// <returns>Valor final da instalação do item.</returns>
        public double CalculaValorFinalInstalacao(string descricao, bool isPerc, double partOperador)
        {
            double valorAtual = 0.0;

            //Localiza o item no sumário
            int pos = Sumarios.FindIndex(s => s.Descricao == descricao);

            //Incluído por Luiz Felipe em julho/2011 para atualizar os itens
            //relativos a Obras Civis no Sumário da Prévia, que não possuem
            //valor de instalação, e que não eram atualizados nesta rotina
            if (descricao.Trim().ToUpper() == "OBRAS CIVIS" ||
                descricao.Trim().ToUpper() == "OBRAS CIVIS POSTO" ||
                descricao.Trim().ToUpper() == "OBRAS CIVIS AMPM" ||
                descricao.Trim().ToUpper() == "OBRAS CIVIS JETOIL" ||
                descricao.Trim().ToUpper() == "OBRAS CIVIS JETOIL MOTOS" ||
                descricao.Trim().ToUpper() == "OBRAS CIVIS PARCEIROS")
            {
                Sumarios[pos].ValorOriginalInstal = Sumarios[pos].Valor;
                valorAtual = Sumarios[pos].Valor;
            }
            //Itens relativos a Equipamentos e/ou Elementos de Imagem
            else
            {
                Sumarios[pos].ValorOriginalInstal = Sumarios[pos].ValorInstalacao;
                valorAtual = Sumarios[pos].ValorInstalacao;
            }

            //Se for calcular pelo percentual, atribui o valor escolhido ao atributo e
            //calcula o valor final de instalação dos equipamentos com o desconto do 
            //percentual do operador
            if (isPerc == true)
            {
                Sumarios[pos].PercentPartOperador = partOperador;

                //Calcula o valor do operador em função do percentual da participação
                Sumarios[pos].ValorPartOperador = Convert.ToDouble(String.Format("###,###,##0.00", valorAtual * (partOperador / 100.0)));
                valorAtual = valorAtual - (valorAtual * (partOperador / 100.0));
            }
            //Não é pelo percentual
            else
            {
                Sumarios[pos].ValorPartOperador = partOperador;

                //Calcula o percentual do operador em função do valor da participação
                Sumarios[pos].PercentPartOperador = Convert.ToDouble(String.Format("###,###,##0.00", partOperador / valorAtual * 100.0));

                //Abate diretamente a participação do operador
                valorAtual = valorAtual - partOperador;
            }

            //Atribui o valor final calculado ao atributo
            Sumarios[pos].ValorFinalInstal = valorAtual;

            //Retorna o valor calculado
            return valorAtual;
        }

        /// <summary>
        /// Método público que monta a tela de detalhes da Prévia.
        /// </summary>
        /// <param name="caminho">Caminho do local onde está o arquivo de detalhamento da Prévia.</param>
        public void CriaDetalhePrevia(string caminho)
        {
            TextWriter tw = new StreamWriter(caminho + "detalhePrevia.htm");

            tw.WriteLine( "<html>");
            tw.WriteLine( "<head>");
            tw.WriteLine( "<style>");
            tw.WriteLine( ".Titulo{PADDING-RIGHT: 0.75pt; PADDING-LEFT: 0.75pt; FONT-SIZE: 8pt; BACKGROUND: #FFFFFF; PADDING-BOTTOM: 0.75pt; COLOR: black; PADDING-TOP: 0.75pt; FONT-FAMILY: Arial}");
            tw.WriteLine( ".CabecalhotdTable {PADDING-RIGHT: 0.75pt; PADDING-LEFT: 0.75pt; FONT-SIZE: 8pt; BACKGROUND: #6295aa; PADDING-BOTTOM: 0.75pt; COLOR: white; PADDING-TOP: 0.75pt; FONT-FAMILY: Arial}");
            tw.WriteLine( ".DadostdTable {PADDING-RIGHT: 0.75pt; PADDING-LEFT: 0.75pt; FONT-SIZE: 8pt; BACKGROUND: #ffffff; PADDING-BOTTOM: 0.75pt; COLOR: black; PADDING-TOP: 0.75pt; FONT-FAMILY: Arial}");
            tw.WriteLine( ".SubTitulo{PADDING-RIGHT: 0.75pt; PADDING-LEFT: 0.75pt; FONT-SIZE: 8pt; BACKGROUND: #b5b6b9; PADDING-BOTTOM: 0.75pt; COLOR: black; PADDING-TOP: 0.75pt; FONT-FAMILY: Arial}");
            tw.WriteLine( "</style>");
            tw.WriteLine( "</head>");
            tw.WriteLine( "<body>");
        
            //Cabeçalho Prévia
            tw.WriteLine( "<!--Início Tabela de título-->");
            tw.WriteLine( "<table width='100%' class='Titulo'>");
            tw.WriteLine( "<tbody>");
            tw.WriteLine( "<tr>");
            tw.WriteLine( "<td>");
            tw.WriteLine( "<b>Código: </b>");
            tw.WriteLine( "</td>");
            tw.WriteLine( "<td>" + Codigo);
            tw.WriteLine( "</td>");
            tw.WriteLine( "<td>");
            tw.WriteLine( "<b>Data Elaboração: </b>");
            tw.WriteLine( "</td>");
            tw.WriteLine( "<td>" + DataElaboracao);
            tw.WriteLine( "</td>");
            tw.WriteLine( "<td>");
            tw.WriteLine( "<b>Data Envio: </b>");
            tw.WriteLine( "</td>");
            tw.WriteLine( "<td>" + DataEnvio);
            tw.WriteLine( "</td>");
            tw.WriteLine( "</tr>");
            tw.WriteLine( "<tr>");
            tw.WriteLine( "<td>");
            tw.WriteLine( "<b>Descrição: </b>");
            tw.WriteLine( "</td>");
            tw.WriteLine( "<td>" + Descricao);
            tw.WriteLine( "</td>");
            tw.WriteLine( "</tr>");
            tw.WriteLine( "<tr>");
            tw.WriteLine( "<td>");
            tw.WriteLine( "<b>Data Validade: </b>");
            tw.WriteLine( "</td>");
            tw.WriteLine( "<td>" + DataValidade);
            tw.WriteLine( "</td>");
            tw.WriteLine( "<td>");
            tw.WriteLine( "<b>Obra: </b></td>");
            tw.WriteLine( "<td>" + Obra);
            tw.WriteLine( "</td>");
            tw.WriteLine( "</tr>");
            tw.WriteLine( "<tr>");
            tw.WriteLine( "<td>");
            tw.WriteLine( "<b>Bases Equipamento: </b>");
            tw.WriteLine( "</td>");
            tw.WriteLine( "<td>" + BaseEquipamento);
            tw.WriteLine( "</td>");
            tw.WriteLine( "<td>");
            tw.WriteLine( "<b>Bases Servico: </b></td>");
            tw.WriteLine( "<td>" + BaseServico);
            tw.WriteLine( "</td>");
            tw.WriteLine( "</tr>");
            tw.WriteLine( "<tr>");
            tw.WriteLine( "<td>");
            tw.WriteLine( "<b>Engenheiro Responsável: </b>");
            tw.WriteLine( "</td>");
            tw.WriteLine( "<td>" + EngenheiroResp);
            tw.WriteLine( "</td>");
            tw.WriteLine( "<td>");
            tw.WriteLine( "<b>Volume Vendas Previsto (m3): </b>");
            tw.WriteLine( "</td>");
            tw.WriteLine( "<td>" + VolVendasPrevisto);
            tw.WriteLine( "</td>");
            tw.WriteLine( "<td>");
            tw.WriteLine( "<b>Área do Terreno (m2): </b>");
            tw.WriteLine( "</td>");
            tw.WriteLine( "<td>" + Area);
            tw.WriteLine( "</td>");
            tw.WriteLine( "</tr>");
            tw.WriteLine( "<tr>");
            tw.WriteLine( "<td>");
            tw.WriteLine( "<b>Classe de Risco: </b>");
            tw.WriteLine( "</td>");
            tw.WriteLine( "<td>" + ClasseRisco);
            tw.WriteLine( "</td>");
            tw.WriteLine( "<td>");
            tw.WriteLine( "<b>Conceito de Imagem: </b>");
            tw.WriteLine( "</td>");
            tw.WriteLine( "<td>" + ConceitoImagem);
            tw.WriteLine( "</td>");
            tw.WriteLine( "</tr>");
            tw.WriteLine( "<tr>");
            tw.WriteLine( "<td>");
            tw.WriteLine( "<b>Possui Política: </b>");
            tw.WriteLine( "</td>");
            tw.WriteLine( "<td>" + Politica);
            tw.WriteLine( "</td>");
            tw.WriteLine( "<td>");
            tw.WriteLine( "<b>Versão: </b>");
            tw.WriteLine( "</td>");
            tw.WriteLine( "<td>" + Versao);
            tw.WriteLine( "</td>");
            tw.WriteLine( "</tr>");
            tw.WriteLine( "<tr>");
            tw.WriteLine( "<td>");
            tw.WriteLine( "<b>Elaborador: </b>");
            tw.WriteLine( "</td>");
            tw.WriteLine( "<td>" + Elaborador);
            tw.WriteLine( "</td>");
            tw.WriteLine( "<td>");
            tw.WriteLine( "<b>Responsável Envio: </b>");
            tw.WriteLine( "</td>");
            tw.WriteLine( "<td>" + ResponsavelEnvio);
            tw.WriteLine( "</td>");
            tw.WriteLine( "</tr>");
            tw.WriteLine( "<tr>");
            tw.WriteLine( "<td>");
            tw.WriteLine( "<b>Segmento de Mercado: </b>");
            tw.WriteLine( "</td>");
            tw.WriteLine("<td>" + SegmentoMercado);
            tw.WriteLine( "</td>");
            tw.WriteLine( "<td>");
            tw.WriteLine( "<b>Total Orçado: </b>");
            tw.WriteLine( "</td>");
            tw.WriteLine( "<td>" + TotalOrcamento);
            tw.WriteLine( "</td>");
            tw.WriteLine( "</tr>");
            tw.WriteLine( "</tbody>");
            tw.WriteLine( "</table>");
            tw.WriteLine( "<!--Fim Tabela de título-->");
        
    //        //Sumarios
    //        tw.WriteLine( "<!--Inicio Cabecalho Sumario-->");
    //        tw.WriteLine( "<table width='100%'>");
    //        tw.WriteLine( "<tbody>");
    //        tw.WriteLine( "<tr class='CabecalhotdTable'>");
    //        tw.WriteLine( "<td width='85%'>");
    //        //<img src='image003.gif'>
    //        tw.WriteLine( "<b>=> SUMÁRIOS </b>");
    //        tw.WriteLine( "</td>");
    //        tw.WriteLine( "</tr>");
    //        tw.WriteLine( "</tbody>");
    //        tw.WriteLine( "</table>");
    //        tw.WriteLine( "<!--Fim Cabecalho Sumario-->");
    //
    //        tw.WriteLine( "<!--Inicio Dados Sumario-->");
    //        tw.WriteLine( "<table width='100%'>");
    //        tw.WriteLine( "<tbody>");
    //        tw.WriteLine( "<tr>");
    //        tw.WriteLine( "<td>");
    //        tw.WriteLine( "<b>Descrição</b>");
    //        tw.WriteLine( "</td>");
    //        tw.WriteLine( "<td>");
    //        tw.WriteLine( "<b>Valor Equipamento </b>");
    //        tw.WriteLine( "</td>");
    //        tw.WriteLine( "<td>");
    //        tw.WriteLine( "<b>Valor Instalação</b>");
    //        tw.WriteLine( "</td>");
    //        tw.WriteLine( "<td>");
    //        tw.WriteLine( "<b>Valor Total </b>");
    //        tw.WriteLine( "</td>");
    //        tw.WriteLine( "</tr>");
    //
    //        For i = 1 To mcolSumario.count
    //
    //            tw.WriteLine( "<tr>");
    //            tw.WriteLine( "<td>" & mcolSumario.item(i).Descricao);
    //            tw.WriteLine( "</td>"
    //            tw.WriteLine( "<td>" & Format(mcolSumario.item(i).Valor, MOEDA));
    //            tw.WriteLine( "</td>"
    //            tw.WriteLine( "<td>" & Format(mcolSumario.item(i).ValorInstalacao, MOEDA));
    //            tw.WriteLine( "</td>"
    //            tw.WriteLine( "<td>" & Format((mcolSumario.item(i).Valor + mcolSumario.item(i).ValorInstalacao), MOEDA));
    //            tw.WriteLine( "</td>");
    //            tw.WriteLine( "</tr>");
    //
    //        Next i
    //
    //        tw.WriteLine( "</tbody>");
    //        tw.WriteLine( "</table>");
    //
    //        tw.WriteLine( "<!--Fim Dados Sumario-->");
    //        tw.WriteLine( "<br />");
    //
            //Detalhes e Serviços
            for (int i = 0; i < Detalhes.Count; i++)
            {        
                tw.WriteLine( "<!--Início Tabela de cabeçalho titulo principal-->");
                tw.WriteLine( "<table width='100%'>");
                tw.WriteLine( "<tbody>");
                tw.WriteLine( "<tr class='CabecalhotdTable'>");
                tw.WriteLine( "<td width='85%'>");
                //<img src='image003.gif'/>
                tw.WriteLine( "<b>=> " + Detalhes[i].Descricao + "</b>");
                tw.WriteLine( "</td>");
                tw.WriteLine( "<td align='Right'>");
                tw.WriteLine( "<b>" + String.Format("###,###,##0.00", Detalhes[i]) + " </b>");
                tw.WriteLine( "</td>");
                tw.WriteLine( "</tr>");
                tw.WriteLine( "</tbody>");
                tw.WriteLine( "</table>");
                tw.WriteLine( "<!--Fim Tabela de cabeçalho-->");

                //Tratamento diferente para obras civis
                //If UCase(mcolDetalhe.item(i).Descricao) = "1 - OBRAS CIVIS" Then

                    //For j = 1 To mcolDetalhe.item(i).colServico.count
                        
                    //        tw.WriteLine( "<!--Início Tabela de Dados Obras Civis-->"
                    //        tw.WriteLine( "<table width='100%'>"
                    //        tw.WriteLine( "<tbody>"
                    //        tw.WriteLine( "<tr class='DadostdTable'>"
                    //        tw.WriteLine( "<td>"
                    //        '<img height='1' src='image004.gif' width='25' />
                    //        tw.WriteLine( "<img height='1' src='image004.gif' width='25' /><b>" & mcolDetalhe.item(i).colServico.item(j).Nome & "</b>"
                    ////        tw.WriteLine( "</td>"
                    //        tw.WriteLine( "<td align='Right'>"
                    //        tw.WriteLine( "<b>" & Format(mcolDetalhe.item(i).colServico.item(j).Valor, MOEDA) & "</b>"
                    //        tw.WriteLine( "</td>"
                    //        tw.WriteLine( "</tr>"
                    //        tw.WriteLine( "</tbody>"
                    //        tw.WriteLine( "</table>"
                    //        tw.WriteLine( "<!--Fim Tabela de Dados Obras Civis-->"
        
                    //Next j
                
                //Else
                
                if (Detalhes[i].Descricao.ToUpper() != "1 - OBRAS CIVIS")
                {
                
                    tw.WriteLine( "<table width='100%'>");
                    tw.WriteLine( "<tbody>");
                    tw.WriteLine( "<tr class='SubTitulo'>");
                    tw.WriteLine( "<!--Subtitulo-->");
                    tw.WriteLine( "<td>");
                
                    if (Detalhes[i].Descricao.ToUpper() == "3 - FORNECIMENTO E INSTALAÇÃO DE ELEMENTOS DE IMAGEM")
                    {
                        tw.WriteLine( "<img height='1' src='image004.gif' width='25'/> <b>Elementos de Imagem</b>");
                    }
                    else
                    {
                        tw.WriteLine( "<img height='1' src='image004.gif' width='25'/> <b>Equipamentos</b>");
                    }
                
                    tw.WriteLine( "</td>");
                    tw.WriteLine( "<td align='middle' width='100'>");
                    tw.WriteLine( "<b>Un. Medida</b>");
                    tw.WriteLine( "</td>");
                    tw.WriteLine( "<td align='middle' width='100'>");
                    tw.WriteLine( "<b>Quantidade</b>");
                    tw.WriteLine( "</td>");
                    tw.WriteLine( "<td align='middle' width='100'>");
                    tw.WriteLine( "<b>Valor Unitário</b>");
                    tw.WriteLine( "</td>");
                    tw.WriteLine( "<td align='middle' width='100'>");
                    tw.WriteLine( "<b>Valor Total</b>");
                    tw.WriteLine( "</td>");
                    tw.WriteLine( "</tr>");
                    tw.WriteLine( "</tbody>");
                    tw.WriteLine( "</table>");
                        
                    //Printando os equipamentos
                    if (Detalhes[i].EquipamentosPrevia.Count > 0)
                    {                   
                        for (int k = 0; k < Detalhes[i].EquipamentosPrevia.Count; k++)
                        {                       
                            tw.WriteLine( "<!--Início Tabela de Dados Fornecimento e Instalações de Equipamentos-->");
                            tw.WriteLine( "<table width='100%'>");
                            tw.WriteLine( "<tbody>");
                            tw.WriteLine( "<tr class='DadostdTable'>");
                            tw.WriteLine( "<td>");
                            tw.WriteLine( "<img height='1' src='image004.gif' width='25'/> <b>" + Detalhes[i].EquipamentosPrevia[k].Nome + "</b>");
                            tw.WriteLine( "</td>");
                        
                            //Alterado por Luiz Felipe em março/2011 - imprimia valores em linhas de título
                            if (Detalhes[i].EquipamentosPrevia[k].UnidadeMedida != "")
                            {
                                tw.WriteLine( "<td align='middle' width='100'>" + Detalhes[i].EquipamentosPrevia[k].UnidadeMedida);
                                tw.WriteLine( "</td>");
                                tw.WriteLine( "<td align='middle' width='100'>" + Detalhes[i].EquipamentosPrevia[k].Quantidade);
                                tw.WriteLine( "</td>");
                                tw.WriteLine( "<td align='right' width='100'>");
                                tw.WriteLine( "<b>" + String.Format("###,###,##0.00", Detalhes[i].EquipamentosPrevia[k].ValorUnitario) + "</b>");
                                tw.WriteLine( "</td>");
                            }
                        
                            tw.WriteLine( "<td align='right' width='100'>");
                            tw.WriteLine( "<b>" + String.Format("###,###,##0.00",Detalhes[i].EquipamentosPrevia[k].Valor) + "</b>");
                            tw.WriteLine( "</td>");
                            tw.WriteLine( "</tr>");
                            tw.WriteLine( "</tbody>");
                            tw.WriteLine( "</table>");
                            tw.WriteLine( "<!--Fim Tabela de Dados Fornecimento e Instalações de Equipamentos-->");
                        
                        }
                    }                    
                    else
                    {                       
                        tw.WriteLine( "<!--Início Tabela de Dados Fornecimento e Instalações de Equipamentos-->");
                        tw.WriteLine( "<table width='100%'>");
                        tw.WriteLine( "<tbody>");
                        tw.WriteLine( "<tr class='DadostdTable'>");
                        tw.WriteLine( "<td align='center'>");
                        tw.WriteLine( "<b><font color='red'>Nenhum item encontrado.</font></b>");
                        tw.WriteLine( "</td>");
                        tw.WriteLine( "</tr>");
                        tw.WriteLine( "</tbody>");
                        tw.WriteLine( "</table>");
                        tw.WriteLine( "<!--Fim Tabela de Dados Fornecimento e Instalações de Equipamentos-->");                       
                    }
                }
                
                tw.WriteLine( "<table width='100%'>");
                tw.WriteLine( "<tbody>");
                tw.WriteLine( "<tr class='SubTitulo'>");
                tw.WriteLine( "<!--Subtitulo-->");
                tw.WriteLine( "<td>");
                tw.WriteLine( "<img height='1' src='image004.gif' width='25'/> <b>Serviços</b>");
                tw.WriteLine( "</td>");
                tw.WriteLine( "<td align='middle' width='100'>");
                tw.WriteLine( "<b>Un. Medida</b>");
                tw.WriteLine( "</td>");
                tw.WriteLine( "<td align='middle' width='100'>");
                tw.WriteLine( "<b>Quantidade</b>");
                tw.WriteLine( "</td>");
                tw.WriteLine( "<td align='middle' width='100'>");
                tw.WriteLine( "<b>Valor Unitário</b>");
                tw.WriteLine( "</td>");
                tw.WriteLine( "<td align='middle' width='100'>");
                tw.WriteLine( "<b>Valor Total</b>");
                tw.WriteLine( "</td>");
                tw.WriteLine( "</tr>");
                tw.WriteLine( "</tbody>");
                tw.WriteLine( "</table>");
            
                //Printando os serviços
                if (Detalhes[i].Servicos.Count > 0)
                {                
                    for (int l = 1; l < Detalhes[i].Servicos.Count; l++)
                    {
                        tw.WriteLine( "<!--Início Tabela de Dados Fornecimento e Instalações de Serviços-->");
                        tw.WriteLine( "<table width='100%'>");
                        tw.WriteLine( "<tbody>");
                        tw.WriteLine( "<tr class='DadostdTable'>");
                    
                        tw.WriteLine( "<td>");
                        if (Detalhes[i].Servicos[l].UnidadeMedida == "")
                        {
                            tw.WriteLine( "<img height='1' src='image004.gif' width='25'/> <b>" + Detalhes[i].Servicos[l].Nome + "</b>");
                        }
                        else
                        {
                            tw.WriteLine( "<img height='1' src='image004.gif' width='25'/> " + Detalhes[i].Servicos[l].Nome);
                        }
                        tw.WriteLine( "</td>");
                    
                        tw.WriteLine( "<td align='middle' width='100'>" + Detalhes[i].Servicos[l].UnidadeMedida);
                        tw.WriteLine( "</td>");
                    
                        if (Detalhes[i].Servicos[l].UnidadeMedida != "")
                        {
                            tw.WriteLine( "<td align='middle' width='100'>" + Detalhes[i].Servicos[l].Quantidade);
                            tw.WriteLine( "</td>");
                        }
                    
                        tw.WriteLine( "<td align='right' width='100'>");
                        if (Detalhes[i].Servicos[l].UnidadeMedida != "")
                        {
                            tw.WriteLine( String.Format("###,###,##0.00", Detalhes[i].Servicos[l].ValorUnitario));
                        }
                        tw.WriteLine( "</td>");
                    
                        tw.WriteLine( "<td align='right' width='100'>");
                        if (Detalhes[i].Servicos[l].UnidadeMedida == "")
                        {
                            tw.WriteLine( "<b>" + String.Format("###,###,##0.00", Detalhes[i].Servicos[l].Valor) + "<b>");
                        }
                        else
                        {
                            tw.WriteLine( String.Format("###,###,##0.00", Detalhes[i].Servicos[l].Valor));
                        }
                        tw.WriteLine( "</td>");
                    
                        tw.WriteLine( "</tr>");
                        tw.WriteLine( "</tbody>");
                        tw.WriteLine( "</table>");
                        tw.WriteLine( "<!--Fim Tabela de Dados Fornecimento e Instalações de Equipamentos-->");
                    
                    }
                }
                else
                {                   
                    tw.WriteLine( "<!--Início Tabela de Serviços-->");
                    tw.WriteLine( "<table width='100%'>");
                    tw.WriteLine( "<tbody>");
                    tw.WriteLine( "<tr class='DadostdTable'>");
                    tw.WriteLine( "<td align='center'>");
                    tw.WriteLine( "<b><font color='red'>Nenhum item encontrado.</font></b>");
                    tw.WriteLine( "</td>");
                    tw.WriteLine( "</tr>");
                    tw.WriteLine( "</tbody>");
                    tw.WriteLine( "</table>");
                    tw.WriteLine( "<!--Fim Tabela de Serviços-->");
                    
                }
                
                //End If
            
            }

            tw.WriteLine( "</body>");
            tw.WriteLine( "</html>");
        
            tw.Close(); 
        }

        #endregion
    }
}
