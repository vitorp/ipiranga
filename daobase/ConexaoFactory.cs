﻿using System.Data;
using System.Data.OleDb;
using System.Data.OracleClient;
using System.Data.SqlClient;
////using System.Diagnostics.Contracts;
using daobase.Enumerados;

public static class ConexaoFactory
    {
        public static IDbConnection ObterConexao()
        {
            //#region Contracts
            ////Contract.Requires(!string.IsNullOrEmpty(ConexaoBase.GetInstance().StringConexao), Mensagens.VAL_CONEXAO_STRING);
            //Contract.Ensures(ConexaoBase.GetInstance().Conexao != null, Mensagens.ERR_CONEXAO_NAO_CRIOU);
            //#endregion

            IDbConnection conexao = null;

            switch (ConexaoBase.GetInstance().TipoBaseDados)
            {
                case BaseDadosEnum.Access:
                    conexao = new OleDbConnection(ConexaoBase.GetInstance().StringConexao);
                    break;

                case BaseDadosEnum.Oracle:
                    conexao = new OracleConnection(ConexaoBase.GetInstance().StringConexao);
                    break;

                case BaseDadosEnum.SQLServer:
                    conexao = new SqlConnection(ConexaoBase.GetInstance().StringConexao);
                    break;
            }

            return conexao;
        }
    }
