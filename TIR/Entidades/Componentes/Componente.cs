﻿using System.Collections.Generic;
using TIR.Entidades.Encaixes;

namespace TIR.Entidades.Componentes
{
    /// <summary>
    /// Entidade que representa um componente básico.
    /// Os componentes podem ser Posto, AmPm, JetOil e JetOil Motos.
    /// </summary>
    public class Componente
    {
        #region Propriedades

        public int Payback { get; set; }
        public double NPV { get; set; }
        public double TIR { get; set; }
        public int CodTipoComp { get; set; }
        public double[] Fluxo { get; set; }
        public List<Encaixe> Encaixes { get; set; }

        #endregion
    }
}
