﻿
using System.Xml.Serialization;
using System;
namespace TIR.Entidades.Encaixes
{
    [XmlInclude(typeof(Benfeitoria))]
    [XmlInclude(typeof(Equipamento))]
    [XmlInclude(typeof(Financiamento))]
    [XmlInclude(typeof(FundoComercio))]
    [XmlInclude(typeof(AntecAluguel))]
    [XmlInclude(typeof(DespReceita))]
    [XmlInclude(typeof(Terreno))]
    [XmlInclude(typeof(RecebLuvas))]
    public abstract class Encaixe : ICloneable
    {
        #region Propriedades

        public string Descricao { get; set; }
        public double Valor { get; set; }
        public int MesConcessao { get; set; }
        public double[] Fluxo { get; set; }
        public int CodEcxDcx { get; set; }
        public int CodGrupoExDx { get; set; }
        public int CodCalcTIR { get; set; }
        public bool IsAlocaVerba { get; set; }
        public int CodVerba { get; set; }
        public bool IsPO { get; set; }

        #endregion

        #region ICloneable Members

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

    }
}
