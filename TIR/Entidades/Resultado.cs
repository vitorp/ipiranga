﻿
using System;
namespace TIR.Entidades
{
    [Serializable()]
    public class Resultado
    {
        #region Propriedades

        public int codEcxDcx { get; set; }  //código do Encaixe
        public string Identificador { get; set; }   //identificação para a grid
        public double[] Fluxo { get; set; }     //fluxo

        #endregion
    }
}
