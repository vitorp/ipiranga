﻿using System;
using System.Collections.Generic;
using tir.dao;
using tir.dao.Entidades;
using TIR.Entidades;
using TIR.Entidades.Componentes;
using TIR.Entidades.Encaixes;
using TIR.Entidades.ItensFranquias;

namespace TIR.Bo.Calculos
{
    public class CalculoRoyaltyVar : ICalculo
    {
        #region Propriedades
        public Franquia Franquia { get; set; }
        #endregion

        public double[] Calcular(Encaixe enc, TIRNegociacao tir)
        {
            //converte encaixe
            RoyaltyVar royalty = (RoyaltyVar)enc;

            royalty.Descricao = "Royalty variável";

            //define prazo total, tipo de negociação, perfil e redimensiona fluxo
            //int prazoTotal = Franquia.TempoContrato + 1;
            //Incluídos os meses sem venda por Luiz Felipe - out/2014
            //int prazoTotal = Franquia.TempoContrato + tir.PeriodoMatura + 1;
            //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
            int prazoTotal = Franquia.TempoContrato + tir.PeriodoMatura + tir.MesesCompVolume + 1;

            string tpNeg = tir.Cliente.segmento;
            int localiz = Convert.ToInt32(tir.Cliente.Perfil);
            //royalty.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + 2];
            //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
            //royalty.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 2];
            //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
            royalty.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 2];

            //Fluxo auxiliar da TIR para o cálculo do Capital de Giro Franquia - por Luiz Felipe - out/2014
            //tir.FluxoAuxiliar = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 2];
            //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
            tir.FluxoAuxiliar = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 2];

            bool isFranquiaRenovacao = false;
            //--- Alteração asseguramento de franquias - Agosto/2002
            if (Franquia.TpNegFranquia.Equals(2) || Franquia.TpNegFranquia.Equals(1))
            {
                isFranquiaRenovacao = true;
            }

            //busca parâmetros de negociação
            ParamNegDao paramDAO = new ParamNegDao();
            ParamNeg paramNeg = paramDAO.ObterParametro(tir.Categoria, tpNeg, localiz);

            //busca alíquota de imposto de renda e pis
            OutrosParamDao outrosDAO = new OutrosParamDao();
            OutrosParam outros = outrosDAO.ObterParametro("PercIR");
            OutrosParam outrosPis = outrosDAO.ObterParametro("ReceitaPIS");
            OutrosParam outrosIss = outrosDAO.ObterParametro("PercISS");
            float percIr = (float)outros.ValorParametroNumerico;
            double aliqPisCof = (double)outrosPis.ValorParametroNumerico;
            float percIss = (float)outrosIss.ValorParametroNumerico;

            //busca faixa de Redutor Fat
            RedutorFatDao reduDAO = new RedutorFatDao();
            List<RedutorFat> redutores = reduDAO.ListarPorComponente(Franquia.CodPadraoComp, Franquia.CodPerfilComp, Franquia.CodTipoComp);
            RoyalCbpiDao royalDAO = new RoyalCbpiDao();
            List<RoyalCbpi> royalties = royalDAO.ListarPorComponente(Franquia.CodPadraoComp, Franquia.CodPerfilComp, Franquia.CodTipoComp);

            int mesInicio = 0;

            //--- Gerar o fluxo de RoyaltyVar - migração franquias maio/2004
            if (isFranquiaRenovacao.Equals(true) && !tir.Categoria.Equals(41))
            {
                //só calcula se tiver prazo proposto
                if (tir.ContrAdicional > 0)
                {
                    mesInicio = tir.ContrExistente + 1;
                }
                else
                {
                    //não precisa calcular
                    return royalty.Fluxo;
                }
            }
            else
            {
                mesInicio = 1;
            }

            //int max = Franquia.QtElementosFaixa; ??

            double redutor = 0.0;

            for (int i = mesInicio + tir.PeriodoMatura; i <= (prazoTotal - 1 + (mesInicio - 1)); i++)
            {
                //acha o redutor de vendas
                for (int j = 0; j <= redutores.Count - 1; j++)
                {
                    if (redutores[j].NumeroMesFaixaRedutores.Equals(0))
                    {
                        break;
                    }

                    if (Franquia.TpNegFranquia > 0)
                    {
                        if (j.Equals(redutores.Count - 1))
                        {
                            redutor = (double)redutores[j].PercentualReducaoFaturamento;

                            break;
                        }
                    }
                    else
                    {
                        if (i - tir.PeriodoMatura <= redutores[j].NumeroMesFaixaRedutores || j.Equals(redutores.Count - 1))
                        {
                            redutor = (double)redutores[j].PercentualReducaoFaturamento;

                            break;
                        }
                    }
                }

                float percOK = 0;

                //--- acha o percentual de royalty ARCO
                for (int j = 0; j <= royalties.Count - 1; j++)
                {
                    if (royalties[j].NumeroMesFaixaRoyalties.Equals(0))
                    {
                        break;
                    }

                    if (Franquia.TpNegFranquia > 0)
                    {
                        if (j.Equals(royalties.Count - 1))
                        {
                            percOK = (float)royalties[j].PercentualRoyaltiesCbpi;
                            break;
                        }
                    }
                    else
                    {
                        if (i <= royalties[j].NumeroMesFaixaRoyalties || j.Equals(royalties.Count - 1))
                        {
                            percOK = (float)royalties[j].PercentualRoyaltiesCbpi;
                            break;
                        }
                    }
                }

                //***Incluído por Luiz Felipe em out/2014
                //Monta fluxo auxiliar para ser usado mais tarde no cálculo do Capital de Giro Franquia
                double vlRoyalty = percOK / 100.0 * Franquia.ValorFaturamento * (1.0 - redutor / 100.0) * tir.Inflacao[i];
                tir.FluxoAuxiliar[i] = vlRoyalty;
                //***

                double vlRoyaltyFao = percOK / 100.0 * Franquia.ValorFaturamento * (1.0 - redutor / 100.0) / tir.ValorFAO;
                //Alterado por Luiz Felipe - out /2014
                //Cálculo: valor royalties em FAO * (1 - (percentual ISS + alíquota PisCofins) / 100) * (1 – percentual IR / 100)
                //royalty.Fluxo[i] = vlRoyaltyFao * (1.0 - percIr / 100.0) * (1.0 - aliqPisCof / 100.0);
                royalty.Fluxo[i] = vlRoyaltyFao * (1.0 - (percIss + aliqPisCof) / 100.0) * (1.0 - percIr / 100.0);
            }

            //retorno com sucesso
            return royalty.Fluxo;        
        }
    }
}
