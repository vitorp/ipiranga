﻿using System;
using System.Collections.Generic;

namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que representa os dados de conta corrente do cliente.
    /// </summary>
    public class ContaCorrente
    {
        #region Propriedades

        public DateTime Data { get; set; }
        public long QtdChequesDev { get; set; }
        public double VlrChequesDev { get; set; }
        public bool IndChequeVisado { get; set; }
        public double VlrMaximo { get; set; }
        public string DataMaximo { get; set; }

        public List<FaixasDia> FaixasDia { get; set; }
        public List<Compromissos> Compromissos { get; set; }
        public List<Financiamentos> Financiamentos { get; set; }
        public List<Titulos> Titulos { get; set; }

        #endregion
    }
}
