﻿using Microsoft.VisualBasic;
using TIR.Entidades;
using TIR.Entidades.Encaixes;

namespace TIR.Bo.Calculos
{
    public abstract class FechamentoAno
    {
        #region Atributos 

        //atributos protegidos, utilizados pelas sub-classes
        protected double dblWork = 0;
        protected int ultimoMes = 0;
        protected double cMsaldoDev = 0;
        protected double jurCarTotal = 0;
        protected double prestacao = 0;
        protected int mesInicio = 0;
        protected int mesInicioRenovacao = 0;
        protected double[] saldo;
        protected double[] juros;
        protected double[] amort;
        protected double[] prest;
        protected double[] saldoSub;
        protected double[] recDesp;
        protected double[] valorJurCar;

        #endregion

        #region Métodos Protegidos

        /// <summary>
        /// Método que faz o fechamento do ano.
        /// </summary>
        /// <param name="encaixe">Encaixe que está sendo calculado.</param>
        /// <param name="tir">Documento TIR com informações necessárias ao cálculo.</param>
        /// <param name="mes">Mês atual</param>
        protected void fecharAno(Encaixe encaixe, TIRResultado tir, int mes)
        {
            //declaração de variáveis
            double inflacaoMensal;
            double inflacaoMesDev;
            double inflacaoMesCar;
            double acumuladoCorrecao;  //--- correção sivirino em 24/06/2005
            double acumCm = 0;

            //converte encaixe para uso no método
            IFechamentoAno enc = (IFechamentoAno)encaixe;

            inflacaoMensal = tir.Inflacao[mes + mesInicioRenovacao] / tir.Inflacao[mes - 1 + mesInicioRenovacao] - 1.0;
            inflacaoMesDev = 1 + inflacaoMensal * enc.PercCorMonSaldo / 100.0;
            inflacaoMesCar = 1 + inflacaoMensal * enc.PercCorMonCarencia / 100.0;
            inflacaoMensal = 1 + inflacaoMensal * enc.PercCorMonet / 100.0;
            
            acumuladoCorrecao = 0.0;

            for (int mesFch = mes - 11; mesFch <= mes; mesFch++)
            {
                if (mesFch == 1)
                {
                    //--- calcula momento zero
                    if (ultimoMes + enc.Carencia >= 12)
                    {
                        saldoSub[0] = enc.ValorFinanc;
                    }
                    else
                    {
                        saldoSub[0] = saldo[mes]; 
                    }

                    acumCm = 0.0;
                }

                if (mesFch != 1 && mesFch == (mes - 11) && mesFch > ultimoMes + enc.Carencia)
                {
                    saldoSub[mesFch] = saldo[mes] * inflacaoMensal;
                    recDesp[mesFch] = saldoSub[mesFch] - saldo[mes];
                }
                else
                {
                    if (mesFch <= ultimoMes)
                    {
                        //--- período de liberação
                        saldoSub[mesFch] = saldoSub[mesFch - 1] * inflacaoMesDev;  
                    }
                    else if (mesFch <= ultimoMes + enc.Carencia)
                    {
                        //--- período de carência
                        saldoSub[mesFch] = saldoSub[mesFch - 1] * inflacaoMesCar;
                        valorJurCar[mesFch] = saldoSub[mesFch - 1] * enc.PercJurosCarencia / 100.0;
                    }
                    else
                    {
                        saldoSub[mesFch] = saldoSub[mesFch - 1] * inflacaoMensal; 
                    }

                    recDesp[mesFch] = saldoSub[mesFch] - saldoSub[mesFch - 1]; 
                }

                acumuladoCorrecao = acumuladoCorrecao + recDesp[mesFch];  
            }

            if (mes == 12 || mes <= ultimoMes + enc.Carencia)
            {
                acumCm += (saldoSub[mes] - saldoSub[mes - 12]);
            }
            else
            {
                acumCm += acumuladoCorrecao; //--- correção sivirino junho/2005
            }

            if (mes > ultimoMes + enc.Carencia && ultimoMes + enc.Carencia + enc.Amortizacao - mes > 0)
            {
                dblWork = saldo[mes] + acumCm + cMsaldoDev;
                prestacao = Financial.Pmt(enc.PercJuros / 100.0, ultimoMes + enc.Carencia + enc.Amortizacao - mes, (-1.0) * dblWork, 0.0, 0);
                saldo[mes] = dblWork;
                acumCm = 0.0;
                cMsaldoDev = 0.0;
            }
        }

        #endregion
    }
}
