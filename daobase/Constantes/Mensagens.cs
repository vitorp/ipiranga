﻿
namespace daobase
{
    public struct Mensagens
    {
        #region Mensagens de Validação
        public const string VAL_CONEXAO_NULA = "A conexão não pode ser nula.";
        public const string VAL_CONEXAO_INICIALIZADA = "Conexão deve estar inicializada.";
        public const string VAL_CONEXAO_ATIVA = "Conexão com a base de dados deve estar ativa.";
        public const string VAL_CONEXAO_STRING = "A string de conexão deve ser informada.";
        public const string VAL_TRANSACAO_ATIVA = "Transação deve estar ativa.";
        #endregion

        #region Mensagens de Erro
        public const string ERR_CONEXAO_NAO_CRIOU = "A conexão não pôde ser criada por algum motivo.";
        public const string ERR_CONEXAO_NAO_ABRIU = "A conexão não pôde ser aberta por algum motivo.";
        public const string ERR_CONEXAO_NAO_ENCERROU = "Não foi possível encerrar a conexão.";
        public const string ERR_TRANSACAO_NAO_CRIOU = "A transação não pôde ser criada corretamente.";
        public const string ERR_TRANSACAO_JA_EXISTE = "Já existe uma transação ativa com a base de dados.";
        public const string ERR_TRANSACAO_COMMIT = "Commit da transação não pôde ser realizado corretamente.";
        #endregion
    }
}
