﻿using System;
using TIR.Entidades;
using TIR.Entidades.Encaixes;

namespace TIR.Bo.Calculos
{
    public class CalculoTerreno : ICalculo
    {
        public double[] Calcular(Encaixe enc, TIRNegociacao tir)
        {
            try
            {
                //converte encaixe
                Terreno terreno = (Terreno)enc;

                //define prazo total e valor em FAO
                //int prazoTotal = tir.ContrAdicional + tir.ContrExistente + 1;
                //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
                int prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 1;
                double valorEmFao = terreno.Valor / tir.ValorFAO;

                //verifica se o prazo está coerente
                if (prazoTotal < 2)
                {
                    throw new Exception("CalculoTerreno.calcular: Não há prazo para calcular terreno.");
                }

                //redimensiona vetor de fluxo
                //terreno.Fluxo = new double[prazoTotal + 1];
                //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
                terreno.Fluxo = new double[prazoTotal + tir.MesesCompVolume + 1];

                //calcula o fluxo
                terreno.Fluxo[terreno.MesConcessao] = valorEmFao * (-1.0) / tir.Inflacao[terreno.MesConcessao];
                terreno.Fluxo[prazoTotal] = valorEmFao;

                //retorno positivo
                return terreno.Fluxo;
            }
            catch (Exception ex)
            {
                throw new Exception("CalculoTerreno.calcular: Erro ao calcular Terreno.\n" + ex.Message, ex);
            }
        }
    }
}
