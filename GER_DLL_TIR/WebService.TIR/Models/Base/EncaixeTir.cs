using System.Runtime.Serialization;

namespace WebService.TIR.Models.Base
{
    [DataContract]
    public abstract class EncaixeTir
    {
        [DataMember]
        public double Valor { get; set; }

        [DataMember]
        public int MesConcessao { get; set; }

        //[DataMember]
        //public int CodEcxDcx { get; set; }

        //[DataMember]
        //public int CodGrupoExDx { get; set; }

        //[DataMember]
        //public bool IsPO { get; set; }
    }
}