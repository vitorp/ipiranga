﻿using System;
using System.Data;
//using System.Diagnostics.Contracts;
using System.Text;
using daobase;
using tir.dao.Entidades;

namespace tir.dao
{
    public class TipoComDao : DaoBase<TipoCom>
    {
        #region Métodos Públicos
        /// <summary>
        /// Obtem um Tipo de Componente.
        /// </summary>
        /// <param name="codTipo">Tipo de componente.</param>
        /// <returns>Objeto TipoCom.</returns>
        public TipoCom ObterPorCodigo(long codTipo)
        {
            #region Contracts
            //Contract.Requires(codTipo > 0, Mensagens.TipoCom.VAL_CODIGO_TIPO);
            #endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select c.cd_tipo_com, ");
                sb.Append("c.nm_tipo_com, ");
                sb.Append("c.cd_tipo_com_NOVO, ");
                sb.Append("c.no_mes_min_terc, ");
                sb.Append("c.no_mes_max_terc, ");
                sb.Append("c.no_mes_min_prop, ");
                sb.Append("c.no_mes_max_prop ");
                sb.Append("from TipoCom c ");
                sb.AppendFormat("where c.cd_tipo_com = {0} ", DaoUtil.FormatarParametro("codTipo"));

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codTipo"), codTipo, typeof(Int16)));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //se há resultado
                if (dr.Read())
                {
                    //valor de retorno
                    return MontarObjeto(dr);
                }
                else
                {
                    //valor de retorno nulo
                    return null;
                }
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("TipoComDao.ObterPorCodigo",
                //"Erro ao obter um Tipo de Componente pelo seu código.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }
        #endregion

        #region Overrides

        protected override TipoCom MontarObjeto(IDataReader dr)
        {
            TipoCom ent = new TipoCom();
            try
            {
                ent.CodigoTipoComponente = (Int32)dr["cd_tipo_com"];
                ent.NomeTipoComponente = (string)dr["nm_tipo_com"];
                ent.CodigoTipoComponenteNovo = dr["cd_tipo_com_NOVO"] != DBNull.Value ? (string)dr["cd_tipo_com_NOVO"] : "";
                ent.NumeroMinimoMesesTerceiro = dr["no_mes_min_terc"] != DBNull.Value ? (Int16?)dr["no_mes_min_terc"] : null;
                ent.NumeroMaximoMesesTerceiro = dr["no_mes_max_terc"] != DBNull.Value ? (Int16?)dr["no_mes_max_terc"] : null;
                ent.NumeroMinimoMesesProprio = dr["no_mes_min_prop"] != DBNull.Value ? (Int16?)dr["no_mes_min_prop"] : null;
                ent.NumeroMaximoMesesProprio = dr["no_mes_max_prop"] != DBNull.Value ? (Int16?)dr["no_mes_max_prop"] : null;
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
