﻿
namespace TIR.Entidades
{
    // <summary>
    /// Entidade que representa o tipo de negócio
    /// </summary>
    public class TipoNegocio
    {
        #region Propriedades

        public string Codigo { get; set; }
        public string Descricao { get; set; }

        #endregion
    }
}
