﻿using System;
using Microsoft.VisualBasic;
using tir.dao;
using tir.dao.Entidades;
using TIR.Entidades;
using TIR.Entidades.Componentes;
using TIR.Entidades.Encaixes;
using TIR.Fabricas;

namespace TIR.Bo.Calculos
{
    public class CalculoTIRAreaAbast
    {
        #region Métodos Públicos

        public double[] Calcular(Componente comp, TIRNegociacao tir)
        {
            AreaAbastecimento abast = (AreaAbastecimento)comp;

            bool calculouAlgo = false;

            //int meses = tir.ContrExistente + tir.ContrAdicional + 2;
            //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
            //int meses = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 2;
            //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
            int meses = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 2;

            abast.Fluxo = new double[meses];

            //--- Cálculo dos volumes de todos os Produtos envolvidos
            //--- Esta alteração se fez necessária em 18/08 devido
            //    aos prazos em dias e margens para cada produto
            //    serem calculados através do volume total na tabela
            //    de prazos mínimos.

            abast.TodosVolumes = new double[meses];

            //Só calcula se houver produtos
            if (abast.Produtos != null && abast.Produtos.Count > 0)
            {
                AreaAbastecimento.TotalVolumeMesAno1 = 0;
                AreaAbastecimento.TotalVolumeMesAno1SemLubes = 0;
                AreaAbastecimento.TotalVolumeMesAno2SemLubes = 0;
                AreaAbastecimento.TotalVolumeMesAno3SemLubes = 0;

                foreach (TIR.Entidades.Produto p in abast.Produtos)
                {
                    if (p.CalcularVolume(tir) == false)
                    {
                        throw new Exception("AreaAbast::CalcularTIR: Erro ao calcular área de abastecimento. O cálculo do volume do produto falhou.");
                    }

                    if (p.MediaMesAno1 + p.MediaMesAno2 + p.MediaMesAno3 > 0.0)
                    {
                        calculouAlgo = true;

                        for (int i = 0; i < meses; i++)
                        {
                            abast.TodosVolumes[i] += p.FluxoVol[i];
                        }

                        AreaAbastecimento.TotalVolumeMesAno1 += p.MediaMesAno1;

                        //Inserido por Luiz Felipe em jul/2012
                        if (p.CodProduto != 12)
                        {
                            AreaAbastecimento.TotalVolumeMesAno1SemLubes += p.MediaMesAno1;
                            AreaAbastecimento.TotalVolumeMesAno2SemLubes += p.MediaMesAno2;
                            AreaAbastecimento.TotalVolumeMesAno3SemLubes += p.MediaMesAno3;
                        }
                    }
                }
            }

            //---Cálculo de todos os encaixes e desencaixes
            if (abast.Encaixes != null && abast.Encaixes.Count > 0)
            {
                foreach (Encaixe ecxDcx in abast.Encaixes)
                {
                    //não calcula p/encaixes de produto
                    if (ecxDcx.CodEcxDcx < 90 || ecxDcx.CodEcxDcx > 92)
                    {
                        //85 Despesa Meio Ambiente (deve ser enviada à PNE mas não será calculada na TIR)
                        if (ecxDcx.CodEcxDcx != 85)
                        {
                            ecxDcx.Fluxo = CalculoFactory.Calcular(ecxDcx, comp, tir);
                            if (ecxDcx.Fluxo == null)
                            {
                                throw new Exception("AreaAbast::CalcularTIR: Erro ao calcular área de abastecimento. O cálculo dos encaixes/desencaixes falhou.");
                            }
                            else
                            {
                                calculouAlgo = true;
                            }

                            for (int i = 0; i < meses; i++)
                            {
                                abast.Fluxo[i] += ecxDcx.Fluxo[i];
                            }
                        }
                        else
                        {
                            //Calcula apenas o valor da prestação
                            ecxDcx.Fluxo = CalculoFactory.Calcular(ecxDcx, comp, tir);
                            if (ecxDcx.Fluxo == null)
                            {
                                throw new Exception("AreaAbast::CalcularTIR: Erro ao calcular área de abastecimento. O cálculo da prestação da despesa de Meio Ambiente falhou.");
                            }
                        }
                    }
                }
            }

            //----- Verifica volume mínimo do posto
            string TpNeg = tir.Cliente.segmento;
            int Localiz = Convert.ToInt32(tir.Cliente.Perfil);

            ParamNegDao paramDAO = new ParamNegDao();
            ParamNeg param = paramDAO.ObterParametro(tir.Categoria, TpNeg, Localiz);

            double maiorVolume = 0.0;
            for (int i = 0; i < meses; i++)
            {
                if (abast.TodosVolumes[i] > maiorVolume)
                {
                    maiorVolume = abast.TodosVolumes[i];
                }
            }

            if (abast.TodosVolumes[tir.ContrExistente + tir.ContrAdicional] > 0)
            {
                if (maiorVolume < param.VolumeTotalMinimoPositivo)
                {
                    throw new Exception("AreaAbast::CalcularTIR: Erro ao calcular área de abastecimento. A soma dos volumes (área de abastecimento) desta análise não alcançou o mínimo (" +
              Convert.ToString(param.VolumeTotalMinimoPositivo) + " m3) exigido pelo tipo de negociação selecionado.");
                }
            }

            if (abast.AluguelMargem != null)
            {
                if (maiorVolume == 0.0 && abast.AluguelMargem.PercAluguel > 0)
                {
                    throw new Exception("AreaAbast::CalcularTIR: Erro ao calcular área de abastecimento. Foi informado o percentual de aluguel sobre volume de produto do posto sem que haja volume informado para produto.");
                }
            }

            //--- Produtos envolvidos

            //Só calcula se houver produtos
            if (abast.Produtos != null && abast.Produtos.Count > 0)
            {
                foreach (TIR.Entidades.Produto p in abast.Produtos)
                {
                    CalculoProduto calcProd = new CalculoProduto();
                    p.Fluxo = calcProd.Calcular(p, tir);
                    if (p.Fluxo == null)
                    {
                        throw new Exception("AreaAbast::CalcularTIR: Erro ao calcular área de abastecimento. O cálculo do produto falhou.");
                    }

                    calculouAlgo = true;

                    for (int i = 0; i < meses; i++)
                    {
                        abast.Fluxo[i] += p.Fluxo[i];
                    }
                }
            }

            //--- Cria ENCAIXES respectivos
            if (calculouAlgo == true)
            {
                AluguelVol alugVol = new AluguelVol();
                alugVol.Fluxo = new double[meses];

                CapitalGiro capGiro = new CapitalGiro();
                capGiro.Fluxo = new double[meses];

                Margem margem = new Margem();
                margem.Fluxo = new double[meses];

                EstoqTecnico estTec = new EstoqTecnico();
                estTec.Fluxo = new double[meses];

                alugVol.CodEcxDcx = 90;
                alugVol.Descricao = "Aluguel de Volume";

                capGiro.CodEcxDcx = 91;
                capGiro.Descricao = "Capital de Giro";

                margem.CodEcxDcx = 92;
                margem.Descricao = "Margens";

                for (int i = 0; i < meses; i++)
                {
                    alugVol.Fluxo[i] = CalcularFluxoAluguelVolume(abast, i);
                    capGiro.Fluxo[i] = CalcularFluxoCapitalGiro(abast, i);
                    margem.Fluxo[i] = CalcularFluxoMargem(abast, i);
                    estTec.Fluxo[i] = CalcularFluxoEstoqueTecnico(abast, i);
                }

                //remove os encaixes de produto anteriores
                abast.Encaixes.RemoveAll(e => e.CodEcxDcx == 90);
                abast.Encaixes.RemoveAll(e => e.CodEcxDcx == 91);
                abast.Encaixes.RemoveAll(e => e.CodEcxDcx == 92);

                //adiciona os encaixes de produto montados
                //acima na Área de Abastecimento
                abast.Encaixes.Add(alugVol);
                abast.Encaixes.Add(capGiro);
                abast.Encaixes.Add(margem);
            }
            else
            {
                abast.TIR = 0.0;
                abast.NPV = 0.0;
                abast.Payback = 0;

                return abast.Fluxo;
            }


            //---- A tir é calculada por ano, por isso a fórmula abaixo

            try
            {
                //calcula o valor da tir
                double[] fluxo = abast.Fluxo;
                abast.TIR = (Math.Pow((1.0 + Financial.IRR(ref fluxo, 1.15151291201983E-02)), 12.0) - 1.0) * 100.0;
            }
            catch
            {
                //se tem erro na fórmula, define valor padrão para a TIR
                abast.TIR = -999999.777777;
            }

            //---- Cálculo do NPV (valor presente)

            //variáveis de apoio
            double[] Awork = new double[abast.Fluxo.GetUpperBound(0) + 1];
            double CustoAno;

            //varre fluxo
            for (int i = 1; i <= abast.Fluxo.GetUpperBound(0); i++)
            {
                //copia fluxo no vetor de trabalho (atenção aos índices)
                Awork[i - 1] = abast.Fluxo[i];
            }

            //busca custo de oportunidade
            OutrosParamDao outrosDAO = new OutrosParamDao();
            OutrosParam outros = outrosDAO.ObterParametro("CustoOport");

            //calcula custo anual
            CustoAno = CustoAno = Math.Pow((1.0 + (double)outros.ValorParametroNumerico / 100.0), (1.0 / 12.0)) - 1.0;

            //calcula npv
            abast.NPV = (Financial.NPV(CustoAno, ref Awork) + abast.Fluxo[0]) * tir.ValorFAO;

            //formata valor da tir para o caso em que não tenha sido possível calcular (extremos)
            if (Math.Abs(abast.TIR) == 999999.777777 && abast.NPV > 0.0)
            {
                abast.TIR = 999999.777777;
            }

            //----- Cálculo do PayBack ----------

            //declaração de variáveis
            double wTaxa;
            double wSaldo;
            int wMaior;
            bool wFlag;

            //começa cálculo do saldo e da taxa
            wSaldo = abast.Fluxo[0];
            wTaxa = Math.Pow((1.0 + CustoAno / 100.0), (1.0 / 12.0)) - 1.0;
            wMaior = -1;

            //seta flag como falso
            wFlag = false;

            //varre fluxo
            //for (int i = 1; i <= tir.ContrAdicional + tir.ContrExistente + 1; i++)
            for (int i = 1; i < tir.Fluxo.GetUpperBound(0) + 1 ; i++)
            {
                //cálculo do saldo
                wSaldo += abast.Fluxo[i] / Math.Pow((1.0 + wTaxa), i);

                if (wFlag == false)
                {
                    if (wSaldo >= 0.0)
                    {
                        wMaior = i;
                        wFlag = true;
                    }
                }
                else
                {
                    if (wSaldo < 0.0)
                    {
                        wFlag = false;
                    }
                }
            }

            //define payback
            abast.Payback = wMaior;

            if (wFlag == false)
            {
                abast.Payback = -1;
            }

            //retorno com sucesso
            return abast.Fluxo;
        }

        #endregion

        #region Métodos Privados

        /// <summary>
        /// Método privado que calcula o Fluxo de Aluguel de Volume para
        /// todos os Produtos da Área de Abstecimento, para o mês desejado.
        /// </summary>
        /// <param name="abast">Objeto Área de Abastecimento.</param>
        /// <param name="Mes">Mês para o qual se deseja calcular o Fluxo.</param>
        /// <returns>Valor do Fluxo para o Mês.</returns>
        private double CalcularFluxoAluguelVolume (AreaAbastecimento abast, int Mes)
        {
            //declaração de variáveis
            double Valor = 0.0;

            if (abast.Produtos != null)
            {
                //varre a coleção de produtos
                for (int i = 0; i < abast.Produtos.Count; i++)
                {
                    Valor += abast.Produtos[i].AluguelVol.Fluxo[Mes];
                }
            }

            return Valor;
        }

        /// <summary>
        /// Método privado que calcula o Fluxo de Capital de Giro para
        /// todos os Produtos da Área de Abstecimento, para o mês desejado.
        /// </summary>
        /// <param name="abast">Objeto Área de Abastecimento.</param>
        /// <param name="Mes">Mês para o qual se deseja calcular o Fluxo.</param>
        /// <returns>Valor do Fluxo para o Mês.</returns>
        private double CalcularFluxoCapitalGiro(AreaAbastecimento abast, int Mes)
        {
            //declaração de variáveis
            double Valor = 0.0;

            if (abast.Produtos != null)
            {
                //varre a coleção de produtos
                for (int i = 0; i < abast.Produtos.Count; i++)
                {
                    Valor += abast.Produtos[i].CapitalGiro.Fluxo[Mes];
                }
            }

            return Valor;
        }

        /// <summary>
        /// Método privado que calcula o Fluxo de Margens para todos
        /// os Produtos da Área de Abstecimento, para o mês desejado.
        /// </summary>
        /// <param name="abast">Objeto Área de Abastecimento.</param>
        /// <param name="Mes">Mês para o qual se deseja calcular o Fluxo.</param>
        /// <returns>Valor do Fluxo para o Mês.</returns>
        private double CalcularFluxoMargem(AreaAbastecimento abast, int Mes)
        {
            //declaração de variáveis
            double Valor = 0.0;

            if (abast.Produtos != null)
            {
                //varre a coleção de produtos
                for (int i = 0; i < abast.Produtos.Count; i++)
                {
                    Valor += abast.Produtos[i].Margem.Fluxo[Mes];
                }
            }

            return Valor;
        }

        /// <summary>
        /// Método privado que calcula o Fluxo de Estoque Técnico para
        /// todos os Produtos da Área de Abstecimento, para o mês desejado.
        /// </summary>
        /// <param name="abast">Objeto Área de Abastecimento.</param>
        /// <param name="Mes">Mês para o qual se deseja calcular o Fluxo.</param>
        /// <returns>Valor do Fluxo para o Mês.</returns>
        private double CalcularFluxoEstoqueTecnico(AreaAbastecimento abast, int Mes)
        {
            //declaração de variáveis
            double Valor = 0.0;

            if (abast.Produtos != null)
            {
                //varre a coleção de produtos
                for (int i = 0; i < abast.Produtos.Count; i++)
                {
                    Valor += abast.Produtos[i].EstoqTecnico.Fluxo[Mes];
                }
            }

            return Valor;
        }

        #endregion
    }
}
