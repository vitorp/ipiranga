﻿
namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que representa o endereço do cliente.
    /// </summary>
    public class Endereco
    {
        #region Propriedades

        public string Bairro { get; set; }
        public string CEP { get; set; }
        public string Logradouro { get; set; }
        public string Municipio { get; set; }
        public string UF { get; set; }

        #endregion
    }
}
