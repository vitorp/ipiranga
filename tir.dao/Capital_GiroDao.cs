﻿using tir.dao.Entidades;
using System;
using System.Data;
using System.Text;
using daobase;
using System.Collections.Generic;

namespace tir.dao
{
    public class Capital_GiroDao : DaoBase<Capital_Giro>
    {
        #region Métodos Públicos

        /// <summary>
        /// Busca valores de Preço de Compra e Frete de Entrega por 
        /// Código da Unidade e Código do Produto.
        /// </summary>
        /// <param name="CodUnid">Código da Unidade.</param>
        /// <param name="CodProduto">Código do Produto.</param>
        /// <returns>Lista de objetos Capital_Giro.</returns>
        public List<Capital_Giro> ObterCompraFrete(string CodUnid, string CodProduto)
        {
            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select c.cd_unid_oper, ");
                sb.Append("c.cd_prod, ");
                sb.Append("c.no_mes_faixa, ");
                sb.Append("c.vr_prc_compra, ");
                sb.Append("c.frete_entrega, ");
                sb.Append("c.qt_dias_estoque, ");
                sb.Append("c.qt_dias_compra ");
                sb.Append("from Capital_Giro c ");
                sb.AppendFormat("where c.cd_unid_oper = {0} ", DaoUtil.FormatarParametro("CodUnid"));
                sb.AppendFormat("and c.cd_prod = {0} ", DaoUtil.FormatarParametro("CodProduto"));
                sb.Append("order by c.cd_unid_oper, c.cd_prod, c.no_mes_faixa");

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("CodUnid"), CodUnid, typeof(String), 6));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("CodProduto"), CodProduto, typeof(String), 3));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //valor de retorno - alíquota de IOF
                return MontarLista(dr);
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("CustoDaBaseDao.ObterTaxaMovimentacao",
                //"Erro ao buscar taxa de movimentação por unidade, tipo de negociação, código do produto e número do mês da faixa.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        #endregion

        #region Overrides

        protected override Capital_Giro MontarObjeto(IDataReader dr)
        {
            Capital_Giro ent = new Capital_Giro();
            try
            {
                ent.CodigoUnidade = (string)dr["cd_unid_oper"];
                ent.CodigoProduto = (string)dr["cd_prod"];
                ent.NumeroMesFaixa = Convert.ToInt16(dr["no_mes_faixa"]);
                ent.ValorPrecoCompra = Convert.ToDecimal(dr["vr_prc_compra"]);
                ent.FreteEntrega = (double)dr["frete_entrega"];
                ent.QuantidadeDiasEstoque = (Single)dr["qt_dias_estoque"];
                ent.QuantidadeDiasCompra = (Single)dr["qt_dias_compra"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }

        #endregion
    }
}
