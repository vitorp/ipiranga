﻿using System;

namespace tir.dao.Entidades
{
    /// <summary>
    /// Entidade que representa aplicação / verba.
    /// </summary>
    public class AplicacaoVerba
    {
        #region Propriedades
        public Nullable<Int16> CodigoUnidadeNegociacao { get; set; }
        public Nullable<Int16> CodigoTipoNegociacao { get; set; }
        public Nullable<Int16> IdRubricaCorp { get; set; }
        public Nullable<Int16> CodigoTipoEncaixeDesencaixe { get; set; }
        #endregion
    }
}
