﻿using System;
using System.Data;
using System.Text;
//using System.Diagnostics.Contracts;
using daobase;
using tir.dao.Entidades;

namespace tir.dao
{
    public class RotinaCalculoTIRDao : DaoBase<RotinaCalculoTIR>
    {
        #region Métodos Públicos
        /// <summary>
        /// Método que retorna um item.
        /// </summary>
        /// <param name="codCalculo">Código de Cálculo.</param>
        /// <returns>Objeto RotinaCalculoTIR.</returns>
        public RotinaCalculoTIR ObterPorCodigo (int codCalculo)
        {
            #region Contracts
            //Contract.Requires(codCalculo > 0, Mensagens.RotinaCalculoTIR.VAL_CODIGO_CALCULO);
            #endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select r.CD_CALC_TIR, ");
                sb.Append("r.DS_CALC_TIR ");
                sb.Append("from NEGCLI_ROTINA_CALCULO_TIR r ");
                sb.AppendFormat("where r.CD_CALC_TIR = {0} ", DaoUtil.FormatarParametro("codCalculo"));

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codCalculo"), codCalculo, typeof(Int16)));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //se há resultado
                if (dr.Read())
                {
                    //valor de retorno - alíquota de IOF
                    return MontarObjeto(dr);
                }
                else
                {
                    //valor de retorno nulo
                    return null;
                }
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("DencxUnidNegcc.ListarPorUnidade",
                //"Erro ao listar os Encaixes/Desencaixes de uma Unidade de Negociação.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }
        #endregion

        #region Overrides
        protected override RotinaCalculoTIR MontarObjeto(IDataReader dr)
        {
            RotinaCalculoTIR ent = new RotinaCalculoTIR();

            try
            {
                ent.CodigoCalculoTIR = (Int16)dr["CD_CALC_TIR"];
                ent.DescricaoCalculoTIR = (string)dr["DS_CALC_TIR"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
