﻿
namespace tir.dao.Entidades
{
    public class TipoProd
    {
        #region Propriedades
        public decimal CodigoTipoProduto { get; set; }
        public string NomeTipoProduto { get; set; }
        #endregion
    }
}
