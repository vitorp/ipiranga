﻿
using System;
namespace tir.dao.Entidades
{
    public class RoyalArco
    {
        #region Propriedades
        public Int32 CodigoTipoComponente { get; set; }
        public Int32 CodigoPadraoComponente { get; set; }
        public Int32 CodigoPerfilComponente { get; set; }
        public Int16 NumeroMesFaixaRoyalties { get; set; }
        public Single PercentualRoyaltiesArco { get; set; }
        #endregion
    }
}
