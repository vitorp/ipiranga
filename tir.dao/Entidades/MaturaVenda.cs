﻿
using System;

namespace tir.dao.Entidades
{
    public class MaturaVenda
    {
        #region Propriedades

        public Int16 CodigoTipoNegociacao { get; set; }
        public Int16 QuantidadeMesesMaturacao { get; set; }
        public Single PercentualVolumeVendas { get; set; }

        #endregion
    }
}
