﻿using System;
using System.Collections.Generic;
using System.Data;
//using System.Diagnostics.Contracts;
using System.Text;
using daobase;
using tir.dao.Entidades;

namespace tir.dao
{
    public class CustosDao : DaoBase<Custos>
    {
        #region Métodos Públicos

        /// <summary>
        /// Método que obtem uma faixa de custo.
        /// </summary>
        /// <param name="CodGV">Código da GV.</param>
        /// <param name="SiglaUF">Sigla da UF do Cliente.</param>
        /// <param name="TipoNeg">Tipo de Negociação.</param>
        /// <param name="CodProdu">Código do Produto.</param>
        /// <param name="Perfil">Perfil.</param>
        /// <param name="NoMes">Número do Mês.</param>
        /// <returns>Objeto Custos.</returns>
        public Custos ObterFaixaCusto(string CodGV, string SiglaUF, string TipoNeg, string CodProdu, int Perfil, int NoMes)
        {
            #region Contracts
            //Contract.Requires(CodGV != null, Mensagens.Custos.VAL_CODIGO_GV);
            //Contract.Requires(TipoNeg != null, Mensagens.Custos.VAL_TIPO_NEGOCIACAO);
            //Contract.Requires(CodProdu != null, Mensagens.Custos.VAL_CODIGO_PRODUTO);
            //Contract.Requires(Perfil > 0, Mensagens.Custos.VAL_ID_PERFIL);
            //Contract.Requires(NoMes > 0, Mensagens.Custos.VAL_NO_MES);
            #endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select c.cd_gv, ");
                sb.Append("c.Sg_UF, ");
                sb.Append("c.sg_tpneg, ");
                sb.Append("c.id_perfil, ");
                sb.Append("c.cd_prod, ");
                sb.Append("c.no_mes_faixa, ");
                sb.Append("c.vr_custo ");
                sb.Append("from Custos_UF c ");
                sb.AppendFormat("where c.cd_gv = {0} ", DaoUtil.FormatarParametro("CodGV"));
                sb.AppendFormat("and c.Sg_UF = {0} ", DaoUtil.FormatarParametro("SiglaUF"));
                sb.AppendFormat("and c.sg_tpneg = {0} ", DaoUtil.FormatarParametro("TipoNeg"));
                sb.AppendFormat("and c.id_perfil = {0} ", DaoUtil.FormatarParametro("Perfil"));
                sb.AppendFormat("and c.cd_prod = {0} ", DaoUtil.FormatarParametro("CodProdu"));
                sb.Append("order by c.no_mes_faixa ");

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("CodGV"), CodGV, typeof(String), 6));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("SiglaUF"), SiglaUF, typeof(String), 2));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("TipoNeg"), TipoNeg, typeof(String), 3));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("Perfil"), Perfil, typeof(Int16)));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("CodProdu"), CodProdu, typeof(String), 3));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //se há resultado
                if (!dr.Equals(null))
                {
                    //valor de retorno
                    List<Custos> LisCustos = MontarLista(dr);

                    //valor padrão de retorno
                    Custos CustoDesejado = null;

                    //varre faixas
                    for (int i = 0; i <= LisCustos.Count - 1; i++)
                    {
                        //condição para buscar custo
                        if (NoMes <= LisCustos[i].NumeroMesFaixa || i == LisCustos.Count - 1)
                        {
                            //valor encontrado para faixa de custo
                            CustoDesejado = LisCustos[i];
                            break;
                        }
                    }

                    //valor de retorno
                    return CustoDesejado;
                }
                else
                {
                    //valor de retorno nulo
                    return null;
                }
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("CustosDao.ObterFaixaCusto",
                //"Erro ao buscar valor do custo por GV, UF, tipo de negociação, código do produto, perfil e número do mês da faixa.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        #endregion

        #region Overrides

        protected override Custos MontarObjeto(IDataReader dr)
        {
            Custos ent = new Custos();
            try
            {
                ent.CodigoGV = (string)dr["cd_gv"];
                ent.SiglaUF = (string)dr["Sg_UF"];
                ent.SegmentoTipoNegociacao = (string)dr["sg_tpneg"];
                ent.IdPerfil = (Int16)dr["id_perfil"];
                ent.CodigoProduto = (string)dr["cd_prod"];
                ent.NumeroMesFaixa = (Int16)dr["no_mes_faixa"];
                ent.ValorCusto = (double)dr["vr_custo"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
