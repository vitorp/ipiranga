﻿using System;
using System.Collections.Generic;
using tir.dao;
using tir.dao.Entidades;
using TIR.Entidades;
using TIR.Entidades.Componentes;
using TIR.Entidades.Encaixes;

namespace TIR.Bo.Calculos
{
    public class CalculoEstoqTecnico : ICalculo
    {
        public double[] Calcular(Encaixe enc, TIRNegociacao tir)
        {
            try
            {
                //converte encaixe para estoque técnico
                EstoqTecnico est = (EstoqTecnico)enc;

                //define prazo total
                //int prazoTotal = tir.ContrAdicional + tir.ContrExistente + 1;
                //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
                //int prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 1;
                //Alterado em jun/2014 para incluir os meses de compensação de Volume no dimensionamento do fluxo
                int prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 1;

                //redimensiona vetor do fluxo
                est.Fluxo = new double[prazoTotal + 1];

                //define tipo de negociação e localização
                string tpNeg = tir.Cliente.segmento;
                int localiz = Convert.ToInt32(tir.Cliente.Perfil);

                //verifica se passou total de m3
                if (est.TotalM3.Equals(0.0))
                {
                    //não precisa calcular
                    return est.Fluxo;
                }

                //varre o vetor com percentuais de liberção
                double ICMS = 0.0;
                for (int i = 0; i <= est.PercLiberacao.GetUpperBound(0); i++)
                {
                    ICMS += est.PercLiberacao[i];
                }

                //verifica se o percentual total de liberação é de 100%
                if (!ICMS.Equals(100.0))
                {
                    throw new Exception("CalculoEstoqTecnico.calcular: Erro ao calcular estoque técnico. O percentual total de liberação de estoque técnico tem que ser de 100% para o produto " + est.CodProduto.ToString() + ".");
                }

                //verifica se há mês de retorno
                if (est.ParcelasMesRetorno < 1)
                {
                    throw new Exception("CalculoEstoqTecnico.calcular: Erro ao calcular estoque técnico. É preciso informar o mês de retorno ou número de parcelas do estoque técnico para o produto " + est.CodProduto.ToString() + ".");
                }

                //--- busca faixas de preco na tabela de MargensPrazo
                MargemPrzDao margemDAO = new MargemPrzDao();
                List<MargemPrz> margens = margemDAO.ListarMargemPrazo(tir.UnidOrg, tir.Cliente.Endereco.UF, tpNeg, localiz, String.Format("{0:000}", est.CodProduto));

                //--- Acesso aos Encargos da Revenda
                double encRev = 0.0;

                AreaAbastecimento oAbast = tir.Cliente.Posto; //(AreaAbastecimento)tir.Cliente.Componentes[0];

                for (int i = 0; i <= oAbast.AluguelMargem.CodProd.Length - 1; i++)
                {
                    if (oAbast.AluguelMargem.CodProd[i] == est.CodProduto)
                    {
                        encRev = oAbast.AluguelMargem.VrEncargo[i];
                        break;
                    }
                }

                if (encRev.Equals(0.0))
                {
                    //instancia dalc para buscar encargo
                    EncargoRevDao encargoDAO = new EncargoRevDao();
                    EncargoRev encargo = encargoDAO.ObterEncargo(tir.Cliente.Endereco.UF, String.Format("{0:000}", est.CodProduto));
                    encRev = (double)encargo.ValorMargem;
                }

                if (tpNeg.Equals("CON") || tpNeg.Equals("TRR"))
                {
                    encRev = 0.0;
                }

                //--- Acesso a tabela de aliquotas icms 
                //GrupoProdDao grupoDAO = new GrupoProdDao();
                //GrupoProd grProd = grupoDAO.ObterGrupoProduto(String.Format("{0:000}", est.CodProduto));
                //est.CodGrupo = Convert.ToInt64(grProd.CodigoGrupoProduto);
                ProdutoDao prdDAO = new ProdutoDao();
                tir.dao.Entidades.Produto prd = prdDAO.ObterProduto(String.Format("{0:000}", est.CodProduto));
                est.CodGrupo = Convert.ToInt64(prd.CodigoGrupoProduto);

                //instancia dalc para buscar alíquora de icms
                AliquotaDao aliqDAO = new AliquotaDao();
                Aliquota aliq = aliqDAO.ObterAliquota(tir.Cliente.Endereco.UF, est.CodGrupo.ToString().Trim());
                float aliquotaIcms = (float)aliq.PercentualICMS;

                //--- Cálculo do valor do volume entregue
                ICMS = 0.0;
                double totalValor = 0.0;
                double precoMes = 0.0;
                int ultimoMes = 0;

                for (int i = 0; i <= est.PercLiberacao.GetUpperBound(0); i++)
                {
                    if (est.PercLiberacao[i] > 0)
                    {
                        //acha valores conforme a faixa
                        for (int j = 0; j <= margens.Count - 1; j++)
                        {
                            if (margens[j].NumeroMesFaixa == 0)
                            {
                                break;
                            }

                            if (i <= margens[j].NumeroMesFaixa || j.Equals(margens.Count - 1))
                            {
                                precoMes = (double)margens[j].ValorPrecoVenda;

                                break;
                            }
                        }

                        est.Fluxo[i] = est.PercLiberacao[i] * est.TotalM3 / 100.0 * precoMes * (-1.0);
                        double precoBomba = est.PercLiberacao[i] * est.TotalM3 / 100.0 * (precoMes + encRev) * (-1.0);

                        //--- icms de remessa
                        ICMS = ICMS + precoBomba * aliquotaIcms / 100.0 * (-1.0);
                        ultimoMes = i;
                        totalValor = totalValor + est.Fluxo[i] * (-1.0);
                    }
                }

                est.Valor = totalValor;

                //--- Cálculo do valor a receber
                if (est.Parcelado.Equals(false))
                {
                    est.Fluxo[est.ParcelasMesRetorno] = ICMS * (1.0 / tir.Inflacao[est.ParcelasMesRetorno] - 1.0) + totalValor;
                }
                else
                {
                    if (est.Carencia > 0)
                    {
                        ultimoMes = ultimoMes + est.Carencia;
                    }

                    for (int i = ultimoMes + 1; i <= ultimoMes + est.ParcelasMesRetorno; i++)
                    {
                        est.Fluxo[i] = (totalValor / est.ParcelasMesRetorno) - ((ICMS
                                      / est.ParcelasMesRetorno) - (ICMS / est.ParcelasMesRetorno) / tir.Inflacao[i]);
                    }
                }

                //retorno com sucesso
                return est.Fluxo;
            }
            catch (Exception ex)
            {
                throw new Exception("CaluloEstoqTecnico.calcular: Erro ao calcular estoque técnico.\n" + ex.Message, ex);
            }
        }
    }
}
