﻿
namespace tir.dao.Entidades
{
    public class TipoInvestimento
    {
        #region Propriedades
        public decimal CodigoTipoInvestimento { get; set; }
        public string NomeInvestimento { get; set; }
        #endregion
    }
}
