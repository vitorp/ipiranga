﻿using System.Collections.Generic;

namespace TIR.Entidades
{
    public class Proposta
    {
        #region Propriedades

        public string Nome { get; set; }
        public double ValorFAO { get; set; }

        //Propriedades tipo classe
        public List<Negcc> Negociacoes { get; set; }

        #endregion
    }
}
