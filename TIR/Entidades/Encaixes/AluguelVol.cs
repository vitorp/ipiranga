﻿
namespace TIR.Entidades.Encaixes
{
    /// <summary>
    /// Encaixe que representa um aluguel de volume.
    /// </summary>
    public class AluguelVol : Encaixe
    {
        #region Propriedades

        public long CodProduto { get; set; }

        #endregion
    }
}
