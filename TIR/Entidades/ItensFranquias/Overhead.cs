﻿using TIR.Entidades.Encaixes;

namespace TIR.Entidades.ItensFranquias
{
    /// <summary>
    /// Item de franquia que representa Overhead.
    /// </summary>
    public class Overhead : Encaixe
    {
    }
}
