﻿using System;
using System.Data;
using System.Text;
////using System.Diagnostics.Contracts;
using daobase;
using tir.dao.Entidades;

namespace tir.dao
{
    public class TipoEncxDencxDao : DaoBase<TipoEncxDencx>
    {
        #region Métodos Públicos
        /// <summary>
        /// Método que retorna um item.
        /// </summary>
        /// <param name="codTipoEcxDcx">Código do Tipo de Encaixe/Desencaixe.</param>
        /// <returns>Objeto TipoEncxDencx.</returns>
        public TipoEncxDencx ObterPorCodigoEncaixe(int codTipoEcxDcx)
        {
            #region Contracts
            //Contract.Requires(codTipoEcxDcx > 0, Mensagens.TipoEncxDencx.VAL_CODIGO_TIPO);
            #endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select e.CD_TP_ENCX_DENCX, ");
                sb.Append("e.NM_TP_ENCX_DENCX, ");
                sb.Append("e.CD_CALC_TIR, ");
                sb.Append("e.ID_VERBA_INVES, ");
                sb.Append("e.CD_EMPR, ");
                sb.Append("e.CD_GRP_ENCX_DENCX ");
                sb.Append("from NEGCLI_TIPO_ENCX_DENCX e ");
                sb.AppendFormat("where e.CD_TP_ENCX_DENCX = {0} ", DaoUtil.FormatarParametro("codTipoEcxDcx"));

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codTipoEcxDcx"), codTipoEcxDcx, typeof(Int16)));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //se há resultado
                if (dr.Read())
                {
                    //valor de retorno - alíquota de IOF
                    return MontarObjeto(dr);
                }
                else
                {
                    //valor de retorno nulo
                    return null;
                }
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("TipoEncxDencxDao.ObterPorCodigoEncaixe",
                //"Erro ao obter Encaixe/Desencaixe por código do Tipo.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }
        #endregion

        #region Overrides
        protected override TipoEncxDencx MontarObjeto(IDataReader dr)
        {
            TipoEncxDencx ent = new TipoEncxDencx();

            try
            {
                ent.CodigoTipoEncaixeDesencaixe = (Int16?)dr["CD_TP_ENCX_DENCX"];
                ent.NomeTipoEnciaxeDesencaixe = (string)dr["NM_TP_ENCX_DENCX"];
                ent.CodigoCalculoTIR = (Int16?)dr["CD_CALC_TIR"];
                ent.IdVerbaInvestimento = (string)dr["ID_VERBA_INVES"];
                ent.CodigoEmpresa = (string)dr["CD_EMPR"];
                ent.CodigoGrupoEncaixeDesencaixe = (Int16?)dr["CD_GRP_ENCX_DENCX"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
