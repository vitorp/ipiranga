﻿using System;
using Microsoft.VisualBasic;
using tir.dao;
using tir.dao.Entidades;
using TIR.Constantes;
using TIR.Entidades;
using TIR.Entidades.Componentes;
using TIR.Entidades.Encaixes;

namespace TIR.Bo.Calculos
{
    public class CalculoFinanciamento : FechamentoAno, ICalculo
    {
        #region Propriedades
        public Franquia Franquia { get; set; }
        #endregion

        public double[] Calcular(Encaixe enc, TIRNegociacao tir)
        {
            try
            {
                //converte encaixe para financiamento
                Financiamento fin = (Financiamento)enc;

                int contratoFranquia = 0;

                //verifica se passou o objeto franquia
                if (Franquia != null)
                {
                    //se passou busca o tempo de contrato da franquia
                    contratoFranquia = Franquia.TempoContrato;

                    bool isFranquiaRenovacao = false;
                    //verificar pelo tipo se é renovação
                    if (Franquia.TpNegFranquia.Equals(2) || Franquia.TpNegFranquia.Equals(1))
                    {
                        isFranquiaRenovacao = true;
                    }
                }
                else
                {
                    //não passou o objeto franquia, logo não tem tempo de contrato
                    contratoFranquia = 0;
                }

                //---- Acessar a alíquota de IOF - criado em 24/02/99
                //---- Ver na classe de gravação do Oracle, quando gravar
                //---- o financiamento, somar o valor de IOF ao valor de financ.

                float aliqIof = 0;

                //instancia dao e busca alíquota de IOF
                AliqIOFDao aliqDAO = new AliqIOFDao();
                AliqIOF iof = aliqDAO.ObterPorMes(fin.Amortizacao);

                //se não encontrou alíquota ou se o encaixe é "46) Fundo de Comércio - Financiado"
                if (iof == null || fin.CodEcxDcx.Equals(TipoCodEcxDcx.FUNDO_DE_COMERCIO_FINANCIADO))
                {
                    //não utilizar IOF
                    aliqIof = 0;
                }
                else
                {
                    //verifica se é pessoa física ou jurídica
                    if (fin.IndPessoaFisica.Equals(true))
                    {
                        //define alíquota de IOF para pessoa física
                        aliqIof = (float)iof.PercentualIOFPessoaFisica;
                    }
                    else
                    {
                        //define alíquota de IOF para pessoa jurídica
                        aliqIof = (float)iof.PercentualIOFPessoaJuridica;
                    }
                }

                //----- Todos os calculos são em Reais (moeda corrente)
                //----- Somente ao final os valores serão convertidos para FAO.

                double Fao = tir.ValorFAO;
                int prazoTotal = 0;

                //-- 31) Migração Franquias maio/2004
                if (contratoFranquia.Equals(0) || tir.Categoria.Equals(41))
                {
                    //define o prazo total para migração de franquias
                    //prazoTotal = tir.ContrAdicional + tir.ContrExistente + 1;
                    //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
                    prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 1;
                }
                else
                {
                    //define o prazo total
                    prazoTotal = contratoFranquia + 1;
                }

                //---- em junho/2004 a Assessoria de Planejamento de MK
                //---- através de Paula, Andrea e Sivirino, decidiram
                //---- que o financiamento para Franquias deveria começar
                //---- sempre do início do contr. existente se houver.

                // TODO: Analisar aqui com o Felipe, pois o prazo total é alterado!!!

                //redefine prazo total
                //prazoTotal = tir.ContrAdicional + tir.ContrExistente + 1;
                //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
                prazoTotal = tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 1;

                //redimensiona vetores
                //fin.Fluxo = new double[prazoTotal + 1];
                //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
                fin.Fluxo = new double[prazoTotal + tir.MesesCompVolume + 1];
                saldo = new double[prazoTotal + 1];
                juros = new double[prazoTotal + 1];
                amort = new double[prazoTotal + 1];
                prest = new double[prazoTotal + 1];
                saldoSub = new double[prazoTotal + 1];
                recDesp = new double[prazoTotal + 1];
                valorJurCar = new double[prazoTotal + 1];

                //se não tem valor de financiamento no encaixe
                if (fin.ValorFinanc <= 0.0)
                {
                    //sai do método
                    return fin.Fluxo;
                }

                //salva valor
                double valorOriginal = fin.ValorFinanc;

                //verifica se há amortização
                if (fin.Amortizacao <= 0)
                {
                    throw new Exception("CalculoFinanciamento.calcular: Erro ao calcular financiamento. É preciso informar o número de meses para amortização.");
                }

                float sngWork = 0;

                for (int i = 0; i <= fin.PercLiberacao.GetUpperBound(0); i++)
                {
                    sngWork += fin.PercLiberacao[i];
                    if (fin.PercLiberacao[i] > 0.0)
                    {
                        ultimoMes = i;
                    }
                }

                //só permite 100% de percentual total de liberação
                if (!sngWork.Equals(100))
                {
                    throw new Exception("CalculoFinanciamento.calcular: Erro ao calcular financiamento. O percentual total de liberação de financiamento tem que ser de 100%.");
                }

                //verifica se os prazos estão coerentes
                if (fin.Amortizacao + fin.Carencia + ultimoMes > prazoTotal)
                {
                    throw new Exception("CalculoFinanciamento.calcular: Erro ao calcular financiamento. A soma dos prazos de amortização, carência e liberação da última parcela não pode ultrapassar a soma dos contratos adicionais e existentes.");
                }

                //--- obter alíquota de IR e PIS/COFINS
                OutrosParamDao outrosDAO = new OutrosParamDao();
                OutrosParam ir = outrosDAO.ObterParametro("PercIR");
                OutrosParam pis = outrosDAO.ObterParametro("ReceitaPIS");
                float aliqIr = (float)ir.ValorParametroNumerico;
                double aliqPisCof = (double)pis.ValorParametroNumerico;

                //--- Cálculo da liberação e cálculo correção saldo devedor
                mesInicio = 0;
                mesInicioRenovacao = 0;

                //não calcula liberação para fundo de comércio financiado (18/07/2007 - decisão Sivirino e Felipe)
                if (!fin.CodEcxDcx.Equals(TipoCodEcxDcx.FUNDO_DE_COMERCIO_FINANCIADO))
                {
                    for (int i = mesInicio; i <= ultimoMes; i++)
                    {
                        if (fin.PercLiberacao[i] > 0)
                        {
                            fin.Fluxo[i + mesInicioRenovacao] = fin.ValorFinanc * fin.PercLiberacao[i] / 100.0 * (-1.0);
                            if (fin.PercCorMonSaldo > 0 && ultimoMes - i > 0)
                            {
                                cMsaldoDev += fin.Fluxo[i + mesInicioRenovacao] * (-1.0) * (tir.Inflacao[ultimoMes - i] - 1.0) * fin.PercCorMonSaldo / 100.0;
                            }

                            fin.Fluxo[i + mesInicioRenovacao] = (fin.Fluxo[i + mesInicioRenovacao] + fin.Fluxo[i + mesInicioRenovacao] * aliqIof / 100.0 * (1 - aliqIr / 100.0)) / Fao;
                            fin.Fluxo[i + mesInicioRenovacao] = (fin.Fluxo[i + mesInicioRenovacao] / tir.Inflacao[i + mesInicioRenovacao]);
                        }
                    }
                }

                //salvando o valor de IOF para o objeto financ
                fin.ValorIOF = fin.ValorFinanc * aliqIof / 100.0;

                //--- juros total de carência, se tiver vê correção monetária da carência
                if (fin.Carencia > 0 && fin.PercJurosCarencia > 0.0)
                {
                    dblWork = (fin.ValorFinanc + cMsaldoDev) * (((tir.Inflacao[ultimoMes + fin.Carencia] / tir.Inflacao[ultimoMes]) - 1.0) * fin.PercCorMonCarencia / 100.0 + 1.0);
                    jurCarTotal = dblWork * (Math.Pow((fin.PercJurosCarencia / 100.0 + 1.0), fin.Carencia) - 1.0);
                }

                if (fin.JurosCarRepact.Equals(true))
                {
                    fin.ValorFinanc += jurCarTotal;
                }

                dblWork = 0;

                //-- cálculo da prestação
                prestacao = Financial.Pmt(fin.PercJuros / 100.0, fin.Amortizacao, (-1.0) * fin.ValorFinanc, 0.0, 0);
                //int intWork = 0;

                //salvando o Valor da prestação para o objeto financ
                fin.ValorPrestacao = prestacao;

                double valorComJurosCarencia = valorOriginal * (Math.Pow((fin.PercJurosCarencia / 100.0 + 1.0), fin.Carencia));
                fin.ValorPrestacao = Financial.Pmt(fin.PercJuros / 100.0, fin.Amortizacao, (-1.0) * valorComJurosCarencia, 0.0, 0);

                mesInicio = 1;
                for (int i = mesInicio; i <= ultimoMes + fin.Carencia + fin.Amortizacao; i++)
                {
                    if (i > ultimoMes + fin.Carencia)
                    {
                        //período de recebimento das prestações
                        prest[i] = prestacao / tir.Inflacao[i + mesInicioRenovacao];

                        if (saldo[i - 1].Equals(0))
                        {
                            saldo[i - 1] = fin.ValorFinanc;
                        }

                        juros[i] = saldo[i - 1] * fin.PercJuros / 100.0;
                        amort[i] = prestacao - juros[i];
                        saldo[i] = saldo[i - 1] - amort[i];

                        if (saldo[i] < 0.0000001)
                        {
                            saldo[i] = 0.0;
                        }
                    }

                    if (((i / 12.0 - i / 12) * 12) == 0)
                    {
                        fecharAno(fin, tir, i);
                    }
                }

                for (int i = mesInicio; i <= ultimoMes + fin.Carencia + fin.Amortizacao; i++)
                {
                    if (i > ultimoMes + fin.Carencia)
                    {
                        //---> piscofins nos juros 24/02/99
                        fin.Fluxo[i + mesInicioRenovacao] = prest[i] - (juros[i] + recDesp[i]) / tir.Inflacao[i + mesInicioRenovacao]
                               * aliqIr / 100.0 - juros[i] / tir.Inflacao[i + mesInicioRenovacao] * (1.0 - aliqIr / 100.0) * aliqPisCof / 100.0
                               - recDesp[i] / tir.Inflacao[i + mesInicioRenovacao] * (1.0 - aliqIr / 100.0) * aliqPisCof / 100.0;
                    }
                    else if (i <= ultimoMes)
                    {
                        //-- liberação
                        fin.Fluxo[i + mesInicioRenovacao] = fin.Fluxo[i + mesInicioRenovacao]
                               - recDesp[i] / Fao / tir.Inflacao[i + mesInicioRenovacao]
                               * aliqIr / 100.0 - recDesp[i] / Fao / tir.Inflacao[i + mesInicioRenovacao] * (1.0 - aliqIr / 100.0) * aliqPisCof / 100.0;
                    }
                    else
                    {
                        //-- carência ---> piscofins nos juros 24/02/99
                        fin.Fluxo[i + mesInicioRenovacao] = fin.Fluxo[i + mesInicioRenovacao] - (recDesp[i]
                               + valorJurCar[i]) / tir.Inflacao[i + mesInicioRenovacao] * aliqIr / 100.0
                               - valorJurCar[i] / tir.Inflacao[i + mesInicioRenovacao] * (1.0 - aliqIr / 100.0) * aliqPisCof / 100.0
                               - recDesp[i] / tir.Inflacao[i + mesInicioRenovacao] * (1.0 - aliqIr / 100.0) * aliqPisCof / 100.0;
                    }

                    if (i > ultimoMes)
                    {
                        fin.Fluxo[i + mesInicioRenovacao] /= Fao;
                    }
                }

                //-- Juros de carência não repactuados
                if (fin.JurosCarRepact.Equals(false) && (ultimoMes + fin.Carencia) > 0)
                {
                    fin.Fluxo[ultimoMes + fin.Carencia + mesInicioRenovacao] += (jurCarTotal / tir.Inflacao[ultimoMes + fin.Carencia + mesInicioRenovacao] / Fao);
                }

                fin.ValorFinanc = valorOriginal;

                //retorno com sucesso
                return fin.Fluxo;
            }
            catch (Exception ex)
            {
                throw new Exception("CalculoFinanciamento.calcular: Erro ao calcular o fluxo para financiamento.\n" + ex.Message, ex);
            }
        }
    }
}
