﻿
namespace TIR.Entidades.Encaixes
{
    /// <summary>
    /// Encaixe que representa um estoque técnico.
    /// </summary>
    public class EstoqTecnico : Encaixe
    {
        #region Propriedades

        public long CodProduto { get; set; }
        public long CodGrupo { get; set; }
        public double TotalM3 { get; set; }
        public float[] PercLiberacao { get; set; }
        public bool Parcelado { get; set; }
        public int ParcelasMesRetorno { get; set; }
        public int Carencia { get; set; }

        #endregion
    }
}
