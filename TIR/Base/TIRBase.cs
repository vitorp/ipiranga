﻿using System.Configuration;
using daobase.Enumerados;

namespace TIR.Base
{
    public class TIRBase
    {
        public TIRBase()
        {
            SetConexaoBase(ConfigurationManager.ConnectionStrings["ConexaoAccessTestes"].ConnectionString);
        }

        public TIRBase(string conn)
        {
            SetConexaoBase(conn);
        }

        protected void SetConexaoBase(string conn)
        {
            ConexaoBase.GetInstance().StringConexao = conn;
            ConexaoBase.GetInstance().TipoBaseDados = BaseDadosEnum.Access;
            ConexaoBase.GetInstance().Abrir();
        }
    }
}
