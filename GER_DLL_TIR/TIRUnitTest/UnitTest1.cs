﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TIR.Bo.Calculos;
using TIR.Entidades;
using TIR.Entidades.Componentes;
using TIR.Entidades.Encaixes;

namespace TIRUnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void CalculoNovoNegocioGasolina()
        {
            var stopwatch = System.Diagnostics.Stopwatch.StartNew();

            // Configurações básicas do cliente
            var cliente = new Cliente
            {
                Endereco = new Endereco
                {
                    Bairro = "Jd Tupanci",
                    CEP = "06414025",
                    UF = "SP",
                    Logradouro = "Werner Goldberg 157",
                    Municipio = "Barueri"
                },
                Perfil = "1", // 1 - Urbano
                segmento = "REV",
                razaoSocial = "Autoposto VHMP Osasco",
                Posto = new AreaAbastecimento
                {
                    AluguelMargem = new AluguelMargem
                    {
                        PercAluguel = 0
                    },
                    Encaixes = new List<Encaixe>()
                },

                // Configuração de base Supridora
                BasesSupridoras = new List<BaseSupridora>
                {
                    new BaseSupridora
                    {
                        Codigo = "076213" // TODO: Listagem de todos os códigos e nomes de bases supridoras
                    }
                }
            };

            // Configurações básicas de Produto: Gasolina Comum
            var produto = new Produto
            {
                CodGrupo = 111,
                CodProduto = 002, // Gasolina Comum
                MediaMesAno1 = 100,
                MediaMesAno2 = 100,
                MediaMesAno3 = 100,

                AluguelVol = new AluguelVol // TODO: Quais são as informações principais para o Aluguel de Volume?
                {
                    CodProduto = 2
                },
                CapitalGiro = new CapitalGiro // TODO: Quais são as informações principais para o Capital de Giro?
                {
                     CodProduto = 2
                },
                Margem = new Margem // TODO: Quais são as informações principais para o calculo de Margem?
                {
                    CodProduto = 2
                },
                EstoqTecnico = new EstoqTecnico // TODO: Quais são as informações principais para o EstoqTecnico?
                {
                    CodProduto = 2
                }
            };

            // Encaixe para o Produto: Gasolina Comum
            cliente.Posto.Produtos = new List<Produto> {produto};
            cliente.Posto.AluguelMargem.CodProd = new[] { 2L };
            cliente.Posto.AluguelMargem.VrEncargo = new[] { 0.00 };  // TODO: De onde vem essa informação?
            cliente.Contratos = new List<Contratos>();

            cliente.Hipotecas = new List<Hipotecas>();
            var hipoteca = new Hipotecas();

            cliente.Garantias = new List<Garantias>();
            var garantias = new Garantias();

            cliente.Contratos = new List<Contratos>();
            var contratos = new Contratos();
            

            // Criando negociação
            // TODO: Onde configuro o tipo de contrato? Isso muda para o cálculo final?
            var negociacao = new Negcc
            {
                NomeNegociacao = "Negociação Novo Negócio - Gasolina",
                TIR = new TIRNegociacao
                {
                    Cliente = cliente,
                    Categoria = 3,
                    ValorFAO = 7.3702
                },
            };
            
            // Tempo de Contrato
            negociacao.TIR.ContrExistente = 0;
            negociacao.TIR.ContrAdicional = 60;
            negociacao.TIR.PeriodoMatura = 5;

            // Adiciona código de Gerencia de Vendas
            negociacao.TIR.UnidOrg = "070211";

            var proposta = new Proposta
            {
                Negociacoes = new List<Negcc>()
                {
                    negociacao
                }
            };

            // Calcular TIR
            var connectionString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
            var calculadora = new CalculoTIRProposta(connectionString);
            var resultado = calculadora.CalcularTIRProposta(proposta);

            stopwatch.Stop();
            
            Console.WriteLine("TIR: {0}", resultado.TIR);
            Console.WriteLine("NPV: {0}", resultado.NPV);
            Console.WriteLine("PAYBACK: {0}", resultado.Payback);
            Console.WriteLine("--");
            Console.WriteLine("Time elapsed: {0}ms", stopwatch.Elapsed.Milliseconds);

            Assert.IsNotNull(resultado);
        }
    }
}
