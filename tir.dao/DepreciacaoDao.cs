﻿using System;
using System.Data;
using System.Text;
using daobase;
using tir.dao.Entidades;

namespace tir.dao
{
    public class DepreciacaoDao : DaoBase<Depreciacao>
    {
        #region Métodos Públicos

        /// <summary>
        /// Método que obtém os parâmetros de Depreciação para um Encaixe.
        /// </summary>
        /// <param name="codigoEcx">Código do Encaixe.</param>
        /// <returns>Objeto Depreciação.</returns>
        public Depreciacao ObterDepreciacao(Int16 codigoEcx)
        {
            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select d.CD_TP_ENCX_DENCX, ");
                sb.Append("d.NO_MES_DEPREC, ");
                sb.Append("d.NO_MES_REINVEST, ");
                sb.Append("d.PC_REINVEST, ");
                sb.Append("d.VENDA_FINAL ");
                sb.Append("from Depreciacao d ");
                sb.AppendFormat("where d.CD_TP_ENCX_DENCX = {0} ", DaoUtil.FormatarParametro("codigoEcx"));

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codigoEcx"), codigoEcx, typeof(Int16)));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //se há resultado
                if (dr.Read())
                {
                    //valor de retorno
                    return MontarObjeto(dr);
                }
                else
                {
                    //valor de retorno nulo
                    return null;
                }
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("PrazoDepreciacaoDao.ObterDepreciacao",
                //"Erro ao obter a Depreciação para um Encaixe.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        #endregion

        #region Overrides

        protected override Depreciacao MontarObjeto(IDataReader dr)
        {
            Depreciacao ent = new Depreciacao();
            try
            {
                ent.CodigoECX = (Int16)dr["CD_TP_ENCX_DENCX"];
                ent.MesesDepreciacao = (Int16)dr["NO_MES_DEPREC"];
                ent.MesesReinvestimento = (Int16)dr["NO_MES_REINVEST"];
                ent.PercReinvestimento = (Single)dr["PC_REINVEST"];
                ent.VendaFinal = (string)dr["VENDA_FINAL"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
