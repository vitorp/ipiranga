﻿using System;
using System.Data;
//using System.Diagnostics.Contracts;
using System.Text;
using daobase;
using tir.dao.Entidades;

namespace tir.dao
{
    public class ParamNegDao : DaoBase<ParamNeg>
    {
        #region Métodos Públicos
        /// <summary>
        /// Método que obtem um Parâmetro de Negociação.
        /// </summary>
        /// <param name="tipoCategProj">Tipo de negociação.</param>
        /// <param name="siglaSegtoMerc">Segmento de mercado.</param>
        /// <param name="idPerfil">Perfil.</param>
        /// <returns>Objeto ParamNeg.</returns>
        public ParamNeg ObterParametro(int tipoCategProj, string siglaSegtoMerc, int idPerfil)
        {

            //#region Contracts
            //Contract.Requires(tipoCategProj > 0, Mensagens.ParamNeg.VAL_TIPO_CATEGORIA);
            //Contract.Requires(siglaSegtoMerc != null, Mensagens.ParamNeg.VAL_SEGMENTO_MERCADO);
            //Contract.Requires(idPerfil > 0, Mensagens.ParamNeg.VAL_ID_PERFIL);
            //#endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select p.Tp_Categ_Proj, ");
                sb.Append("p.Sg_segto_merc, ");
                sb.Append("p.Id_perfil_posto, ");
                sb.Append("p.Qt_Mes_Max, ");
                sb.Append("p.Vo_Tot_Min, ");
                sb.Append("p.Ind_Matura, ");
                sb.Append("p.Ind_Deprec25, ");
                sb.Append("p.Ind_contr_exist, ");
                sb.Append("p.IndRecAluguelExist, ");
                sb.Append("p.Ind_Prazo_Minimo ");
                sb.Append("from ParamNeg p ");
                sb.AppendFormat("where p.Tp_Categ_Proj = {0} ", DaoUtil.FormatarParametro("tipoCategProj"));
                sb.AppendFormat("and p.Sg_segto_merc = {0} ", DaoUtil.FormatarParametro("siglaSegtoMerc"));
                sb.AppendFormat("and p.Id_perfil_posto = {0} ", DaoUtil.FormatarParametro("idPerfil"));

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("tipoCategProj"), tipoCategProj, typeof(Int16)));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("siglaSegtoMerc"), siglaSegtoMerc, typeof(String), 3));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("idPerfil"), idPerfil, typeof(Int16)));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //se há resultado
                if (dr.Read())
                {
                    //valor de retorno - alíquota de IOF
                    return MontarObjeto(dr);
                }
                else
                {
                    //valor de retorno nulo
                    return null;
                }
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("ParamNegDao.ObterParametro",
                //"Erro ao obter Parâmetro por Tipo da Categoria, Sigla do Segmento de Mercado e Perfil.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        /// <summary>
        /// Método que obtem um Parâmetro de Negociação de acordo com
        /// o tipo de Negociação e que tenha Maturação de Vendas.
        /// </summary>
        /// <param name="tipoCategProj">Tipo de negociação.</param>
        /// <returns>Objeto ParamNeg.</returns>
        public ParamNeg ObterMaturacao(int tipoCategProj)
        {
            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select p.Tp_Categ_Proj, ");
                sb.Append("p.Sg_segto_merc, ");
                sb.Append("p.Id_perfil_posto, ");
                sb.Append("p.Qt_Mes_Max, ");
                sb.Append("p.Vo_Tot_Min, ");
                sb.Append("p.Ind_Matura, ");
                sb.Append("p.Ind_Deprec25, ");
                sb.Append("p.Ind_contr_exist, ");
                sb.Append("p.IndRecAluguelExist, ");
                sb.Append("p.Ind_Prazo_Minimo ");
                sb.Append("from ParamNeg p ");
                sb.AppendFormat("where p.Tp_Categ_Proj = {0} ", DaoUtil.FormatarParametro("tipoCategProj"));
                sb.Append("and p.ind_matura = -1 ");
                //sb.AppendFormat("and p.Sg_segto_merc = {0} ", DaoUtil.FormatarParametro("siglaSegtoMerc"));
                //sb.AppendFormat("and p.Id_perfil_posto = {0} ", DaoUtil.FormatarParametro("idPerfil"));

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("tipoCategProj"), tipoCategProj, typeof(Int16)));
                //cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("siglaSegtoMerc"), siglaSegtoMerc, typeof(String), 3));
                //cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("idPerfil"), idPerfil, typeof(Int16)));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //se há resultado
                if (dr.Read())
                {
                    //valor de retorno - alíquota de IOF
                    return MontarObjeto(dr);
                }
                else
                {
                    //valor de retorno nulo
                    return null;
                }
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("ParamNegDao.ObterParametro",
                //"Erro ao obter Parâmetro por Tipo da Categoria, Sigla do Segmento de Mercado e Perfil.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        #endregion

        #region Overrides

        protected override ParamNeg MontarObjeto(IDataReader dr)
        {
            ParamNeg ent = new ParamNeg();
            try
            {
                ent.TipoCategoriaProjeto = (Int16)dr["Tp_Categ_Proj"];
                ent.SiglaSegmentoMercado = (string)dr["Sg_segto_merc"];
                ent.PerfilPosto = (Int16)dr["Id_perfil_posto"];
                ent.QuantidadeMaximaMesesProjeto = (Int16)dr["Qt_Mes_Max"];
                ent.VolumeTotalMinimoPositivo = (double)dr["Vo_Tot_Min"];
                ent.IndicadorMaturacao = (Boolean)dr["Ind_Matura"];
                ent.IndicadorDepreciacao25 = (Boolean)dr["Ind_deprec25"];
                ent.IndicadorContratoExistente = (Boolean)dr["Ind_contr_exist"];
                ent.IndicadorRecebimentoAluguelExistente = (Boolean)dr["IndRecAluguelExist"];
                ent.IndicadorPrazoMinimo = dr["Ind_Prazo_Minimo"] != DBNull.Value ? (Boolean?)dr["Ind_Prazo_Minimo"] : null;
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
