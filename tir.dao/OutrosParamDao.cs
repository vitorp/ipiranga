﻿using System;
using System.Data;
//using System.Diagnostics.Contracts;
using System.Text;
using daobase;
using tir.dao.Entidades;

namespace tir.dao
{
    public class OutrosParamDao : DaoBase<OutrosParam>
    {
        #region Métodos Públicos
        /// <summary>
        /// Método que retorna um determinado Parâmetro.
        /// </summary>
        /// <param name="siglaParam">Sigla do Parâmtero.</param>
        /// <returns>Objeto OutrosParam.</returns>
        public OutrosParam ObterParametro(string siglaParam)
        {
            #region Contracts
            ////Contract.Requires(siglaParam != null, Mensagens.OutrosParam.VAL_SIGLA_PARAM);
            #endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select p.Sg_Param, ");
                sb.Append("p.Nm_Param, ");
                sb.Append("p.Vr_Param_Num, ");
                sb.Append("p.Vr_Param_txt ");
                sb.Append("from OutrosParam p ");
                sb.AppendFormat("where p.Sg_Param = {0} ", DaoUtil.FormatarParametro("siglaParam"));

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("siglaParam"), siglaParam, typeof(String), 12));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //se há resultado
                if (dr.Read())
                {
                    //valor de retorno - alíquota de IOF
                    return MontarObjeto(dr);
                }
                else
                {
                    //valor de retorno nulo
                    return null;
                }
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("OutrosParamDao.ObterParametro",
                //"Erro ao obter Parâmetro pela sigla.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }
        #endregion
        
        #region Overrides

        protected override OutrosParam MontarObjeto(IDataReader dr)
        {
            OutrosParam ent = new OutrosParam();
            try
            {
                ent.CodigoParametro = (string)dr["Sg_Param"];
                ent.NomeParametro = (string)dr["Nm_Param"];
                ent.ValorParametroNumerico = (double)dr["Vr_Param_Num"];
                ent.ValorParametroTexto = dr["Vr_param_Txt"] != DBNull.Value ? (string)dr["Vr_param_Txt"] : string.Empty;
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
