﻿using System;
using System.Collections.Generic;
using System.Data;
//using System.Diagnostics.Contracts;
using System.Text;
using daobase;
using tir.dao.Entidades;

namespace tir.dao
{
    public class MaturaVendaDao : DaoBase<MaturaVenda>
    {
        #region Métodos Públicos

        #endregion
        /// <summary>
        /// Lista maturação de vendas do tipo de negociação.
        /// </summary>
        /// <param name="tpCategProj">Tipo de negociação.</param>
        /// <returns>Lista de objetos MaturaVenda.</returns>
        public List<MaturaVenda> ListarPorTipoNegociacao(int tpCategProj)
        {
            #region Contracts
            //Contract.Requires(tpCategProj > 0, Mensagens.MaturaVenda.VAL_TIPO_NEGOCIACAO);
            #endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select m.Tp_Categ_Proj, ");
                sb.Append("m.No_Mes_Maturac, ");
                sb.Append("m.Pc_Vo_Venda ");
                sb.Append("from MaturacaoVenda m ");
                sb.AppendFormat("where m.Tp_Categ_Proj = {0} ", DaoUtil.FormatarParametro("tpCategProj"));
                sb.Append("order by m.Tp_Categ_Proj, m.No_Mes_Maturac");

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("tpCategProj"), tpCategProj, typeof(Int16)));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //valor de retorno
                return MontarLista(dr);
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("MaturaVendaDao.ListarPorTipoNegociacao",
                //"Erro ao listar Maturação de Vendas do tipo de Negociação", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        #region Overrides

        protected override MaturaVenda MontarObjeto(IDataReader dr)
        {
            MaturaVenda ent = new MaturaVenda();
            try
            {
                ent.CodigoTipoNegociacao = (Int16)dr["Tp_Categ_Proj"];
                ent.QuantidadeMesesMaturacao = (Int16)dr["No_Mes_Maturac"];
                ent.PercentualVolumeVendas = (Single)dr["Pc_Vo_Venda"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
