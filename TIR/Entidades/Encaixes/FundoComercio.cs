﻿
namespace TIR.Entidades.Encaixes
{
    /// <summary>
    /// Encaixe que represeta um fundo de comércio.
    /// </summary>
    public class FundoComercio : Encaixe
    {
        #region Propriedades

        public double ValorCompra { get; set; }
        public int MesCompraInicial { get; set; }
        public int MesCompraFinal { get; set; }
        public double ValorVendaVista { get; set; }
        public double ValorVenda { get; set; }
        public int MesVendaVista { get; set; }
        public int MesVendaInicial { get; set; }
        public int MesVendaFinal { get; set; }
        public int NumMesesCompra { get; set; }
        public int NumMesesVenda { get; set; }

        #endregion
    }
}
