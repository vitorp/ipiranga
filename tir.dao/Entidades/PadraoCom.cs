﻿
namespace tir.dao.Entidades
{
    public class PadraoCom
    {
        #region Propriedades
        public int CodigoTipoComponente { get; set; }
        public int CodigoPadraoComponente { get; set; }
        public string NomePadraoComponente { get; set; }
        #endregion
    }
}
