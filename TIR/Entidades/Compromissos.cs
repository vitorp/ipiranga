﻿
namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que representa os compromissos.
    /// </summary>
    public class Compromissos
    {
        #region Propriedades

        public float ValorAVencer { get; set; }
        public float ValorVencido { get; set; }
        public TipoCompromisso TipoCompromisso { get; set; }

        #endregion
    }
}
