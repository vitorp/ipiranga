﻿
namespace tir.dao
{
    public struct Mensagens
    {
        public struct AliqIOF
        {
            public const string VAL_NO_MES = "Número do mês deve ser maior que zero.";
        }

        public struct Aliquota
        {
            public const string VAL_SIGLA_UF = "Sigla da UF não pode ser nula.";
            public const string VAL_CODIGO_GRUPO = "Código do Grupo não pode ser nulo.";
        }

        public struct AplicacaoVerba
        {
            public const string VAL_CODIGO_UNIDADE = "Código da Unidade tem de ser maior que zero.";
            public const string VAL_TIPO_NEGOCIACAO = "Código do Tipo de Negociação tem de ser maior que zero.";
            public const string VAL_TIPO_ECX_DCX = "Código do Encaixe/Desencaixe tem de ser maior que zero.";
        }

        public struct Bandeira
        {
            public const string VAL_CODIGO_BANDEIRA = "Código da Bandeira tem de ser maior que zero.";
        }

        public struct CustoDaBase
        {
            public const string VAL_CODIGO_UNIDADE = "Código da Unidade não pode ser nulo.";
            public const string VAL_TIPO_NEGOCIACAO = "Código do Tipo de negociação não pode ser nulo.";
            public const string VAL_CODIGO_PRODUTO = "Código do Produto não pode ser nulo.";
            public const string VAL_NO_MES = "Número do mês deve ser maior que zero.";
        }

        public struct CustoOverhead
        {
            public const string VAL_CODIGO_GV = "Código da Gerência de Vendas não pode ser nulo.";
            public const string VAL_CODIGO_PADRAO = "Código do Padrão da Franquia tem de ser maior que zero.";
            public const string VAL_CODIGO_PERFIL = "Código do Perfil da Franquia tem de ser maior que zero.";
            public const string VAL_CODIGO_TIPO = "Código do Tipo da Franquia deve ser maior que zero.";
        }

        public struct CustoTerc
        {
            public const string VAL_CODIGO_PADRAO = "Código do Padrão do Componente tem de ser maior que zero.";
            public const string VAL_CODIGO_PERFIL = "Código do Perfil do Componente tem de ser maior que zero.";
            public const string VAL_CODIGO_TIPO = "Código do Tipo do Componente deve ser maior que zero.";
        }

        public struct Custos
        {
            public const string VAL_CODIGO_GV = "Código da Gerência de Vendas não pode ser nulo.";
            public const string VAL_TIPO_NEGOCIACAO = "Código do Tipo de negociação não pode ser nulo.";
            public const string VAL_CODIGO_PRODUTO = "Código do Produto não pode ser nulo.";
            public const string VAL_ID_PERFIL = "Código do Perfil deve ser maior que zero.";
            public const string VAL_NO_MES = "Número do mês deve ser maior que zero.";
        }

        public struct DencxUnidNegcc
        {
            public const string VAL_CODIGO_UNIDADE = "Código da Unidade de Negociação tem de ser maior que zero.";
        }

        public struct DescontoMin
        {
            public const string VAL_TIPO_NEGOCIACAO = "Código do Tipo de negociação não pode ser nulo.";
            public const string VAL_CODIGO_PRODUTO = "Código do Produto não pode ser nulo.";
        }

        public struct EncargoRev
        {
            public const string VAL_SIGLA_UF = "Sigla da UF não pode ser nula.";
            public const string VAL_CODIGO_PRODUTO = "Código do Produto não pode ser nulo.";
        }

        public struct FatorReducaoGNV
        {
            public const string VAL_CODIGO_GV = "Código da Gerência de Vendas não pode ser nulo.";
        }

        public struct GrupoProduto
        {
            public const string VAL_CODIGO_GRUPO = "Código do Grupo do Produto não pode ser nulo.";
        }

        public struct Inflacao
        {
            public const string VAL_NO_ANO = "Número do ano tem de ser maior que zero.";
        }

        public struct MargemPrazo
        {
            public const string VAL_CODIGO_GV = "Código da Gerência de Vendas não pode ser nulo.";
            public const string VAL_TIPO_NEGOCIACAO = "Código do Tipo de negociação não pode ser nulo.";
            public const string VAL_ID_PERFIL = "Código do Perfil deve ser maior que zero.";
            public const string VAL_CODIGO_PRODUTO = "Código do Produto não pode ser nulo.";
        }

        public struct MaturaVenda
        {
            public const string VAL_TIPO_NEGOCIACAO = "Código do Tipo de negociação deve ser maior que zero.";
        }

        public struct OutrosParam
        {
            public const string VAL_SIGLA_PARAM = "Sigla do Parâmetro não pode ser nula.";
        }

        public struct ParamNeg
        {
            public const string VAL_TIPO_CATEGORIA = "Tipo da Categoria tem de ser maior que zero.";
            public const string VAL_SEGMENTO_MERCADO = "Código do Tipo de negociação não pode ser nulo.";
            public const string VAL_ID_PERFIL = "Código do Perfil deve ser maior que zero.";
        }

        public struct PerfilCom
        {
            public const string VAL_CODIGO_PADRAO = "Código do Padrão da Franquia tem de ser maior que zero.";
            public const string VAL_CODIGO_PERFIL = "Código do Perfil da Franquia tem de ser maior que zero.";
            public const string VAL_CODIGO_TIPO = "Código do Tipo da Franquia deve ser maior que zero.";
        }

        public struct PrazosMin
        {
            public const string VAL_TIPO_NEGOCIACAO = "Código do Tipo de negociação não pode ser nulo.";
            public const string VAL_ID_PERFIL = "Código do Perfil deve ser maior que zero.";
            public const string VAL_CODIGO_PRODUTO = "Código do Produto não pode ser nulo.";
        }

        public struct RedutorFat
        {
            public const string VAL_CODIGO_PADRAO = "Código do Padrão do Componente tem de ser maior que zero.";
            public const string VAL_CODIGO_PERFIL = "Código do Perfil do Componente tem de ser maior que zero.";
            public const string VAL_CODIGO_TIPO = "Código do Tipo do Componente deve ser maior que zero.";
        }

        public struct RotinaCalculoTIR
        {
            public const string VAL_CODIGO_CALCULO = "Código de Cálculo tem de ser maior que zero.";
        }

        public struct RoyalArco
        {
            public const string VAL_CODIGO_PADRAO = "Código do Padrão do Componente tem de ser maior que zero.";
            public const string VAL_CODIGO_PERFIL = "Código do Perfil do Componente tem de ser maior que zero.";
            public const string VAL_CODIGO_TIPO = "Código do Tipo do Componente deve ser maior que zero.";
        }

        public struct RoyalCbpi
        {
            public const string VAL_CODIGO_PADRAO = "Código do Padrão do Componente tem de ser maior que zero.";
            public const string VAL_CODIGO_PERFIL = "Código do Perfil do Componente tem de ser maior que zero.";
            public const string VAL_CODIGO_TIPO = "Código do Tipo do Componente deve ser maior que zero.";
        }

        public struct TipoCom
        {
            public const string VAL_CODIGO_TIPO = "Código do Tipo do Componente deve ser maior que zero.";
        }

        public struct TipoEncxDencx
        {
            public const string VAL_CODIGO_TIPO = "Código de Tipo de Encaixe/Desencaixe tem de ser maior que zero.";
        }

        public struct TipoNegociacao
        {
            public const string VAL_CODIGO_TIPO = "Código de Tipo de Negociação tem de ser maior que zero.";
        }

        public struct UnidadeNegociacao
        {
            public const string VAL_CODIGO_UNIDADE = "Código da Unidade de Negociação tem de ser maior que zero.";        
        }

        #region Mensagens de Validação
        #endregion

        #region Mensagens de Erro
        #endregion
    }
}
