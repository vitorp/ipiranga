﻿using System.Runtime.Serialization;
using WebService.TIR.Models.Encaixes;
using WebService.TIR.Models.Franquias;

namespace WebService.TIR.Models
{
    [DataContract]
    public class ParametrosTir
    {
        [DataMember(Order = 0)]
        public double ValorFao { get; set; }

        [DataMember(Order = 1)]
        public string CodigoDeGerenciaDeVendas { get; set; }

        [DataMember(Order = 2)]
        public string CodigoBaseSupridora { get; set; }

        [DataMember(Order = 3)]
        public string UF { get; set; }

        [DataMember(Order = 4)]
        public int Categoria { get; set; }

        [DataMember(Order = 5)]
        public string Perfil { get; set; }

        [DataMember(Order = 6)]
        public string Segmento { get; set; }

        [DataMember(Order = 7)]
        public int TempoDeContratoExistente { get; set; }

        [DataMember(Order = 8)]
        public int TempoDeContratoAdicional { get; set; }

        [DataMember(Order = 9)]
        public int TempoDePeriodoMatura { get; set; }
        
        [DataMember(Order = 10)]
        public ProdutoTir[] Produtos { get; set; } 

        [DataMember(Order = 11)]
        public bool HasAmPm { get; set; }

        [DataMember(Order = 12)]
        public AmPmTir AmPm { get; set; }

        [DataMember(Order = 13)]
        public bool HasJetOil { get; set; }
        
        [DataMember(Order = 14)]
        public JetOilTir JetOil { get; set; }

        [DataMember(Order = 15)]
        public bool HasJetOilMotos { get; set; }

        [DataMember(Order = 16)]
        public JetOilMotosTir JetOilMotos { get; set; }

        [DataMember(Order = 17)]
        public FinanciamentoTir[] Financiamentos { get; set; }

        [DataMember(Order = 18)]
        public BenfeitoriaTir[] Benfeitorias { get; set; }

        [DataMember(Order = 19)]
        public DespesaReceitaTir[] DespesasReceitas { get; set; }

        [DataMember(Order = 20)]
        public AluguelAntecipadoTir[] AntecipacaoAluguel { get; set; }

        [DataMember(Order = 21)]
        public AluguelAntecipadoCtfTir[] AntecipacaoAluguelCtf { get; set; }

        [DataMember(Order = 22)]
        public BonificacaoAntecipadaTir[] BonificacaoAntecipadaTir { get; set; }

        [DataMember(Order = 23)]
        public BonificacaoPostecipadaTir[] BonificacaoPostecipadaTir { get; set; }

        [DataMember(Order = 24)]
        public EquipamentoTir[] Equipamentos { get; set; }

        [DataMember(Order = 25)]
        public RecebimentoDeLuvasTir[] RecebimentoDeLuvas { get; set; }
    }
}
