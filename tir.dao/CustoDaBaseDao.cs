﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using daobase;
//using System.Diagnostics.Contracts;
using tir.dao.Entidades;

namespace tir.dao
{
    public class CustoDaBaseDao : DaoBase<CustoDaBase>
    {
        #region Métodos Públicos

        /// <summary>
        /// Busca um valor de Taxa de Movimentação por Código da Unidade,
        /// Tipo de Negociação, Código do Produto e número do Mês.
        /// </summary>
        /// <param name="CodUnid">Código da Unidade.</param>
        /// <param name="TipoNeg">Tipo de negociação.</param>
        /// <param name="CodProduto">Código do Produto.</param>
        /// <param name="NumeroMes">Número do Mês.</param>
        /// <returns>Objeto CustoDaBase.</returns>
        public CustoDaBase ObterTaxaMovimentacao(string CodUnid, string TipoNeg, string CodProduto, decimal NumeroMes)
        {
            #region Contracts
            //Contract.Requires(CodUnid != null, Mensagens.CustoDaBase.VAL_CODIGO_UNIDADE);
            //Contract.Requires(TipoNeg != null, Mensagens.CustoDaBase.VAL_TIPO_NEGOCIACAO);
            //Contract.Requires(CodProduto != null, Mensagens.CustoDaBase.VAL_CODIGO_PRODUTO);
            //Contract.Requires(NumeroMes > 0, Mensagens.CustoDaBase.VAL_NO_MES);
            #endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select c.cd_unid, ");
                sb.Append("c.sg_tpneg, ");
                sb.Append("c.cd_prod, ");
                sb.Append("c.no_mes_faixa, ");
                sb.Append("c.Vr_Taxa_Mov ");
                sb.Append("from CustoDaBase c ");
                sb.AppendFormat("where c.cd_unid = {0} ", DaoUtil.FormatarParametro("CodUnid"));
                sb.AppendFormat("and c.sg_tpneg = {0} ", DaoUtil.FormatarParametro("TipoNeg"));
                sb.AppendFormat("and c.cd_prod = {0} ", DaoUtil.FormatarParametro("CodProduto"));
                sb.AppendFormat("and c.no_mes_faixa = {0} ", DaoUtil.FormatarParametro("NumeroMes"));
                sb.Append("order by c.cd_unid, c.sg_tpneg, c.cd_prod, c.no_mes_faixa");

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("CodUnid"), CodUnid, typeof(String), 6));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("TipoNeg"), TipoNeg, typeof(String), 3));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("CodProduto"), CodProduto, typeof(String), 3));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("NumeroMes"), NumeroMes, typeof(Int16)));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //se há resultado
                if (dr.Read())
                {
                    //valor de retorno - alíquota de IOF
                    return MontarObjeto(dr);
                }
                else
                {
                    //valor de retorno nulo
                    return null;
                }
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("CustoDaBaseDao.ObterTaxaMovimentacao",
                //"Erro ao buscar taxa de movimentação por unidade, tipo de negociação, código do produto e número do mês da faixa.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        /// <summary>
        /// Obtem uma faixa de valores de Custo por Unidade,
        /// Segmento de Mercado e Código de Produto.
        /// </summary>
        /// <param name="CodUnid">Código da Unidade.</param>
        /// <param name="TipoNeg">Segmento de Mercado.</param>
        /// <param name="CodProduto">Código do Produto.</param>
        /// <param name="NoMes">Mês para buscar a faixa de custo.</param>
        /// <returns>Objeto CustoDabase.</returns>
        public CustoDaBase ObterFaixaCustoBase(string CodUnid, string TipoNeg, string CodProduto, int NoMes)
        {
            #region Contracts
            //Contract.Requires(CodUnid != null, Mensagens.CustoDaBase.VAL_CODIGO_UNIDADE);
            //Contract.Requires(TipoNeg != null, Mensagens.CustoDaBase.VAL_TIPO_NEGOCIACAO);
            //Contract.Requires(CodProduto != null, Mensagens.CustoDaBase.VAL_CODIGO_PRODUTO);
            #endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select c.cd_unid, ");
                sb.Append("c.sg_tpneg, ");
                sb.Append("c.cd_prod, ");
                sb.Append("c.no_mes_faixa, ");
                sb.Append("c.Vr_Taxa_Mov ");
                sb.Append("from CustoDaBase c ");
                sb.AppendFormat("where c.cd_unid = {0} ", DaoUtil.FormatarParametro("CodUnid"));
                sb.AppendFormat("and c.sg_tpneg = {0} ", DaoUtil.FormatarParametro("TipoNeg"));
                sb.AppendFormat("and c.cd_prod = {0} ", DaoUtil.FormatarParametro("CodProduto"));
                sb.Append("order by c.no_mes_faixa ");

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("CodUnid"), CodUnid, typeof(String), 6));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("TipoNeg"), TipoNeg, typeof(String), 3));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("CodProduto"), CodProduto, typeof(String), 3));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //se há resultado
                if (!dr.Equals(null))
                {
                    //busca faixas de custo
                    List<CustoDaBase> Custos = MontarLista(dr);

                    //valor padrão de retorno
                    CustoDaBase CustoDesejado = null;

                    //varre faixas
                    for (int i = 0; i <= Custos.Count - 1; i++)
                    {
                        //condição para buscar custo
                        if (NoMes <= Custos[i].NumeroMesFaixa || i == Custos.Count - 1)
                        {
                            //valor encontrado para faixa de custo
                            CustoDesejado = Custos[i];
                            break;
                        }
                    }

                    //valor de retorno
                    return CustoDesejado;
                }
                else
                {
                    //valor de retorno nulo
                    return null;
                }
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("CustoDaBaseDao.ObterFaixaCustoBase",
                //"Erro ao obter uma faixa de Valor de Custo por unidade, tipo de negociação e código do produto.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        /// <summary>
        /// Método que lista os Custos da Base.
        /// </summary>
        /// <param name="codUnid">Código da base.</param>
        /// <param name="tipoNegcc">Tipo de negociação.</param>
        /// <param name="codProdu">Código do produto.</param>
        /// <returns>Lista de objetos CustoDaBase.</returns>
        public List<CustoDaBase> ListarCustosBase(string codUnid, string tipoNegcc, string codProdu)
        {
            #region Contracts
            //Contract.Requires(codUnid != null, Mensagens.CustoDaBase.VAL_CODIGO_UNIDADE);
            //Contract.Requires(tipoNegcc != null, Mensagens.CustoDaBase.VAL_TIPO_NEGOCIACAO);
            //Contract.Requires(codProdu != null, Mensagens.CustoDaBase.VAL_CODIGO_PRODUTO);
            #endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select c.cd_unid, ");
                sb.Append("c.sg_tpneg, ");
                sb.Append("c.cd_prod, ");
                sb.Append("c.no_mes_faixa, ");
                sb.Append("c.Vr_Taxa_Mov ");
                sb.Append("from CustoDaBase c ");
                sb.AppendFormat("where c.cd_unid = {0} ", DaoUtil.FormatarParametro("codUnid"));
                sb.AppendFormat("and c.sg_tpneg = {0} ", DaoUtil.FormatarParametro("tipoNegcc"));
                sb.AppendFormat("and c.cd_prod = {0} ", DaoUtil.FormatarParametro("codProdu"));
                sb.Append("order by c.no_mes_faixa ");

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codUnid"), codUnid, typeof(String), 6));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("tipoNegcc"), tipoNegcc, typeof(String), 3));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codProdu"), codProdu, typeof(String), 3));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //valor de retorno
                return MontarLista(dr);
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("CustoDaBaseDao.ListarFaixasCusto",
                //"Erro ao buscar faixas de Valor de Custo por unidade, tipo de negociação e código do produto.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }
        #endregion

        #region Overrides

        protected override CustoDaBase MontarObjeto(IDataReader dr)
        {
            CustoDaBase ent = new CustoDaBase();
            try
            {
                ent.CodigoUnidade = (string)dr["cd_unid"];
                ent.SegmentoTipoNegocio = (string)dr["sg_tpneg"];
                ent.CodigoProduto = (string)dr["cd_prod"];
                ent.NumeroMesFaixa = (Int16)dr["no_mes_faixa"];
                ent.ValorTaxaMovimentacao = (double)dr["Vr_Taxa_Mov"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }

        #endregion
    }
}
