﻿using System;

namespace tir.dao.Entidades
{
    public class UnidadeNegociacao
    {
        #region Atributos
        public Int16 CodigoUnidadeNegociacao { get; set; }
        public string NomeUnidadeNegociacao { get; set; }
        #endregion
    }
}
