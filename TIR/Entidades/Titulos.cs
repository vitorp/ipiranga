﻿
namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que representa os títulos.
    /// </summary>
    public class Titulos
    {
        #region Propriedades

        public string NumDocumento { get; set; }
        public string CodTipoDocumento { get; set; }
        public string TipoDocumento { get; set; }
        public double ValorSaldo { get; set; }
        public string Vencimento { get; set; }
        public int NumParcela { get; set; }
        public string CodUnidOrg { get; set; }
        public string TipoCobranca { get; set; }
        public int NumDiasAtraso { get; set; }
        public double ValorJuros { get; set; }
        public bool IsAcomoda { get; set; }

        #endregion
    }
}
