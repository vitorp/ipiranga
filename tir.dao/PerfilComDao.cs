﻿using System;
using System.Data;
//using System.Diagnostics.Contracts;
using System.Text;
using daobase;
using tir.dao.Entidades;

namespace tir.dao
{
    public class PerfilComDao : DaoBase<PerfilCom>
    {
        #region Métodos Públicos
        /// <summary>
        /// Obtem Perfil de Componente.
        /// </summary>
        /// <param name="codPadrao">Padrão de componente.</param>
        /// <param name="codPerfil">Perfil de componente.</param>
        /// <param name="codTipo">Tipo de componente.</param>
        /// <returns>Objeto PerfilCom.</returns>
        public PerfilCom ObterPerfilComponente(long codPadrao, long codPerfil, long codTipo)
        {
            #region Contracts
            //Contract.Requires(codPadrao > 0, Mensagens.PerfilCom.VAL_CODIGO_PADRAO);
            //Contract.Requires(codPerfil > 0, Mensagens.PerfilCom.VAL_CODIGO_PERFIL);
            //Contract.Requires(codTipo > 0, Mensagens.PerfilCom.VAL_CODIGO_TIPO);
            #endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select p.cd_tipo_com, ");
                sb.Append("p.cd_padrao_com, ");
                sb.Append("p.cd_perfil_com, ");
                sb.Append("p.nm_perfil_com, ");
                sb.Append("p.vl_fat_mes, ");
                sb.Append("p.vl_max_financ, ");
                sb.Append("p.vl_tx_franquia, ");
                sb.Append("p.max_parcela_tx_franq, ");
                sb.Append("p.vl_fdo_propaganda, ");
                sb.Append("p.pc_fdo_propaganda, ");
                sb.Append("p.vl_max_equip ");
                sb.Append("from PerfilCom p ");
                sb.AppendFormat("where p.cd_padrao_com = {0} ", DaoUtil.FormatarParametro("codPadrao"));
                sb.AppendFormat("and p.cd_perfil_com = {0} ", DaoUtil.FormatarParametro("codPerfil"));
                sb.AppendFormat("and p.cd_tipo_com = {0} ", DaoUtil.FormatarParametro("codTipo"));

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codPadrao"), codPadrao, typeof(Int16)));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codPerfil"), codPerfil, typeof(Int16)));
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codTipo"), codTipo, typeof(Int16)));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //se há resultado
                if (dr.Read())
                {
                    //valor de retorno
                    return MontarObjeto(dr);
                }
                else
                {
                    //valor de retorno nulo
                    return null;
                }
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("PerfilComDao.ObterPerfilComponente",
                //"Erro ao obter o Perfil do Componente para uma Franquia.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }
        #endregion

        #region Overrides

        protected override PerfilCom MontarObjeto(IDataReader dr)
        {
            PerfilCom ent = new PerfilCom();
            try
            {
                ent.CodigoTipoComponente = (Int32)dr["cd_tipo_com"];
                ent.CodigoPadraoComponente = (Int32)dr["cd_padrao_com"];
                ent.CodigoPerfilComponente = (Int32)dr["cd_perfil_com"];
                ent.NomePerfilComponente = (string)dr["nm_perfil_com"];
                ent.ValorFaturamentoMensal = (double)dr["vl_fat_mes"];
                ent.ValorMaximoFinanciamento = (double)dr["vl_max_financ"];
                ent.ValorTaxaFranquia = (double)dr["vl_tx_franquia"];
                ent.NumeroMaximoParcelasTaxaFranquia = (Int16)dr["max_parcela_tx_franq"];
                ent.ValorFundoPropaganda = (double)dr["vl_fdo_propaganda"];
                ent.PercentualFundoPropaganda = (Single)dr["pc_fdo_propaganda"];
                ent.ValorMaximoEquipamento = (double)dr["vl_max_equip"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
