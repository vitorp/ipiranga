﻿using System;
using System.Data;
using System.Text;
//using System.Diagnostics.Contracts;
using daobase;
using tir.dao.Entidades;

namespace tir.dao
{
    public class BandeiraDao : DaoBase<Bandeira>
    {
        #region Métodos Públicos

        public Bandeira ObterPorCodigo(long codBand)
        {
            #region Contracts
            //Contract.Requires(codBand > 0, Mensagens.Bandeira.VAL_CODIGO_BANDEIRA);
            #endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select b.cd_band, ");
                sb.Append("b.nm_band, ");
                sb.Append("b.id_propr_band, ");
                sb.Append("b.cd_ativ_neg, ");
                sb.Append("b.ds_ativ_neg, ");
                sb.Append("b.cd_tipo_ativ_neg, ");
                sb.Append("b.ds_tipo_ativ_neg ");
                sb.Append("from Bandeira b ");
                sb.AppendFormat("where b.cd_band = {0} ", DaoUtil.FormatarParametro("codBand"));

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codBand"), codBand, typeof(Int16)));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //se há resultado
                if (dr.Read())
                {
                    //valor de retorno - alíquota de IOF
                    return MontarObjeto(dr);
                }
                else
                {
                    //valor de retorno nulo
                    return null;
                }
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("AplicacaoVerbaDao.Obter",
                //"Erro ao buscar uma Aplicação de Verba.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        #endregion

        #region Overrides
        protected override Bandeira MontarObjeto(IDataReader dr)
        {
            Bandeira ent = new Bandeira();

            try
            {
                ent.CodigoBandeira = (Int32)dr["cd_band"];
                ent.NomeBandeira = (string)dr["nm_band"];
                ent.IdProprBand = (string)dr["id_propr_band"];
                ent.CodigoAtividadeNeg = (Int32)dr["cd_ativ_neg"];
                ent.DescricaoAtividadeNeg = (string)dr["ds_ativ_neg"];
                ent.CodigoTipoAtividadeNeg = (Int32)dr["cd_tipo_ativ_neg"];
                ent.DescricaoTipoAtividadeNeg = (string)dr["ds_tipo_ativ_neg"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
