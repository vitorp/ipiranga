﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using WebService.TIR.Models.Base;

namespace WebService.TIR.Models.Encaixes
{
    [DataContract]
    public class RecebimentoDeLuvasTir 
    {
        [DataMember]
        public int MesSaidaVista { get; set; }

        [DataMember]
        public double ValorVista { get; set; }

        [DataMember]
        public int Amortizacao { get; set; }

        [DataMember]
        public int Carencia { get; set; }

        [DataMember]
        public float PercCorMonet { get; set; }

        [DataMember]
        public float PercJuros { get; set; }

        [DataMember]
        public float PercCorMonSaldo { get; set; }

        [DataMember]
        public float PercCorMonCarencia { get; set; }

        [DataMember]
        public float PercJurosCarencia { get; set; }

        [DataMember]
        public double ValorFinanc { get; set; }

        [DataMember]
        public bool JurosCarRepact { get; set; }
    }
}