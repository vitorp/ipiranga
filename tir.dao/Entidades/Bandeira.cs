﻿using System;

namespace tir.dao.Entidades
{
    public class Bandeira
    {
        #region Propriedades

        public Nullable<Int32> CodigoBandeira { get; set; }
        public string NomeBandeira { get; set; }
        public string IdProprBand { get; set; }
        public Nullable<Int32> CodigoAtividadeNeg { get; set; }
        public string DescricaoAtividadeNeg { get; set; }
        public Nullable<Int32> CodigoTipoAtividadeNeg { get; set; }
        public string DescricaoTipoAtividadeNeg { get; set; }

        #endregion
    }
}
