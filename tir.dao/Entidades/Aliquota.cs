﻿
using System;
namespace tir.dao.Entidades
{
    public class Aliquota
    {
        #region Proporiedades
        public string SiglaUF { get; set; }
        public string GrupoProduto { get; set; }
        public Single PercentualICMS { get; set; }
        #endregion
    }
}
