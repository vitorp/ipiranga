﻿using daobase.Enumerados;

namespace daobase
{
    public static class DaoUtil
    {
        public static string FormatarParametro(string nomeParametro)
        {
            string paramFormatado = "";

            switch (ConexaoBase.GetInstance().TipoBaseDados)
            {
                case BaseDadosEnum.Access:
                case BaseDadosEnum.Oracle:
                    paramFormatado = string.Format(":{0}", nomeParametro);
                    break;

                case BaseDadosEnum.SQLServer:
                    paramFormatado = string.Format("@{0}", nomeParametro);
                    break;
            }

            return paramFormatado;
        }

        public static string IsNull(string nomeCampo, string valorSeNulo)
        {
            string funcao = "";

            switch (ConexaoBase.GetInstance().TipoBaseDados)
            {
                case BaseDadosEnum.Access:
                    funcao = string.Format("NZ({0}, {1})", nomeCampo, valorSeNulo);
                    break;

                case BaseDadosEnum.Oracle:
                    funcao = string.Format("NVL({0}, {1})", nomeCampo, valorSeNulo);
                    break;

                case BaseDadosEnum.SQLServer:
                    funcao = string.Format("ISNULL({0}, {1})", nomeCampo, valorSeNulo);
                    break;
            }

            return funcao;
        }
    }
}
