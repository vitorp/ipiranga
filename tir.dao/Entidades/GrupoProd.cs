﻿
namespace tir.dao.Entidades
{
    public class GrupoProd
    {
        #region Propriedades
        public string CodigoGrupoProduto { get; set; }
        public string NomeGrupoProduto { get; set; }
        #endregion
    }
}
