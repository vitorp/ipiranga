﻿using TIR.Entidades.Encaixes;

namespace TIR.Entidades.ItensFranquias
{
    /// <summary>
    /// Item de franquia que representa Capital de Giro de Franquia.
    /// </summary>
    public class CapitalGiroFranquia : Encaixe
    {
    }
}
