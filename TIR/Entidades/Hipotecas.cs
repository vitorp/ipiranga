﻿using System;

namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que representa as hipotécas.
    /// </summary>
    public class Hipotecas
    {
        #region Propriedades

        public long Numero { get; set; }
        public double Valor { get; set; }
        public double ValorPRE { get; set; }
        public double Avaliacao { get; set; }
        public DateTime DataIni { get; set; }
        public DateTime DataFim { get; set; }
        public string Descricao { get; set; }
        public bool IndProposta { get; set; }

        public Garantidor Garantidor { get; set; }
        public TipoGarantia TipoGarantia { get; set; }
        public TipoCobertura TipoCobertura { get; set; }

        #endregion
    }
}
