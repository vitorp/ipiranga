﻿
namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que representa uma venda anual.
    /// </summary>
    public class VendasAnual
    {
        #region Propriedades

        public string Ano { get; set; }
        public string TipoProduto { get; set; }
        public float Volume { get; set; }

        #endregion
    }
}
