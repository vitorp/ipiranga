﻿using System;
using tir.dao;
using tir.dao.Entidades;
using TIR.Entidades;
using TIR.Entidades.Componentes;
using TIR.Entidades.Encaixes;
using TIR.Entidades.ItensFranquias;

namespace TIR.Bo.Calculos
{
    public class CalculoTaxaFranquia : ICalculo
    {
        #region Propriedades
        public Franquia Franquia { get; set; }
        #endregion

        public double[] Calcular(Encaixe enc, TIRNegociacao tir)
        {
            try
            {
                //converte encaixe
                TaxaFranquia taxa = (TaxaFranquia)enc;

                taxa.Descricao = "Taxa de Franquia";

                //define prazo total e redimensiona fluxo
                //int prazoTotal = Franquia.TempoContrato + 1;
                //Incluídos os meses sem venda por Luiz Felipe - out/2014
                int prazoTotal = Franquia.TempoContrato + tir.PeriodoMatura + 1;

                //taxa.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + 2];
                //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
                //taxa.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 2];
                //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
                taxa.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 2];

                //define valor de taxa de franquia
                double valor = Franquia.VlTxFranquia / tir.ValorFAO;

                //verifica se há valor de taxa de franquia
                if (valor <= 0)
                {
                    //não precisa calcular o fluxo
                    return taxa.Fluxo;
                }

                bool isFranquiaRenovacao = false;
                //    '----- Alteração asseguramento de franquias - Agosto/2002
                if (Franquia.TpNegFranquia.Equals(2) || Franquia.TpNegFranquia.Equals(1))
                {
                    isFranquiaRenovacao = true;
                }

                //busca alíoquotas de imposto de renda, ISS e PIS
                OutrosParamDao outrosDAO = new OutrosParamDao();
                OutrosParam outros = outrosDAO.ObterParametro("PercIR");
                OutrosParam outrosIss = outrosDAO.ObterParametro("PercISS");
                OutrosParam outrosPis = outrosDAO.ObterParametro("ReceitaPIS");
                float percIr = (float)outros.ValorParametroNumerico;
                float percIss = (float)outrosIss.ValorParametroNumerico;
                double aliqPisCof = (double)outrosPis.ValorParametroNumerico;

                int mesInicio = 0;
                string mensagem = string.Empty;
                //migração franquias maio/2004
                if (isFranquiaRenovacao.Equals(true) && !tir.Categoria.Equals(41))
                {
                    mesInicio = tir.ContrExistente + 1;
                    mensagem = " Verifique o contrato proposto para o caso de renovação de franquias.";
                }
                else
                {
                    mesInicio = 1;
                    mensagem = String.Empty;
                }

                if (Franquia.IndParceladoTxFranquia.Equals(true))
                {
                    //-- Gerar o fluxo de TaxaFranquia
                    valor = valor / Franquia.NoParcelaTxFranquia;

                    //verifica se o tempo de contrato está coerente  (se não dará subscript out of range abaixo)
                    if (mesInicio + tir.PeriodoMatura > taxa.Fluxo.GetUpperBound(0)
                        || Franquia.NoParcelaTxFranquia + tir.PeriodoMatura + mesInicio - 1 > taxa.Fluxo.GetUpperBound(0))
                    {
                        throw new Exception("CalculoTaxaFranquia.calcular: Erro ao calcular taxa de franquia. Não há tempo de contrato suficiente para o cálculo do fluxo da taxa de franquia.");
                    }

                    for (int i = mesInicio + tir.PeriodoMatura; i <= Franquia.NoParcelaTxFranquia + tir.PeriodoMatura + mesInicio - 1; i++)
                    {
                        //Alterado por Luiz Felipe - out/2014
                        //Cálculo: valor * (1 - (percentual ISS + alíquota PisCofins) / 100) * (1 – percentual IR / 100)
                        //taxa.Fluxo[i] = valor / (1.0 + percIss / 100.0) * (1.0 - percIr / 100.0) * (1.0 - aliqPisCof / 100.0);
                        taxa.Fluxo[i] = valor * (1.0 - (percIss + aliqPisCof) / 100.0) * (1.0 - percIr / 100.0);                       
                    }
                }
                else
                {
                    //--- parcela única
                    //Alterado por Luiz Felipe - out/2014
                    //Cálculo: valor * (1 - (percentual ISS + alíquota PisCofins) / 100) * (1 – percentual IR / 100)
                    /*taxa.Fluxo[Franquia.NoParcelaTxFranquia + tir.PeriodoMatura + (mesInicio - 1)] = 
                        valor / (1.0 + percIss / 100.0) * (1.0 - percIr / 100.0) * (1.0 - aliqPisCof / 100.0);*/
                    taxa.Fluxo[Franquia.NoParcelaTxFranquia + tir.PeriodoMatura + (mesInicio - 1)] =
                        valor * (1.0 - (percIss + aliqPisCof) / 100.0) * (1.0 - percIr / 100.0);
                }

                //retorno com sucesso
                return taxa.Fluxo;
            }
            catch (Exception ex)
            {
                throw new Exception("CalculoTaxaFranquia.calcular: Erro ao calcular taxa de franquia.\n" + ex.Message, ex);
            }
        }
    }
}
