﻿using System;
using System.Collections.Generic;

namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que representa os contratos.
    /// </summary>
    public class Contratos
    {
        #region Propriedades

        public string Numero { get; set; }
        public string TipoContrato { get; set; }
        public DateTime InicioVigencia { get; set; }
        public DateTime FimVigencia { get; set; }
        public bool IsComodato { get; set; }
        public bool IsProposto { get; set; }
        public string TipoPrazo { get; set; }
        public DateTime DataAssinatura { get; set; }
        public bool IsAcaoRenovatoria { get; set; }

        public List<VolumesMensal> Volumes { get; set; }

        #endregion
    }
}
