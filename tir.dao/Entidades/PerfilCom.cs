﻿
using System;
namespace tir.dao.Entidades
{
    public class PerfilCom
    {
        #region Propriedades
        public Int32 CodigoTipoComponente { get; set; }
        public Int32 CodigoPadraoComponente { get; set; }
        public Int32 CodigoPerfilComponente { get; set; }
        public string NomePerfilComponente { get; set; }
        public double ValorFaturamentoMensal { get; set; }
        public double ValorMaximoFinanciamento { get; set; }
        public double ValorTaxaFranquia { get; set; }
        public Int16 NumeroMaximoParcelasTaxaFranquia { get; set; }
        public double ValorFundoPropaganda { get; set; }
        public Single PercentualFundoPropaganda { get; set; }
        public double ValorMaximoEquipamento { get; set; }
        #endregion
    }
}
