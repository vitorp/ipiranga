﻿using System;
using System.Data;
//using System.Diagnostics.Contracts;
using System.Text;
using daobase;
using tir.dao.Entidades;

namespace tir.dao
{
    public class UnidadeNegociacaoDao : DaoBase<UnidadeNegociacao>
    {
        #region Métodos Públicos
        public UnidadeNegociacao ObterPorCodigoUnidade(int codUnidNegcc)
        {
            #region Contracts
            ////Contract.Requires(codUnidNegcc > 0, Mensagens.UnidadeNegociacao.VAL_CODIGO_UNIDADE);
            #endregion

            //instancia comando e datareader
            IDbCommand cmd = _conn.CreateCommand();
            IDataReader dr = null;

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("select u.CD_UNID_NEGCC, ");
                sb.Append("u.NM_UNID_NEGCC ");
                sb.Append("from NEGCLI_UNIDADE_NEGOCIACAO u ");
                sb.AppendFormat("where u.CD_UNID_NEGCC = {0} ", DaoUtil.FormatarParametro("codUnidNegcc"));

                #region Parâmetros
                cmd.Parameters.Add(CriarParametro(DaoUtil.FormatarParametro("codUnidNegcc"), codUnidNegcc, typeof(Int16)));
                #endregion

                cmd.Connection = _conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sb.ToString();
                cmd.Transaction = TransacaoCorrente();
                cmd.Prepare();

                //executa comando
                dr = cmd.ExecuteReader();

                //se há resultado
                if (dr.Read())
                {
                    //valor de retorno - alíquota de IOF
                    return MontarObjeto(dr);
                }
                else
                {
                    //valor de retorno nulo
                    return null;
                }
            }
            catch //(Exception ex)
            {
                throw; //new DaoException("TipoEncxDencxDao.ObterPorCodigoEncaixe",
                //"Erro ao obter Encaixe/Desencaixe por código do Tipo.", ex);
            }
            finally
            {
                if (dr != null)
                    dr.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }
        #endregion

        #region Overrides
        protected override UnidadeNegociacao MontarObjeto(IDataReader dr)
        {
            UnidadeNegociacao ent = new UnidadeNegociacao();

            try
            {
                ent.CodigoUnidadeNegociacao = (Int16)dr["CD_UNID_NEGCC"];
                ent.NomeUnidadeNegociacao = (string)dr["NM_UNID_NEGCC"];
            }
            catch (Exception)
            {
                throw;
            }

            return ent;
        }
        #endregion
    }
}
