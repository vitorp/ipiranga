﻿
namespace tir.dao.Entidades
{
    public class InicDeprec
    {
        #region Propriedades
        public decimal CodigoTipoInvestimento { get; set; }
        public decimal QuantidadeMesesMaturacao { get; set; }
        public decimal NumeroMesSaidaCaixa { get; set; }
        public decimal NumeroMesInicioDepreciacao { get; set; }
        #endregion
    }
}
