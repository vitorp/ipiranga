﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TIR.Constantes
{
    public static class TipoComponente
    {
        public const int AREA_ABASTECIMENTO = 1;
        public const int CONSUMIDOR = 2;
        public const int AMPM = 3;
        public const int JET_OIL_MOTOS = 4;
        public const int JET_OIL = 5;
    }
}
