﻿using System;
using System.Collections.Generic;
using tir.dao;
using tir.dao.Entidades;
using TIR.Entidades;
using TIR.Entidades.Componentes;
using TIR.Entidades.Encaixes;
using TIR.Entidades.ItensFranquias;

namespace TIR.Bo.Calculos
{
    public class CalculoPropaganda : ICalculo
    {
        #region Propriedades
        public Franquia Franquia { get; set; }
        #endregion

        public double[] Calcular(Encaixe enc, TIRNegociacao tir)
        {
            try
            {
                //converte encaixe
                Propaganda propaganda = (Propaganda)enc;

                propaganda.Descricao = "Propaganda";

                //define prazo total e redimensiona fluxo
                int prazoTotal = Franquia.TempoContrato + 1;
                //propaganda.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + 2];
                //Alterado em jun/2014 para incluir os meses sem venda no dimensionamento do fluxo
                //propaganda.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + 2];
                //Alterado em fev/2015 para incluir os meses de compensação de volume no dimensionamento do fluxo
                propaganda.Fluxo = new double[tir.ContrAdicional + tir.ContrExistente + tir.PeriodoMatura + tir.MesesCompVolume + 2];

                bool isFranquiaRenovacao = false;

                //--- Alteração asseguramento de franquias - Agosto/2002
                if (Franquia.TpNegFranquia.Equals(2) || Franquia.TpNegFranquia.Equals(1))
                {
                    isFranquiaRenovacao = true;
                }

                //procura perfil de componente
                PerfilComDao perfilDAO = new PerfilComDao();
                PerfilCom perfilCom = perfilDAO.ObterPerfilComponente(Franquia.CodPadraoComp, Franquia.CodPerfilComp, Franquia.CodTipoComp);
                float pcProp = (float)perfilCom.PercentualFundoPropaganda;
                double vlProp = (double)perfilCom.ValorFundoPropaganda;

                //busca alíquota de imposto de renda
                OutrosParamDao outrosDAO = new OutrosParamDao();
                OutrosParam outros = outrosDAO.ObterParametro("PercIR");
                float percIr = (float)outros.ValorParametroNumerico;

                int mesInicio = 0;
                //se for renovação e negociação 31
                if (isFranquiaRenovacao.Equals(true) && !tir.Categoria.Equals(41))
                {
                    mesInicio = tir.ContrExistente + 1;
                }
                else
                {
                    mesInicio = 1;
                }

                double vlPropagEmFao = 0.0;

                if (pcProp > 0)
                {
                    RedutorFatDao redutDAO = new RedutorFatDao();
                    List<RedutorFat> redutores = redutDAO.ListarPorComponente(Franquia.CodPadraoComp, Franquia.CodPerfilComp, Franquia.CodTipoComp);

                    double redutor = 0.0;

                    //gerar fluxo de propaganda
                    for (int i = mesInicio + tir.PeriodoMatura; i <= (prazoTotal - 1 + (mesInicio - 1)); i++)
                    {
                        //acha o redutor - alteração em 04/05/2002 - renovação de franquias
                        for (int j = 0; j <= redutores.Count - 1; j++)
                        {
                            if (redutores[j].NumeroMesFaixaRedutores.Equals(0))
                            {
                                break;
                            }

                            if (Franquia.TpNegFranquia > 0)
                            {
                                if (j.Equals(redutores.Count - 1))
                                {
                                    redutor = (double)redutores[j].PercentualReducaoFaturamento;
                                }
                            }
                            else
                            {
                                if (i <= redutores[j].NumeroMesFaixaRedutores || j.Equals(redutores.Count - 1))
                                {
                                    redutor = (double)redutores[j].PercentualReducaoFaturamento;
                                }
                            }
                        }

                        vlPropagEmFao = pcProp / 100.0 * Franquia.ValorFaturamento * (1.0 - redutor / 100.0) / tir.ValorFAO;
                        propaganda.Fluxo[i] = vlPropagEmFao * (1.0 - percIr / 100.0) * (-1.0);
                    }
                }
                else
                {
                    //--- se tiver valor ao invés de percentual de propag.
                    vlPropagEmFao = vlProp / tir.ValorFAO;
                    propaganda.Fluxo[mesInicio - 1] = vlPropagEmFao * (1.0 - percIr / 100.0) * (-1.0);
                }

                //retorno com sucesso
                return propaganda.Fluxo;
            }
            catch (Exception ex)
            {
                throw new Exception("CalculoPropaganda.calcular. Erro ao calcular propaganda.\n" + ex.Message, ex);
            }
        }
    }
}
