﻿
namespace TIR.Entidades
{
    /// <summary>
    /// Entidade que representa o volume mensal.
    /// </summary>
    public class VolumesMensal
    {
        #region Propriedades

        public float Volume { get; set; }
        public GrupoProduto GrupoProduto { get; set; }

        #endregion
    }
}
